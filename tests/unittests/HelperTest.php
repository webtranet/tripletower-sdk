<?php
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
	/********************
	*   PREPARATIONS    *
	********************/
	public static function setUpBeforeClass() : void
	{

	}

	public static function tearDownAfterClass() : void
	{

	}

	public function setUp() : void
	{

	}

	public function tearDown() : void
	{

	}


	/********************
	*      TESTS        *
	********************/
	public function versionSerialization_DataProvider()
	{
		return
		[
			["0.0"],
			["1.0"],
			["1.1"],

			["0.0.0"],
			["1.0.0"],
			["1.1.1"],

			["0.0.0.0"],
			["1.0.0.0"],
			["1.1.1.1"]
		];
	}

	/**
	 * @dataProvider versionSerialization_DataProvider
	 */
	public function testversionSerialization_Test(string $versionString)
	{
		$version = new TripleTowerSDK\Helper\Version($versionString);
		$serializedVersion = TripleTowerSDK\Serializer\Serializer::jsonSerialize($version);

		$this->assertIsString($serializedVersion);
	}
}
