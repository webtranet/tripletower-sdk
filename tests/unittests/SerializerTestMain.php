#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Tower\CliTower;
use \TripleTowerSDK\Serializer\Serializer;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname($rootFolder);

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

try
{
	
	require_once 'SerializerTest.php';

	$tempFolder = __DIR__ . "/tmp";
	if( !is_dir($tempFolder) )
		mkdir($tempFolder);

	echo "Creating SerializerTest..." . PHP_EOL;
	$originalSerializerTest = new SerializerTest();
	file_put_contents("$tempFolder/originalSerializerTest.txt", var_export($originalSerializerTest, true));
	echo PHP_EOL;
	
	echo "Serializing SerializerTest..." . PHP_EOL;
	$serializedSerializerTest = Serializer::jsonSerialize($originalSerializerTest);
	file_put_contents("$tempFolder/serializerTestText.json", var_export($serializedSerializerTest, true));
	echo PHP_EOL;

	echo "Unserialized SerializerTest..." . PHP_EOL;
	$copySerializerTest = Serializer::jsonUnserialize($serializedSerializerTest);
	file_put_contents("$tempFolder/Unserialized_SerializerTest.txt", var_export($copySerializerTest, true));
	echo PHP_EOL;

	echo "Unserialized Expected SerializerTest..." . PHP_EOL;
	$expectedSerializerTest = Serializer::jsonUnserialize($serializedSerializerTest, "SerializerTest");
	file_put_contents("$tempFolder/Unserialized_Expected_SerializerTest.txt", var_export($expectedSerializerTest, true));
}
catch(\Throwable $t)
{
	echo $t->getMessage();
	echo $t->getTraceAsString();
}