<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

class SerializerTest
{
	// Constants
	const CONSTANT = "CONSTANT";
	const PUB_CONSTANT = "PUB_CONSTANT";
	const PRT_CONSTANT = "PRT_CONSTANT";
	const PRV_CONSTANT = "PRV_CONSTANT";

	// No type
	public $nullNoType = null;
	public $pubNoType;
	protected $prtNoType2;
	private $prvNoType3;

	// Bool
	public bool $uninitBool;
	public ?bool $uninitBoolNullable;
	public ?bool $nullBoolNullable = null;
	public bool $initBool = true;
	public bool $pubBool1 = false;
	protected bool $prtBool2 = true;
	private bool $prvBool3 = false;

	// Int
	public int $uninitInt;
	public ?int $uninitIntNullable;
	public ?int $nullIntNullable = null;
	public int $initInt = 0;
	public int $pubInt1 = 1;
	protected int $prtInt2 = 2;
	private int $prvInt3 = 3;

	// Float
	public float $uninitFloat;
	public ?float $uninitFloatNullable;
	public ?float $nullFloatNullable = null;
	public float $initFloat = 0.0;
	public float $pubFloat1 = 1.1;
	protected float $prtFloat2 = 2.2;
	private float $prvFloat3 = 3.3;

	// String
	public string $uninitString;
	public ?string $uninitStringNullable;
	public ?string $nullStringNullable = null;
	public string $initString = "0";
	public string $pubString1 = "1";
	protected string $prtString2 = "2";
	private string $prvString3 = "3";

	// Array
	public array $uninitArray;
	public ?array $uninitArrayNullable;
	public ?array $nullArrayNullable = null;
	public array $initArray = [1, 2, 3];
	public array $pubArray1 = [];
	protected array $prtArray2 = [];
	private array $prvArray3 = [];
	public array $arrayDateTimes = [];

	// Object
	public \stdClass $uninitObject;
	public ?\stdClass $uninitObjectNullable;
	public ?\stdClass $nullObjectNullable = null;
	public \stdClass $pubObject1;
	protected \stdClass $prtObject2;
	private \stdClass $prvObject3;

	function __construct()
	{
		$this->pubObject1 = new \stdClass();
		$this->prtObject2 = new \stdClass();
		$this->prvObject3 = new \stdClass();
		
		$this->arrayDateTimes = [new \DateTime(), new \DateTime(), new \DateTime()];
	}

	function print()
	{
		echo "hallo";
	}
}