#!/bin/bash

# TripleTower SDK - https://webtranet.online/tripletower-sdk
#
# @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
# @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
# @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
#

currentdir=${PWD}
testsuite=$1

# Check if user has sufficient privileges
if [ "$(whoami)" != "root" ]; then
	echo "Please start babel as root"
	exit 1
fi

# If no test suite is given, ask for it
if [ -z "$1" ]; then
	echo "Please enter the test suite name:"
	read testsuite
fi

# If test suite has an ".json" ending, then remove it
if [[ "$testsuite" == *.json ]]; then
	testsuite="${testsuite%.json}"
fi

logfile="./temp/"${testsuite}"_"$(date -I)".log"

cd "$(dirname "$0")"
echo "Start testing via babel with "${testsuite} 2>&1 | tee -a $logfile

# Install PHP if not already installed
if ! hash php 2>/dev/null; then
	echo "PHP not installed, installing PHP now" 2>&1 | tee -a $logfile

	# Installing on Debian/Ubuntu/Mint
	if hash apt-get 2>/dev/null; then
		aptdcon --refresh && apt-get update && apt-get -o Dpkg::Options::="--force-confnew" install --assume-yes php-cli php-mysql
	fi

	# Installing on SLES/openSuse
	if hash zypper 2>/dev/null; then
		zypper refresh && zypper install php-cli
	fi

	# Installing on RHEL/CentOS/Fedora
	if hash yum 2>/dev/null; then
		yum check-update && yum install php-cli
	fi

	# Installing on CentOS/Fedora
	if hash dnf 2>/dev/null; then
		dnf check-update && dnf install php-cli
	fi

	# Installing on Arch Linux
	if hash pacman 2>/dev/null; then
		pacman -Syu && pacman -S php-cli
	fi

	echo "PHP installed"
fi

# Start babel
php "./bin/babel.php" -i ${testsuite} 2>&1 | tee -a $logfile
cd ${currentdir}
