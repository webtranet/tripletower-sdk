@echo off
REM
REM TripleTower SDK - https://webtranet.online/tripletower-sdk
REM
REM @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
REM @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
REM @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
REM
title Babel

cls
set currentdir=%cd%
set testsuite=%~1

cd /d %~dp0

REM Check if user started babel.bat by double clicking or via commandline
set calledByDblClick="false"
for /f delims^=^"^ tokens^=1^,2 %%a in ("%cmdcmdline%") do set path1="%%a" && set path2="%%b"
if %path2% == "%~0" (
	set calledByDblClick="true"
)
if %path2% == " /C " (
	set calledByDblClick="true"
)

call :printLogo

set printhelp="false"
if "%testsuite%" == "-h" set printhelp="true"
if "%testsuite%" == "-help" set printhelp="true"
if "%testsuite%" == "--help" set printhelp="true"
if "%testsuite%" == "v" set printhelp="true"
if "%testsuite%" == "-v" set printhelp="true"
if "%testsuite%" == "-version" set printhelp="true"
if "%testsuite%" == "--version" set printhelp="true"
if "%testsuite%" == "-?" set printhelp="true"
if "%testsuite%" == "/?" set printhelp="true"
if %printhelp% == "true" (
	call :printHelp
	goto :toolDone
)

call :setupLogFile logfile

REM If no test suite is given, ask for it
if "%testsuite%" == "" (
	set /P testsuite=Please enter the test suite name:
)

REM If test suite has an ".json" ending, then remove it
if /i "%testsuite%:~-5%"==".json" (
	set "testsuite=%testsuite:~0,-5%"
)

echo %time%: Start testing via babel with "%testsuite%" 2>&1 | "..\wtee\wtee.exe" -a %logfile%

REM Temporarily disabled PHP from loading ini files if specified
set PHP_INI_SCAN_DIR=

REM Start testing
"..\php\php.exe" ".\bin\babel.php" -i "%testsuite%" 2>&1 | "..\wtee\wtee.exe" -a %logfile%

:toolDone
cd /d %currentdir%

REM Hold command prompt open if started by double clicking
if %calledByDblClick% == "true" (
	pause
)
goto :eof


REM ===========================================================================
REM =                                 METHODS                                 =
REM ===========================================================================

REM This is a helper function to print the logo
:printLogo

echo.
echo  //=========================================================================\\
echo  ^|^|                    ____          ____  ______ _                         ^|^|
echo  ^|^|                   ^|  _ \   /\   ^|  _ \^|  ____^| ^|                        ^|^|
echo  ^|^|                   ^| ^|_) ^| /  \  ^| ^|_) ^| ^|__  ^| ^|                        ^|^|
echo  ^|^|                   ^|  _ ^< / /\ \ ^|  _ ^<^|  __^| ^| ^|                        ^|^|
echo  ^|^|                   ^| ^|_) / ____ \^| ^|_) ^| ^|____^| ^|____                    ^|^|
echo  ^|^|                   ^|____/_/    \_\____/^|______^|______^|                   ^|^|
echo  ^|^|                                                                         ^|^|
echo  \\=========================================================================//
echo.

goto :eof

REM ===========================================================================

REM This is a helper function to print the help text
:printHelp
echo   浜様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo   �                                                                         �
echo   � DESC                                                                    �
echo   �                                                                         �
echo   �    Executes the babel test suite and triggers the CI/CD pipeline.       �
echo   �    What is being tested can be specified in the config file:            �
echo   �    ./babel/conf/^<testsuite^>.json                                        �
echo   �                                                                         �
echo   �                                                                         �
echo   � SYNOPSIS                                                                �
echo   �                                                                         �
echo   �    babel ^<testsuite^>                                                    �
echo   �                                                                         �
echo   �                                                                         �
echo   �    ^<testsuite^>                                                          �
echo   �                                                                         �
echo   �         The test suite you want to execute. Basically specifies the     �
echo   �         config file names. Use "default" if you are unsure.             �
echo   �                                                                         �
echo   �                                                                         �
echo   � EXAMPLE                                                                 �
echo   �                                                                         �
echo   �    Starting default test suite:                                         �
echo   �                                                                         �
echo   �        babel default                                                    �
echo   �                                                                         �
echo   藩様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo.

goto :eof

REM ===========================================================================

REM This is a helper function to format the log file name according to the current date
:setupLogFile

REM If Windows Management Instrumentation is not available then usual date is returned
set wmicAvailable="true"
wmic.exe alias /? >nul 2>&1 || set wmicAvailable="false"

if %wmicAvailable% == "true" (

	REM Tokenize 2nd line of output of wmic.exe and set the according variables and format them to have always only two digits
	for /f "skip=1 tokens=1-3" %%i in ('WMIC Path Win32_LocalTime Get Day^,Month^,Year /Format:table') do (
		set day=00%%i
		set month=00%%j
		set year=%%k
		goto dateLoopEnd
	)
	:dateLoopEnd
	set day=%day:~-2%
	set month=%month:~-2%
	set "%~1=.\temp\%testsuite%_%year%-%month%-%day%.log"
	goto :eof
) else (
	echo wmic.exe not available, using regional date settings for naming logfile
	set "%~1=.\temp\babel_%date%.log"
	goto :eof
)
REM Statements "goto :eof" have to stay inside the if-else clauses due to a weird bug in batch