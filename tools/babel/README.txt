                       ____          ____  ______ _      
                      |  _ \   /\   |  _ \|  ____| |     
                      | |_) | /  \  | |_) | |__  | |     
                      |  _ < / /\ \ |  _ <|  __| | |     
                      | |_) / ____ \| |_) | |____| |____ 
                      |____/_/    \_\____/|______|______|

-------------------------------------------------------------------------------

Babel is the tripletower-sdk testing tool suite.
It contains tests and a plugin-facility in order to integrate into a CI-CD
pipline.