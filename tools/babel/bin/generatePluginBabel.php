#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

// If babel is fully installed use TripleTowerSDK-sdk/libs, if remotely installed use libs folder directly aside instead
$libsFolder = is_dir("$sdkFolder/libs") ? "$sdkFolder/libs" :  dirname($rootFolder)."/libs";
require_once "$libsFolder/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

echo "Starting babel plugin generation".PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;

$pluginName = "YOURPLUGINNAMEHERE";

$pluginContent = file_get_contents("plugin.babel.template");
if( empty($pluginContent) )
	throw new \Exception("Could not load plugin.babel.template");

$keysToReplace = [":pluginName"];
$replacements = [$pluginName];
if( ($pluginContent = str_replace($keysToReplace, $replacements, $pluginContent)) === false )
	throw new \Exception("Could not replace variables in plugin.babel.template");

$babelPluginPath = "$rootFolder/bin/plugins/babel_$pluginName.php";
echo basename($babelPluginPath, ".php") . " plugin is being created -> ";

// Create it
if( empty(file_put_contents($babelPluginPath, $pluginContent)) )
	throw new \Exception("Could not save new plugin to file '$babelPluginPath'");

echo "success" . PHP_EOL;