<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name utf8encoding Babel Plugin
 * @desc
 *
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Babel\IBabelPlugin;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;

class babel_utf8encoding extends IBabelPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"fileTypesToTest" => [ "php", "js", "css", "html", "htm", "json", "xml", "cfg", "conf", "ini", "txt" ]
	];

	protected $configLinuxDefaults =
	[
		"fileTypesToTest" => [ "php", "js", "css", "html", "htm", "json", "xml", "cfg", "conf", "ini", "txt" ]
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}

	public function getTestDependencies() : array
	{
		return [];
	}

	public function test() : TripleTowerError
	{
		$testResult = new DevError(DevError::SUCCESS);

		foreach( $this->getFoldersToTest() as $folderToTest )
		{
			$fsIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($folderToTest,
				\FilesystemIterator::CURRENT_AS_SELF |
				\FilesystemIterator::SKIP_DOTS |
				\FilesystemIterator::UNIX_PATHS));

			foreach($fsIterator as $fileToTest)
			{
				if( is_dir($fileToTest->getPathname()) )
					continue;

				if( !in_array( strtolower($fileToTest->getExtension()), array_map("strtolower", $this->config["fileTypesToTest"]), true) )
					continue;

				$fileContent = file_get_contents( $fileToTest->getPathname() );
				if( $fileContent === false )
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "FAILED - File '{$fileToTest->getPathname()}' could not be read" . PHP_EOL;
					continue;
				}

				if( mb_check_encoding($fileContent, "UTF-8") === false )
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "FAILED - File '{$fileToTest->getPathname()}' contains non-UTF-8 characters" . PHP_EOL;
					continue;
				}
			}
		}

		return $testResult;
	}

	public function getCleanupDependencies() : array
	{
		return [];
	}

	public function cleanup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}