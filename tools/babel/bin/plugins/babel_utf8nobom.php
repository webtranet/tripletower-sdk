<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name utf8nobom Babel Plugin
 * @desc
 *
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Babel\IBabelPlugin;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;

class babel_utf8nobom extends IBabelPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"fileTypesToTest" => [ "php", "js", "css", "html", "htm", "json", "xml", "cfg", "conf", "ini", "txt" ]
	];

	protected $configLinuxDefaults =
	[
		"fileTypesToTest" => [ "php", "js", "css", "html", "htm", "json", "xml", "cfg", "conf", "ini", "txt" ]
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}

	public function getTestDependencies() : array
	{
		return [];
	}

	public function test() : TripleTowerError
	{
		$boms =
		[
			"UTF-8"		 => pack("CCC", 0xef, 0xbb, 0xbf),
			"UTF-16-BE"  => pack("CC", 0xfe, 0xff),
			"UTF-16-LE"  => pack("CC", 0xff, 0xfe),
			"UTF-32-BE"  => pack("CCCC", 0x00, 0x00, 0xfe, 0xff),
			"UTF-32-LE"  => pack("CCCC", 0xff, 0xfe, 0x00, 0x00),
			"UTF-7-1"    => pack("CCCC", 0x2b, 0x2f, 0x76, 0x38),
			"UTF-7-2"    => pack("CCCC", 0x2b, 0x2f, 0x76, 0x39),
			"UTF-7-3"    => pack("CCCC", 0x2b, 0x2f, 0x76, 0x2b),
			"UTF-7-4"    => pack("CCCC", 0x2b, 0x2f, 0x76, 0x2f),
			"UTF-1"      => pack("CCC", 0xf7, 0x64, 0x4c),
			"UTF-EBCDIC" => pack("CCCC", 0xdd, 0x73, 0x66, 0x73),
			"SCSU"       => pack("CCC", 0x0e, 0xfe, 0xff),
			"BOCU-1"     => pack("CCC", 0xfb, 0xee, 0x28),
			"GB-18030"   => pack("CCCC", 0x84, 0x31, 0x95, 0x33)
		];

		$testResult = new DevError(DevError::SUCCESS);

		foreach( $this->getFoldersToTest() as $folderToTest )
		{
			$fsIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($folderToTest,
				\FilesystemIterator::CURRENT_AS_SELF |
				\FilesystemIterator::SKIP_DOTS |
				\FilesystemIterator::UNIX_PATHS));

			foreach($fsIterator as $fileToTest)
			{
				if( is_dir($fileToTest->getPathname()) )
					continue;

				if( !in_array( strtolower($fileToTest->getExtension()), array_map("strtolower", $this->config["fileTypesToTest"]), true) )
					continue;

				$propableBom = $fileToTest->openFile()->fread(8);
				if( $propableBom === false )
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "FAILED - File '{$fileToTest->getPathname()}' could not be read" . PHP_EOL;
					continue;
				}

				foreach($boms as $bomType => $realBom)
				{
					if( strlen($propableBom) < strlen($realBom) )
						continue;

					if( strncmp($propableBom, $realBom, strlen($realBom)) === 0 )
					{
						$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
						echo "FAILED - File '{$fileToTest->getPathname()}' contains a '$bomType-BOM'" . PHP_EOL;
						continue 2;
					}
				}
			}
		}

		return $testResult;
	}

	public function getCleanupDependencies() : array
	{
		return [];
	}

	public function cleanup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}