<?php declare(strict_types=1);

$signingCert	= "tripletower.localhost.crt";
$signingKey		= "tripletower.localhost.key";
$description	= "Hanoi (untrusted dev build)";
$url			= "tripletower.localhost";
$hanoiPath      = "hanoi.exe";

// If one is set, but not the other, then it's an error
if( $argc !== 2 && $argc !== 5 )
	throw new \Exception("You have to set both, the cert and the key or don't set any to use default cert and key");

if( $argc === 5 )
	list($signingCert, $signingKey, $description, $url) = [$argv[1], $argv[2], $argv[3], $argv[4]];

$command = "osslsigncode sign -nolegacy -certs \"$signingCert\" -key \"$signingKey\" -n \"$description\" -i \"$url\" -in \"$hanoiPath\" -out \"temp_$hanoiPath\"";
Helper::exec($command)->throwIfNoSuccess();
