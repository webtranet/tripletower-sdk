<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name namespacesused Babel Plugin
 * @desc
 *
 */

use TripleTowerSDK\Babel\IBabelPlugin;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\TripleTowerError;

class babel_namespacesused extends IBabelPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	const TOKEN_CODE  = 0;
	const TOKEN_VALUE = 1;
	const TOKEN_LINE  = 2;


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"fileTypesToTest" => [ "php" ]
	];

	protected $configLinuxDefaults =
	[
		"fileTypesToTest" => [ "php" ]
	];


	/***********************************
	* PUBLIC METHODS                   *
    ***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}

	public function getTestDependencies() : array
	{
		return [];
	}

	public function test() : TripleTowerError
	{
		$testResult = new DevError(DevError::SUCCESS);

		foreach( $this->getFoldersToTest() as $folderToTest )
		{
			$fsIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($folderToTest,
				\FilesystemIterator::CURRENT_AS_SELF |
				\FilesystemIterator::SKIP_DOTS |
				\FilesystemIterator::UNIX_PATHS));

			foreach($fsIterator as $fileToTest)
			{
				if( is_dir($fileToTest->getPathname()) )
					continue;

				if( !in_array( strtolower($fileToTest->getExtension()), array_map("strtolower", $this->config["fileTypesToTest"]), true) )
					continue;

				$phpFile = $fileToTest->openFile();
				$phpCode = $phpFile->fread( $phpFile->getSize() );
				if( $phpCode === false )
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "FAILED - File '{$fileToTest->getPathname()}' could not be read" . PHP_EOL;
					continue;
				}

				if( !$this->checkNamespaces($phpCode, $fileToTest->getPathname())->isSuccess() )
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
			}
		}

		return $testResult;
	}

	public function getCleanupDependencies() : array
	{
		return [];
	}

	public function cleanup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function checkNamespaces(string $phpCode, string $filePathName) : TripleTowerError
	{
		$nameSpaces = [];
		$classes = [];
		$tokens = token_get_all($phpCode);

		for($loop = 0; $loop < count($tokens); $loop++)
		{
			if (is_array($tokens[$loop]))
			{
				switch(token_name($tokens[$loop][self::TOKEN_CODE]))
				{
					// Search for namespaces
					case "T_USE":
						do
						{
							// Consider only names space uses and no variable import for anonymous closures
							if( !is_array($tokens[$loop]) || token_name($tokens[$loop][self::TOKEN_CODE]) === "T_VARIABLE" )
								break;

							// Only use class name without leading namespaces
							if( (is_array($tokens[$loop]) && token_name($tokens[$loop][self::TOKEN_CODE]) === "T_STRING") &&
								(!is_array($tokens[$loop+1]) || token_name($tokens[$loop+1][self::TOKEN_CODE]) !== "T_NS_SEPARATOR" ))
							{
								$nameSpaces[] = $tokens[$loop][self::TOKEN_VALUE];
								break;
							}
							$loop++;
						}while($loop < count($tokens));
						break;

					// Search for class usages -> MyClass::myAttribute
					case "T_DOUBLE_COLON":

						// Classes with namespaces are considered correct, we're only for searching namespace-less classes
						if( !is_array($tokens[$loop-1]) || token_name($tokens[$loop-1][self::TOKEN_CODE]) !== "T_STRING")
							break;

						// The class "self" and "parent" are ok to be used without namespace usage
						if( in_array($tokens[$loop-1][self::TOKEN_VALUE], ["self", "parent"]) )
							break;

						// Furthermore don't evaluate objects with a namespace -> Asdf\Fdsa::myAttribute
						// or which are dynamic object properties -> $asdf->fdsa::myAttribute
						if( is_array($tokens[$loop-2]) && isset($tokens[$loop-2]) && in_array(token_name($tokens[$loop-2][self::TOKEN_CODE]), ["T_NS_SEPARATOR", "T_OBJECT_OPERATOR"]) )
							break;

						$classes[] = $tokens[$loop-1][self::TOKEN_VALUE];
						break;

					// Search for class usages -> catch ( MyClass $ex )
					// Search for class usages -> new MyClass
					case "T_NEW":
					case "T_CATCH":
						do
						{
							if( is_array($tokens[$loop]) )
							{
								if( token_name($tokens[$loop][self::TOKEN_CODE]) === "T_STRING" )
								{
									// The class "self" and "parent" are ok to be used without namespace usage
									if( $tokens[$loop][self::TOKEN_VALUE] === "self" || $tokens[$loop][self::TOKEN_VALUE] === "parent" )
										break;
								}

								// Classes with namespaces are considered correct, we're only searching for namespace-less classes
								if(token_name($tokens[$loop][self::TOKEN_CODE]) === "T_NS_SEPARATOR")
									break;

								// Dynamic classes are not covered by the namespace tests
								if(token_name($tokens[$loop][self::TOKEN_CODE]) === "T_VARIABLE")
									break;
							}

							// Only use the class name without leading namespaces
							if( (is_array($tokens[$loop]) && token_name($tokens[$loop][self::TOKEN_CODE]) === "T_STRING") &&
								(!is_array($tokens[$loop+1]) || token_name($tokens[$loop+1][self::TOKEN_CODE]) !== "T_NS_SEPARATOR" ))
							{
								$classes[] = $tokens[$loop][self::TOKEN_VALUE];
								break;
							}
							$loop++;
						}while($loop < count($tokens));
						break;
				}
			}
		}

		// Namespaces duplicate test
		$duplicateNamespaces = [];
		if( count($nameSpaces) !== count( array_unique( array_map("strtolower", $nameSpaces) ) ) )
		{
			$loweredNameSpaces = array_map("strtolower", $nameSpaces);
			$loweredDuplicateNamespaces = array_diff_key($loweredNameSpaces, array_unique($loweredNameSpaces));
			$duplicateNamespaces = array_intersect_key($nameSpaces, $loweredDuplicateNamespaces);
			$nameSpaces = array_intersect_key($nameSpaces, array_unique($loweredNameSpaces));
		}

		// Missing namespaces test
		$classes = array_intersect_key($classes, array_unique(array_map("strtolower", $classes)));
		$missingNameSpaces = array_filter($classes, function($class) use ($nameSpaces)
		{
			foreach($nameSpaces as $nameSpace)
				if( strtolower(substr($nameSpace, -strlen($class))) === strtolower($class) )
					return false;

			return true;
		});

		$testResult = new DevError(DevError::SUCCESS);
		if( count($duplicateNamespaces) > 0 || count($missingNameSpaces) > 0 )
		{
			if( count($duplicateNamespaces) > 0 )
			{
				echo "\tDuplicate namespaces found in $filePathName".PHP_EOL;
				foreach($duplicateNamespaces as $duplicateNamespace)
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "\t\t$duplicateNamespace".PHP_EOL;
				}
			}
			if( count($missingNameSpaces) > 0)
			{
				echo "\tMissing namespaces found in $filePathName".PHP_EOL;
				foreach($missingNameSpaces as $missingNameSpace)
				{
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
					echo "\t\t$missingNameSpace".PHP_EOL;
				}
			}
			echo PHP_EOL;
		}

		return $testResult;
	}
}

