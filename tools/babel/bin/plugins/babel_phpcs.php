<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name phpcs Babel Plugin
 * @desc
 *
 */

use TripleTowerSDK\Babel\IBabelPlugin;
use TripleTowerSDK\Date\DateTime;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;

class babel_phpcs extends IBabelPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[

	];

	protected $configLinuxDefaults =
	[

	];


	/***********************************
	* PUBLIC METHODS                   *
    ***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}

	public function getTestDependencies() : array
	{
		return [];
	}

	public function test() : TripleTowerError
	{
		$testResult = new DevError(DevError::SUCCESS);

		foreach( $this->getFoldersToTest() as $folderToTest )
		{
			$argvBackup = $_SERVER["argv"];
			try
			{
				$_SERVER["argv"] =
				[
					"--do-not-cache-result",
					"--log-junit",
					dirname(__FILE__, 3) . "/temp/phpunit/" . date(DateTime::DEFAULT_FORMAT_FILEYSTEM) . "_" . basename(Helper::removeLastChar($folderToTest, "/")) . "_results.xml",
					"$folderToTest/tests/unittests"
				];
//				if( !ob_start() )
//					throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not filter PHP Unit's output via ob_start");
				$runner = new PHP_CodeSniffer\Runner();
				if( !emtpy($runner->runPHPCS()) );
					$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
//				ob_end_clean();
			}
			catch( \Throwable $t )
			{
				ob_end_clean();
				$testResult = new DevError(DevError::TEST_FAILED, DevError::SILENT);
				echo "FAILED - PHP Unit failed for  '$folderToTest/tests/unittests' because of {$t->getMessage()}" . PHP_EOL;
				continue;
			}

			$_SERVER["argv"] = $argvBackup;
		}
		return $testResult;
	}

	public function getCleanupDependencies() : array
	{
		return [];
	}

	public function cleanup() : TripleTowerError
	{
		return new DevError(DevError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function searchForClassMapFiles(string $folderToTest) // array(string)
	{
		$foundClassMapFiles = [];

		$potentialClassMapFiles =
		[
			"$folderToTest/.classmap.php",
			"$folderToTest/bin/.classmap.php",
			"$folderToTest/tests/unittests/.classmap.php"
		];
		foreach($potentialClassMapFiles as $potentialClassMapFile)
			if( is_file($potentialClassMapFile) )
				$foundClassMapFiles[] = $potentialClassMapFile;

		return $foundClassMapFiles;
	}
}

