<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Babel
{
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Network\Proxy;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;

	abstract class IBabelPlugin
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		* @name $pluginName
		* @desc The name of the plugin itself.
		*/
		protected $pluginName;

		/**
		* @name $config
		* @desc An associative array containing all plugin specific values found in the babel.json config file.
		*/
		protected $config;

		/**
		* @name $configWindowsDefaults
		* @desc An associative array containing all the default values on a Windows used if not specified in the babel.json
		*       config file. The value null indicates a mandatory field.
		*/
		protected $configWindowsDefaults = null;

		/**
		* @name $configLinuxDefaults
		* @desc An associative array containing all the default values on a Linux used if not specified in the babel.json
		*       config file. The value null indicates a mandatory field.
		*/
		protected $configLinuxDefaults = null;

		/**
		* @name $caFileName
		* @desc The file name of the certificate authority certificate file to trust for downloading files to test.
		*		This file is being searched in the certs folder.
		*/
		protected $caFileName = "";

		/**
		* @name $proxy
		* @desc A proxy object which holds all information if a proxy is specified in the config.
		*/
		protected $proxy = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/**
		* @name __construct
		* @desc Instantiates the babel plugin.
		* @param array $config An associative array containing all plugin specific values found in the babel.json config file
		* @return IBabelPlugin
		*/
		public function __construct(JsonConfig $config)
		{
			$this->pluginName = get_class($this);

			// Handle config
			if( is_null($this->configWindowsDefaults) )
			{
				throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "'$this->pluginName.php' has been implemented incorrectly. Please overwrite \$configWindowsDefaults.");
			}
			else if( is_null($this->configLinuxDefaults) )
			{
				throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "'$this->pluginName.php' has been implemented incorrectly. Please overwrite \$configLinuxDefaults.");
			}

			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
					$this->config = new JsonConfig($this->configWindowsDefaults);
				break;

				default:
					$this->config = new JsonConfig($this->configLinuxDefaults);

			}

			$this->config->merge($config);
			foreach($this->config as $configKey => $configValue)
			{
				if( is_null($configValue) )
					throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Mandatory config value '$configKey' could not be found in plugin '$this->pluginName'.");
			}

			// Handle Certificate Authority certificate file
			if( isset($_SERVER["TT"]->conf->caFileName) && $_SERVER["TT"]->conf->caFileName !== "" )
				$this->caFileName = $_SERVER["TT"]->conf->caFileName;

			// Handle proxy
			if( isset($_SERVER["TT"]->conf->proxy) )
			{
				$this->proxy = new Proxy($_SERVER["TT"]->conf->proxy->url ?? "",
											$_SERVER["TT"]->conf->proxy->port ?? 8080,
											$_SERVER["TT"]->conf->proxy->userName ?? get_current_user(),
											$_SERVER["TT"]->conf->proxy->password ?? "");
			}
		}

		/**
		* @name createTempPluginFolder
		* @desc Creates the folder %temp%/babel/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @return string Returns the newly created path followed by a slash
		*/
		public function createTempPluginFolder()
		{
			$babelFolder = sys_get_temp_dir()."/babel_plugins";
			if( file_exists($babelFolder) )
			{
				if(is_file($babelFolder) )
				{
					if( !unlink($babelFolder) )
						throw new FsError(FsError::UNLINK_FAILED, FsError::ERR, "There is a file called '$babelFolder' which could not be deleted in order to create a folder with the same name");
					if( !mkdir($babelFolder) )
						throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$babelFolder' could not be created");
				}
			}
			else
			{
				if( !mkdir($babelFolder) )
					throw new \Exception("The folder '$babelFolder' could not be created");
			}

			$pluginFolder = "$babelFolder/$this->pluginName";
			if( file_exists($pluginFolder) )
			{
				if(is_file($pluginFolder) )
				{
					if( !unlink($pluginFolder) )
						throw new FsError(FsError::UNLINK_FAILED, FsError::ERR, "There is a file called '$pluginFolder' which could not be deleted in order to create a folder with the same name");
					if( !mkdir($pluginFolder) )
						throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$pluginFolder' could not be created");
				}
			}
			else
			{
				if( !mkdir($pluginFolder) )
					throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$pluginFolder' could not be created");
			}

			return "$pluginFolder/";
		}

		/**
		* @name getTempPluginFolder
		* @desc Creates the folder %temp%/babel/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @param bool $testIfExists Indicates if the existance of the folder should be performed
		* @return string Returns the newly created path followed by a slash
		*/
		public function getTempPluginFolder(bool $testIfExists = true)
		{
			$pluginFolder = sys_get_temp_dir()."/babel_plugins/$this->pluginName/";
			if( $testIfExists && !is_dir($pluginFolder) )
				throw new FsError(FsError::FILE_ALREADY_EXISTS, FsError::ERR, "Pluginfolder '$pluginFolder' is no folder but should have been created");

			return $pluginFolder;
		}

		/**
		* @name removeTempPluginFolder
		* @desc Removes the folder %temp%/babel/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @return FsError Returns if it could delete the temp folder
		*/
		public function removeTempPluginFolder()
		{
			return Helper::unlink_recursive( $this->getTempPluginFolder() );
		}

		/**
		* @name getSetupDependencies
		* @desc Returns an array of plugin names which have to be setup before
		*       the implemented setup method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getSetupDependencies() : array;

		/**
		* @name setup
		* @desc Does all the stuff needed to test test/task later on.
		* @param void
		* @return void
		*/
		abstract public function setup() : TripleTowerError;

		/**
		* @name getTestDependencies
		* @desc Returns an array of plugin names which have to be tested before
		*       the implemented test method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getTestDependencies() : array;

		/**
		* @name test
		* @desc Tests the test/task.
		* @param void
		* @return void
		*/
		abstract public function test() : TripleTowerError;

		/**
		* @name getCleanupDependencies
		* @desc Returns an array of plugin names which have to be cleaned up before
		*       the implemented cleanup method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getCleanupDependencies() : array;

		/**
		* @name cleanup
		* @desc Does alle the post cleanup work.
		* @param void
		* @return void
		*/
		abstract public function cleanup() : TripleTowerError;


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function getFoldersToTest() : array
		{
			if( !isset($_SERVER["TT"]->conf["foldersToTest"]) )
				throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Could not find mandatory config parameter 'foldersToTest'");

			$foldersToTest = $_SERVER["TT"]->conf["foldersToTest"];
			if( !is_array($foldersToTest) )
				$foldersToTest = [$foldersToTest];

			$tripleTowerStackFolder = !OsNames::isWindows() ? "/srv" : dirname($_SERVER["TT"]->tower->getToolsFolder(), 2);

			for($loop = 0; $loop < count($foldersToTest); $loop++)
			{
				// Absolute paths are ok
				if( file_exists($foldersToTest[$loop]) )
					continue;

				// Relative path to TripleTowerStack are ok, but they have to be translated to absolute paths
				if( file_exists("$tripleTowerStackFolder/{$foldersToTest[$loop]}") )
				{
					$foldersToTest[$loop] = Helper::normalizeFolderPath("$tripleTowerStackFolder/{$foldersToTest[$loop]}");
					continue;
				}

				// Folder not found
				throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Specified folder '{$foldersToTest[$loop]}' does not exist");
			}

			return $foldersToTest;
		}
	}
}