#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Tower\CliTower;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\FsError;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

echo "Starting babel test suite" . PHP_EOL;
echo PHP_EOL;

try
{
	// Check operating system support
	$currentOperatingSystem = OsNames::getCurrentOsType();
	if( $currentOperatingSystem !== OsNames::OS_TYPE_LINUX && $currentOperatingSystem !== OsNames::OS_TYPE_WINDOWS )
		throw new \Exception("Only Linux and Windows are currently supported by the babel test suite");

	// Create tower
	$cliTower = new CliTower($rootFolder, $sdkFolder, $loader, [["p:", "pluginfolder:"], ["s::", "skip::"]]);

	// Load plugins
	$plugins = loadBabelPlugins( $cliTower->cmdLineOptions->pluginfolder );
	$failedTests = [];
	if( count($plugins) > 0)
	{
		setupPlugins($plugins, $cliTower->cmdLineOptions->skip);
		$failedTests = testPlugins($plugins, $cliTower->cmdLineOptions->skip);
		cleanupPlugins($plugins, $cliTower->cmdLineOptions->skip);
	}
	else
	{
		echo "No test plugins specified." . PHP_EOL;
	}

	echo PHP_EOL;
	echo "Finished testing" . PHP_EOL;
	echo "-----------------------" . PHP_EOL;
	echo "Result: " . count($failedTests) . ((count($failedTests)===1)?" test":" tests") . " failed" . PHP_EOL;
	if( count($failedTests) > 0 )
	{
		echo implode (PHP_EOL, $failedTests);
		exit(1);
	}
}
catch(\Throwable $t)
{
	echo "ERROR: " . $t->getMessage() . PHP_EOL;
	exit( $t->getCode() );
}
exit(0);

/**
 * @name loadBabelPlugins
 * @desc Searches for all plugins which implement the IBabelPlugin-interface and
 *       which are defined in the config file. Those get loaded and instantiated
 *       together with all of their dependencies.
 * @param string $pluginFolder The folder to search for babel plugins
 * @return IBabelPlugin-array List of instantiated babel plugins
 */
function loadBabelPlugins(string $pluginFolder)
{
	// Use default plugin folder if none specified
	$pluginFolder = ( empty($pluginFolder) ? $_SERVER["TT"]->tower->getBinFolder()."plugins" : $pluginFolder);

	$plugins = [];

	// Remove plugins with install=false option in config
	foreach($_SERVER["TT"]->conf as $pluginName => $pluginConfig)
	{
		// Only consider hanoi plugin entries
		if( strpos(strtolower($pluginName), "babel_") !== 0)
			continue;

		// Ignore all plugins with test=false entry
		if( isset($pluginConfig->test) && ( empty($pluginConfig->test) || strtolower($pluginConfig->test) === "false" ) )
			continue;

		$pluginFilePath="$pluginFolder/$pluginName.php";
		if( !is_file($pluginFilePath) )
			throw new FsError( FsError::FILE_NOT_FOUND, FsError::ERR, "File '$pluginFilePath' not found.");

		// Load plugin file
		require_once $pluginFilePath;

		// Check if class has been successfully loaded
		$className = strtolower( str_replace([".", "-", " "], ["_", "_", "_"], $pluginName) );
		if( !class_exists($className, false) )
			throw new \Exception("Class plugin '$className' not found but is defined in the configuration file.");

		// Check if class is a real plugin
		if( !is_subclass_of($className, 'TripleTowerSDK\\Babel\\IBabelPlugin') )
			throw new \Exception("Class plugin '$className' is declared but is not derived from IBabelPlugin.");

		// Instantiate plugin
		$plugins[] = new $className($pluginConfig);
	}

	// Check if all necessary plugins are loaded to solve dependencies
	$dependencies = [];
	foreach($plugins as $plugin)
		$dependencies = array_merge($dependencies, $plugin->getSetupDependencies(), $plugin->getTestDependencies(), $plugin->getCleanupDependencies());

	foreach(array_unique($dependencies) as $dependency)
	{
		$dependencyFound = false;
		foreach($plugins as $plugin)
		{
			if( get_class($plugin) === $dependency )
				$dependencyFound = true;
		}
		if( !$dependencyFound )
			throw new \Exception("Plugin '$dependency' not found. But it's a dependency. It has also to be defined in the configuration file.");
	}

	// All available plugins
	return $plugins;
}


function setupPlugins(array $plugins, string $skip="")
{
	$finishedPlugins = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getSetupDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Setup $pluginName... ";
				if( $skip !== "setup" && $skip !== $pluginName )
				{
					$setupStart = time();
					$plugins[$loop]->createTempPluginFolder();
					$plugins[$loop]->setup()->throwIfNotSuccess();
					$finishedPlugins[] = $pluginName;
					$setupTime = time() - $setupStart;
					echo "OK (" . secondsToHumanString($setupTime) . ")" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}
}

function testPlugins(array $plugins, string $skip="")
{
	$finishedPlugins = [];
	$failedTests = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getTestDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Test $pluginName... ";
				if( $skip !== "test" && $skip !== $pluginName )
				{
					$testStart = time();
					try
					{
						$testResult = $plugins[$loop]->test();
					}
					catch( \Throwable $t )
					{
						$testResult = new DevError(DevError::TEST_FAILED, DevError::ERR, $t->getMessage());
					}

					if( !$testResult->isSuccess() )
						$failedTests[] = $pluginName;
					$finishedPlugins[] = $pluginName;
					$testTime = time() - $testStart;
					echo ($testResult->isSuccess() ? "OK" : "FAILED" ) . " (" . secondsToHumanString($testTime) . ")" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}

	return $failedTests;
}

function cleanupPlugins(array $plugins, $skip="")
{
	$finishedPlugins = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getCleanupDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Cleanup $pluginName... ";
				if( $skip !== "cleanup" && $skip !== $pluginName )
				{
					$cleanupStart = time();
					try
					{
						$cleanupResult = $plugins[$loop]->cleanup();
						if( !$cleanupResult->isSuccess() )
							echo "Cleanup Error: '" . $cleanupResult->getMessage() . "'";
					}
					catch( \Throwable $t )
					{
						$testResult = new DevError(DevError::TEST_FAILED, DevError::ERR, $t->getMessage());
					}

					$plugins[$loop]->removeTempPluginFolder();
					$finishedPlugins[] = $pluginName;
					$cleanupTime = time() - $cleanupStart;
					echo "OK (" . secondsToHumanString($cleanupTime) . ")" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}
}

function secondsToHumanString(int $seconds)
{
	$string = "";
	$s = (int)((($seconds)                              ) %60);
	$m = (int)((($seconds-$s)                /(60      )) %60);
	$h = (int)((($seconds-$s-$m*60)          /(60*60   )) %24);
	$d = (int)((($seconds-$s-$m*60-$h*60*60) /(60*60*24))    );
	if( $d > 0 )
		$string .= "{$d}d ";
	if( $d > 0 || $h > 0)
		$string .= "{$h}h ";
	if( $d > 0 || $h > 0 || $m > 0)
		$string .= "{$m}m ";
	return $string . "{$s}s";
}