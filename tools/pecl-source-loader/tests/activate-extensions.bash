#!/bin/bash

for phpextension in ../mods-available/*.ini; do
	phpextensionname=$(basename "$phpextension" .ini)
	phpextensionfile=$(basename "$phpextension")
	
	ln -s "../mods-available/$phpextensionfile"
	php.exe -m 2> /dev/null | grep -i "$phpextensionname"
	unlink $phpextensionfile
	printf "\n"
done

