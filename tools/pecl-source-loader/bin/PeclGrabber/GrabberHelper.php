<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace PeclSourceLoader\PeclGrabber;

use TripleTowerSDK\Error\NetworkError;
use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Error\DevError;

class GrabberHelper
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	protected static $caFile = "";
	protected static $testUrl = "https://example.org/";
	protected static $proxyDetected = false;
	protected static $proxyName = "";
	protected static $proxyPort = "";
	protected static $proxyPass = "";
	

	public static function detectProxy()
	{
		// Already detected, don't try twice
		if( self::$proxyDetected )
			return new NetworkError(NetworkError::SUCCESS);

		// If no proxy defined, then there can't be any proxy detected
		if( !isset($_SERVER["TT"]->conf->proxy) || !empty($_SERVER["TT"]->conf->proxy->proxyName) )
			return new NetworkError(NetworkError::SUCCESS);
		self::$proxyName = $_SERVER["TT"]->conf->proxy->proxyName;
		self::$proxyPort = $_SERVER["TT"]->conf->proxy->proxyPort;

		// Set common options for both curl handles
		$curlCommonOptions =
		[
			CURLOPT_CONNECT_ONLY => true,
			CURLOPT_NOBODY => true,
			CURLOPT_HEADER => false,
			CURLOPT_RETURNTRANSFER => false,
			CURLOPT_BINARYTRANSFER => false,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_SSL_VERIFYSTATUS => true,
			CURLOPT_FAILONERROR => true,
			CURLOPT_TIMEOUT => 5
		];

		$curlHandleWithoutProxy = curl_init(self::$testUrl);
		curl_setopt_array($curlHandleWithoutProxy, $curlCommonOptions);

		$curlHandleWithProxy = curl_init(self::$testUrl);
		curl_setopt_array($curlHandleWithProxy, array_merge($curlCommonOptions,
		[
			CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
			CURLOPT_PROXY => self::$proxyName.":".self::$proxyPort,
			CURLOPT_PROXYPORT => self::$proxyPort
		]));

		if( !empty($_SERVER["TT"]->conf->proxy->caFile) )
		{
			self::$caFile = $_SERVER["TT"]->conf->proxy->caFile;
			if( !is_file(self::$caFile) )
				return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Could not find ca-file '" . self::$caFile . "'");
			curl_setopt($curlHandleWithoutProxy, CURLOPT_CAINFO, self::$caFile);
			curl_setopt($curlHandleWithProxy, CURLOPT_CAINFO, self::$caFile);
		}

		if( !empty($_SERVER["TT"]->conf->proxy->proxyUser) )
		{
			self::$proxyUser = $_SERVER["TT"]->conf->proxy->proxyUser;
			self::$proxyPass = $_SERVER["TT"]->conf->proxy->proxyPass;
			curl_setopt($curlHandle, CURLOPT_PROXYUSERPWD, self::$proxyUser.":".self::$proxyPass);
		}

		if( !empty($_SERVER["TT"]->conf->proxy->proxyCaFile) )
		{
			self::$proxyCaFile = $_SERVER["TT"]->conf->proxy->proxyCaFile;
			if( !is_file(self::$proxyCaFile) )
				return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Could not find proxy ca-file '" . self::$proxyCaFile . "'");
			curl_setopt($curlHandleWithProxy, CURLOPT_PROXY_CAINFO, self::$proxyCaFile);
		}

		// First check without proxy
		if( curl_exec($curlHandleWithoutProxy) === true )
			return new NetworkError(NetworkError::SUCCESS);

		// Second check with proxy
		if( ($proxyResult = curl_exec($curlHandleWithoutProxy)) !== true )
			return new NetworkError(NetworkError::CONNECTION_LOSS, NetworkError::ERR, "Could not connect to '" . self::$testUrl  . "', neither with nor without using proxy settings");

		$self::$proxyDetected = true;
		return new NetworkError(NetworkError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected static function applyProxyHeadersIfNecessary($curlHandle)
	{
		// Use HKEY_CURRENT_USER > Software > Microsoft > Windows > CurrentVersion > Internet Settings > ProxyServer
		// or from local config files -> Always necessary if an authentication is necessary for the proxy
		// Use https://stackoverflow.com/a/15286518 to put this into Helper

		if( get_resource_type($curlHandle) !== "curl" )
			return new DevError(DevError::INVALID_INPUT_VALUE, DevError::ERR, "The given handle for applying proxy settings is not a curl handle");

		if( self::$proxyDetected )
		{
			curl_setopt_array($curlHandle,
			[
				CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
				CURLOPT_PROXY => self::$proxyName.":".self::$proxyPort,
				CURLOPT_PROXYPORT => self::$proxyPort
			]);

			if( !empty(self::$proxyUser) )
				curl_setopt($curlHandle, CURLOPT_PROXYUSERPWD,self::$proxyUser.":".self::$proxyPass);

			if( !empty(self::$proxyCaFile) )
				curl_setopt($curlHandle, CURLOPT_PROXY_CAINFO, self::$proxyCaFile);
		}
		return $curlHandle;
	}
}
