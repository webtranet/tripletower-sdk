<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace PeclSourceLoader\PeclGrabber;

use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Error\NetworkError;
use TripleTowerSDK\Error\DevError;
use TripleTowerSDK\Error\SysError;

class GrabberSwarm implements \Iterator
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $droneIndex = 0;
	protected $droneHive = [];

	protected $proxyDetected = false;


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function __construct($peclNames, string $droneType)
	{
		// Prepare pecl names
		$this->droneIndex = 0;
		
		// Sanitize -> to_array + lowercase + unique
		$peclNames = array_unique( array_map('strtolower', (is_array($peclNames) ? $peclNames : [(string)$peclNames])) );

		// Load all pecl plugins
		$arrayWalkResult = array_walk($peclNames, function(&$peclName) use ($droneType)
		{
			$droneClassName = trim("PeclSourceLoader\\PeclGrabber\\GrabberDrones\\$droneType");
			$this->droneHive[] = new $droneClassName($peclName);
		});
		if( !$arrayWalkResult )
			throw new SysError( SysError::INITIALIZATION_ERROR, SysError::ERR, t("PECLSOURCELOADER.COULDNOTCREATEDRONEHIVE") );
	}

	public function detectProxy()
	{
		// Already detected, don't try twice
		if( $this->proxyDetected )
			return new NetworkError(NetworkError::SUCCESS);

		// If no proxy defined, then there can't be any proxy detected
		if( !isset($_SERVER["TT"]->conf->proxy) || !empty($_SERVER["TT"]->conf->proxy->proxyName) )
			return new NetworkError(NetworkError::SUCCESS);
		$this->proxyName = $_SERVER["TT"]->conf->proxy->proxyName;
		$this->proxyPort = $_SERVER["TT"]->conf->proxy->proxyPort;

		// Set common options for both curl handles
		$curlCommonOptions =
		[
			CURLOPT_CONNECT_ONLY => true,
			CURLOPT_NOBODY => true,
			CURLOPT_HEADER => false,
			CURLOPT_RETURNTRANSFER => false,
			CURLOPT_BINARYTRANSFER => false,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_SSL_VERIFYSTATUS => true,
			CURLOPT_FAILONERROR => true,
			CURLOPT_TIMEOUT => 5
		];

		$curlHandleWithoutProxy = curl_init("$this->peclBaseUrl");
		curl_setopt_array($curlHandleWithoutProxy, $curlCommonOptions);

		$curlHandleWithProxy = curl_init("$this->peclBaseUrl");
		curl_setopt_array($curlHandleWithProxy, array_merge($curlCommonOptions,
		[
			CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
			CURLOPT_PROXY => "$this->proxyName:$this->proxyPort",
			CURLOPT_PROXYPORT => $this->proxyPort
		]));

		if( !empty($_SERVER["TT"]->conf->proxy->caFile) )
		{
			$this->caFile = $_SERVER["TT"]->conf->proxy->caFile;
			if( !is_file($this->caFile) )
				return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Could not find ca-file '$this->caFile'");
			curl_setopt($curlHandleWithoutProxy, CURLOPT_CAINFO, $this->caFile);
			curl_setopt($curlHandleWithProxy, CURLOPT_CAINFO, $this->caFile);
		}

		if( !empty($_SERVER["TT"]->conf->proxy->proxyUser) )
		{
			$this->proxyUser = $_SERVER["TT"]->conf->proxy->proxyUser;
			$this->proxyPass = $_SERVER["TT"]->conf->proxy->proxyPass;
			curl_setopt($curlHandle, CURLOPT_PROXYUSERPWD, "$this->proxyUser:$this->proxyPass");
		}

		if( !empty($_SERVER["TT"]->conf->proxy->proxyCaFile) )
		{
			$this->proxyCaFile = $_SERVER["TT"]->conf->proxy->proxyCaFile;
			if( !is_file($this->proxyCaFile) )
				return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Could not find proxy ca-file '$this->proxyCaFile'");
			curl_setopt($curlHandleWithProxy, CURLOPT_PROXY_CAINFO, $this->proxyCaFile);
		}

		// First check without proxy
		if( curl_exec($curlHandleWithoutProxy) === true )
			return new NetworkError(NetworkError::SUCCESS);

		// Second check with proxy
		if( ($proxyResult = curl_exec($curlHandleWithoutProxy)) !== true )
			return new NetworkError(NetworkError::CONNECTION_LOSS, NetworkError::ERR, "Could not connect to '$this->peclBaseUrl', neither with nor without using proxy settings");

		$this->proxyDetected = true;
		return new NetworkError(NetworkError::SUCCESS);
	}

	public function rewind()
	{
		$this->droneIndex = 0;
	}

	public function current()
	{
		return $this->droneHive[$this->droneIndex];
	}

	public function key()
	{
		return $this->droneIndex;
	}

	public function next()
	{
		++$this->droneIndex;
	}

	public function valid()
	{
		return isset($this->droneHive[$this->droneIndex]);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function applyRequestHeaders($curlHandle)
	{
		if( get_resource_type($curlHandle) !== "curl" )
			return DevError(DevError::INVALID_INPUT_VALUE, DevError::ERR, "The given handle for applying standard headers is not a curl handle");

		curl_setopt_array($curlHandle,
		[
			CURLOPT_HEADER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_SSL_VERIFYPEER => true,
			//CURLOPT_SSL_VERIFYSTATUS => true, // TODO: Why is there no OSCP response if requested?
			CURLOPT_FAILONERROR => true
		]);

		if( !empty($this->caFile) )
			curl_setopt($curlHandle, CURLOPT_CAINFO, $this->caFile);

		return $curlHandle;
	}


	protected function applyProxyHeadersIfNecessary($curlHandle)
	{
		if( get_resource_type($curlHandle) !== "curl" )
			return DevError(DevError::INVALID_INPUT_VALUE, DevError::ERR, "The given handle for applying proxy settings is not a curl handle");

		if( $this->proxyDetected )
		{
			curl_setopt_array($curlHandle,
			[
				CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
				CURLOPT_PROXY => "$this->proxyName:$this->proxyPort",
				CURLOPT_PROXYPORT => $this->proxyPort
			]);

			if( !empty($this->proxyUser) )
				curl_setopt($curlHandle, CURLOPT_PROXYUSERPWD, "$this->proxyUser:$this->proxyPass");

			if( !empty($this->proxyCaFile) )
				curl_setopt($curlHandle, CURLOPT_PROXY_CAINFO, $this->proxyCaFile);
		}
		return $curlHandle;
	}
}
