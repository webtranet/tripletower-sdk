<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace PeclSourceLoader\PeclGrabber\GrabberDrones;

abstract class AGrabberDrone
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $peclName = "";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	abstract public static function getDronType();

	public function __construct(string $peclName)
	{
		$this->peclName = $peclName;
	}

	public function getPeclName()
	{
		return $this->peclName;
	}

	abstract public function grabLink();
	abstract public function download(string $link, string $destinationFolder);
	abstract public function extract();


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}
