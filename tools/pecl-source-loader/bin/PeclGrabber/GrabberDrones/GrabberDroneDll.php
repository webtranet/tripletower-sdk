<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace PeclSourceLoader\PeclGrabber\GrabberDrones;

use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\HttpError;
use TripleTowerSDK\Error\SysError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Helper\Version;

class GrabberDroneDll extends AGrabberDrone
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	//protected $peclBaseUrl = "https://windows.php.net/downloads/pecl/releases";
	protected $peclBaseUrl = "https://downloads.php.net/~windows/pecl/releases";
	protected $localFilePath = "";

	protected $threadSafety = "ts";
	protected $phpVersion = "8.1";
	protected $visualCompiler = "vs16";
	protected $cpuArchitecture = "x64";


	/***********************************
	* PUBLIC METHODS                     *
	***********************************/

	public static function getDronType()
	{
		return __CLASS__;
	}

	public function __construct(string $peclName)
	{
		parent::__construct($peclName);
	}

	public function grabLink()
	{
		$versionOverviewLink = "$this->peclBaseUrl/$this->peclName";

		$highestVersionLink = $this->grabHighestVersionLink($versionOverviewLink);
		if( TripleTowerError::isError( $highestVersionLink ))
			return $highestVersionLink;

		$peclZipLink = $this->grabPeclZipLink($highestVersionLink);
		if( TripleTowerError::isError( $peclZipLink ))
			return $peclZipLink;

		return $peclZipLink;
	}

	public function download(string $peclZipLink, string $destinationFolder)
	{
		if( !Helper::createPathIfNotExists($destinationFolder) )
			return new FsError( FsError::FOLDER_CREATION_FAILED, FsError::ERR, t("PECLSOURCELOADER.DOWNLOADFOLDERCOULDNOTBECREATED", $destinationFolder) );

		$localFilePath = "$destinationFolder/" . basename($peclZipLink);
		$downloadError = Helper::downloadFile($peclZipLink, $localFilePath );
		if($downloadError->isSuccess())
			$this->localFilePath = $localFilePath;

		return $downloadError;
	}

	public function extract()
	{
		// Gather base info and intermediate names like:
		// /temp/abc-1.2.3.tgz -> /temp/abc.tgz -> /temp/abc.tar -> /temp/abc/ -> /temp/abc-1.2.3/
		if( !file_exists( $this->localFilePath ) )
			return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, t("PECLSOURCELOADER.LOCALFILENOTFOUNDFOREXTRACTION", $this->localFilePath) );

		$destinationFolderName = pathinfo($this->localFilePath, PATHINFO_DIRNAME);
		$peclLibraryFilename = "php_$this->peclName.dll";

		$peclZip = new \ZipArchive();
		if( $peclZip->open($this->localFilePath, \ZipArchive::CHECKCONS) !== true )
			return new SysError(SysError::MALFORMED_ZIPFILE, SysError::ERR, t("PECLSOURCELOADER.LOCALZIPFILECORRUPT"));

		if( $peclZip->extractTo($destinationFolderName, $peclLibraryFilename) !== true )
			return new FsError( FsError::FILE_CREATION_FAILED, FsError::ERR, t("PECLSOURCELOADER.LOCALZIPFILEEXTRACTIONFAILED"));

		if( $peclZip->close() !== true )
			return new SysError( SysError::UNEXPECTED_ZIP_ERROR, SysError::ERR, t("PECLSOURCELOADER.LOCALZIPFILECLOSINGFAILED"));

		// Remove old compressed file
		if( !unlink($this->localFilePath) )
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTUNLINK", $this->localFilePath) );

		$this->localFilePath = "$destinationFolderName/$peclLibraryFilename";

		return new FsError(FsError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function grabHighestVersionLink( string $versionOverviewLink )
	{
		$versionOverviewPage = Helper::downloadFile($versionOverviewLink);
		if( TripleTowerError::isError( $versionOverviewPage) )
			return $versionOverviewPage;

		$doc = new \DOMDocument();
		if( !$doc->loadHTML($versionOverviewPage) )
			return new HttpError(HttpError::HTTP_UNSUPPORTED_MEDIA_TYPE, HttpError::WARN, "Could not parse version over view page '$versionOverviewLink'");

		$xpath = new \DOMXPath($doc);
		//$entries = $xpath->query('/html/body/pre/a'); // Use this for https://windows.php.net/downloads/pecl/releases/
		$entries = $xpath->query('/html/body/table/tr/td/a'); // Use this for https://downloads.php.net/~windows/pecl/releases/

		$highestVersionNode = null;
		$highestVersion = new Version("0.0");
		for($loop = 1; $loop < $entries->length; $loop++)
		{
			// Skip first entry because it's always the "To Parent Directory" link
			try
			{
				$nodeVersion = new Version($entries[$loop]->nodeValue);
				if( $nodeVersion->bigger( $highestVersion ) )
				{
					$highestVersion = $nodeVersion;
					$highestVersionNode = $entries[$loop];
				}
			}
			catch( \Exception $ex )
			{
				continue;
			}
		}

		if( $highestVersionNode === null )
			return new FsError( FsError::FILE_NOT_FOUND, FsError::SILENT, t("PECLSOURCELOADER.NOPECLLINKFOUND"));

		return $versionOverviewLink . "/" . $highestVersionNode->attributes["href"]->nodeValue;
	}

	protected function grabPeclZipLink( string $highestVersionLink )
	{
		$highestVersionLinkPage = Helper::downloadFile($highestVersionLink);
		if( TripleTowerError::isError( $highestVersionLinkPage) )
			return $highestVersionLinkPage;

		$doc = new \DOMDocument();
		if( !$doc->loadHTML($highestVersionLinkPage) )
			return new HttpError(HttpError::HTTP_UNSUPPORTED_MEDIA_TYPE, HttpError::WARN, "Could not parse version over view page '$highestVersionLinkPage'");
		$xpath = new \DOMXPath($doc);
		//$entries = $xpath->query('/html/body/pre/a'); // Use this for https://windows.php.net/downloads/pecl/releases/
		$entries = $xpath->query('/html/body/table/tr/td/a'); // Use this for https://downloads.php.net/~windows/pecl/releases/

		for($loop = 1; $loop < $entries->length; $loop++)
		{
			// Skip first entry because it's always the "To Parent Directory" link
			$regex = "php_$this->peclName-[0-9a-z\\.]+-$this->phpVersion-$this->threadSafety-$this->visualCompiler-$this->cpuArchitecture\\.zip";
			$matches = [];
			if( preg_match("/$regex/i", $entries[$loop]->nodeValue, $matches) === 1 )
			{
				return $highestVersionLink . "/" .  $entries[$loop]->attributes["href"]->nodeValue;
			}
		}

		return new FsError( FsError::FILE_NOT_FOUND, FsError::SILENT, t("PECLSOURCELOADER.NOPECLLINKFOUND"));
	}
}