<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace PeclSourceLoader\PeclGrabber\GrabberDrones;

use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;

class GrabberDroneSource extends AGrabberDrone
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/
	const CODE_STATE_STABLE = "stable";
	const CODE_STATE_BETA   = "beta";
	const CODE_STATE_ALPHA  = "alpha";
	const CODE_STATE_DEVEL  = "devel";


	const COLUMN_VERSION     =  1;
	const COLUMN_STATE       =  3;
	const COLUMN_RELEASEDATE =  5;
	const COLUMN_DOWNLOAD    =  7;
	const COLUMN_CHANGELOG   =  9;
	const NUMBER_OF_COLUMNS  = 11;

	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $peclBaseUrl = "https://pecl.php.net/package";
	protected $localFilePath = "";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public static function getDronType()
	{
		return __CLASS__;
	}

	public function __construct(string $peclName)
	{
		parent::__construct($peclName);
	}

	public function grabLink()
	{
		$packageOverviewSite = Helper::downloadFile("$this->peclBaseUrl/$this->peclName");
		if( TripleTowerError::isError( $packageOverviewSite) )
			return $packageOverviewSite;

		$doc = new \DOMDocument();
		if( !$doc->loadHTML($packageOverviewSite) )
			return new HttpError(HttpError::HTTP_UNSUPPORTED_MEDIA_TYPE, HttpError::WARN, "Could not parse version over view page '$packageOverviewSite'");

		$xpath = new \DOMXPath($doc);
		$query = '//table/tr';
		$peclTableRows = $xpath->query($query);

		$extractedCodeState    = "";
		$extractedAbsolutePath = "";
		$codeStates = [self::CODE_STATE_STABLE, self::CODE_STATE_BETA, self::CODE_STATE_ALPHA, self::CODE_STATE_DEVEL];
		foreach($codeStates as $codeState)
		{
			$extractedCodeState    = $codeState;
			$extractedAbsolutePath = $this->parseTables($peclTableRows, $codeState);
			if( !empty($extractedAbsolutePath) )
				break;
		}

		if( empty($extractedAbsolutePath) )
			return new FsError(FsError::FILE_NOT_FOUND, FsError::NOTICE, t("PECLSOURCELOADER.LINKCOULDNOTBEGRABBED") );

		echo "found $extractedCodeState... ";
		return parse_url($this->peclBaseUrl, PHP_URL_SCHEME) . "://" . parse_url($this->peclBaseUrl, PHP_URL_HOST) . $extractedAbsolutePath;
	}

	public function download(string $link, string $destinationFolder)
	{
		if( !Helper::createPathIfNotExists($destinationFolder) )
			return new FsError( FsError::FOLDER_CREATION_FAILED, FsError::ERR, t("PECLSOURCELOADER.DOWNLOADFOLDERCOULDNOTBECREATED", $destinationFolder) );

		$localFilePath = "$destinationFolder/" . basename($link);
		$downloadError = Helper::downloadFile($link, $localFilePath );
		if($downloadError->isSuccess())
			$this->localFilePath = $localFilePath;

		return $downloadError;
	}

	public function extract()
	{
		// Gather base info and intermediate names like:
		// /temp/abc-1.2.3.tgz -> /temp/abc.tgz -> /temp/abc.tar -> /temp/abc/ -> /temp/abc-1.2.3/
		if( !file_exists( $this->localFilePath ) )
			return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, t("PECLSOURCELOADER.LOCALFILENOTFOUNDFOREXTRACTION", $this->localFilePath) );

		$pathInfo = pathinfo($this->localFilePath);


		$simpleName = $this->peclName;
		$dirName = $pathInfo["dirname"];
		$fileName = $pathInfo["filename"];
		$extension = $pathInfo["extension"];

		$originalFilePath			= $this->localFilePath;				// /temp/abc-1.2.3.tgz
		$compressedSimpleFilePath	= "$dirName/$simpleName.$extension";// /temp/abc.tgz
		$archivedFilePath			= "$dirName/$simpleName.tar";		// /temp/abc.tar
		$extractedSimpleFolderPath	= "$dirName/$simpleName";			// /temp/abc
		$extractedFolderPath		= "$dirName/$fileName";				// /temp/abc-1.2.3

		// Remove old comopressed file
		if( file_exists($compressedSimpleFilePath) && !unlink($compressedSimpleFilePath) )
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTUNLINK", $compressedSimpleFilePath) );

		// Rename to simple file name
		if( rename( $originalFilePath, $compressedSimpleFilePath ) === false )
			return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTRENAME", $originalFilePath, $compressedSimpleFilePath) );

		// Decompress from tgz to tar
		if( file_exists($archivedFilePath) && !unlink($archivedFilePath) )
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTUNLINK", $archivedFilePath) );

		$compressedSimpleFilePhar = new \PharData($compressedSimpleFilePath);
		$compressedSimpleFilePhar->decompress();
		unset($compressedSimpleFilePhar);
		if( !unlink($compressedSimpleFilePath) )
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTUNLINK", $compressedSimpleFilePath) );

		// Unarchive from tar
		$archivedFilePhar = new \PharData($archivedFilePath);
		$archivedFilePhar->extractTo($extractedSimpleFolderPath, null, true);
		unset($archivedFilePhar);
		if( !unlink($archivedFilePath) )
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTUNLINK", $archivedFilePath) );

		// Move subfolder to destination
		if( rename( "$extractedSimpleFolderPath/$fileName", "$extractedFolderPath" ) === false )
			return new FsError(FsError::FOLDER_RENAMING_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTRENAMEEXTRACTEDFOLDER", "$extractedSimpleFolderPath/$fileName", "$extractedFolderPath") );

		if( !Helper::unlink_recursive($extractedSimpleFolderPath)->isSuccess() )
			return new FsError(FsError::FOLDER_DELETION_FAILED, FsError::ERR, t("PECLSOURCELOADER.COULDNOTDELETETEMPEXTRACTEDFOLDER", $extractedSimpleFolderPath) );

		return new FsError(FsError::SUCCESS);
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function parseTables(\DOMNodeList $peclTableRows, string $codeState) : string
	{
		$latestDate = strtotime('01-01-1970');
		$extractedAbsolutePath = "";

		foreach($peclTableRows as $peclTableRow)
		{
			// Even there are only 5 "real" columns,
			// the HTML has 6 additional #text nodes
			// -> results in 11 columns
			if( count($peclTableRow->childNodes) === self::NUMBER_OF_COLUMNS )
			{
				if( $peclTableRow->childNodes[self::COLUMN_STATE]->nodeValue === $codeState )
				{
					$newDate = strtotime( $peclTableRow->childNodes[self::COLUMN_RELEASEDATE]->nodeValue );
					if($newDate > $latestDate)
					{
						$latestDate = $newDate;
						$aTag = $peclTableRow->childNodes[self::COLUMN_DOWNLOAD]->childNodes[0];
						$extractedAbsolutePath = $aTag ->attributes->item(0)->nodeValue;
					}
				}
			}
		}

		return $extractedAbsolutePath;
	}
}