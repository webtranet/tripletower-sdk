#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Tower\CliTower;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Helper\Helper;
use PeclSourceLoader\PeclGrabber\GrabberSwarm;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

$cliTower = new CliTower($rootFolder, $sdkFolder, $loader);
$conf = $_SERVER["TT"]->conf;
$logger = $_SERVER["TT"]->logger;

if( empty($conf->destinationFolder) )
{
	$conf->destinationFolder = "$rootFolder/temp/pecl/";
	if( is_dir($conf->destinationFolder) )
		Helper::unlink_recursive($conf->destinationFolder)->throwIfNotSuccess();

	Helper::createPathIfNotExists($conf->destinationFolder);
}

if( !is_dir( $conf->destinationFolder ) )
	throw new ConfigError( ConfigError::INVALID_CONFIGURATION, ConfigError::CRIT, "Destination folder for pecl extensions '$conf->destinationFolder' not found.");

$grabberSwarm = new GrabberSwarm($conf->packages, $_SERVER["TT"]->tower->getTowerInstance() );
foreach($grabberSwarm as $grabberDrone)
{
	echo $grabberDrone->getPeclName().": ";
	echo "grabbing... ";
	$peclLink = $grabberDrone->grabLink();
	if( TripleTowerError::isError($peclLink) )
	{
		echo "Error: " . $peclLink->getMessage() . PHP_EOL;
		continue;
	}

	echo "downloading... ";
	$downloadResult = $grabberDrone->download($peclLink, $conf->destinationFolder);
	if( !$downloadResult->isSuccess() )
	{
		echo "Error: " . $downloadResult->getMessage() . PHP_EOL;
		continue;
	}

	echo "extracting... ";
	$extractResult = $grabberDrone->extract();
	if( !$extractResult->isSuccess() )
	{
		echo "Error: " . $extractResult->getMessage() . PHP_EOL;
		continue;
	}

	echo "Success!".PHP_EOL;
}

echo PHP_EOL;
echo "Done" . PHP_EOL;