@echo off
REM
REM TripleTower SDK - https://webtranet.online/tripletower-sdk
REM
REM @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
REM @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
REM @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
REM
title classmap-generator

cls

REM Temporarily disabled PHP from loading ini files if specified
set PHP_INI_SCAN_DIR=

REM Start class map generator
"%~dp0\..\php\php" "%~dp0\classmap-generator.php"