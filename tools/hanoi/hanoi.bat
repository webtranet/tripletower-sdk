@echo off
REM
REM TripleTower SDK - https://webtranet.online/tripletower-sdk
REM
REM @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
REM @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
REM @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
REM
title Hanoi

cls
set currentdir=%cd%
set host=%~1
set remoteservertype=%~2
set username=%~3
set password=%~4

cd /d %~dp0

REM Check if user started hanoi.bat by double clicking or via commandline
set calledByDblClick="false"
for /f delims^=^"^ tokens^=1^,2 %%a in ("%cmdcmdline%") do set path1="%%a" && set path2="%%b"
if %path2% == "%~0" (
	set calledByDblClick="true"
)
if %path2% == " /C " (
	set calledByDblClick="true"
)

if not "%remoteservertype%" == "local" (
	call :printLogo
) else (
	REM Reset remoteservertype if it is "local" since it's just an indicator for a local installation called by remote
	set remoteservertype=
)

set printhelp="false"
if "%host%" == "-h" set printhelp="true"
if "%host%" == "-help" set printhelp="true"
if "%host%" == "--help" set printhelp="true"
if "%host%" == "v" set printhelp="true"
if "%host%" == "-v" set printhelp="true"
if "%host%" == "-version" set printhelp="true"
if "%host%" == "--version" set printhelp="true"
if "%host%" == "-?" set printhelp="true"
if "%host%" == "/?" set printhelp="true"
if %printhelp% == "true" (
	call :printHelp
	goto :toolDone
)

call :setupLogFile logfile

REM Check if user has sufficient privileges
set startedAsAdmin=true
net session >nul 2>&1 || set startedAsAdmin=false
if "%startedAsAdmin%" == "false" (
	echo Please start hanoi as administrator 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	goto :toolDone
)

REM If no host is given, ask for it
if "%host%" == "" (
	set /P host=Please enter server host name: 
)

REM If host name has an ".json" ending, then remove it
if /i "%host:~-5%"==".json" (
	set "host=%host:~0,-5%"
)

if "%remoteservertype%" == "" (

	echo %time%: Deploying software via hanoi locally for "windows" on %host% 2>&1 | "..\wtee\wtee.exe" -a %logfile%

	REM Temporarily disabled PHP from loading ini files if specified
	set PHP_INI_SCAN_DIR=

	REM Start hanoi locally
	"..\php\php.exe" ".\bin\hanoi.php" -i "%host%" 2>&1 | "..\wtee\wtee.exe" -a %logfile%

) else if "%remoteservertype%" == "linux" (

	echo %time%: Deploying software via hanoi for %remoteservertype% on %host% 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	if "%username%" == "" (
		set /P username=Please enter username:
	)
	if "%password%" == "" (
		set /P password=Please enter password:
	)

	REM Turn hanoi folder into an self extracting archive and put it into temp
	REM Options:
	REM -sfx7zConLinuxX64.sfx  - selfextracting archive for linux
	REM -mx9                    - compression level 9 (ultra)
	REM -ms=on                  - use solid archive
	REM -x!hanoi\?git           - exclude git folder
	REM -x!hanoi\logs           - exclude logs folder
	REM -x!hanoi\bin\*\*        - exclude all files in subfolders inside bin like php, paexec, etc.
	echo 1/6: Creating self extractring archive 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	"..\7zip\7za.exe" a "%temp%\hanoi.bin" -sfx7zConLinuxX64.sfx -mx9 -ms=on -x!hanoi\logs -x!hanoi\bin\*\* "..\hanoi" "..\..\libs" 2>&1 >nul | "..\wtee\wtee.exe" -a %logfile%
	if not exist "%temp%\hanoi.bin" (
		echo Since a 7zip error occured Hanoi is skipping further execution 2>&1 | "..\wtee\wtee.exe" -a %logfile%
		goto :toolDone
	)

	REM Push the self extracting archive to the remote host's temp folder
	REM Options: force scp, force ssh2, port 22, username, password, source, destination
	echo 2/6: Pushing hanoi to remote server 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	"..\putty\pscp.exe" -scp -2 -batch -P 22 -l %username% -pw %password% %temp%\hanoi.bin %host%:/tmp >nul 2>&1 | "..\wtee\wtee.exe" -a %logfile%

	REM Remotly clean up old stuff and remotely extract hanoi
	REM Options: force scp, force ssh2, port 22, username, password, source, destination
	echo 3/6: Extracting hanoi folder to %host%'s temp folder 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	"..\putty\plink.exe" -ssh -2 -batch -P 22 -l %username% -pw %password% %host% "rm -rf /tmp/hanoi && rm -rf /tmp/libs && chmod u+x /tmp/hanoi.bin && /tmp/hanoi.bin -o/tmp" >nul 2>&1 | "..\wtee\wtee.exe" -a %logfile%

	REM Call hanoi remotely
	REM Options: force scp, force ssh2, port 22, username, password, source, destination
	echo 4/6: Calling hanoi remotely 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	"..\putty\plink.exe" -ssh -2 -batch -P 22 -l %username% -pw %password% %host% "chmod u+x /tmp/hanoi/hanoi.sh && /tmp/hanoi/hanoi.sh %host% local" 2>&1 | "..\wtee\wtee.exe" -a %logfile%

	REM Delete remote hanoi folders and self extracting archive
	REM Options: force scp, force ssh2, port 22, username, password, source, destination
	echo 5/6: Removing hanoi from %host% 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	"..\putty\plink.exe" -ssh -2 -batch -P 22 -l %username% -pw %password% %host% "rm -rf /tmp/hanoi && rm -rf /tmp/libs && rm -f /tmp/hanoi.bin" >nul 2>&1 | "..\wtee\wtee.exe" -a %logfile%

	REM Delete local self extracting archive
	REM Options: delete quietly, force deletion
	echo 6/6: Deleting local self extracting archive >nul 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	del /q /f "%temp%\hanoi.bin" 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	if exist "%temp%\hanoi.exe" (
		echo Self extracting archive could not be deleted.
		goto :toolDone
	)

	echo Finished deployment...

	goto :toolDone

) else if "%remoteservertype%" == "windows" (

	echo %time%: Deploying software via hanoi for %remoteservertype% on %host% 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	if "%username%" == "" (
		set /P username=Please enter username:
	)
	if "%password%" == "" (
		set /P password=Please enter password:
	)

	REM Turn hanoi folder into an self extracting archive and put it into temp
	REM Options:
	REM -sfx7zSDWinX64.sfx  - selfextracting archive for windows
	REM -mx9                - compression level 9 (ultra)
	REM -ms=on              - use solid archive
	REM -x!hanoi\?git       - exclude git folder
	REM -x!hanoi\logs       - exclude logs folder
	echo 1/3: Creating self extractring archive 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	call :createSfx 
	if not exist "%temp%\hanoi-remote-binary.bat" (
		echo Since a 7zip error occured Hanoi is skipping further execution 2>&1 | "..\wtee\wtee.exe" -a %logfile%
		goto :toolDone
	)

	REM Call hanoi remotely
	REM It's necessary to have cmdkey.exe called upfront for remote installation
	REM Options:
	REM /accepteula         - accept eula automatically
	REM \\host              - host name to connect to
	REM -h                  - run with highest privileges
	REM -c                  - copy the specified program to the remote host for execution
	REM -f                  - force overwrite if file already exists
	REM -csrc               - alternative path to the file to copy 
	echo 2/3: Pushing hanoi to remote server, extracting and executing it remotely 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	cmdkey.exe /add:%host% /user:%username% /pass:%password% >nul
	"..\paexec\paexec.exe" /accepteula \\%host% -h -c -f -csrc "%temp%\hanoi-remote-binary.bat" hanoi-remote-binary.bat 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	cmdkey.exe /delete:%host% >nul
	
	REM Options: delete quietly, force deletion
	REM Options:
	REM /q                  - quite mode
	REM /f                  - force deletion
	echo 3/3: Deleting local self extracting archive 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	del /q /f "%temp%\hanoi-remote-binary.bat" 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	if exist "%temp%\hanoi-remote-binary.bat" (
		echo Self extracting archive could not be deleted.
		goto :toolDone
	)

	echo Finished deployment...

	goto :toolDone

) else (
	echo The given remote server type is unknown. Only "linux" and "windows" are currently supported. 2>&1 | "..\wtee\wtee.exe" -a %logfile%
	goto :toolDone
)

:toolDone
cd /d %currentdir%

REM Hold command prompt open if started by double clicking
if %calledByDblClick% == "true" (
	pause
)
goto :eof


REM ===========================================================================
REM =                                 METHODS                                 =
REM ===========================================================================

REM This is a helper function to print the logo
:printLogo

echo.
echo  //=========================================================================\\
echo  ^|^|                    _    _          _   _  ____ _____                    ^|^|
echo  ^|^|                   ^| ^|  ^| ^|   /\   ^| \ ^| ^|/ __ \_   _^|                   ^|^|
echo  ^|^|                   ^| ^|__^| ^|  /  \  ^|  \^| ^| ^|  ^| ^|^| ^|                     ^|^|
echo  ^|^|                   ^|  __  ^| / /\ \ ^| . ` ^| ^|  ^| ^|^| ^|                     ^|^|
echo  ^|^|                   ^| ^|  ^| ^|/ ____ \^| ^|\  ^| ^|__^| ^|^| ^|_                    ^|^|
echo  ^|^|                   ^|_^|  ^|_/_/    \_\_^| \_^|\____/_____^|                   ^|^|
echo  ^|^|                                                                         ^|^|
echo  \\=========================================================================//
echo.

goto :eof

REM ===========================================================================

REM This is a helper function to create hanoi.exe for a remote installation
:createSfx

REM Clean stuff from old creations if present
del /q /f "%temp%\hanoi.7z" "%temp%\hanoi.exe" "%temp%\hanoi.b64" "%temp%\hanoi-remote-binary.bat" >nul 2>&1


REM
REM 1/3. Create hanoi-remote-binary.bat file
REM

REM Command 1 [on remote host]: Don't output commands
echo @echo off > %temp%\hanoi-remote-binary.bat

REM Command 2 [on remote host]: base64-decode %hanoi_remote_bin% to "hanoi.exe" to temp-folder ("certutil.exe" ignores batch commands :-)
echo certutil -f -decode %%0 %%temp%%\hanoi.exe ^>nul >> %temp%\hanoi-remote-binary.bat

REM Command 3 [on remote host]: run "hanoi.exe" and extract files to "C:\temp\"
echo %%temp%%\hanoi.exe -y -o"C:\temp\" ^>nul >> %temp%\hanoi-remote-binary.bat

REM Command 4 [on remote host]: delete [on remote host] "hanoi.exe"
echo del %%temp%%\hanoi.exe ^>nul >> %temp%\hanoi-remote-binary.bat

REM Command 5 [on remote host]: run "hanoi.bat" with parameter "%host%"
echo call "C:\temp\hanoi-temp\tripletower-sdk\tools\hanoi\hanoi.bat" "%host%" local >> %temp%\hanoi-remote-binary.bat

REM Command 6 [on remote host]: delete all files extracted by "hanoi.exe"
echo start cmd /c "rmdir /s /q C:\temp\hanoi-temp" >> %temp%\hanoi-remote-binary.bat

REM Command 7 [on remote host]: exit batch to ignore all following lines
echo exit /b >> %temp%\hanoi-remote-binary.bat


REM
REM 2/3. Create 7z file
REM

REM Create hanoi.7z file
setlocal EnableDelayedExpansion

	REM Add every folder in the whitelist to hanoi.7z and move the folder within the archive
	REM IMPORTANT: Do not add a trailing backslash to those folders!
	REM
	REM Options:
	REM -mx9                    - compression level 9 (ultra)
	REM -ms=on                  - use solid archive
	REM -x^^!"hanoi\temp\*"     - exclude temp folder
	REM -x^^!"hanoi\conf\*"     - exclude conf folder
	for %%i in (
		etc\ssl
		libs\tripletower-lib
		libs\zend
		tools\7zip
		tools\hanoi
		tools\php
		tools\putty
		tools\wtee
		tools\hanoi\conf\global.json
		tools\hanoi\conf\%host%.json
	) do (
		call set dirToAdd=%%i
		for %%f in (!dirToAdd!) do (
			call set dirToRenameFrom=%%~nxf
		)
		"..\7zip\7za.exe" a "%temp%\hanoi.7z" -mx9 -ms=on "..\..\..\tripletower-sdk\!dirToAdd!" -x^^!"hanoi\temp\*" -x^^!"hanoi\conf\*" >nul
		"..\7zip\7za.exe" rn "%temp%\hanoi.7z" "!dirToRenameFrom!" "hanoi-temp\tripletower-sdk\!dirToAdd!" >nul
	)
endlocal


REM
REM 3/3. Glue -> base64(sfx, 7z)
REM

REM Put the self extractor and the 7z together to "hanoi.exe"
copy /b "..\7zip\7zSDWinX64.sfx" + "%temp%\hanoi.7z" "%temp%\hanoi.exe" >nul
del /q /f "%temp%\hanoi.7z"

REM Base64-encode "hanoi.exe" to "hanoi.b64"
certutil -f -encode %temp%\hanoi.exe %temp%\hanoi.b64 >nul
del /q /f "%temp%\hanoi.exe"

REM Append "hanoi.b64" to "hanoi-remote-binary.bat"
type %temp%\hanoi.b64 >> %temp%\hanoi-remote-binary.bat
del /q /f "%temp%\hanoi.b64"

goto :eof

REM ===========================================================================

REM This is a helper function to print the help text
:printHelp
echo   浜様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo   �                                                                         �
echo   � DESC                                                                    �
echo   �                                                                         �
echo   �    Installs tripletower-sdk and additional software according to what   �
echo   �    is specified in the server config file in                            �
echo   �    ./hanoi/conf/^<hostname^>.json                                         �
echo   �                                                                         �
echo   �                                                                         �
echo   � SYNOPSIS                                                                �
echo   �                                                                         �
echo   �    hanoi ^<hostname^> [remote-os] [username] [password]                   �
echo   �                                                                         �
echo   �                                                                         �
echo   �    ^<hostname^>                                                           �
echo   �                                                                         �
echo   �         The hostname you want to install software on. This parameter    �
echo   �         is also used to find the config json file.                      �
echo   �                                                                         �
echo   �    remote-host-os                                                       �
echo   �                                                                         �
echo   �        Defaults to "local". Specifies if an remote installation is      �
echo   �        started and what remote server os is to expect.                  �
echo   �                                                                         �
echo   �        Possible values:                                                 �
echo   �                                                                         �
echo   �            local                                                        �
echo   �            linux                                                        �
echo   �            windows                                                      �
echo   �                                                                         �
echo   �    username                                                             �
echo   �                                                                         �
echo   �        Defaults to "". If specified this username is used to connect    �
echo   �        to the remote server.                                            �
echo   �                                                                         �
echo   �    password                                                             �
echo   �                                                                         �
echo   �        Defaults to "". If specified this is used as the user password   �
echo   �        for given user to connect to the remote server. Passwordless     �
echo   �        authentication is not supported.                                 �
echo   �                                                                         �
echo   �                                                                         �
echo   � EXAMPLE                                                                 �
echo   �                                                                         �
echo   �    Local installation:                                                  �
echo   �                                                                         �
echo   �        hanoi tripletower.localhost                                      �
echo   �                                                                         �
echo   �                                                                         �
echo   �    Remote installation:                                                 �
echo   �                                                                         �
echo   �        hanoi remoteserver.example.com windows johndoe secretpassword    �
echo   �                                                                         �
echo   藩様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo.

goto :eof

REM ===========================================================================

REM This is a helper function to format the log file name according to the current date
:setupLogFile

REM If Windows Management Instrumentation is not available then usual date is returned
set wmicAvailable="true"
wmic.exe alias /? >nul 2>&1 || set wmicAvailable="false"

if %wmicAvailable% == "true" (

	REM Tokenize 2nd line of output of wmic.exe and set the according variables and format them to have always only two digits
	for /f "skip=1 tokens=1-3" %%i in ('WMIC Path Win32_LocalTime Get Day^,Month^,Year /Format:table') do (
		set day=00%%i
		set month=00%%j
		set year=%%k
		goto dateLoopEnd
	)
	:dateLoopEnd
	set day=%day:~-2%
	set month=%month:~-2%
	set "%~1=.\temp\%host%_%year%-%month%-%day%.log"
	goto :eof
) else (
	echo wmic.exe not available, using regional date settings for naming logfile
	set "%~1=.\temp\hanoi_%date%.log"
	goto :eof
)
REM Statements "goto :eof" have to stay inside the if-else clauses due to a weird bug in batch