@echo off
REM
REM TripleTower SDK - https://webtranet.online/tripletower-sdk
REM
REM @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
REM @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
REM @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
REM
title Hanoi GUI

cls
set currentdir=%cd%
cd /d %~dp0

"..\php\php.exe" ".\bin\hanoi-gui.php" -i "hanoi-gui"

cd /d %currentdir%
