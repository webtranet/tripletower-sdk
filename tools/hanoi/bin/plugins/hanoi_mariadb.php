<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Helper\DaemonHandler;

class hanoi_mariadb extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $preexistingInstallationFound = false;

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://mirror.wtnet.de/mariadb/mariadb-10.6.19/winx64-packages/mariadb-10.6.19-winx64.zip",
		"serviceName" => "MySQL",

		"destination" => "C:/tripletower-stack/mariadb",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",
		"privateKeyPath" => null,
		"certificatePath" => null,
		"caCertificatepath" => null,

		"backup" => true,
		"backupPrefix" => "mariadb-backup",
		"rootPassword" => "tripletower",
		"bindAdress" => "127.0.0.1",
		"serverName" => "tripletower.localhost",
		"logging" => false
	];

	protected $configLinuxDefaults =
	[
		"serviceName" => "mysql",

		"tripletowersdk" => "/srv/tripletower-sdk",
		"privateKeyPath" => "/srv/tripletower-sdk/etc/ssl/private/tripletower.localhost.key",
		"certificatePath" => "/srv/tripletower-sdk/etc/ssl/certs/tripletower.localhost.crt",
		"caCertificatePath" => "/srv/tripletower-sdk/etc/ssl/certs/tripletower_ca.crt",

		"backup" => true,
		"backupPrefix" => "mariadb-backup",
		"rootPassword" => "tripletower",
		"bindAdress" => "127.0.0.1",
		"serverName" => "tripletower.localhost",
		"logging" => false
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				// Download files
				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
				{
						$downloadError->addMessage("Could not copy file '" . $this->config["packageSourcePath"] . "' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");
						throw $downloadError;
				}

				// Unzip new mariadb
				$zip = new \ZipArchive();
				$archiveFile = $tempDir.basename($this->config["packageSourcePath"]);
				if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
				{
					if( $zipError === \ZipArchive::ER_INCONS )
						echo "'" . basename($this->config["packageSourcePath"]) . "' failed consistency checks but can be extracted anyway - ";
					else
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");

					if( ($zipError = $zip->open($archiveFile)) !== true )
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
				}

				$tempMariaDbFolder = $tempDir.basename($this->config["packageSourcePath"], ".zip");
				if( $zip->extractTo($tempMariaDbFolder) !== true )
					throw new \Exception("Could not extract content of zip archive $tempDir" . basename($this->config["packageSourcePath"]) . " to $tempMariaDbFolder");
				if( $zip->close() !== true )
					throw new \Exception("Could not close zip archive $tempDir" . basename($this->config["packageSourcePath"]));

				// Remove debug symbol files
				$innerTempMariaDbFolder = "$tempMariaDbFolder/".basename($this->config["packageSourcePath"], ".zip");
				$pdbIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($innerTempMariaDbFolder));
				foreach( $pdbIterator as $file )
				{
					if( $file->isFile() && $file->getExtension() === "pdb" )
						unlink( $file->getPathname() );
				}

				// Remove mariadb test folder
				$mariadbTestFolder = "$innerTempMariaDbFolder/mysql-test";
				if( is_dir($mariadbTestFolder) )
					Helper::unlink_recursive($mariadbTestFolder)->throwIfNotSuccess();

				// Remove data folder and create an empty folder
				$mariadbDataFolder = "$innerTempMariaDbFolder/data";
				if( is_dir($mariadbDataFolder) )
					Helper::unlink_recursive($mariadbDataFolder)->throwIfNotSuccess();

			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_tripletowersdk'];
	}

	public function install()
	{
		// Find tripletowersdk folder
		$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["tripletowersdk"] );
		if( !is_dir($tripletowersdkFolder) )
			throw new \Exception("Could not find folder '$tripletowersdkFolder'");

		// mariadb config file for symlink
		$mariaDbConfLinkName = "";
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Normalize destination
				$destination = Helper::normalizeFilePath($this->config["destination"]);

				// Find previous mariadb version
				$this->preexistingInstallationFound = is_dir("$destination/data");

				// Create destination folder if it does not exist
				if( !is_dir($destination) && !mkdir($destination, 0777, true) )
					throw new \Exception("Could not create mariadb destination folder '$destination'");

				// Create datafolder if it does not extist
				$dataFolder = "$destination/data";
				if( !is_dir( $dataFolder ) && !mkdir($dataFolder, 0777, true) )
					throw new \Exception("The folder '$dataFolder' could not be created");

				// Create backupsfolder if it does not extist
				$backupsFolder = "$destination/backups";
				if( !is_dir( $backupsFolder ) && !mkdir($backupsFolder, 0777, true) )
					throw new \Exception("The folder '$backupsFolder' could not be created");

				// Create a backup if specified
				if( (bool)($this->config["backup"]) )
				{
					if( $this->preexistingInstallationFound )
					{
						$dumpCommand = '"'.$destination.'/bin/mariadb-dump.exe" ' .
							'--user=root ' .
							'--password=' . $this->config["rootPassword"] . ' ' .
							'--all-databases ' .
							'> "'.$backupsFolder.'/'.$this->config["backupPrefix"].'_'.date("Y-m-d_H-i-s").'.sql"';
						Helper::exec($dumpCommand);
					}
					else
						echo "no previous installation found so backup not created - ";
				}

				// Remove service
				DaemonHandler::deleteDaemon( $this->config["serviceName"] )->throwIfNotSuccess();

				// Remove old /mariadb/bin folder from path variable
				$oldMariaDbExePath = Helper::findExecutableInPathVariable("mariadbd.exe");
				if( !TripleTowerError::isError($oldMariaDbExePath) )
					Helper::removePathFromPathVariable(dirname($oldMariaDbExePath))->throwIfNotSuccess("Could not remove old mysql path '$oldMariaDbExePath' from Path variable");
				else
					$oldMariaDbExePath->throwIfNotSuccessOrSameAs(new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT));

				// Delete everything except data and backups folder
				$deleteIterator = new \FilesystemIterator($destination, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS );
				foreach($deleteIterator as $mariadbOldFile)
				{
					// Don't touch data and backups folder
					if( $mariadbOldFile->isDir() && ($mariadbOldFile->getBasename() === "data" || $mariadbOldFile->getBasename() === "backups") )
						continue;

					// Remove everything else
					Helper::unlink_recursive($mariadbOldFile->getPathname())->throwIfNotSuccess();
				}

				// Copy whole mariadb folders except data and backups
				$sourceFolder = $this->getTempPluginFolder() . basename($this->config["packageSourcePath"], ".zip");
				$copyIterator = new \FilesystemIterator("$sourceFolder/" . basename($this->config["packageSourcePath"], ".zip"), \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS);
				foreach($copyIterator as $mariadbNewFile)
				{
					// Don't touch data folder if it already existed
					if( $mariadbNewFile->isDir() && $mariadbNewFile->getBasename() === "data" && $this->preexistingInstallationFound)
						continue;

					// Don't touch backups folder
					if( $mariadbNewFile->isDir() && $mariadbNewFile->getBasename() === "backups" )
						continue;

					// Copy everything else
					$destinationSubfolder = "$destination/" . $mariadbNewFile->getBasename();
					if( Helper::copy_recursive($mariadbNewFile->getRealPath(), $destinationSubfolder) !== true )
						throw new \Exception("Could not copy mariadb file or folder from '" . $mariadbNewFile->getRealPath() . "' to '$destinationSubfolder'");
				}

				// mariadb config file for symlink
				$mariaDbConfLinkName = "$destination/my.cnf";

				// Add mariadb to path variable
				Helper::addPathToPathVariable("$destination/bin")->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Find previous mariadb version
				$this->preexistingInstallationFound = is_dir("/var/lib/mysql");

				if( $this->preexistingInstallationFound === true )
				{
					if( (bool)($this->config["backup"]) )
					{
						// Create backupsfolder if it does not extist
						$backupsFolder = "/var/srv/mariadb/backups";
						if( !is_dir($backupsFolder) && mkdir( $backupsFolder, 0644, true ) !== true)
							throw new \Exception("Could not create folder '$backupsFolder'");

						$dumpCommand = '"mariadb-dump" ' .
							'--user=root ' .
							'--password=' . $this->config["rootPassword"] . ' ' .
							'--all-databases ' .
							'> "'.$backupsFolder.'/'.$this->config["backupPrefix"].'_'.date("Y-m-d_H-i-s").'.sql"';
						Helper::exec($dumpCommand);
					}
					else
					{
						echo "no previous installation found so backup not created - ";
					}
				}

				// Install via package manager
				$installError = Helper::genericLinuxInstall("mariadb-server");
				if( !$installError->isSuccess() )
					throw $installError;

				$mariaDbConfLinkName = "/etc/mysql/my.cnf";
			}
		}

		// Create mariadb config file symlink
		$mariaDbConfFileName = "$tripletowersdkFolder/etc/mariadb/my.cnf";
		Helper::createSymlink($mariaDbConfFileName, $mariaDbConfLinkName, true)->throwIfNotSuccess();

		// Tidy serverName, means dots and slashes get replaced by _ because not allowed under ubuntu
		$mariaDbCompliantServerName = str_replace([".", "/"], ["_", "_"], $this->config["serverName"]);

		// Parse my.cnf, search for tokens and replace them
		Helper::replaceInFile('$mySqlBindAdress',   $this->config["bindAdress"], $mariaDbConfFileName)->throwIfNotSuccess();
		Helper::replaceInFile('$serverName',        $mariaDbCompliantServerName, $mariaDbConfFileName)->throwIfNotSuccess();
		Helper::replaceInFile('$activateLogging',   $this->config["logging"] ? "TRUE" : "FALSE", $mariaDbConfFileName)->throwIfNotSuccess();
		Helper::replaceInFile('$privateKeyPath',    $this->config["privateKeyPath"], $mariaDbConfFileName)->throwIfNotSuccess();
		Helper::replaceInFile('$certificatePath',   $this->config["certificatePath"], $mariaDbConfFileName)->throwIfNotSuccess();
		Helper::replaceInFile('$caCertificatePath', $this->config["caCertificatePath"], $mariaDbConfFileName)->throwIfNotSuccess();
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		$mySqlExecutable ="";
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Normalize destination
				$destination = Helper::normalizeFilePath($this->config["destination"]);

				// Full path required. Mariadb added to path variable but not available in current console-session
				$mySqlExecutable = "$destination/bin/mariadb.exe";

				if( $this->preexistingInstallationFound === false )
				{
					// Initialize system tables and install mariadb as service
					Helper::exec('"'.$destination.'/bin/mariadb-install-db.exe" --datadir="'.$destination.'/data" --service='.$this->config["serviceName"].' --password='.$this->config["rootPassword"]);
					Helper::exec("sc config \"" . $this->config["serviceName"] . "\" obj= \"LocalSystem\" password= \"\"");
					if( is_file("$destination/data/my.ini") && !unlink("$destination/data/my.ini") )
						throw new \Exception("Could not remove bogus '$destination/data/my.ini'");

				}
				else
				{
					// Install service only - all other information are already in mariadb's data folder
					DaemonHandler::installDaemon( $this->config["serviceName"], "$destination/bin/mariadbd.exe", ["display" => $this->config["serviceName"], "description" => "MariaDB database server", "params" => $this->config["serviceName"]] );
					Helper::exec("sc config \"" . $this->config["serviceName"] . "\" obj= \"LocalSystem\" password= \"\"");
					DaemonHandler::startDaemon( $this->config["serviceName"] );
					DaemonHandler::stopDaemon( $this->config["serviceName"] );
				}

				// Upgrade the mariadb tables
				Helper::exec('"'.$destination.'/bin/mariadb-upgrade-service.exe" --service='.$this->config["serviceName"]);
			}
			break;

			default:
			{
				$mySqlExecutable = "mysql";
			}
		}

		// Restart new mariadb service (start if not running)
		DaemonHandler::restartDaemon( $this->config["serviceName"] )->throwIfNotSuccess();

		// Secure mariadb (from from mysql_secure_installation.pl)
		Helper::exec("\"$mySqlExecutable\" " .
			"--user=root " .
			"--password=".$this->config["rootPassword"]." " .
			"--force " .
			"--execute " .
			"\"DELETE FROM mysql.user WHERE user=''; " .
			"DELETE FROM mysql.user WHERE user='root' AND host NOT IN ('localhost', '127.0.0.1', '::1'); " .
			"DROP DATABASE IF EXISTS test; " .
			"DELETE FROM mysql.db WHERE db='test' OR db='test\_%'; " .
			"FLUSH PRIVILEGES;\"");
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}