<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name wapp_webtra.about Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Tower;

class hanoi_wapp_webtra_about extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/Webtra.About.git",
		"tripletower" => "C:/tripletower-stack/tripletower",

		"git" => ""
	];

	protected $configLinuxDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/Webtra.About.git",
		"tripletower" => "/srv/tripletower",

		"git" => ""
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		$packageSourcePath = $this->config["packageSourcePath"];

		// Creating folders
		$tempDir = $this->getTempPluginFolder();

		// Download file if not a git repository
		if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "git" )
		{
			// It seems to be a git repository. Test if it exists.
			if( !Helper::remoteFileExists($packageSourcePath)->isSuccess() )
				throw new \Exception("Wapp packageSourcePath '$packageSourcePath' could not be found");
		}
		else if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "zip" )
		{
			// A zip -> Download
			copy( $packageSourcePath, $tempDir );

			// Unzip
			$zip = new \ZipArchive();
			$archiveFile = $tempDir.basename($packageSourcePath);
			if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
			{
				echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
				if( ($zipError = $zip->open($archiveFile)) !== true )
					throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
			}
			if( $zip->extractTo($tempDir.basename($packageSourcePath, ".zip")) !== true )
				throw new \Exception("Could not extract content of zip archive $tempDir" . basename($packageSourcePath) . " to $tempDir" . basename($packageSourcePath, ".zip"));
			if( $zip->close() !== true )
				throw new \Exception("Could not close zip archive $tempDir" . basename($packageSourcePath));
		}
		else
		{
			// Not supported!
			throw new \Exception( "Wapp packageSourcePath '$packageSourcePath' has to be either a git web URL or a zip file" );
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_tripletower'];
	}

	public function install()
	{
		$packageSourcePath = $this->config["packageSourcePath"];
		$tempDir = $this->getTempPluginFolder();

		// Find tripletower folder
		$wappsDestination = Helper::normalizeFilePath($this->config["tripletower"] . "/" . Tower::WAPPS_TOWER);
		if( !is_dir( $wappsDestination ) )
			throw new \Exception("Wapp destination '$wappsDestination' does not exist but is mandatory");

		$destination = "$wappsDestination/" . basename($packageSourcePath, ".git");
		if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "git" )
		{
			// It's git! Clone/pull stuff from repository
			$git = "git";
			if( $this->config["git"] !== "" )
				$git = Helper::normalizeFilePath( $this->config["git"] );
			else
				$git = Helper::normalizeFilePath( Helper::findExecutableInPathVariable("git.exe") );

			if( !is_file($git) )
				throw new \Exception("Could not find file 'git' executable");

			if( !is_dir($destination . '/.git') )
			{
				Helper::exec('"' . $git . '" clone "' . $packageSourcePath . '" "' . $destination . '"');
			}
			else
			{
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" reset --hard @{upstream}');
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" clean -d --force');
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" pull --all');
			}
		}
		else if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "zip" )
		{
			if( is_dir($destination) )
				Helper::unlink_recursive( $destination )->throwIfNotSuccess();
			Helper::copy_recursive( $tempDir.basename($packageSourcePath, ".zip"), $wappsDestination );
		}
		else
		{
			// Not supported!
			throw new \Exception( "Wapp packageSourcePath '$packageSourcePath' has to be either a git web URL or a zip file" );
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}