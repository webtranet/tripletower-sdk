<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\CryptoError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\DaemonHandler;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Helper\GlobalInstance;

class hanoi_tripletowersdk extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/tripletower-sdk.git",
		"destination" => "C:/tripletower-stack/tripletower-sdk",
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT,
		"privateKeyPath" => "",
		"privateKeyPass" => "",
		"certificatePath" => "",
		"caCertificatePath" => "",
		"git" => ""
	];

	protected $configLinuxDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/tripletower-sdk.git",
		"destination" => "/srv/tripletower-sdk",
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT,
		"privateKeyPass" => ""
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		// Apache is only a dependency if it's available and running,
		// because it has to be stopped first so git can update apache's dlls
		if( class_exists("hanoi_apache", false) && DaemonHandler::isDaemonRunning(hanoi_apache::APACHE_SERVICE_NAME)->isSuccess() )
			return ["hanoi_apache"];
		else
			return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing to check
			}
			break;

			default:
			{
				// Nothing to check
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ["hanoi_git"];
	}

	public function install()
	{
		$git = "";

		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Find git executable
				$git = $this->config["git"] !== "" ? $this->config["git"] : Helper::findExecutableInPathVariable("git.exe");
				if( !is_file($git) )
					throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not find file 'git.exe'");

			}
			break;

			default:
			{
				$git = "git";
			}
		}

		// Clone/pull stuff from git repository
		if( !is_dir($this->config["destination"] . '/.git') )
		{
			Helper::exec('"' . $git . '" clone "' . $this->config["packageSourcePath"] . '" "' . $this->config["destination"] . '"');
		}
		else
		{
			Helper::exec('"' . $git . '" --git-dir="' . $this->config["destination"] . '/.git" --work-tree="' . $this->config["destination"] . '" reset --hard @{upstream}');
			Helper::exec('"' . $git . '" --git-dir="' . $this->config["destination"] . '/.git" --work-tree="' . $this->config["destination"] . '" clean -d --force');
			Helper::exec('"' . $git . '" --git-dir="' . $this->config["destination"] . '/.git" --work-tree="' . $this->config["destination"] . '" pull --all');
		}


		if( $this->config["environment"] === GlobalInstance::ENVIRONMENT_PRODUCTION || $this->config["certificatePath"] !== "" )
		{
			// Find tripletowersdk folder
			$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["destination"] );

			// Copy ssl private key and decrypt it if necessary
			$privateKeyFileDestination = Helper::downloadFile($this->config["privateKeyPath"], "$tripletowersdkFolder/etc/ssl/private/", false, $this->caFileName, $this->proxy);
			if( TripleTowerError::isError($privateKeyFileDestination) )
				throw $privateKeyFileDestination;

			$privateKeyHandle = openssl_pkey_get_private( file_get_contents($privateKeyFileDestination) );
			if( $privateKeyHandle === false )
			{
				$privateKeyHandle = openssl_pkey_get_private( file_get_contents($privateKeyFileDestination), $this->config["privateKeyPass"] );
				if( $privateKeyHandle === false )
					throw new CryptoError(CryptoError::OPENSSL_DECRYPTION_FAILED, CryptoError::ERR, "Could not decrypt '$privateKeyFileDestination'");

				if( openssl_pkey_export_to_file( $privateKeyHandle, $privateKeyFileDestination, null, ["config" => "$tripletowersdkFolder/etc/ssl/openssl.cnf"] ) === false )
					throw new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Exporting private key to file caused an error");
			}

			openssl_free_key($privateKeyHandle);

			// Copy ssl certificate
			$certificateFileDestination = Helper::downloadFile($this->config["certificatePath"], "$tripletowersdkFolder/etc/ssl/certs/", false, $this->caFileName, $this->proxy);
			if( TripleTowerError::isError($certificateFileDestination) )
				throw $certificateFileDestination;

			// Copy ssl ca-certificate
			$caCertificateFileDestination = Helper::downloadFile($this->config["caCertificatePath"], "$tripletowersdkFolder/etc/ssl/certs/", false, $this->caFileName, $this->proxy);
			if( TripleTowerError::isError($caCertificateFileDestination) )
				throw $caCertificateFileDestination;
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}