<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\SysError;
use TripleTowerSDK\Helper\GlobalInstance;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Helper\DaemonHandler;

class hanoi_php extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://windows.php.net/downloads/releases/latest/php-8.1-Win32-vs16-x64-latest.zip",
		"destination" => "C:/tripletower-stack/php",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT,

		"sendmail" => ""
	];

	protected $configLinuxDefaults =
	[
		"tripletowersdk" => "/srv/tripletowersdk",
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				// Download files
				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
				{
					$downloadError->addMessage("Could not copy file '" . $this->config["packageSourcePath"] . "' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");
					throw $downloadError;
				}

				// Unzip new php
				$zip = new \ZipArchive();
				$archiveFile = $tempDir.basename($this->config["packageSourcePath"]);
				if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
				{
					echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
					if( ($zipError = $zip->open($archiveFile)) !== true )
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
				}
				if( $zip->extractTo($tempDir.basename($this->config["packageSourcePath"], ".zip")) !== true )
					throw new \Exception("Could not extract content of zip archive $tempDir" . basename($this->config["packageSourcePath"]) . " to $tempDir" . basename($this->config["packageSourcePath"], ".zip"));
				if( $zip->close() !== true )
					throw new \Exception("Could not close zip archive $tempDir" . basename($this->config["packageSourcePath"]));
			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
				return ['hanoi_vcredist2017', 'hanoi_tripletowersdk', 'hanoi_sendmail'];

			default:
				return ['hanoi_tripletowersdk', 'hanoi_sendmail'];
		}
	}

	public function install()
	{
		// Find tripletowersdk folder
		$tripletowersdkFolder = $this->config["tripletowersdk"];
		if( !is_dir($tripletowersdkFolder) )
			throw new \Exception("Could not find folder '$tripletowersdkFolder'");

		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Find sendmail folder
				$sendmailFolder = Helper::normalizeFilePath( $this->config["sendmail"] !== "" ? $this->config["sendmail"] : dirname(Helper::findExecutableInPathVariable("sendmail.exe")) );
				if( !is_dir($sendmailFolder) )
					throw new \Exception("Could not find folder 'sendmail'");

				// Remove old /php folder from path variable
				$oldPhpExePath = Helper::findExecutableInPathVariable("php.exe");
				if( !TripleTowerError::isError($oldPhpExePath) )
					Helper::removePathFromPathVariable(dirname($oldPhpExePath))->throwIfNotSuccess("Could not remove old php path '$oldPhpExePath' from Path variable");
				else
					$oldPhpExePath->throwIfNotSuccessOrSameAs(new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT));

				// Stop "Distributed Transaction Coordinator" service because it locks the oci.dll
				$msdtcDaemon = "MSDTC";
				$msdtcDaemonWasRunning = false;
				if( DaemonHandler::isDaemonInstalled($msdtcDaemon)->isSuccess() && DaemonHandler::isDaemonRunning($msdtcDaemon)->isSuccess() )
				{
					$msdtcDaemonWasRunning = true;
					DaemonHandler::stopDaemon("MSDTC")->throwIfNotSuccess();
				}

				// Delete old php folder
				$destination = Helper::normalizeFilePath($this->config["destination"]);
				if( is_dir($destination) )
					Helper::unlink_recursive($destination)->throwIfNotSuccess();

				// Start "Distributed Transaction Coordinator" service if it was running
				if( $msdtcDaemonWasRunning )
					DaemonHandler::startDaemon($msdtcDaemon)->throwIfNotSuccess();

				// Copy php folder to destination
				$sourceFolder = $this->getTempPluginFolder() . basename($this->config["packageSourcePath"], ".zip");
				if( !Helper::copy_recursive($sourceFolder, $destination) )
					throw new \Exception("Could not copy '$sourceFolder' to '$destination'");

				// Find out if it's about a development machine or a productive server
				$environmentString = ($this->config["environment"] === GlobalInstance::ENVIRONMENT_DEVELOPMENT) ? "dev" : "prod";

				// Create php config file symlink
				$phpConfigFile = "$destination/php.ini";
				Helper::createSymlink("$tripletowersdkFolder/etc/php/php_$environmentString.ini", $phpConfigFile, true)->throwIfNotSuccess();

				// Parse php-configs, search them for tokens and replace them
				Helper::replaceInFile('$tripletowerSdk', $tripletowersdkFolder, $phpConfigFile)->throwIfNotSuccess();
				Helper::replaceInFile('$phpRoot', $destination, "$tripletowersdkFolder/etc/apache2/conf_windows/mods-available/php.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$phpRoot', $destination, "$tripletowersdkFolder/etc/apache2/conf_windows/mods-available/php.load")->throwIfNotSuccess();
				Helper::replaceInFile('$sendmail', "$sendmailFolder/sendmail.exe -t -i", $phpConfigFile)->throwIfNotSuccess();

				// Add php extension variable
				$phpModsFolder = realpath("$tripletowersdkFolder/etc/php/mods-enabled");
				if( $phpModsFolder === false )
					throw new \Exception("Php extension folder '$tripletowersdkFolder/etc/php/mods-enabled' could not be found so it could not be added to path variable");

				$phpModsFolder = str_replace("/", "\\", $phpModsFolder); // insert path windows style
				Helper::addEnvironmentVariable("PHP_INI_SCAN_DIR", $phpModsFolder)->throwIfNotSuccess();

				// Add php to path variable
				Helper::addPathToPathVariable($destination)->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Install via package manager
				Helper::genericLinuxInstall("php")->throwIfNotSuccess();

				// Find out if it's about a development machine or a productive server
				$environmentString = ($this->config["environment"] === GlobalInstance::ENVIRONMENT_DEVELOPMENT) ? "dev" : "prod";

				// Create php symlink for apache intergration
				$phpConfigFile = '/etc/php/8.1/apache2/php.ini';
				Helper::createSymlink("$tripletowersdkFolder/etc/php/php_$environmentString.ini", $phpConfigFile, true)->throwIfNotSuccess();

				// Create php symlink for cli-applications
				$phpConfigFile = '/etc/php/8.1/cli/php.ini';
				Helper::createSymlink("$tripletowersdkFolder/etc/php/php_$environmentString.ini", $phpConfigFile, true)->throwIfNotSuccess();
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{

			}
			break;

			default:
			{

			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}