<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name netbeans Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;

class hanoi_netbeans extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://archive.apache.org/dist/netbeans/netbeans/22/netbeans-22-bin.zip",
		"destination" => "C:/tripletower-stack/netbeans",
		"javaHome" => "C:/tripletower-stack/java"
	];

	protected $configLinuxDefaults =
	[

	];

	protected $linuxPackage = "netbeans";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create contructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = $tempDir . basename($packageRemoteSourcePath);

				// Download file
				$downloadError = Helper::downloadFile( $packageRemoteSourcePath, $packageLocalSourcePath, true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '$packageRemoteSourcePath' to '$packageLocalSourcePath'");

				// Unzip new netbeans
				$zip = new \ZipArchive();
				$archiveFile = $tempDir.basename($this->config["packageSourcePath"]);
				if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
				{
					echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
					if( ($zipError = $zip->open($archiveFile)) !== true )
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
				}
				if( $zip->extractTo($tempDir.basename($this->config["packageSourcePath"], ".zip")) !== true )
					throw new \Exception("Could not extract content of zip archive $tempDir" . basename($this->config["packageSourcePath"]) . " to $tempDir" . basename($this->config["packageSourcePath"], ".zip"));
				if( $zip->close() !== true )
					throw new \Exception("Could not close zip archive $tempDir" . basename($this->config["packageSourcePath"]));
			}
			break;

			default:
			{
				// Updating package list if a linuxpackage is available
				if($this->linuxPackage !== "")
				{
					$refreshError = Helper::refreshPackageList();
					if( !$refreshError->isSuccess() )
						throw $refreshError;
				}
				else
				{
					throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
				}
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ["hanoi_openjdk17"];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

				// Check for javaHome folder
				if( !is_dir($this->config["javaHome"]) )
					throw new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not find javaHome '{$this->config["javaHome"]}' for netbeans");

				// Delete old netbeans folder
				$destination = Helper::normalizeFilePath($this->config["destination"]);
				if( is_dir($destination) )
					Helper::unlink_recursive($destination)->throwIfNotSuccess();

				// Copy netbeans folder to destination
				$sourceFolder = $this->getTempPluginFolder() . basename($this->config["packageSourcePath"], ".zip") . "/netbeans";
				if( !Helper::createPathIfNotExists($destination) || !Helper::copy_recursive($sourceFolder, $destination) )
					throw new \Exception("Could not copy '$sourceFolder' to '$destination'");

				$netbeansExePath    = "$destination/bin/netbeans.exe";
				$netbeansConfigPath = "$destination/etc/netbeans.conf";

				$jdkHome = Helper::getEnvironmentVariable("JAVA_HOME");
				if( TripleTowerError::isError($jdkHome) )
					throw $jdkHome;

				$jdkHomeConfigEntryPattern = "/^[ \\t]*#{0,1}[ \\t]*netbeans_jdkhome[ \\t]*=[ \\t]*\"\\/path\\/to\\/jdk\"[ \\t\\r]*$/m";
				$jdkHomeConfigReplacement  = "netbeans_jdkhome=\"$jdkHome\"";
				Helper::regexReplaceInFile($jdkHomeConfigEntryPattern, $jdkHomeConfigReplacement, $netbeansConfigPath)->throwIfNotSuccess();

				// Create desktop shortcut
				$shortcutName = "Apache-NetBeans.lnk";
				Helper::createWindowsShortcut( $netbeansExePath, getenv("PUBLIC") . "/Desktop/$shortcutName" );

				// Create start menu shortcut
				$startMenueFolderPath = getenv("PROGRAMDATA") . "/Microsoft/Windows/Start Menu/Programs/Apache NetBeans/";
				Helper::createPathIfNotExists( $startMenueFolderPath, null, null, function(){return true;} )->throwIfNotSuccess();
				Helper::createWindowsShortcut( $netbeansExePath, "$startMenueFolderPath$shortcutName")->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Install via package manager if linuxpackage available
				Helper::genericLinuxInstall($this->linuxPackage)->throwIfNotSuccess();
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}