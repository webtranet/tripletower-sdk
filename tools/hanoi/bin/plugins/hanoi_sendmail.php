<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;

class hanoi_sendmail extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://www.glob.com.au/sendmail/sendmail.zip",
		"destination" => "C:/tripletower-stack/sendmail",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",

		"smtpServer" => "localhost",
		"smtpPort" => "25",
		"smtpUser" => "",
		"smtpPassword" => ""
	];

	protected $configLinuxDefaults =
	[

	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
				{
					$downloadError->addMessage("Could not copy file '" . $this->config["packageSourcePath"] . "' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");
					throw $downloadError;
				}

				// Unzip new sendmail
				$zip = new \ZipArchive();
				$archiveFile = $tempDir.basename($this->config["packageSourcePath"]);
				if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
				{
					echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
					if( ($zipError = $zip->open($archiveFile)) !== true )
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
				}
				if( $zip->extractTo($tempDir.basename($this->config["packageSourcePath"], ".zip")) !== true )
					throw new \Exception("Could not extract content of zip archive $tempDir" . basename($this->config["packageSourcePath"]) . " to $tempDir" . basename($this->config["packageSourcePath"], ".zip"));
				if( $zip->close() !== true )
					throw new \Exception("Could not close zip archive $tempDir" . basename($this->config["packageSourcePath"]));
			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_tripletowersdk'];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Find tripletowersdk folder
				$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["tripletowersdk"] );
				if( !is_dir($tripletowersdkFolder) )
					throw new \Exception("Could not find folder '$tripletowersdkFolder'");

				// Remove old /sendmail folder from path variable
				$oldSendmailExePath = Helper::findExecutableInPathVariable("sendmail.exe");
				if( !TripleTowerError::isError($oldSendmailExePath) )
					Helper::removePathFromPathVariable( dirname($oldSendmailExePath) )->throwIfNotSuccess("Could not remove old sendmail path '$oldSendmailExePath' from Path variable");
				else
					$oldSendmailExePath->throwIfNotSuccessOrSameAs(new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT));

				// Delete old sendmail folder
				if( is_dir($this->config["destination"]) )
					Helper::unlink_recursive($this->config["destination"])->throwIfNotSuccess();

				// Copy sendmail folder to destination
				$sourceFolder = $this->getTempPluginFolder() . basename($this->config["packageSourcePath"], ".zip");
				if( !Helper::copy_recursive($sourceFolder, $this->config["destination"]) )
					throw new \Exception("Could not copy '$sourceFolder' to '" . $this->config["destination"] . "'");

				// Create sendmail symlink
				$sendmailConfigFile = $this->config["destination"] . "/sendmail.ini";
				$createLinkError = Helper::createSymlink("$tripletowersdkFolder/etc/sendmail/sendmail_windows.ini", $sendmailConfigFile, true);
				if( !$createLinkError->isSuccess() )
					throw $createLinkError;

				Helper::replaceInFile('$smtpServer',   $this->config["smtpServer"],   $sendmailConfigFile)->throwIfNotSuccess();
				Helper::replaceInFile('$smtpPort',     $this->config["smtpPort"],     $sendmailConfigFile)->throwIfNotSuccess();
				Helper::replaceInFile('$smtpUser',     $this->config["smtpUser"],     $sendmailConfigFile)->throwIfNotSuccess();
				Helper::replaceInFile('$smtpPassword', $this->config["smtpPassword"], $sendmailConfigFile)->throwIfNotSuccess();

				// Add sendmail to path variable
				Helper::addPathToPathVariable($this->config["destination"])->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Install via package manager
				$installError = Helper::genericLinuxInstall("sendmail");
				if( !$installError->isSuccess() )
					throw $installError;
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{

			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}