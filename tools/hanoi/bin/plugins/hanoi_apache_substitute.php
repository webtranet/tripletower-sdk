<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_apache_substitute extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[

	];

	protected $configLinuxDefaults =
	[

	];

	protected $apacheExtensionName = "substitute";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_apache'];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Get mod-enabled folder
				$modsEnabledFolder = dirname(dirname(Helper::findExecutableInPathVariable("httpd.exe")))."/conf/mods-enabled";
				if( !is_dir($modsEnabledFolder) )
					throw new \Exception("Could not find folder '$modsEnabledFolder'(apache2/mods-enabled)");

				// Get mod-available folder
				$modsAvailableFolder = dirname($modsEnabledFolder) . "/mods-available";
				if( !is_dir($modsAvailableFolder) )
					throw new \Exception("Could not find folder '$modsAvailableFolder'");

				// Find load file of extension
				$extensionLoadPath = "$modsAvailableFolder/$this->apacheExtensionName.load";
				if( is_file($extensionLoadPath) === false )
					throw new \Exception("Could not find file '$extensionLoadPath'");

				// Create a symlink from mods-available to mods-enabled for load file
				$extensionLoadLinkPath = "$modsEnabledFolder/$this->apacheExtensionName.load";
				clearstatcache(true, $extensionLoadLinkPath);
				$createLoadLinkError = Helper::createSymlink($extensionLoadPath, $extensionLoadLinkPath, true);
				if( !$createLoadLinkError->isSuccess() )
					throw $createLoadLinkError;

				// Create a symlink from mods-available to mods-enabled for conf file
				$extensionConfPath = "$modsAvailableFolder/$this->apacheExtensionName.conf";
				$extensionConfLinkPath = "$modsEnabledFolder/$this->apacheExtensionName.conf";
				if( is_file($extensionConfPath) )
				{
					clearstatcache(true, $extensionConfLinkPath);
					$createConfLinkError = Helper::createSymlink($extensionConfPath, $extensionConfLinkPath, true);
					if( !$createConfLinkError->isSuccess() )
						throw $createConfLinkError;
				}
			}
			break;

			default:
			{
				// Install via package manager
				Helper::exec("a2enmod " . $this->apacheExtensionName);
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}