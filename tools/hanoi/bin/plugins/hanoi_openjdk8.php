<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name openjdk8 Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_openjdk8 extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u422-b05/OpenJDK8U-jdk_x64_windows_hotspot_8u422b05.msi",
		"destination" => "C:/tripletower-stack/java",
		"parameters" => "ADDLOCAL=FeatureMain,FeatureEnvironment,FeatureJarFileRunWith,FeatureJavaHome /qn"
	];

	protected $configLinuxDefaults =
	[

	];

	protected $linuxPackage = "openjdk-8-jdk";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create contructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = $tempDir . basename($packageRemoteSourcePath);

				// Download file
				$downloadError = Helper::downloadFile( $packageRemoteSourcePath, $packageLocalSourcePath, true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '$packageRemoteSourcePath' to '$packageLocalSourcePath'");
			}
			break;

			default:
			{
				// Updating package list if a linuxpackage is available
				if($this->linuxPackage !== "")
				{
					$refreshError = Helper::refreshPackageList();
					if( !$refreshError->isSuccess() )
						throw $refreshError;
				}
				else
				{
					throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
				}
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

				$execCommand = 'start /wait "" ';

				if( strtolower( pathinfo($packageLocalSourcePath, PATHINFO_EXTENSION) ) === "msi" )
				{
					$execCommand .= "msiexec /i ";
					$packageLocalSourcePath = str_replace("/", "\\", $packageLocalSourcePath);
				}

				$installDir = "";
				if( !empty($this->conf["destination"]) )
					$installDir = ' INSTALLDIR="' . str_replace('/', '\\', $this->conf["destination"]) . '"';

				$execCommand .= trim('"' . $packageLocalSourcePath . '" ' . $this->config["parameters"] . $installDir);

				// Install file
				Helper::exec($execCommand);
			}
			break;

			default:
			{
				// Install via package manager if linuxpackage available
				$installError = Helper::genericLinuxInstall($this->linuxPackage);
				if( !$installError->isSuccess() )
					throw $installError;
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}