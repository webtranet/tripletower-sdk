<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\DataBase\WappsTowerDBC;
use TripleTowerSDK\Error\SysError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Tower;
use TripleTowerSDK\Helper\GlobalInstance;
use TripleTowerSDK\Serializer\Serializer;

class hanoi_tripletower extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	const COMMENT_TAG_FOR_HOSTFILE	= "added by hanoi deployment -> DO NOT REMOVE THIS LINE!";


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/tripletower.git",
		"destination" => "C:/tripletower-stack/tripletower",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",
		"ipbased" => false,
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT,
		"serverName" => "tripletower.localhost",
		"towerTypes" => [ Tower::SERVICE_TOWER, Tower::WAPPS_TOWER ],

		// global.json
		"verbosity" => TripleTowerError::NOTICE,
		"mailVerbosity" => TripleTowerError::CRIT,
		"mailReceiver" => [],
		"privateKeyPath" => null,
		"certificatePath" => null,
		"profilingEnabled" => false,
		"optimizationEnabled" => false,

		"singleSignOn" => false,
		"authSystems" =>
		[
			"tripleTowerAS" =>
			[
				"ascName" => "TripleTowerSDK\\AuthSystem\\TripleTowerASC",
				"hostName" => "",
				"userName" => "",
				"password" => ""
			]
		],

		// servicetower.json
		"servicetowerPrimaryTower" => "",
		"servicetowerDefaultContentProvider" => "Bootloader",
		"servicetowerDisplayName" => "Your ServiceTower",
		"servicetowerDbInstallerUserName" => "root",
		"servicetowerDbInstallerPassword" => "tripletower",
		"servicetowerDbHostName" => "tripletower.localhost",
		"servicetowerDbName" => "servicetower",
		"servicetowerDbUserName" => "servicetower",
		"servicetowerDbPassword" => "tripletower",

		// wappstower.json
		"wappstowerDefaultContentProvider" => "Webtra.TowerCore",
		"wappstowerDisplayName" => "Your WappsTower",
		"wappstowerDbInstallerUserName" => "root",
		"wappstowerDbInstallerPassword" => "tripletower",
		"wappstowerDbHostName" => "tripletower.localhost",
		"wappstowerDbName" => "wappstower",
		"wappstowerDbUserName" => "wappstower",
		"wappstowerDbPassword" => "tripletower",

		"git" => "",
		"apache" => "",
		"httpPort" => "80",
		"httpsPort" => "443"
	];

	protected $configLinuxDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/tripletower.git",
		"destination" => "/srv/tripletower",
		"tripletowersdk" => "/srv/tripletower-sdk",
		"ipbased" => false,
		"environment" => GlobalInstance::ENVIRONMENT_DEVELOPMENT,
		"serverName" => "tripletower.localhost",
		"towerTypes" => [ Tower::SERVICE_TOWER, Tower::WAPPS_TOWER ],
		"verbosity" => TripleTowerError::NOTICE,
		"mailVerbosity" => TripleTowerError::CRIT,
		"mailReceiver" => [],
		"privateKeyPath" => null,
		"certificatePath" => null,
		"profilingEnabled" => false,
		"optimizationEnabled" => false,

		"singleSignOn" => false,
		"authSystems" =>
		[
			"tripleTowerAS" =>
			[
				"ascName" => "TripleTowerSDK\\AuthSystem\\TripleTowerASC",
				"hostName" => "",
				"userName" => "",
				"password" => ""
			]
		],

		// servicetower.json
		"servicetowerPrimaryTower" => "",
		"servicetowerDefaultContentProvider" => "Bootloader",
		"servicetowerDisplayName" => "Your ServiceTower",
		"servicetowerDbInstallerUserName" => "root",
		"servicetowerDbInstallerPassword" => "tripletower",
		"servicetowerDbHostName" => "tripletower.localhost",
		"servicetowerDbName" => "servicetower",
		"servicetowerDbUserName" => "servicetower",
		"servicetowerDbPassword" => "tripletower",

		// wappstower.json
		"wappstowerDefaultContentProvider" => "Webtra.TowerCore",
		"wappstowerDisplayName" => "Your WappsTower",
		"wappstowerDbInstallerUserName" => "root",
		"wappstowerDbInstallerPassword" => "tripletower",
		"wappstowerDbHostName" => "tripletower.localhost",
		"wappstowerDbName" => "wappstower",
		"wappstowerDbUserName" => "wappstower",
		"wappstowerDbPassword" => "tripletower",

		"git" => "",
		"apache" => "",
		"httpPort" => "80",
		"httpsPort" => "443"
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		if( count($this->config["towerTypes"]) < 1 )
			throw new \Exception("There has to be at least one towertype specified within array.");

		// Test if environment is correctly written in config
		$environments = [GlobalInstance::ENVIRONMENT_DEVELOPMENT, GlobalInstance::ENVIRONMENT_STAGING, GlobalInstance::ENVIRONMENT_PRODUCTION];
		if( !in_array( $this->config["environment"] , $environments) )
			throw new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Environment in config '" . $this->config["environment"] . "' has to be either '" . implode( "', '", $environments ) . "'");

		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{

			}
			break;

			default:
			{

			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_git', 'hanoi_tripletowersdk', 'hanoi_apache', 'hanoi_php', 'hanoi_php_pdo_mysql', 'hanoi_sendmail'];
	}

	public function install()
	{
		// Find tripletowersdk folder
		$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["tripletowersdk"] );
		if( !is_dir($tripletowersdkFolder) )
			throw new \Exception("Could not find folder '$tripletowersdkFolder'");

		// Find environment variables
		$tripleTower = Helper::normalizeFilePath( $this->config["destination"] );
		$packageSourcePath = $this->config["packageSourcePath"];
		$git = "";
		$systemHostsFile = "";

		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Check if necessary folders/files exist
				$git = Helper::normalizeFilePath( $this->config["git"] !== "" ? $this->config["git"] : Helper::findExecutableInPathVariable("git.exe") );
				if( !is_file($git) )
					throw new \Exception("Could not find file 'git.exe'");

				$winDir = getenv("windir");
				if( !$winDir )
					throw new \Exception("Could not receive environment variable 'windir'");
				$systemHostsFile = "$winDir/System32/drivers/etc/hosts";
			}
			break;

			default:
			{
				$git = "git";
				$systemHostsFile = "/etc/hosts";
			}

		}

		// Create hosts file symlink if on dev-machine
		if( $this->config["environment"] === GlobalInstance::ENVIRONMENT_DEVELOPMENT )
			$this->updateHostsfile($systemHostsFile);

		// Clone/pull stuff from repository
		$destination = $this->config["destination"];
		if( !is_dir($destination . '/.git') )
		{
			Helper::exec('"' . $git . '" clone "' . $packageSourcePath . '" "' . $destination . '"');
		}
		else
		{
			Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" reset --hard @{upstream}');
			Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" clean -d --force');
			Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" pull --all');
		}

		// Create alias file for towers and replace according tower config files
		foreach($this->config["towerTypes"] as $towerType)
		{
			$aliasContent  = "# TripleTower generated alias file".PHP_EOL;
			$aliasContent .= "#".PHP_EOL;
			$aliasContent .= "# Required modules: mod_alias mod_authz_core mod_dir".PHP_EOL;
			$aliasContent .= "# Optional module: mod_authn_core if servicetower with SingleSignOn is used".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "# ALIAS-FILES WILL BE AUTOGENERATED BY HANOI DEPLOYMENT".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "AliasMatch ^/$towerType/((?!\\.\\..*).*)/public/((?!\\.\\..*|dumpling\\.jgz|dumpling\\.cgz).*) $tripleTower/$towerType/\$1/public/\$2".PHP_EOL;
			$aliasContent .= "<DirectoryMatch \"$tripleTower/$towerType/.*/public\">".PHP_EOL;
			$aliasContent .= "	AllowOverride None".PHP_EOL;
			$aliasContent .= "	Require all granted".PHP_EOL;
			$aliasContent .= "	DirectoryIndex disabled".PHP_EOL;
			$aliasContent .= "</DirectoryMatch>".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "Alias /$towerType \"$tripleTower/public\"".PHP_EOL;
			$aliasContent .= "<Directory \"$tripleTower/public\">".PHP_EOL;
			$aliasContent .= "	Options None".PHP_EOL;
			$aliasContent .= "	AllowOverride None".PHP_EOL;
			$aliasContent .= "	Require all denied".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# All requests are routed to according FallbackResource file except for static".PHP_EOL;
			$aliasContent .= "	# files which can be served directly.".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	DirectoryIndex disabled".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	<If \"%{REQUEST_URI} =~ m#^/$towerType#\">".PHP_EOL;
			$aliasContent .= "		DirectoryIndex {$towerType}Index.php".PHP_EOL;
			$aliasContent .= "		FallbackResource /$towerType/{$towerType}Index.php".PHP_EOL;
			$aliasContent .= "	</If>".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "</Directory>".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "<Location /$towerType>".PHP_EOL;
			if( $towerType === Tower::SERVICE_TOWER && $this->config["singleSignOn"] === true )
			{
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Authorization realm for use in HTTP authentication, this realm is given".PHP_EOL;
				$aliasContent .= "  # to the client so that the user knows which username and password to send".PHP_EOL;
				$aliasContent .= "  #AuthName \"SSPI Protected Place\"".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Type of user authentication used for this directory".PHP_EOL;
				$aliasContent .= "  AuthType SSPI".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'On' to activate SSPI authentication here".PHP_EOL;
				$aliasContent .= "  SSPIAuth On".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'Off' to allow access control to be passed along to lower modules".PHP_EOL;
				$aliasContent .= "  # if the UserID is not known to this module".PHP_EOL;
				$aliasContent .= "  SSPIOfferSSPI On".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'Off' to allow access control to be passed along to lower modules".PHP_EOL;
				$aliasContent .= "  # if the UserID is not known to this module".PHP_EOL;
				$aliasContent .= "  SSPIAuthoritative On".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'On' to allow the client to authenticate against NT with 'Basic'".PHP_EOL;
				$aliasContent .= "  # authentication instead of using the NTLM protocol".PHP_EOL;
				$aliasContent .= "  #SSPIOfferBasic".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to the name of the package you want to use to authenticate users".PHP_EOL;
				$aliasContent .= "  #SSPIPackage".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to the domain you want users authenticated against for cleartext".PHP_EOL;
				$aliasContent .= "  # authentication - if not specified, all trusted domains are checked".PHP_EOL;
				$aliasContent .= "  #SSPIDomain".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'On' if you want the usernames to have the domain prefix OMITTED,".PHP_EOL;
				$aliasContent .= "  # On = user, Off = DOMAIN\\user. If you have a multi domain environment".PHP_EOL;
				$aliasContent .= "  # set this option to Off to make servicetower single sign on work".PHP_EOL;
				$aliasContent .= "  SSPIOmitDomain Off".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'lower' if you want the username and domain to be lowercase, set".PHP_EOL;
				$aliasContent .= "  # to 'upper' if you want the username and domain to be uppercase, if not".PHP_EOL;
				$aliasContent .= "  # specified, username and domain case conversion is disabled".PHP_EOL;
				$aliasContent .= "  #SSPIUsernameCase lower".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'On' if you want basic authentication to be the higher priority,".PHP_EOL;
				$aliasContent .= "  # means it forces the login prompt".PHP_EOL;
				$aliasContent .= "  #SSPIBasicPreferred".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Set to 'On' if you want an alternative authorization method (like a".PHP_EOL;
				$aliasContent .= "  # SVNPathAuthz file for example) to work at the same level in an".PHP_EOL;
				$aliasContent .= "  # \"authorization chain\": if the SSPI authorization succeeds, the".PHP_EOL;
				$aliasContent .= "  # alternative method is checked, if not a 401 code is returned.".PHP_EOL;
				$aliasContent .= "  #SSPIChainAuth".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
				$aliasContent .= "  # Any valid user will do, useful for logging who accessed each file, for".PHP_EOL;
				$aliasContent .= "  # example".PHP_EOL;
				$aliasContent .= "  Require valid-sspi-user".PHP_EOL;
				$aliasContent .= "".PHP_EOL;
			}
			else
			{
				$aliasContent .= "  Require all granted".PHP_EOL;
			}
			$aliasContent .= "</Location>".PHP_EOL;
			$aliasContent .= "".PHP_EOL;

			$tripleTowerApacheConfPath = OsNames::isWindows() ? "$tripletowersdkFolder/etc/apache2/conf_windows" : "$tripletowersdkFolder/etc/apache2/conf_linux";
			$realApacheConfPath = OsNames::isWindows() ? "$tripletowersdkFolder/etc/apache2/conf_windows" : "/etc/apache2";
			if( !file_put_contents("$tripleTowerApacheConfPath/sites-available/$towerType.conf", $aliasContent) )
				throw new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Could not write alias file '$towerType'");
			Helper::createSymlink("$tripleTowerApacheConfPath/sites-available/$towerType.conf", "$realApacheConfPath/sites-enabled/$towerType.conf", true)->throwIfNotSuccess();

			switch( $towerType )
			{
				case Tower::SERVICE_TOWER:
				{
					// servicetower.json
					$servicetowerPrimaryTower = (!empty($this->config["servicetowerPrimaryTower"]) ? $this->config["servicetowerPrimaryTower"] : $this->config["serverName"]);

					Helper::replaceInJsonFile('$servicetowerPrimaryTower',           $servicetowerPrimaryTower,                           "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$servicetowerDefaultContentProvider', $this->config["servicetowerDefaultContentProvider"], "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$servicetowerDisplayName',            $this->config["servicetowerDisplayName"],            "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();

					Helper::replaceInJsonFile('$servicetowerDbHostName', $this->config["servicetowerDbHostName"], "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$servicetowerDbName',     $this->config["servicetowerDbName"],     "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$servicetowerDbUserName', $this->config["servicetowerDbUserName"], "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$servicetowerDbPassword', $this->config["servicetowerDbPassword"], "$tripleTower/conf/servicetower.json")->throwIfNotSuccess();
				}
				break;

				case Tower::WAPPS_TOWER:
				{
					// wappstower.json
					Helper::replaceInJsonFile('$wappstowerDefaultContentProvider', $this->config["wappstowerDefaultContentProvider"], "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$wappstowerDisplayName',            $this->config["wappstowerDisplayName"],            "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();

					Helper::replaceInJsonFile('$wappstowerDbHostName', $this->config["wappstowerDbHostName"], "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$wappstowerDbName',     $this->config["wappstowerDbName"],     "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$wappstowerDbUserName', $this->config["wappstowerDbUserName"], "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();
					Helper::replaceInJsonFile('$wappstowerDbPassword', $this->config["wappstowerDbPassword"], "$tripleTower/conf/wappstower.json")->throwIfNotSuccess();
				}
				break;

				default:
				{
					throw new \Exception("Towertype '$towerType' is unknown");
				}
			}
		}

		// Prepare mail receivers
		$mailReceiverArray = [];
		foreach(is_array($this->config["mailReceiver"]) ? $this->config["mailReceiver"] : [$this->config["mailReceiver"]] as $mailReceiver)
		{
			if( $mailReceiver !== null && trim($mailReceiver !== "") )
			{
				$mailReceiver = filter_var( trim($mailReceiver), FILTER_SANITIZE_EMAIL);
				if( filter_var($mailReceiver, FILTER_VALIDATE_EMAIL) !== false )
					$mailReceiverArray[] = $mailReceiver;
				else
					throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::CRIT, "Receiver mail adress '$mailReceiver' is no valid email adress");
			}
		}

		// Replace variables in global.json
		Helper::replaceInJsonFile('$environment', $this->config["environment"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$verbosity', $this->config["verbosity"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$mailVerbosity', $this->config["mailVerbosity"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$mailReceiver', $this->config["mailReceiver"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$privateKeyPath',  $this->config["privateKeyPath"],  "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$certificatePath', $this->config["certificatePath"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$profilingEnabled', $this->config["profilingEnabled"], "$tripleTower/conf/global.json")->throwIfNotSuccess();
		Helper::replaceInJsonFile('$optimizationEnabled', $this->config["optimizationEnabled"], "$tripleTower/conf/global.json")->throwIfNotSuccess();

		$authSystems = [];
		foreach($this->config["authSystems"] as $authSystemName => $authSystemObject)
			$authSystems[] = "\"$authSystemName\":" . PHP_EOL . Serializer::jsonSerialize($authSystemObject, false, true, 0, false);
		Helper::replaceInFile('$authSystems', implode(",", $authSystems), "$tripleTower/conf/global.json")->throwIfNotSuccess();
	}

	public function getEnableDependencies() : array
	{
		$mandatoryDependencies = ["hanoi_mariadb", "hanoi_apache_authz_core", "hanoi_apache_dir", "hanoi_apache_alias"];
		$serviceTowerOnlyDependencies = ["hanoi_apache_authn_core"];

		// Only if servicetower will be installed
		if( in_array(Tower::SERVICE_TOWER, $this->config["towerTypes"]) )
			return array_merge($mandatoryDependencies, $serviceTowerOnlyDependencies);
		else
			return $mandatoryDependencies;
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			case OsNames::OS_TYPE_LINUX:
			{
				// Install tripletower's databases for each tower on mariadb if not already installed
				foreach($this->config["towerTypes"] as $towerType)
				{
					switch( $towerType )
					{
						case Tower::SERVICE_TOWER:
						case Tower::WAPPS_TOWER:
						{
							$dbInstallerUserName = $this->config["{$towerType}DbInstallerUserName"];
							$dbInstallerPassword = $this->config["{$towerType}DbInstallerPassword"];

							$dbHostName			= $this->config["{$towerType}DbHostName"];
							$dbName				= $this->config["{$towerType}DbName"];
							$dbUserName			= $this->config["{$towerType}DbUserName"];
							$dbPassword			= $this->config["{$towerType}DbPassword"];
							$installScriptsFolder = Helper::normalizeFilePath($this->config["destination"] . "/install");

							$this->executeDBScripts( $installScriptsFolder, $dbInstallerUserName, $dbInstallerPassword, $dbHostName, $dbName, $dbUserName, $dbPassword );

							// Insert wapps into wapps database.
							if( $towerType === Tower::WAPPS_TOWER )
							{
								$wappstowerExecPath = Helper::normalizeFilePath( $this->config["destination"] ) . "/" . Tower::WAPPS_TOWER . "/";
								$this->insertWappsIntoWappsTowerDb($wappstowerExecPath, 'mysql', $dbHostName, $dbName, $dbUserName, $dbPassword);
							}
						}
						break;

						default:
						{
							throw new \Exception("Towertype '$towerType' is unknown");
						}
					}
				}
			}
			break;

			default:
			{

			}
		}

		if( OsNames::isLinux() )
		{
			Helper::exec("sudo find /srv/tripletower -name temp -type d -exec chown -v -R root:www-data {} \;");
			Helper::exec("sudo find /srv/tripletower -name temp -type d -exec chmod -v 0775 {} \;");
			Helper::exec("sudo apachectl restart");
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/

	protected function generateSslCnfContent(string $serverName) : string
	{
		$altNames = "[ alt_names ]";
		$altNamesCount = 1;
		$altNames = $altNames  . PHP_EOL ."DNS.1 = " . $serverName ;

		return "[req]" . PHP_EOL .
			"req_extensions = req_ext" . PHP_EOL .
			"distinguished_name = req_distinguished_name". PHP_EOL .
			PHP_EOL .
			"[req_distinguished_name]". PHP_EOL .
			PHP_EOL .
			"[ req_ext ]". PHP_EOL .
			"subjectAltName = @alt_names" . PHP_EOL .
			PHP_EOL .
			$altNames;
	}

	protected function insertWappsIntoWappsTowerDb($wappstowerExecPath, $wappstowerDbType, $wappstowerDbHostName, $wappstowerDbName, $wappstowerDbUserName, $wappstowerDbPassword)
	{
		$wappName			= null;
		$wappstowerDb		= new WappsTowerDBC($wappstowerDbType, $wappstowerDbHostName, $wappstowerDbName, $wappstowerDbUserName, $wappstowerDbPassword);
		$wappsDirIterator	= new \DirectoryIterator($wappstowerExecPath);

		foreach($wappsDirIterator as $wappfolder)
		{
			if( $wappfolder->isDot() || !$wappfolder->isDir() )
				continue;

			/*
			 * TODO: Check whole wapp structure like:
			 * - Folders available - checkWappFolders( $wappfolder->getPathname() )
			 * - Manifest consistency, etc.
			 */

			$manifest = Helper::loadJsonFile( $wappfolder->getPathname() . "/meta.json" );
			if( TripleTowerError::isError( $manifest ) )
				throw $manifest;

			$wappName = $manifest->name;
			if( empty( $wappName ) )
				throw new \Exception("WappName is emtpy in file $wappfolder/meta.json'");

			if( $wappName !== $wappfolder->getBasename() )
			{
				// This can happen if a developer backups a folder via copy'n'paste -> so only a warning no error
				$warning = new SysError(SysError::INVALID_CONFIGURATION, SysError::WARN, "Found a wapp with name '$wappName' which does not equal the folder name '$wappfolder'");
				continue;
			}

			// Insert wapp into DB if it got not already inserted earlier
			if( ( $wappstowerDb->getWappIdByName( $wappName ) !== false ) )
				continue;

			$wappVisible = isset($manifest->visible) ? (bool)$manifest->visible : true;
			$insertWappId = $wappstowerDb->insertWapp( $wappName, $wappVisible );

			if( TripleTowerError::isError( $insertWappId ) )
				throw $insertWappId;
		}
	}

	protected function updateHostsfile($systemHostsFile)
	{
		if( is_link( $systemHostsFile ) && !file_exists( $systemHostsFile ) )
		{
			echo PHP_EOL . "Found broken symlink as hosts file, unlinking it..." . PHP_EOL;
			@unlink( $systemHostsFile );

			if( file_exists( $backupSystemHostsFile = $systemHostsFile . "_orig" ) )
			{
				echo PHP_EOL . "Restore hosts file from $backupSystemHostsFile ..." . PHP_EOL;
				if( copy( $backupSystemHostsFile, $systemHostsFile) === false )
					throw new \Exception("Could not restore hosts file '$systemHostsFile' from '$backupSystemHostsFile'.");
			}
			else
			{
				echo PHP_EOL . "Creating new hosts file ..." . PHP_EOL;
				if( file_put_contents($systemHostsFile, "" ) === false )
					throw new \Exception("Could not create hosts file '$systemHostsFile'. It might be protected by an AntiVir program or is open in another program.");
			}
		}

		if( !file_exists($systemHostsFile) )
			throw new \Exception("Hosts file '$systemHostsFile' could not be found");

		// Add serverName to hosts file
		$contentTripletowerHostsFile = "\t127.0.0.1 " . $this->config["serverName"] . "\t# "  . self::COMMENT_TAG_FOR_HOSTFILE . " " . date(\DateTime::W3C);
		if( ($fileContentArray = file($systemHostsFile, FILE_IGNORE_NEW_LINES)) !== false )
		{
			foreach($fileContentArray as $key => $val)
			{
				if( strpos($val, self::COMMENT_TAG_FOR_HOSTFILE) !== false )
					unset( $fileContentArray[$key] );
			}

			$fileContentArray[] = $contentTripletowerHostsFile;

			if( file_put_contents( $systemHostsFile, implode(PHP_EOL, $fileContentArray) ) === false )
				throw new \Exception("Could not write hosts file '$systemHostsFile'. It might be protected by an AntiVir program or is open in another program.");
		}
		else
		{
			throw new \Exception("Could not read hosts file '$systemHostsFile' even it exists.");
		}
	}
}