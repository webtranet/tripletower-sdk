<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name vcredist2015 Hanoi Plugin
 * @desc This package provides a version of the vc redistributable.
 * For more details about the versioning you either ask Microsoft or read the following:
 * https://web.archive.org/web/20220826070112/https://blog.knatten.org/2022/08/26/microsoft-c-versions-explained/
 * 
 * The links to the vcredist files can be found here:
 * https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist
 * https://github.com/abbodi1406/vcredist/tree/master/source_links
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_vcredist2015 extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "http://download.microsoft.com/download/8/C/4/8C46752E-F6FD-43E4-AF10-E046A128CC0A/VC_redist.x64.exe",
		"parameters" => "/install /quiet /norestart"
	];

	protected $configLinuxDefaults =
	[

	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create constructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = $tempDir . basename($packageRemoteSourcePath);

				// Download file
				$downloadError = Helper::downloadFile( $packageRemoteSourcePath, $packageLocalSourcePath, true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '$packageRemoteSourcePath' to '$packageLocalSourcePath'");
			}
			break;

			default:
			{
				throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		$tempDir = $this->getTempPluginFolder();

		$packageRemoteSourcePath = $this->config["packageSourcePath"];
		$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

		$execCommand = 'start /wait "" ';

		if( strtolower( pathinfo($packageLocalSourcePath, PATHINFO_EXTENSION) ) === "msi" )
			$execCommand .= "msiexec /i ";

		$execCommand .= trim('"' . $packageLocalSourcePath . '" ' . $this->config["parameters"]);

		// Install file
		$errorCode = Helper::Exec($execCommand, "", true);
		if( $errorCode !== 0 )
		{
			// Errorcode 3010 means a reboot is required
			if( $errorCode === 3010 )
				echo "installer package inside " . get_class($this) . " wants a reboot, but this can be ignored - ";
			else
				throw new \Exception("'$execCommand' caused error code '$errorCode'");
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		// Nothing needed
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}