<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name php_psr Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;

class hanoi_php_psr extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[

	];

	protected $configLinuxDefaults =
	[

	];

	protected $phpExtensionName = "psr";
	protected $phpExtensionLinuxPackage = "php-psr";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Updating package list
				 Helper::refreshPackageList()->throwIfNotSuccess();
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_php'];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Get mod-enabled folder
				$modsEnabledFolder = Helper::getEnvironmentVariable("PHP_INI_SCAN_DIR");
				if( TripleTowerError::isError($modsEnabledFolder) )
					throw $modsEnabledFolder;

				if( !is_dir($modsEnabledFolder) )
					throw new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not find folder '$modsEnabledFolder'(PHP_INI_SCAN_DIR)");

				// Get deps folder
				$depsFolder = dirname($modsEnabledFolder) . "/deps";
				if( !is_dir($depsFolder) )
					throw new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not find folder '$depsFolder'");

				// Check and if there are dependencies for this extension
				if( is_dir("$depsFolder/$this->phpExtensionName") )
				{
					// Get php folder
					$phpExe = Helper::findExecutableInPathVariable("php.exe");
					if( !is_file($phpExe) )
						throw new \Exception("Could not find file 'php.exe'");
					$phpFolder = Helper::extractFolderName($phpExe);

					foreach( new \FilesystemIterator("$depsFolder/$this->phpExtensionName") as $dependencyFile )
					{
						if( $dependencyFile->isFile() )
							Helper::copyFile( $dependencyFile->getPathname(), "$phpFolder/".$dependencyFile->getFilename() )->throwIfNotSuccess();
					}
				}

				// Get mod-available folder
				$modsAvailableFolder = dirname($modsEnabledFolder) . "/mods-available";
				if( !is_dir($modsAvailableFolder) )
					throw new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not find folder '$modsAvailableFolder'");

				// Get ini file of extension
				$extensionIniPath = "$modsAvailableFolder/$this->phpExtensionName.ini";
				$extensionIniContent = file_get_contents($extensionIniPath);
				if( $extensionIniContent === false )
					throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not read ini file '$extensionIniPath'");

				// Get the extension priority
				// Using \n to split instead of PHP_EOL because it works with \r\n and \n
				$priority = "";
				foreach( explode("\n", $extensionIniContent) as $extensionIniContentLine )
				{
					$pattern = '/^[\s]*;[\s]*priority[\s]*=[\s]*(?<priority>[0-9]+)[\s]*.*$/i';
					$matches = [];
					if( preg_match($pattern, $extensionIniContentLine, $matches) )
					{
						$priority = $matches["priority"];
						break;
					}
				}
				if( empty($priority))
					throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "No priority found in file '$extensionIniPath'");

				// Create a symlink from mods-available to mods-enabled
				$extensionLinkPath = "$modsEnabledFolder/$priority-$this->phpExtensionName.ini";
				clearstatcache(true, $extensionLinkPath);
				Helper::createSymlink($extensionIniPath, $extensionLinkPath, true)->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Install via package manager
				Helper::genericLinuxInstall($this->phpExtensionLinuxPackage)->throwIfNotSuccess();
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}