<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name php_doublemetaphone Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\ConfigError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;

class hanoi_php_doublemetaphone extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[

	];

	protected $configLinuxDefaults =
	[

	];

	protected $phpExtensionName = "doublemetaphone";
	protected $phpExtensionLinuxPackage = "php-doublemetaphone";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				throw new \Exception( get_class($this) . " is not supported on windows");
			}
			break;

			default:
			{
				// Updating package list
				 Helper::refreshPackageList()->throwIfNotSuccess();
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_php'];
	}

	public function install()
	{
		// Install via package manager
		Helper::genericLinuxInstall($this->phpExtensionLinuxPackage)->throwIfNotSuccess();
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		// Nothing needed
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}