<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name OpenSSH Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\FsError;

class hanoi_openssh extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://cygwin.com/setup-x86_64.exe",
		"destination" => "C:/tripletower-stack/openssh",
		"parameters" => "--quiet-mode --no-shortcuts --only-site --no-write-registry",
		"packages" => "unzip,zip,xz,openssl,openssh,ca-certificates,cygrunsrv",
		"mirrorUrl" => ""
	];

	protected $configLinuxDefaults =
	[

	];

	protected $linuxPackage = "openssh-client openssh-server";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create constructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();
				$localPackageTempDir = $tempDir . "packages/";
				if( !file_exists($localPackageTempDir) && !mkdir($localPackageTempDir) )
					throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "Could not create folder package folder '$localPackageTempDir' in order to download files into it." );

				// Download files
				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '{$this->config["packageSourcePath"]}' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");

				// Stopping SSH
				Helper::exec( "net stop cygsshd", "", true );

			}
			break;

			default:
			{
				// Updating package list if a linuxpackage is available
				if($this->linuxPackage !== "")
				{
					$refreshError = Helper::refreshPackageList();
					if( !$refreshError->isSuccess() )
						throw $refreshError;
				}
				else
				{
					throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
				}
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

				$localPackageTempDir = Helper::extractFolderName($packageLocalSourcePath) . "cygwin/";
				$execCommand = 'start /wait /B cmd /c "';

				if( strtolower( pathinfo($packageLocalSourcePath, PATHINFO_EXTENSION) ) === "msi" )
					$execCommand .= "msiexec /i ";

				$execCommand .= trim( '"' . $packageLocalSourcePath . '" ' . $this->config["parameters"] . ' --packages "' . $this->config["packages"] . '" --root "' . $this->config["destination"] . '" --local-package-dir "' . $localPackageTempDir . '"' );
				// Install file
				if( !empty($this->config["mirrorUrl"]) )
					$execCommand .= ' --site "' . $this->config["mirrorUrl"] . '""';

				Helper::exec($execCommand);
			}
			break;

			default:
			{
				// Install via package manager if linuxpackage available
				$installError = Helper::genericLinuxInstall($this->linuxPackage);
				if( !$installError->isSuccess() )
					throw $installError;
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				Helper::exec( $this->config["destination"] . "/bin/mintty.exe --exec /bin/ssh-host-config --yes" );
				Helper::exec( "netsh advfirewall firewall add rule name=\"OpenSSH Server\" action=allow profile=any dir=in protocol=tcp localport=22" );
				Helper::exec( "net start cygsshd" );
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}