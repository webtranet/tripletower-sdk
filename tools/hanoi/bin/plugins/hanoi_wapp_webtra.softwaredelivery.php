<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name wapp_webtra.softwaredelivery Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Tower;

class hanoi_wapp_webtra_softwaredelivery extends IHanoiPlugin
{


	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/Webtra.SoftwareDelivery.git",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",
		"tripletower" => "C:/tripletower-stack/tripletower",

		"git" => "",

		"sourceCodeManagementUrl" => "https://gitlab.com/webtranet"
	];

	protected $configLinuxDefaults =
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/Webtra.SoftwareDelivery.git",
		"tripletowersdk" => "/srv/tripletower-sdk",
		"tripletower" => "/srv/tripletower",

		"git" => "",

		"sourceCodeManagementUrl" => "https://gitlab.com/webtranet"
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		$packageSourcePath = $this->config["packageSourcePath"];

		// Creating folders
		$tempDir = $this->getTempPluginFolder();

		// Download file if not a git repository
		if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "git" )
		{
			// It seems to be a git repository. Test if it exists.
			if( !Helper::remoteFileExists($packageSourcePath)->isSuccess() )
				throw new \Exception("Wapp packageSourcePath '$packageSourcePath' could not be found");
		}
		else if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "zip" )
		{
			// A zip -> Download
			copy( $packageSourcePath, $tempDir );

			// Unzip
			$zip = new \ZipArchive();
			$archiveFile = $tempDir.basename($packageSourcePath);
			if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
			{
				echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
				if( ($zipError = $zip->open($archiveFile)) !== true )
					throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
			}
			if( $zip->extractTo($tempDir.basename($packageSourcePath, ".zip")) !== true )
				throw new \Exception("Could not extract content of zip archive $tempDir" . basename($packageSourcePath) . " to $tempDir" . basename($packageSourcePath, ".zip"));
			if( $zip->close() !== true )
				throw new \Exception("Could not close zip archive $tempDir" . basename($packageSourcePath));
		}
		else
		{
			// Not supported!
			throw new \Exception( "Wapp packageSourcePath '$packageSourcePath' has to be either a git web URL or a zip file" );
		}
	}

	public function getInstallDependencies() : array
	{
		return ['hanoi_tripletower'];
	}

	public function install()
	{
		$packageSourcePath = $this->config["packageSourcePath"];
		$tempDir = $this->getTempPluginFolder();

		// Find tripletowersdk folder
		$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["tripletowersdk"] );
		if( !is_dir($tripletowersdkFolder) )
			throw new \Exception("Could not find folder '$tripletowersdkFolder'");

		// Find tripletower folder
		$wappsDestination = Helper::normalizeFilePath($this->config["tripletower"] . "/" . Tower::WAPPS_TOWER);
		if( !is_dir( $wappsDestination ) )
			throw new \Exception("Wapp destination '$wappsDestination' does not exist but is mandatory");

		$destination = "$wappsDestination/" . basename($packageSourcePath, ".git");
		if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "git" )
		{
			// It's git! Clone/pull stuff from repository
			$git = "git";
			if( $this->config["git"] !== "" )
				$git = Helper::normalizeFilePath( $this->config["git"] );
			else
				$git = Helper::normalizeFilePath( Helper::findExecutableInPathVariable("git.exe") );

			if( !is_file($git) )
				throw new \Exception("Could not find file 'git' executable");

			if( !is_dir($destination . '/.git') )
			{
				Helper::exec('"' . $git . '" clone "' . $packageSourcePath . '" "' . $destination . '"');
			}
			else
			{
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" reset --hard @{upstream}');
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" clean -d --force');
				Helper::exec('"' . $git . '" --git-dir="' . $destination . '/.git" --work-tree="' . $destination . '" pull --all');
			}
		}
		else if( pathinfo( $packageSourcePath, PATHINFO_EXTENSION ) === "zip" )
		{
			if( is_dir($destination) )
				Helper::unlink_recursive( $destination )->throwIfNotSuccess();
			Helper::copy_recursive( $tempDir.basename($packageSourcePath, ".zip"), $wappsDestination );
		}
		else
		{
			// Not supported!
			throw new \Exception( "Wapp packageSourcePath '$packageSourcePath' has to be either a git web URL or a zip file" );
		}

			$aliasContent  = "# TripleTower Virtual Hosts - AUTO CREATED BY HANOI DEPLOYMENT".PHP_EOL;
			$aliasContent .= "#".PHP_EOL;
			$aliasContent .= "# Required modules: mod_alias mod_autoindex mod_dir mod_mime".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "Alias /wappstower/Webtra.SoftwareDelivery/public \"$destination/public\"".PHP_EOL;
			$aliasContent .= "<Directory \"$destination/public\">".PHP_EOL;
			$aliasContent .= "	Options Indexes FollowSymLinks".PHP_EOL;
			$aliasContent .= "	AllowOverride None".PHP_EOL;
			$aliasContent .= "	Require all granted".PHP_EOL;
			$aliasContent .= "	".PHP_EOL;
			$aliasContent .= "	# Styles options".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	DirectoryIndex disabled".PHP_EOL;
			$aliasContent .= "	IndexOptions FancyIndexing SuppressHTMLPreamble HTMLTable VersionSort FoldersFirst IconsAreLinks IconHeight=16 IconWidth=16 NameWidth=* SuppressDescription".PHP_EOL;
			$aliasContent .= "	IndexOrderDefault Ascending Name".PHP_EOL;
			$aliasContent .= "	IndexIgnore .??* *~ *# .softwaredelivery".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# Files inclusion".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	HeaderName /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/header.html".PHP_EOL;
			$aliasContent .= "	ReadmeName /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/footer.html".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# Svg support".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	AddType image/svg+xml svg svgz".PHP_EOL;
			$aliasContent .= "	AddEncoding gzip svgz".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# Default icons".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	DefaultIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-text.svg".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# Folder icons".PHP_EOL;
			$aliasContent .= "	#".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/back.svg ..".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/github.svg .github".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/nodejs.svg node_modules".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-directory.svg ^^DIRECTORY^^".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "	# File icons".PHP_EOL;
			$aliasContent .= "	# https://github.com/file-icons/source".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-binary.svg .com .exe .msi".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-code.svg .c .h .mustache .rust .source .apache .yml .jade".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-media.svg .png .jpg .jpeg .webp .gif .bmp .mp4 .webm .ogv .mov .ico".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-pdf.svg .pdf".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-text.svg .txt .srt README readme Procfile".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-zip.svg .zip .z .gz .Z .tgz .tar.gz .tar .rar".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/gear.svg .htaccess .env .env-dev .env-production .env-staging".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/config.svg .conf .ini .cfg .prefs".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/less.svg .less".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/css.svg .css".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/html.svg .html .htm".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/coffee.svg .coffee".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/java.svg .java".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/markdown.svg .markdown .md".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/eslint.svg *.eslintignore *.eslintrc.js *.eslintrc.json *.eslintrc *.eslintrc.yml".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/webpack.svg webpack.config.js".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/rollup.svg rollup.config.js".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/postcss.svg postcss.config.js *.postcssrc *.postcssrc.json *.postcssrc.js *.postcssrc.yml".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/grunt.svg gruntfile.js Gruntfile.js".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/gulp.svg gulpfile.js Gulpfile.js".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/js.svg .js".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/composer.svg composer.json composer.lock composer.phar".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/bower.svg bower.json".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/npm.svg package.json package-lock.json".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/settings.svg .json".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/mustache.svg .mustache".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/php.svg .php".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/python.svg .py".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/ruby.svg .rb".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/rust.svg .rust".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/sass.svg .sass".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/scss.svg .scss".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/svg.svg .svg .svgx .eps".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/xml.svg .xml".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/typescript.svg .ts".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/vue.svg .vue".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/yarn.svg yarn.lock".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/babel.svg *.babelrc".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/git.svg *.gitignore *.gitattributes".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/editor-config.svg .editorconfig".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/shopify.svg .liquid".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/nunjucks.svg .nunjucks".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/pug.svg .pug".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/adobe-photoshop.svg .psd".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/adobe-illustrator.svg .ai".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/stylus.svg .styl".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/terminal.svg .sh .bash".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/database.svg .sql .mysql .sqlite .dsql .db".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/vagrant.svg vagrantfile Vagrantfile".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-word.svg .doc .docx".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/file-excel.svg .xls .xlsx".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/yaml.svg .yml .yaml".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/brainfuck.svg .b".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/cobol.svg .cbl .cob .cpy".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/docker.svg docker-compose.yml".PHP_EOL;
			$aliasContent .= "	AddIcon /wappstower/Webtra.SoftwareDelivery/public/.softwaredelivery/icons/VirtualBox.svg .ova .vbox".PHP_EOL;
			$aliasContent .= "".PHP_EOL;
			$aliasContent .= "</Directory>".PHP_EOL;

			$tripleTowerApacheConfPath = OsNames::isWindows() ? "$tripletowersdkFolder/etc/apache2/conf_windows" : "$tripletowersdkFolder/etc/apache2/conf_linux";
			$realApacheConfPath = OsNames::isWindows() ? "$tripletowersdkFolder/etc/apache2/conf_windows" : "/etc/apache2";
			if( !file_put_contents("$tripleTowerApacheConfPath/sites-available/softwaredelivery.conf", $aliasContent) )
				throw new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Could not write alias file 'softwaredelivery'");
			Helper::createSymlink("$tripleTowerApacheConfPath/sites-available/softwaredelivery.conf", "$realApacheConfPath/sites-enabled/softwaredelivery.conf", true)->throwIfNotSuccess();

			Helper::replaceInJsonFile('$sourceCodeManagementUrl', $this->config["sourceCodeManagementUrl"], "$destination/conf/wappconfig.json")->throwIfNotSuccess();
	}

	public function getEnableDependencies() : array
	{
		return ["hanoi_apache_alias", "hanoi_apache_autoindex", "hanoi_apache_dir", "hanoi_apache_mime"];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}