<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */



use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Helper\DaemonHandler;

class hanoi_apache extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	const APACHE_SERVICE_NAME = "Apache2.4";


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://www.apachelounge.com/download/VS17/binaries/httpd-2.4.62-240718-win64-VS17.zip",
		"destination" => "C:/tripletower-stack/apache",
		"tripletowersdk" => "C:/tripletower-stack/tripletower-sdk",
		"privateKeyPath" => null,
		"certificatePath" => null,
		"caCertificatePath" => null,
		"defaultApplicationPath" => null,

		"serverName" => "tripletower.localhost",
		"adminMailAddress" => "",
		"httpPort" => "80",
		"httpsPort" => "443",
		"logLevel" => "debug"
	];

	protected $configLinuxDefaults =
	[
		"tripletowersdk" => "/srv/tripletowersdk",
		"privateKeyPath" => null,
		"certificatePath" => null,
		"caCertificatePath" => null,
		"defaultApplicationPath" => null,

		"serverName" => "tripletower.localhost",
		"adminMailAddress" => "",
		// TODO Use a regex to replace the ports configuration for apache on linux
		"httpPort" => "80",
		"httpsPort" => "443",
		"logLevel" => "debug"
	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				// Download files
				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
				{
						$downloadError->addMessage("Could not copy file '" . $this->config["packageSourcePath"] . "' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");
						throw $downloadError;
				}

				// Unzip new apache
				$zip = new \ZipArchive();
				$archiveFile = $tempDir.basename($this->config["packageSourcePath"]);
				if( ($zipError = $zip->open($archiveFile, \ZipArchive::CHECKCONS)) !== true )
				{
					echo "'$archiveFile' failed consistency checks but might can be extracted anyway - ";
					if( ($zipError = $zip->open($archiveFile)) !== true )
						throw new \Exception("Could not open zip archive '$archiveFile' because of zip error: '$zipError'");
				}
				if( $zip->extractTo($tempDir.basename($this->config["packageSourcePath"], ".zip")) !== true )
					throw new \Exception("Could not extract content of zip archive $tempDir" . basename($this->config["packageSourcePath"]) . " to $tempDir" . basename($this->config["packageSourcePath"], ".zip"));
				if( $zip->close() !== true )
					throw new \Exception("Could not close zip archive $tempDir" . basename($this->config["packageSourcePath"]));

				// Stop Apache service if running
				if( DaemonHandler::isDaemonRunning(self::APACHE_SERVICE_NAME)->isSuccess() )
					DaemonHandler::stopDaemon(self::APACHE_SERVICE_NAME)->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
				return ['hanoi_vcredist2017', 'hanoi_tripletowersdk'];

			default:
				return ['hanoi_tripletowersdk'];
		}
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Find tripletowersdk folder
				$tripletowersdkFolder = Helper::normalizeFilePath( $this->config["tripletowersdk"] );
				if( !is_dir($tripletowersdkFolder) )
					throw new \Exception("Could not find folder '$tripletowersdkFolder'");

				// Remove Apache service if installed
				if( DaemonHandler::isDaemonInstalled(self::APACHE_SERVICE_NAME)->isSuccess() )
					DaemonHandler::deleteDaemon(self::APACHE_SERVICE_NAME)->throwIfNotSuccess();

				// Remove old /apache/bin folder from path variable
				$oldApacheExePath = Helper::findExecutableInPathVariable("httpd.exe");
				if( !TripleTowerError::isError($oldApacheExePath) )
					Helper::removePathFromPathVariable(dirname($oldApacheExePath))->throwIfNotSuccess("Could not remove old apache path '$oldApacheExePath' from Path variable");
				else
					$oldApacheExePath->throwIfNotSuccessOrSameAs(new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT));

				// Delete old apache folder
				if( is_dir($this->config["destination"]) )
					Helper::unlink_recursive($this->config["destination"])->throwIfNotSuccess();

				// This is a hack necessary for Windows 10's sucking NTFS implementation because
				// if you're unlinking files/symlinks this might block mkdir() for a few milli seconds.
				sleep(1);

				// Copy apache folder to destination
				$sourceFolder = $this->getTempPluginFolder() . basename($this->config["packageSourcePath"], ".zip");
				if( !Helper::copy_recursive("$sourceFolder/Apache24", $this->config["destination"]) )
					throw new \Exception("Could not copy '$sourceFolder/Apache24' to '" . $this->config["destination"] . "'");

				// Create symlinks to enable configs and sites
				$apacheConfigFolder = $this->config["destination"] . '/conf';

				// Create apache2 symlink
				Helper::createSymlink("$tripletowersdkFolder/etc/apache2/conf_windows", $apacheConfigFolder, true)->throwIfNotSuccess();

				// Create conf-enabled symlinks
				Helper::createSymlink("$apacheConfigFolder/conf-available/tripletower.conf", "$apacheConfigFolder/conf-enabled/tripletower.conf", true)->throwIfNotSuccess();
				Helper::createSymlink("$apacheConfigFolder/conf-available/charset.conf", "$apacheConfigFolder/conf-enabled/charset.conf", true)->throwIfNotSuccess();

				// Create sites-enabled symlinks
				Helper::createSymlink("$apacheConfigFolder/sites-available/000-default.conf", "$apacheConfigFolder/sites-enabled/000-default.conf", true)->throwIfNotSuccess();


				// Parse apache-configs, search them for tokens and replace them

				// apache2.conf
				Helper::replaceInFile('$destination', $this->config["destination"], "$apacheConfigFolder/apache2.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$logLevel',    $this->config["logLevel"],    "$apacheConfigFolder/apache2.conf")->throwIfNotSuccess();

				// ports.conf
				Helper::replaceInFile('$httpPort',  $this->config["httpPort"],  "$apacheConfigFolder/ports.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$httpsPort', $this->config["httpsPort"], "$apacheConfigFolder/ports.conf")->throwIfNotSuccess();

				// 000-default.conf
				$adminMailAddress = (!empty($this->config["adminMailAddress"]) ? $this->config["adminMailAddress"] : "info@".$this->config["serverName"]);
				Helper::replaceInFile('$serverName',             $this->config["serverName"],             "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$adminMailAddress',       $adminMailAddress,                       "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$httpPort',               $this->config["httpPort"],               "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$httpsPort',              $this->config["httpsPort"],              "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$privateKeyPath',         $this->config["privateKeyPath"],         "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$certificatePath',        $this->config["certificatePath"],        "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$caCertificatePath',      $this->config["caCertificatePath"],      "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();
				Helper::replaceInFile('$defaultApplicationPath', $this->config["defaultApplicationPath"], "$apacheConfigFolder/sites-available/000-default.conf")->throwIfNotSuccess();


				// Add /apache/bin to path variable
				Helper::addPathToPathVariable($this->config["destination"]."/bin")->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Install via package manager
				$installError = Helper::genericLinuxInstall("apache2");
				if( !$installError->isSuccess() )
					throw $installError;
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return ["hanoi_apache_alias", "hanoi_apache_authz_core", "hanoi_apache_deflate", "hanoi_apache_filter", "hanoi_apache_headers", "hanoi_apache_log_config", "hanoi_apache_logio", "hanoi_apache_mime", "hanoi_apache_php", "hanoi_apache_setenvif", "hanoi_apache_socache_shmcb", "hanoi_apache_ssl"];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				/* Apply regedit entries to make apache start fast:
				 *
				 * [HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Cryptography\Defaults\Provider Types\Type 001]
				 * "Name"="Microsoft Strong Cryptographic Provider"
				 * "TypeName"="RSA Full (Signature and Key Exchange)"
				 */
				$shell = new \COM("WScript.Shell");
				if( !$shell )
					throw new \Exception("Could not open Microsoft registry object (WScript.Shell) for $this->pluginName");
				$defaultCryptoProvider = $shell->RegRead("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Cryptography\\Defaults\\Provider Types\\Type 001\\Name");
				if( $defaultCryptoProvider !== "Microsoft Strong Cryptographic Provider")
					$shell->RegWrite("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Cryptography\\Defaults\\Provider Types\\Type 001\\Name", "Microsoft Strong Cryptographic Provider");

				// Adding exception to windows firewall
				Helper::exec('netsh advfirewall firewall add rule name="Apache HTTP Server" dir=in action=allow program="' . str_replace('/', '\\', $this->config["destination"].'/bin/httpd.exe') . '"');

				// Install new apache service
				Helper::exec('"'.$this->config["destination"].'/bin/httpd.exe"' . ' -k install');

				// Start new apache service
				Helper::exec('"'.$this->config["destination"].'/bin/httpd.exe"' . ' -k start');

				// Test if service got started
				for($sleepLoop = 0; true; $sleepLoop++)
				{
					// query service information
					$apacheServiceState = win32_query_service_status("Apache2.4");
					if( !is_array($apacheServiceState) )
						throw new \Exception("Could not query service status of 'Apache2.4' while starting");

					// wait for service to start
					if( $apacheServiceState["CurrentState"] === WIN32_SERVICE_START_PENDING )
						sleep(1);

					// wait for a maximum of 30 seconds to stop service
					if( $sleepLoop >= DaemonHandler::MAX_REACTION_TIME )
						throw new \Exception("Service 'Apache2.4' could not be started within " . DaemonHandler::MAX_REACTION_TIME . " seconds");

					// started service successfully
					if( $apacheServiceState["CurrentState"] === WIN32_SERVICE_RUNNING )
						break;
				}
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}