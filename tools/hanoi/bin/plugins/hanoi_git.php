<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;

class hanoi_git extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	const GIT_VERSION_1     = "1";
	const GIT_VERSION_2     = "2";


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://github.com/git-for-windows/git/releases/download/v2.46.0.windows.1/Git-2.46.0-64-bit.exe",
		"destination" => "C:/tripletower-stack/git",
		"sslverify" => true,
		"known_hosts" => "",
		"id_rsa" => ""
	];

	protected $configLinuxDefaults =
	[
		"ca-bundle" => ""
	];

	protected $sshPath = "";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				$tempDir = $this->getTempPluginFolder();

				// Download files
				$downloadError = Helper::downloadFile( $this->config["packageSourcePath"], $tempDir . basename($this->config["packageSourcePath"]), true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
				{
						$downloadError->addMessage("Could not copy file '" . $this->config["packageSourcePath"] . "' to '" . $tempDir . basename($this->config["packageSourcePath"]) . "'");
						throw $downloadError;
				}
			}
			break;

			default:
			{
				$tempDir = $this->getTempPluginFolder();

				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}

			// Look for CA cert file if given
			if( $this->caFileName !== "" && is_file($_SERVER["TT"]->tower->getEtcFolder()."ssl/certs/$this->caFileName") === false )
				throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not find CA certificate file '$caFileName' in certs folder.");

		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Install git
				$tempFolder = $this->getTempPluginFolder();
				$fileSystem = new \COM("Scripting.FileSystemObject");
				if( !$fileSystem )
					throw new \Exception("Could not open Microsoft file system object (Scripting.FileSystemObject) for $this->pluginName");
				$fileVersionArray = explode(".", $fileSystem->GetFileVersion($tempFolder.basename($this->config["packageSourcePath"])));
				if( empty($fileVersionArray) )
					throw new \Exception("Could not get a properly formatted file version of '" . $tempFolder.basename($this->config["packageSourcePath"]) . "' for $this->pluginName");

				// if folder does not exist, git will be installd to C:\Users\<username>\AppData\Local\Programs\Git
				if( !is_dir($this->config["destination"]) )
					if( mkdir( $this->config["destination"], 0777, true ) !== true)
							throw new \Exception("Could not create folder '". $this->config["destination"] . "'");

				switch($fileVersionArray[0])
				{
					case self::GIT_VERSION_1:
						Helper::exec('"'.$tempFolder.basename($this->config["packageSourcePath"]) . '" /SP- /VERYSILENT /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /CLOSEAPPLICATIONS /COMPONENTS="assoc,assoc_sh" /DIR="' . $this->config["destination"] . '"');
						$this->sshPath = $this->config["destination"] . '/.ssh';
					break;

					case self::GIT_VERSION_2;
						Helper::exec('"'.$tempFolder.basename($this->config["packageSourcePath"]) . '" /SP- /VERYSILENT /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /CLOSEAPPLICATIONS /COMPONENTS="assoc,assoc_sh" /DIR="' . $this->config["destination"] . '"');
						$envUserName = getenv("UserName");
						if( $envUserName !== false &&  substr($envUserName, -1) === "$")
						{
							$this->sshPath = $this->config["destination"]."/home/system/.ssh";
						}
						else
						{
							$envUserProfile = getenv("UserProfile");
							if( $envUserProfile === false )
								throw new \Exception("Could not find a destination for .ssh");
							$this->sshPath = "$envUserProfile/.ssh";
						}

					break;

					default:
						throw new \Exception("Git file version '" . $fileVersionArray[0] . "' is not supported by hanoi. Only version Could not get a properly formatted file version of '" . $tempFolder.basename($this->config["packageSourcePath"]) . "' for $this->pluginName");
				}

				// Create known_hosts if available
				if( $this->config["known_hosts"] !== "" )
				{
					if( !is_dir($this->sshPath) && !mkdir($this->sshPath, 0777, true) )
						throw new \Exception("Could not create directory '$this->sshPath'");
					if( isset($this->config["known_hosts"]) && !file_put_contents("$this->sshPath/known_hosts", $this->config["known_hosts"]) )
						throw new \Exception("Could not create file '$this->sshPath/known_hosts'");
				}

				// Create id_rsa if available
				if( $this->config["id_rsa"] !== "" )
				{
					if( !is_dir($this->sshPath) && !mkdir($this->sshPath, 0777, true) )
						throw new \Exception("Could not create directory '$this->sshPath'");
					if( isset($this->config["id_rsa"]) && !file_put_contents("$this->sshPath/id_rsa", base64_decode($this->config["id_rsa"])) )
						throw new \Exception("Could not create file '$this->sshPath/id_rsa'");
				}

				// Add own ca-bundle to the git ca-bundle if available
				if( $this->caFileName !== "" )
				{
					$gitCaBundlePath = $this->config["destination"]."/mingw32/ssl/certs/ca-bundle.crt" ;
					$gitCaBundleContent = is_file($gitCaBundlePath) ? file_get_contents($gitCaBundlePath) : "";
					$newCaBundleContent = file_get_contents($_SERVER["TT"]->tower->getEtcFolder()."ssl/certs/$this->caFileName");
					if( $newCaBundleContent === false )
						throw new \Exception ("Could not open file '$newCaBundleContent'");
					if( strpos($gitCaBundleContent, $newCaBundleContent) !== false || file_put_contents($gitCaBundlePath, $gitCaBundleContent.PHP_EOL.PHP_EOL.$newCaBundleContent) )
						throw new \Exception ("Could not add ca certs '$this->caFileName' to git ca-bundle file '$gitCaBundlePath'");
				}


				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				// The git installer automatically adds git to the path variable so there is no need to add it manually //
				//////////////////////////////////////////////////////////////////////////////////////////////////////////

				// Check if /git/cmd got added to the Path variable
				$gitExePath = Helper::findExecutableInPathVariable("git.exe");
				if( TripleTowerError::isError($gitExePath) )
					throw $gitExePath;

				// Turn off https verification if configured
				if( $this->config["sslverify"] === false )
					Helper::exec('"' . $gitExePath . '" config --global http.sslVerify false');

			}
			break;

			default:
			{
				// Install via package manager
				$installError = Helper::genericLinuxInstall("git");
				if( !$installError->isSuccess() )
					throw $installError;

				// Turn off https verification if configured
				if( $this->config["sslverify"] === false )
					Helper::exec('git config --global http.sslVerify false');

				// Create known_hosts if available
				// Create id_rsa if available
				// Add own ca-bundle to the git ca-bundle if available
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				if( is_dir($this->sshPath) )
					Helper::unlink_recursive($this->sshPath)->throwIfNotSuccess();
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}