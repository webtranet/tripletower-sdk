<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_multitail extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[

	];

	protected $configLinuxDefaults =
	[

	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_LINUX:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
			break;

			default:
			{
				throw new \Exception( get_class($this) . " is a linux only hanoi plugin");
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		// Install via package manager
		$installError = Helper::genericLinuxInstall("multitail");
		if( !$installError->isSuccess() )
			throw $installError;
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{

	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}