<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name firefox Hanoi Plugin
 * @desc
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_firefox extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://download.mozilla.org/?product=firefox-esr-latest&os=win&lang=en-US",
		"parameters" => "-ms"
	];

	protected $configLinuxDefaults =
	[

	];

	protected $linuxPackage = "firefox";


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create contructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = $tempDir . basename($packageRemoteSourcePath);

				// Download file
				$downloadError = Helper::downloadFile( $packageRemoteSourcePath, $packageLocalSourcePath, true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '$packageRemoteSourcePath' to '$packageLocalSourcePath'");
			}
			break;

			default:
			{
				// Updating package list
				$refreshError = Helper::refreshPackageList();
				if( !$refreshError->isSuccess() )
					throw $refreshError;
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

				$execCommand = 'start /wait "" ';

				if( strtolower( pathinfo($packageLocalSourcePath, PATHINFO_EXTENSION) ) === "msi" )
					$execCommand .= "msiexec /i ";

				$execCommand .= trim('"' . $packageLocalSourcePath . '" ' . $this->config["parameters"]);

				// Install file
				Helper::exec($execCommand);
			}
			break;

			default:
			{
				// Install via package manager if linuxpackage available
				if($this->linuxPackage !== "")
				{
					$installError = Helper::genericLinuxInstall($this->linuxPackage);
					if( !$installError->isSuccess() )
						throw $installError;
				}
				else
				{
					throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
				}
			}
		}
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Nothing needed
			}
			break;

			default:
			{
				// Nothing needed
			}
		}
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}