<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @name dotnet3.5 Hanoi Plugin
 * @desc This package provides a version of the .net desktop runtime
 * The links to the .net files can be found here:
 * https://dotnet.microsoft.com/en-us/download/dotnet-framework
 * https://dotnet.microsoft.com/en-us/download/dotnet
 *
 */

use TripleTowerSDK\Hanoi\IHanoiPlugin;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;

class hanoi_dotnet3_5 extends IHanoiPlugin
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $configWindowsDefaults =
	[
		"packageSourcePath" => "https://go.microsoft.com/fwlink/?linkid=2186537",
		"parameters" => "/q /norestart"
	];

	protected $configLinuxDefaults =
	[

	];


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	// TODO create constructor which builds the path variables --> create in parent class?

	public function getSetupDependencies() : array
	{
		return [];
	}

	public function setup()
	{
		switch( OsNames::getCurrentOsType() )
		{
			case OsNames::OS_TYPE_WINDOWS:
			{
				// Creating folders
				$tempDir = $this->getTempPluginFolder();

				$packageRemoteSourcePath = $this->config["packageSourcePath"];
				$packageLocalSourcePath = $tempDir . basename($packageRemoteSourcePath);

				// Download file
				$downloadError = Helper::downloadFile( $packageRemoteSourcePath, $packageLocalSourcePath, true, $this->caFileName, $this->proxy );
				if( !$downloadError->isSuccess() )
					throw $downloadError->addMessage("Could not copy file '$packageRemoteSourcePath' to '$packageLocalSourcePath'");
			}
			break;

			default:
			{
				throw new \Exception( get_class($this) . " is a windows only hanoi plugin");
			}
		}
	}

	public function getInstallDependencies() : array
	{
		return [];
	}

	public function install()
	{
		$tempDir = $this->getTempPluginFolder();


		$packageRemoteSourcePath = $this->config["packageSourcePath"];
		$packageLocalSourcePath = str_replace( ['%20', '/'], [' ', '\\'], ($tempDir . basename($packageRemoteSourcePath)) );

		$execCommand = 'start /wait "" ';

		if( strtolower( pathinfo($packageLocalSourcePath, PATHINFO_EXTENSION) ) === "msi" )
			$execCommand .= "msiexec /i ";

		$execCommand .= trim('"' . $packageLocalSourcePath . '" ' . $this->config["parameters"]);

		// Install file
		Helper::exec($execCommand);
		echo "Microsoft recommends to restart your system after installing '" . basename($this->config["packageSourcePath"]) . "' which might be true or not - ";
	}

	public function getEnableDependencies() : array
	{
		return [];
	}

	public function enable()
	{
		// Nothing needed
	}


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
}