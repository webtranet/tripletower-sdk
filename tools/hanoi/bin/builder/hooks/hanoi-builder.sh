#!/bin/bash
#
# TripleTower SDK - https://webtranet.online/tripletower-sdk
#
# @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
# @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
# @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
#

# Trap for clean up
function cleanup
{
	if [[ -d "$hanoiTempDir" ]]; then
		if [[ ${verbosity,,} == "on" ]]; then
			echo "Removing files by executing 'rm -Rf ${hanoiTempDir}'"
		fi
		
		rm -Rf ${hanoiTempDir}
	fi
	cd ${currentDir}
}
trap cleanup EXIT

# Script switches
alternateHanoiReleaseDir=HERE_GOES_YOUR_ALTERNATE_RELEASEDIR
verbosity="on"

# Environment setup
currentDir=$(pwd)
hanoiTempDir=$(mktemp --directory -t "hanoi_XXXXXXXX")
cd $(dirname $0)
currentScriptDir=$(pwd)

# Script can only be called if it lies within a tripletower-sdk git repository
if [[ -d "${currentScriptDir}/../../../../../../tripletower-sdk" ]]; then
	tripletowerSdkGitDir="$currentScriptDir/../../../../../../tripletower-sdk"
else
	if [[ -d "${currentScriptDir}/../../tripletower-sdk.git" ]]; then
		tripletowerSdkGitDir="$currentScriptDir/../../tripletower-sdk.git"
	else
		echo "ERROR: No tripletower-sdk root folder found, but this builder script can only be called if it lies within a tripletower-sdk git repository"
		exit 1;
	fi
fi

# Check if git is installed
if ! hash git 2>/dev/null; then
	echo "ERROR: Git seems not to be installed but is mandatory"
	exit 2;
fi

# Detect OS type
if [[ "$OSTYPE" == "msys" || "cygwin" ]]; then
	os=windows
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
	os=linux
else
	echo "ERROR: OS could not be detected"
	exit 3;
fi

# Check if an alternative hanoi release dir has been set
hanoiReleaseDir="${tripletowerSdkGitDir}/../tripletower/wappstower/Webtra.SoftwareDelivery/public/hanoi"
if [[ -d ${alternateHanoiReleaseDir} ]]; then
	hanoiReleaseDir=${alternateHanoiReleaseDir}
fi

# Check if the (alternative) hanoi release dir is available
if [[ ! -d ${hanoiReleaseDir} ]]; then
	echo "ERROR: Hanoi release dir '${hanoiReleaseDir}' not found!"
	echo "You can specify an 'alternateHanoiReleaseDir' in this script if you want to"
	exit 4;
fi

# Create the directory which will later be compressed an distributed in temp
if ! mkdir --parents "${hanoiTempDir}/tripletower-sdk" 2>/dev/null; then
	echo "ERROR: Could not create temporary directory: '${hanoiTempDir}/tripletower-sdk'"
	exit 5;
fi

# Extract necessary files for a full hanoi installer
echo extracting hanoi files ...
fileIncludeList="etc/ssl/ \
					libs/tripletower-lib/ \
					libs/zend/ \
					tools/7zip/ \
					tools/hanoi/ \
					tools/php/ \
					tools/putty/ \
					tools/wtee/"

if [[ ${verbosity,,} == "on" ]]; then
	gitArchiveCommand="git archive --verbose --remote=${tripletowerSdkGitDir} HEAD -- ${fileIncludeList} | tar --verbose --directory=${hanoiTempDir}/tripletower-sdk --extract --file - "
	echo ${gitArchiveCommand}
else
	gitArchiveCommand="git archive --remote=${tripletowerSdkGitDir} HEAD -- ${fileIncludeList} | tar --directory=${hanoiTempDir}/tripletower-sdk --extract --file - "
fi

eval ${gitArchiveCommand}
gitArchiveResult=$?
if [[ $gitArchiveResult -ne 0 ]]; then
	echo "ERROR: Could not extract content from tripletower-sdk.git"
	echo "Command '${gitArchiveCommand}' returned '${gitArchiveResult}'"
	exit 6;
fi

# iterate over plugins-dir and execute the script
for filename in $(find ${currentScriptDir}/hanoi-builder-plugins/*.sh 2> /dev/null); do
	scriptToExcuteFullPath=${filename#*}
	scriptToExcuteName=${filename##*/}
	echo ">>> running plugin '$scriptToExcuteName'"
	$scriptToExcuteFullPath ${hanoiTempDir}/tripletower-sdk
	childScriptResult=$?
	if [[ $childScriptResult -ne 0 ]]; then
		echo "ERROR: An error returned from build plugin '${scriptToExcuteName}': '${childScriptResult}'"
		exit 7;
	fi
done

# Creating linux and windows archives
echo Creating linux and windows archives ...

# 7za  - The executable
# a    - add files
# -aoa - overwrite files if present
# -mx9 - compression level 9 (maximum)
# destination file name
# source file path
# command file path
if [[ ${os} == "linux" ]]; then
	sevenzipArchiveCommandWindows="${hanoiTempDir}/tripletower-sdk/tools/7zip/7za a -aoa -mx9 \
								${hanoiTempDir}/hanoi-installer-windows.7z \
								${hanoiTempDir}/tripletower-sdk \
								${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-welcome-screen.cmd"
	sevenzipArchiveCommandLinux="${hanoiTempDir}/tripletower-sdk/tools/7zip/7za a -aoa -mx9 \
								${hanoiTempDir}/hanoi-installer-linux.7z \
								${hanoiTempDir}/tripletower-sdk \
								${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-welcome-screen.sh"
elif [[ ${os} == "windows" ]]; then
	sevenzipArchiveCommandWindows="${hanoiTempDir}/tripletower-sdk/tools/7zip/7za.exe a -aoa -mx9 \
								${hanoiTempDir}/hanoi-installer-windows.7z \
								${hanoiTempDir}/tripletower-sdk \
								${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-welcome-screen.cmd"
	sevenzipArchiveCommandLinux="${hanoiTempDir}/tripletower-sdk/tools/7zip/7za.exe a -aoa -mx9 \
								${hanoiTempDir}/hanoi-installer-linux.7z \
								${hanoiTempDir}/tripletower-sdk \
								${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-welcome-screen.sh"
fi

if [[ ${verbosity,,} == "on" ]]; then
	echo ${sevenzipArchiveCommandLinux}
	echo ${sevenzipArchiveCommandWindows}
fi

eval ${sevenzipArchiveCommandLinux}
	sevenzipCompressResultLinux=$?
if [[ $sevenzipCompressResultLinux -ne 0 ]]; then
	echo "ERROR: An error returned from linux archive creation command - '${sevenzipArchiveCommandLinux}}'"
	echo "Return code: '${sevenzipCompressResultLinux}'"
	exit 8;
fi

eval ${sevenzipArchiveCommandWindows}
	sevenzipCompressResultWindows=$?
if [[ $sevenzipCompressResultWindows -ne 0 ]]; then
	echo "ERROR: An error returned from windows archive creation command - '${sevenzipArchiveCommandWindows}}'"
	echo "Return code: '${sevenzipCompressResultWindows}'"
	exit 8;
fi

# Packing linux and windows sfx...
echo Creating linux and windows sfx...
hanoiSfxName="hanoi_"$(date +%Y-%m-%d_%H-%M-%S)".sfx"
sfxCreateCommandLinux="cat \
					${hanoiTempDir}/tripletower-sdk/tools/7zip/7zConLinuxX64.sfx \
					${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-config-linux.txt \
					${hanoiTempDir}/hanoi-installer-linux.7z \
					> ${hanoiReleaseDir}/${hanoiSfxName}"
hanoiExeName="hanoi_"$(date +%Y-%m-%d_%H-%M-%S)".exe"
sfxCreateCommandWindows="cat \
					${hanoiTempDir}/tripletower-sdk/tools/7zip/7zSDWinX64.sfx \
					${hanoiTempDir}/tripletower-sdk/tools/hanoi/bin/builder/sfx/sfx-config-windows.txt \
					${hanoiTempDir}/hanoi-installer-windows.7z \
					> ${hanoiReleaseDir}/${hanoiExeName}"

if [[ ${verbosity,,} == "on" ]]; then
	echo $sfxCreateCommandLinux
	echo $sfxCreateCommandWindows
fi

eval ${sfxCreateCommandLinux}
	sfxCreateResultLinux=$?
if [[ $sfxCreateResultLinux -ne 0 ]]; then
	echo "ERROR: An error returned from linux sfx creation command - '${sfxCreateCommandLinux}'"
	echo "Return code: '${sfxCreateResultLinux}'"
	exit 9;
fi

eval ${sfxCreateCommandWindows}
	sfxCreateResultWindows=$?
if [[ $sfxCreateResultWindows -ne 0 ]]; then
	echo "ERROR: An error returned from windows sfx creation command - '${sfxCreateCommandWindows}'"
	echo "Return code: '${sfxCreateResultWindows}'"
	exit 9;
fi

## Comment this few lines in to have all old instances deleted automatically in
## order to have always only the latest generated file
#
# echo deleting old installers...
# deleteCommandLinux="find ${hanoiReleaseDir} -maxdepth 1 -type f -regex '.*/hanoi-.*\.sfx?' ! -name ${hanoiSfxName} -delete"
# deleteCommandWindows="find ${hanoiReleaseDir} -maxdepth 1 -type f -regex '.*/hanoi-.*\.exe?' ! -name ${hanoiExeName} -delete"
# if [[ ${verbosity,,} == "on" ]]; then
#	echo $deleteCommandLinux
# 	echo $deleteCommandWindows
# fi
# eval ${deleteCommandLinux} && ${deleteCommandWindows}

echo "Installers for linux and windows created ..."