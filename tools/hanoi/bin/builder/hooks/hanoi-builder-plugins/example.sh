#!/bin/sh

#==============================================================================#
## Title    : This is a example plugin                                         #
## Desc     : Plugin for 'hanoi-builder.sh' to demonstrate how a plugin should #
##            look like.                                                       #
## Author   : Webtranet Affinity Group                                         #
## Date     : 2019-01-01                                                       #
## Version  : 1.0                                                              #
##-----------------------------------------------------------------------------#
## HISTORY                                                                     #
##    2019-01-01: Initial script creation                                      #
##-----------------------------------------------------------------------------#
## EXPCETED PARAMETER:                                                         #
## 1) tripletowersdk_working_path=$1                                           #
#==============================================================================#

tripletowersdk_working_path=$1
# here goes your code to make wonderful things
