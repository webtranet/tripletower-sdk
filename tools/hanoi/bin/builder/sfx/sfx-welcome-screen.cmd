@echo  浜様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
@echo  �                                                                         �
@echo  �                                HANOI                                    �
@echo  �                                                                         �
@echo  麺様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
@echo  �                                                                         �
@echo  � DESC                                                                    �
@echo  �                                                                         �
@echo  �    Installs tripletower-sdk and additional software according to what   �
@echo  �    is specified in the server config file in                            �
@echo  �    ./hanoi/conf/^<hostname^>.json                                         �
@echo  �                                                                         �
@echo  �                                                                         �
@echo  � SYNOPSIS                                                                �
@echo  �                                                                         �
@echo  �    hanoi ^<hostname^> [remote-os] [username] [password]                   �
@echo  �                                                                         �
@echo  �                                                                         �
@echo  �    ^<hostname^>                                                           �
@echo  �                                                                         �
@echo  �         The hostname you want to install software on. This parameter    �
@echo  �         is also used to find the config json file.                      �
@echo  �                                                                         �
@echo  �    remote-host-os                                                       �
@echo  �                                                                         �
@echo  �        Defaults to "local". Specifies if an remote installation is      �
@echo  �        started and what remote server os is to expect.                  �
@echo  �                                                                         �
@echo  �        Possible values:                                                 �
@echo  �                                                                         �
@echo  �            local                                                        �
@echo  �            linux                                                        �
@echo  �            windows                                                      �
@echo  �                                                                         �
@echo  �    username                                                             �
@echo  �                                                                         �
@echo  �        Defaults to "". If specified this username is used to connect    �
@echo  �        to the remote server.                                            �
@echo  �                                                                         �
@echo  �    password                                                             �
@echo  �                                                                         �
@echo  �        Defaults to "". If specified this is used as the user password   �
@echo  �        for given user to connect to the remote server. Passwordless     �
@echo  �        authentication is not supported.                                 �
@echo  �                                                                         �
@echo  �                                                                         �
@echo  � EXAMPLE                                                                 �
@echo  �                                                                         �
@echo  �    Local installation:                                                  �
@echo  �                                                                         �
@echo  �        hanoi tripletower.localhost                                      �
@echo  �                                                                         �
@echo  �                                                                         �
@echo  �    Remote installation:                                                 �
@echo  �                                                                         �
@echo  �        hanoi remoteserver.example.com windows johndoe secretpassword    �
@echo  �                                                                         �
@echo  藩様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
@echo  敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
@echo  � HINT - The temporary files will be deleted when you close this prompt   �
@echo  青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
@echo.
@echo off
@cmd /K "cd tripletower-sdk\tools\hanoi"