#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

// If hanoi is fully installed use TripleTowerSDK-sdk/libs, if remotely installed use libs folder directly aside instead
$libsFolder = is_dir("$sdkFolder/libs") ? "$sdkFolder/libs" :  dirname($rootFolder)."/libs";
require_once "$libsFolder/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

const HANOI_PLUGIN_CREATION_FORCED = true;
const PLUGIN_RECREATION_PREVENTION_LIST = ["com_dotnet", "exif", "gender", "oci8", "openssl", "pdo_mysql", "pdo_oci", "snmp", "win32service", "xdebug"];

echo "Starting hanoi php plugin generation".PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;

$phpExtensionsFolder = "$sdkFolder/etc/php/mods-available";
foreach( glob("$phpExtensionsFolder/*.ini", GLOB_NOSORT) as $phpExtensionPath)
{
	$pluginName = pathinfo($phpExtensionPath, PATHINFO_FILENAME);

	$pluginContent = file_get_contents("plugin.php.template");
	if( empty($pluginContent) )
		throw new \Exception("Could not load plugin.php.template");

	$keysToReplace = [":pluginName", ":pluginLinuxPackage"];
	$replacements = [$pluginName, "php-$pluginName"];
	if( ($pluginContent = str_replace($keysToReplace, $replacements, $pluginContent)) === false )
		throw new \Exception("Could not replace variables in plugin.php.template");

	$phpExtensionHanoiPluginPath = "$rootFolder/bin/plugins/hanoi_php_$pluginName.php";
	echo basename($phpExtensionHanoiPluginPath, ".php") . " plugin is being created -> ";

	// Create if it does not exist
	if( !file_exists( $phpExtensionHanoiPluginPath ) )
	{
		if( empty(file_put_contents($phpExtensionHanoiPluginPath, $pluginContent)) )
			throw new \Exception("Could not save new plugin to file '$phpExtensionHanoiPluginPath'");

		echo "created" . PHP_EOL;
		continue;
	}

	// Don't recreate if not forced
	if( !HANOI_PLUGIN_CREATION_FORCED )
	{
		echo "already exists" . PHP_EOL;
		continue;
	}

	// Don't recreate if in prevention list
	if( in_array( $pluginName, PLUGIN_RECREATION_PREVENTION_LIST ) )
	{
		echo "recreation prevented" . PHP_EOL;
		continue;
	}

	// OK recreate it
	if( empty(file_put_contents($phpExtensionHanoiPluginPath, $pluginContent)) )
		throw new \Exception("Could not recreate plugin and overwrite file '$phpExtensionHanoiPluginPath'");

	echo "recreated" . PHP_EOL;
}

echo PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;
echo "No more plugins to create!".PHP_EOL;