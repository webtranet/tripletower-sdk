#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

// If hanoi is fully installed use TripleTowerSDK-sdk/libs, if remotely installed use libs folder directly aside instead
$libsFolder = is_dir("$sdkFolder/libs") ? "$sdkFolder/libs" :  dirname($rootFolder)."/libs";
require_once "$libsFolder/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

const HANOI_PLUGIN_CREATION_FORCED = true;
const PLUGIN_RECREATION_PREVENTION_LIST = [];

echo "Starting hanoi wapps plugin generation".PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;

$pluginList =
[

	"TowerCore" =>
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/TowerCore.git"
	],

	"Webtra.About" =>
	[
		"packageSourcePath" => "https://gitlab.com/webtranet/Webtra.About.git"
	]
];

foreach( $pluginList as $pluginKey => $pluginInfos )
{
	$pluginContent = file_get_contents("plugin.wapps.template");
	if( empty($pluginContent) )
		throw new \Exception("Could not load plugin.wapps.template");

	$pluginName = strtolower( str_replace(" ", "_", $pluginKey) );
	$pluginClass = strtolower( str_replace([".", "-", " "], ["_", "_", "_"], $pluginKey) );
	if( !preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', "hanoi_$pluginClass") ) // Test if class name can be used -> http://php.net/manual/en/language.oop5.basic.php
		throw new \Exception("Plugin '$pluginClass' contains invalid characters for a class name's usage.");
	$packageSourcePath = $pluginInfos["packageSourcePath"];

	$keysToReplace = [":pluginName", ":pluginClass", ":packageSourcePath"];
	$replacements = [$pluginName, $pluginClass, $packageSourcePath];

	if( ($pluginContent = str_replace($keysToReplace, $replacements, $pluginContent)) === false )
		throw new \Exception("Could not replace variables in plugin.wapps.template");

	$wappsHanoiPluginPath = "$rootFolder/bin/plugins/hanoi_wapp_$pluginName.php";
	echo basename($wappsHanoiPluginPath, ".php") . " plugin is being created -> ";

	// Create if it does not exist
	if( !file_exists( $wappsHanoiPluginPath ) )
	{
		if( empty(file_put_contents($wappsHanoiPluginPath, $pluginContent)) )
			throw new \Exception("Could not save new plugin to file '$wappsHanoiPluginPath'");

		echo "created" . PHP_EOL;
		continue;
	}

	// Don't recreate if not forced
	if( !HANOI_PLUGIN_CREATION_FORCED )
	{
		echo "already exists" . PHP_EOL;
		continue;
	}

	// Don't recreate if in prevention list
	if( in_array( $pluginName, PLUGIN_RECREATION_PREVENTION_LIST ) )
	{
		echo "recreation prevented" . PHP_EOL;
		continue;
	}

	// OK recreate it
	if( empty(file_put_contents($wappsHanoiPluginPath, $pluginContent)) )
		throw new \Exception("Could not recreate plugin and overwrite file '$wappsHanoiPluginPath'");

	echo "recreated" . PHP_EOL;

}

echo PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;
echo "No more plugins to create!".PHP_EOL;