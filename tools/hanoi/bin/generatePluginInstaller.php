#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

// If hanoi is fully installed use TripleTowerSDK-sdk/libs, if remotely installed use libs folder directly aside instead
$libsFolder = is_dir("$sdkFolder/libs") ? "$sdkFolder/libs" :  dirname($rootFolder)."/libs";
require_once "$libsFolder/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

const HANOI_PLUGIN_CREATION_FORCED = true;
const PLUGIN_RECREATION_PREVENTION_LIST = [];

echo "Starting hanoi installer plugin generation".PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;

$pluginList =
[
	// sample: "parameters": "/S /D=\"C:\\tripletower-stack\\7-Zip\""
	"7zip" =>
	[
		"packageSourcePath" => "https://www.7-zip.org/a/7z1900-x64.exe",
		"parameters" => '/S',
		"linuxPackage" => "p7zip"
	],

	// Firefox commandline install parameters: https://firefox-source-docs.mozilla.org/browser/installer/windows/installer/FullConfig.html
	// sample: "parameters": "-ms /InstallDirectoryPath=\"C:\\tripletower-stack\\firefox\" /MaintenanceService=false /TaskbarShortcut=false /DesktopShortcut=false /StartMenuShortcut=true"
	"firefox" =>
	[
		"packageSourcePath" => "https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=en-US",
		"parameters" => "-ms",
		"linuxPackage" => "firefox"
	],

	// sample: "parameters": "/SP- /VERYSILENT /NORESTART /DIR=\"C:\\tripletower-stack\\heidisql\""
	"heidisql" =>
	[
		"packageSourcePath" => "https://www.heidisql.com/installers/HeidiSQL_10.1.0.5464_Setup.exe",
		"parameters" => " /SP- /VERYSILENT /NORESTART",
		"linuxPackage" => "emma"
	],

	// sample: "parameters": "/S /D=C:\\tripletower-stack\\notepad++"
	"notepadplusplus" =>
	[
		"packageSourcePath" => "https://notepad-plus-plus.org/repository/7.x/7.6.4/npp.7.6.4.Installer.x64.exe",
		"parameters" => '/S',
		"linuxPackage" => ""
	],

	// NetBeans commandline install parameters: http://wiki.netbeans.org/NetBeansInstallerTipsAndTricks
	// sample: "parameters": "--silent \"-J-Dnb-base.installation.location=C:\\tripletower-stack\\netbeans\"" --> no trailng backslash when using backshlashes!
	// sample: "parameters": "--silent \"-J-Dnb-base.installation.location=C:/tripletower-stack/netbeans/\""
	"netbeans" =>
	[
		"packageSourcePath" => "https://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-php-windows-x64.exe",
		"parameters" => "--silent",
		"linuxPackage" => "netbeans"
	]
];

foreach( $pluginList as $pluginKey => $pluginInfos )
{
	$pluginContent = file_get_contents("plugin.installer.template");
	if( empty($pluginContent) )
		throw new \Exception("Could not load plugin.installer.template");

	$pluginName = strtolower( str_replace(" ", "_", $pluginKey) );
	$pluginClass = strtolower( str_replace([".", "-", " "], ["_", "_", "_"], $pluginKey) );
	if( !preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', "hanoi_$pluginClass") ) // Test if class name can be used -> http://php.net/manual/en/language.oop5.basic.php
		throw new \Exception("Plugin '$pluginClass' contains invalid characters for a class name's usage.");
	$packageSourcePath = $pluginInfos["packageSourcePath"];
	$parameters = addslashes( isset($pluginInfos["parameters"]) ? $pluginInfos["parameters"] : "" );
	$linuxPackage = isset($pluginInfos["linuxPackage"]) ? $pluginInfos["linuxPackage"] : "";

	$keysToReplace = [":pluginName", ":pluginClass", ":packageSourcePath", ":parameters", ":linuxPackage"];
	$replacements = [$pluginName, $pluginClass, $packageSourcePath, $parameters, $linuxPackage];

	if( ($pluginContent = str_replace($keysToReplace, $replacements, $pluginContent)) === false )
		throw new \Exception("Could not replace variables in plugin.installer.template");

	$installerHanoiPluginPath = "$rootFolder/bin/plugins/hanoi_$pluginName.php";
	echo basename($installerHanoiPluginPath, ".php") . " plugin is being created -> ";

	// Create if it does not exist
	if( !file_exists( $installerHanoiPluginPath ) )
	{
		if( empty(file_put_contents($installerHanoiPluginPath, $pluginContent)) )
			throw new \Exception("Could not save new plugin to file '$installerHanoiPluginPath'");

		echo "created" . PHP_EOL;
		continue;
	}

	// Don't recreate if not forced
	if( !HANOI_PLUGIN_CREATION_FORCED )
	{
		echo "already exists" . PHP_EOL;
		continue;
	}

	// Don't recreate if in prevention list
	if( in_array( $pluginName, PLUGIN_RECREATION_PREVENTION_LIST ) )
	{
		echo "recreation prevented" . PHP_EOL;
		continue;
	}

	// OK recreate it
	if( empty(file_put_contents($installerHanoiPluginPath, $pluginContent)) )
		throw new \Exception("Could not recreate plugin and overwrite file '$installerHanoiPluginPath'");

	echo "recreated" . PHP_EOL;

}

echo PHP_EOL;
echo "------------------------------------".PHP_EOL;
echo PHP_EOL;
echo "No more plugins to create!".PHP_EOL;