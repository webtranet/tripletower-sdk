#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Tower\CliTower;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

echo "Starting hanoi deployment" . PHP_EOL;
echo PHP_EOL;

try
{
	// Check necessary privileges
	$privilegeResult = Helper::hasRootPrivileges();
	if( !$privilegeResult->isSuccess() )
		throw $privilegeResult->raiseError( TripleTowerError::ERR, "You have not the necessary permissions to run hanoi. Please re-run with root privileges.");

	// Check operating system support
	$currentOperatingSystem = OsNames::getCurrentOsType();
	if( $currentOperatingSystem !== OsNames::OS_TYPE_LINUX && $currentOperatingSystem !== OsNames::OS_TYPE_WINDOWS )
		throw new \Exception("Only Linux and Windows are currently supported by hanoi deployment");

	// Create tower
	$cliTower = new CliTower($rootFolder, $sdkFolder, $loader, [["p:", "pluginfolder:"], ["s::", "skip::"]]);

	// Load plugins
	$plugins = loadHanoiPlugins( $cliTower->cmdLineOptions->pluginfolder );

	if( count($plugins) > 0 )
	{
		setupPlugins($plugins, $cliTower->cmdLineOptions->skip);
		installPlugins($plugins, $cliTower->cmdLineOptions->skip);
		enablePlugins($plugins, $cliTower->cmdLineOptions->skip);
	}
	else
	{
		echo "No plugins found in '$pluginFolder'." . PHP_EOL;
	}

	echo "Finished deployment" . PHP_EOL;
}
catch(\Throwable $t)
{
	echo "ERROR: " . $t->getMessage() . PHP_EOL;
}

/**
 * @name loadHanoiPlugins
 * @desc Searches for all plugins which implement the IHanoiPlugin-interface and
 *       which are defined in the config file. Those get loaded and instantiated
 *       together with all of their dependencies.
 * @param string $pluginFolder The folder to search for hanoi plugins
 * @return IHanoiPlugin-array List of instantiated hanoi plugins
 */
function loadHanoiPlugins(string $pluginFolder)
{
	// Use default plugin folder if none specified
	$pluginFolder = ( empty($pluginFolder) ? $_SERVER["TT"]->tower->getBinFolder()."plugins" : $pluginFolder);

	$plugins = [];

	foreach($_SERVER["TT"]->conf as $pluginName => $pluginConfig)
	{
		// Only consider hanoi plugin entries
		if( strpos(strtolower($pluginName), "hanoi_") !== 0)
			continue;

		// Ignore all plugins with install=false entry
		if( isset($pluginConfig->install) && ( empty($pluginConfig->install) || strtolower($pluginConfig->install) === "false" ) )
			continue;

		$pluginFilePath="$pluginFolder/$pluginName.php";
		if( !is_file($pluginFilePath) )
			throw new FsError( FsError::FILE_NOT_FOUND, FsError::ERR, "File '$pluginFilePath' not found.");

		// Load plugin file
		require_once $pluginFilePath;

		// Check if class has been successfully loaded
		$className = strtolower( str_replace([".", "-", " "], ["_", "_", "_"], $pluginName) );
		if( !class_exists($className, false) )
			throw new \Exception("Class plugin '$className' not found but is defined in the configuration file.");

		// Check if class is a real plugin
		if( !is_subclass_of($className, 'TripleTowerSDK\\Hanoi\\IHanoiPlugin') )
			throw new \Exception("Class plugin '$className' is declared but is not derived from IHanoiPlugin.");

		// Instantiate plugin
		$plugins[] = new $className($pluginConfig);
	}

	// Check if all necessary plugins are loaded to solve dependencies
	$dependencies = [];
	foreach($plugins as $plugin)
		$dependencies = array_merge($dependencies, $plugin->getSetupDependencies(), $plugin->getInstallDependencies(), $plugin->getEnableDependencies());

	foreach(array_unique($dependencies) as $dependency)
	{
		$dependencyFound = false;
		foreach($plugins as $plugin)
		{
			if( get_class($plugin) === $dependency )
				$dependencyFound = true;
		}
		if( !$dependencyFound )
			throw new \Exception("Plugin '$dependency' not found. But it's a dependency. It has also to be defined in the configuration file.");
	}

	// All available plugins
	return $plugins;
}


function setupPlugins(array $plugins, string $skip="")
{
	$finishedPlugins = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getSetupDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Setup $pluginName... ";
				if( $skip !== "setup" && $skip !== $pluginName )
				{
					$plugins[$loop]->createTempPluginFolder();
					$plugins[$loop]->setup();
					$finishedPlugins[] = $pluginName;
					echo "OK" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}
}

function installPlugins(array $plugins, string $skip="")
{
	$finishedPlugins = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getInstallDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Installing $pluginName... ";
				if( $skip !== "install" && $skip !== $pluginName )
				{
					$plugins[$loop]->install();
					$finishedPlugins[] = $pluginName;
					echo "OK" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}
}

function enablePlugins(array $plugins, $skip="")
{
	$finishedPlugins = [];
	for($loop = 0, $numberFinishedPluginsBefore = 0; count($finishedPlugins) < count($plugins);  $loop++)
	{
		$pluginName = get_class($plugins[$loop]);

		// Don't execute already finished plugins
		if( array_search($pluginName, $finishedPlugins, true) === false )
		{
			// Execute plugin if there is no dependency left
			if( count(array_diff($plugins[$loop]->getEnableDependencies(), $finishedPlugins)) === 0 )
			{
				echo "Enabling $pluginName... ";
				if( $skip !== "enable" && $skip !== $pluginName )
				{
					$plugins[$loop]->enable();
					$plugins[$loop]->removeTempPluginFolder();

					$finishedPlugins[] = $pluginName;
					echo "OK" . PHP_EOL;
				}
				else
				{
					echo "Skipped" . PHP_EOL;
				}
			}
		}

		// If it was the last plugin, check all plugins again if
		// the dependency was solved. But also check that there is
		// no cyclic dependency
		if( $loop === count($plugins)-1 )
		{
			if( $numberFinishedPluginsBefore === count($finishedPlugins) )
			{
				throw new \Exception("Cyclic dependency found");
			}
			else
			{
				// Iterate through loop again
				$numberFinishedPluginsBefore = count($finishedPlugins);
				$loop = -1; // gets automatically incremented to 0 by for-loop
			}
		}
	}
}