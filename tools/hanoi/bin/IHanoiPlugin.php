<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Hanoi
{
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Network\Proxy;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;

	abstract class IHanoiPlugin
	{

		/***********************************
	    * PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		* @name $pluginName
		* @desc The name of the plugin itself.
		*/
		protected $pluginName;

		/**
		* @name $config
		* @desc An associative array containing all plugin specific values found in the hanoi.json config file.
		*/
		protected $config;

		/**
		* @name $configWindowsDefaults
		* @desc An associative array containing all the default values on a Windows used if not specified in the hanoi.json
		*       config file. The value null indicates a mandatory field.
		*/
		protected $configWindowsDefaults = null;

		/**
		* @name $configLinuxDefaults
		* @desc An associative array containing all the default values on a Linux used if not specified in the hanoi.json
		*       config file. The value null indicates a mandatory field.
		*/
		protected $configLinuxDefaults = null;

		/**
		* @name $caFileName
		* @desc The file name of the certificate authority certificate file to trust for downloading files to install.
		*		This file is being searched in the certs folder.
		*/
		protected $caFileName = "";

		/**
		* @name $proxy
		* @desc A proxy object which holds all information if a proxy is specified in the config.
		*/
		protected $proxy = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/**
		* @name __construct
		* @desc Instantiates the hanoi plugin.
		* @param array $config An associative array containing all plugin specific values found in the hanoi.json config file
		* @return IHanoiPlugin
		*/
		public function __construct(JsonConfig $config)
		{
			$this->pluginName = get_class($this);

			// Handle config
			if( is_null($this->configWindowsDefaults) )
			{
				throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "'$this->pluginName.php' has been implemented incorrectly. Please overwrite \$configWindowsDefaults.");
			}
			else if( is_null($this->configLinuxDefaults) )
			{
				throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "'$this->pluginName.php' has been implemented incorrectly. Please overwrite \$configLinuxDefaults.");
			}

			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
					$this->config = new JsonConfig($this->configWindowsDefaults);
				break;

				default:
					$this->config = new JsonConfig($this->configLinuxDefaults);

			}

			$this->config->merge($config);
			foreach($this->config as $configKey => $configValue)
			{
				if( is_null($configValue) )
					throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Mandatory config value '$configKey' could not be found in plugin '$this->pluginName'.");
			}

			// Handle Certificate Authority certificate file
			if( isset($_SERVER["TT"]->conf->caFileName) && $_SERVER["TT"]->conf->caFileName !== "" )
				$this->caFileName = $_SERVER["TT"]->conf->caFileName;

			// Handle proxy
			if( isset($_SERVER["TT"]->conf->proxy) )
			{
				$this->proxy = new Proxy($_SERVER["TT"]->conf->proxy->url ?? "",
											$_SERVER["TT"]->conf->proxy->port ?? 8080,
											$_SERVER["TT"]->conf->proxy->userName ?? get_current_user(),
											$_SERVER["TT"]->conf->proxy->password ?? "");
			}
		}

		/**
		* @name createTempPluginFolder
		* @desc Creates the folder %temp%/hanoi/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @return string Returns the newly created path followed by a slash
		*/
		public function createTempPluginFolder()
		{
			$hanoiFolder = sys_get_temp_dir()."/hanoi_plugins";
			if( file_exists($hanoiFolder) )
			{
				if(is_file($hanoiFolder) )
				{
					if( !unlink($hanoiFolder) )
						throw new FsError(FsError::UNLINK_FAILED, FsError::ERR, "There is a file called '$hanoiFolder' which could not be deleted in order to create a folder with the same name");
					if( !mkdir($hanoiFolder) )
						throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$hanoiFolder' could not be created");
				}
			}
			else
			{
				if( !mkdir($hanoiFolder) )
					throw new \Exception("The folder '$hanoiFolder' could not be created");
			}

			$pluginFolder = "$hanoiFolder/$this->pluginName";
			if( file_exists($pluginFolder) )
			{
				if(is_file($pluginFolder) )
				{
					if( !unlink($pluginFolder) )
						throw new FsError(FsError::UNLINK_FAILED, FsError::ERR, "There is a file called '$pluginFolder' which could not be deleted in order to create a folder with the same name");
					if( !mkdir($pluginFolder) )
						throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$pluginFolder' could not be created");
				}
			}
			else
			{
				if( !mkdir($pluginFolder) )
					throw new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "The folder '$pluginFolder' could not be created");
			}

			return "$pluginFolder/";
		}

		/**
		* @name getTempPluginFolder
		* @desc Creates the folder %temp%/hanoi/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @param bool $testIfExists Indicates if the existance of the folder should be performed
		* @return string Returns the newly created path followed by a slash
		*/
		public function getTempPluginFolder(bool $testIfExists = true)
		{
			$pluginFolder = sys_get_temp_dir()."/hanoi_plugins/$this->pluginName/";
			if( $testIfExists && !is_dir($pluginFolder) )
				throw new FsError(FsError::FILE_ALREADY_EXISTS, FsError::ERR, "Pluginfolder '$pluginFolder' is no folder but should have been created");

			return $pluginFolder;
		}

		/**
		* @name removeTempPluginFolder
		* @desc Removes the folder %temp%/hanoi/$pluginName/. If it already exists it leaves it as is. If there is file
		*       with the same name it gets deleted and the folder is created instead.
		* @return FsError Returns if it could delete the temp folder
		*/
		public function removeTempPluginFolder()
		{
			return Helper::unlink_recursive( $this->getTempPluginFolder() );
		}

		/**
		* @name getSetupDependencies
		* @desc Returns an array of plugin names which have to be setup before
		*       the implemented setup method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getSetupDependencies() : array;

		/**
		* @name setup
		* @desc Does all the stuff needed to install program/deamon/tool later on.
		* @param void
		* @return void
		*/
		abstract public function setup();

		/**
		* @name getInstallDependencies
		* @desc Returns an array of plugin names which have to be installed before
		*       the implemented install method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getInstallDependencies() : array;

		/**
		* @name install
		* @desc Installs the program/deamon/tool.
		* @param void
		* @return void
		*/
		abstract public function install();

		/**
		* @name getEnableDependencies
		* @desc Returns an array of plugin names which have to be enabled before
		*       the implemented enable method can be called.
		* @param void
		* @return string-array List of plugin names
		*/
		abstract public function getEnableDependencies() : array;

		/**
		* @name enable
		* @desc Does all the post installation work and starts the program/deamon/tool if necessary.
		* @param void
		* @return void
		*/
		abstract public function enable();


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function executeDBScripts( string $installScriptsFolder, string $dbInstallerUserName, string $dbInstallerPassword, string $dbHostName, string $dbName, string $dbUserName, string $dbPassword, array $replacements = [], string $dbScriptPrefix = "" )
		{
			if( empty($dbScriptPrefix) )
				$dbScriptPrefix = strtolower( $dbName ) . "db";

			// TODO only host: 'localhost' allowed under linux when 'unix_socket' plugin is set in user-table
			// additional info: https://kofler.info/root-login-problem-mit-mariadb/
			if( OsNames::isLinux() )
				$dbHostName = "localhost";

			$mariaDb = new \PDO("mysql:host=$dbHostName", $dbInstallerUserName, $dbInstallerPassword);

			// TODO Errors in install scripts do not throw or indicate any errors
			$mariaDb->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);

			$schemaStmt = $mariaDb->prepare("SELECT schema_name FROM information_schema.schemata WHERE SCHEMA_NAME = '$dbName'");
			if( $schemaStmt->execute() === false )
				throw new \Exception("Could not connect to mariadb host '$dbHostName' to determine if DB '$dbName' is already installed");

			// Find current version of database
			$version = "0.0.0";
			$update = false;
			if( $schemaStmt->fetch(\PDO::FETCH_ASSOC) === false )
			{
				$createStmt = $mariaDb->prepare("CREATE DATABASE IF NOT EXISTS $dbName /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */");
				if( $createStmt->execute() === false )
					throw new \Exception("Could not connect create database '$dbName' on mariadb host '$dbHostName'" . " because of " . $createStmt->errorCode() . PHP_EOL . print_r($createStmt->errorInfo(), true) );

				if( $mariaDb->exec("USE " . $dbName) === false )
					throw new \Exception("Could not use database '$dbName' on mariadb host '$dbHostName'");
			}
			else
			{
				if( $mariaDb->exec("USE " . $dbName) === false )
					throw new \Exception("Could not use database '$dbName' on mariadb host '$dbHostName'");

				$emptyDbStmt = $mariaDb->prepare("SELECT COUNT(DISTINCT table_name) AS numberoftables FROM information_schema.columns WHERE table_schema = '$dbName'");
				if( $emptyDbStmt->execute() === false )
					throw new \Exception("Could not connect create database '$dbName' on mariadb host '$dbHostName'" . " because of " . $emptyDbStmt->errorCode() . PHP_EOL . print_r($emptyDbStmt->errorInfo(), true) );

				if( $emptyDbStmt->fetch(\PDO::FETCH_ASSOC)["numberoftables"] > 0 )
				{
					$versionStmt = $mariaDb->prepare(
						"SELECT versions.version " .
						"FROM versions " .
						"WHERE versions.version_id = (SELECT MAX(versions.version_id) FROM versions);");
					if( $versionStmt->execute() === false )
						throw new \Exception("Could not read version table in '$dbName' on mariadb host '$dbHostName'" . " because of " . $versionStmt->errorCode() . PHP_EOL . print_r($versionStmt->errorInfo(), true) );
					$versionEntry = $versionStmt->fetch(\PDO::FETCH_ASSOC);
					if( $versionEntry === false || !isset($versionEntry["version"]) || empty($versionEntry["version"]) )
						throw new \Exception("Could not find a version entry in versions table in '$dbName' on mariadb host '$dbHostName'");
					$version = trim($versionEntry["version"]);
					$update = true;
				}
			}

			$matches = [];
			$versionPattern = "(?<major>[0-9]+)\\.(?<minor>[0-9]+)\\.(?<revision>[0-9]+)";
			if( !preg_match("/^$versionPattern/", $version, $matches) )
				throw new \Exception("The version found in version table in '$dbName' on mariadb host '$dbHostName' has no valid format (major.minor.revision)");
			$currentMajor    = (int)$matches["major"];
			$currentMinor    = (int)$matches["minor"];
			$currentRevision = (int)$matches["revision"];

			if( !$update )
			{
				$installScripts = glob("$installScriptsFolder/" . $dbScriptPrefix . "_installscript_*.sql", GLOB_NOSORT);
				if( !is_array($installScripts) || empty($installScripts) )
					throw new \Exception("Could not glob through install script folder '$installScriptsFolder' with filter '$dbScriptPrefix" . "_installscript_*.sql'");

				$scriptPathPattern = "/^" . str_replace("/", "\\/", preg_quote("$installScriptsFolder/$dbScriptPrefix") . "_installscript_") . "$versionPattern\\.sql$/";

				$initInstallScript = "";
				foreach( $installScripts as $installScript )
				{
					$filePath = Helper::normalizeFilePath($installScript);
					if( !is_file($filePath) )
						return false;

					$matches = [];
					if( !preg_match($scriptPathPattern, $filePath, $matches) )
						continue;

					$major    = (int)$matches["major"];
					$minor    = (int)$matches["minor"];
					$revision = (int)$matches["revision"];

					if( $currentMajor > $major )
						continue;

					if( $currentMajor < $major )
					{
						$currentMajor     = $major;
						$currentMinor    = $minor;
						$currentRevision = $revision;
						$initInstallScript = $installScript;
						continue;
					}

					if( $currentMinor > $minor )
						continue;

					if( $currentMinor < $minor )
					{
						$currentMinor    = $minor;
						$currentRevision = $revision;
						$initInstallScript = $installScript;
						continue;
					}

					if( $currentRevision < $revision )
					{
						$currentRevision = $revision;
						$initInstallScript = $installScript;
					}
				}
			}

			$installScripts = glob("$installScriptsFolder/" . $dbScriptPrefix . "_updatescript_*.sql", GLOB_NOSORT);
			if( !is_array($installScripts) )
				throw new \Exception("Could not glob through update script folder '$installScriptsFolder'");

			if( !empty($installScripts) )
			{
				$scriptPathPattern = "/^" . str_replace("/", "\\/", preg_quote("$installScriptsFolder/$dbScriptPrefix") . "_updatescript_") . "$versionPattern\\.sql$/";

				// Filter already installed scripts
				$installScripts = array_filter($installScripts, function($value) use ($scriptPathPattern, $currentMajor, $currentMinor, $currentRevision)
				{
					// If it's not a file
					$filePath = Helper::normalizeFilePath($value);
					if( !is_file($filePath) )
						return false;

					// If pattern is not matching
					$matches = [];
					if( !preg_match($scriptPathPattern, $filePath, $matches) )
						return false;

					// If script is already installed
					$major    = (int)$matches["major"];
					$minor    = (int)$matches["minor"];
					$revision = (int)$matches["revision"];

					if( $currentMajor < $major )
						return true;
					if( $currentMajor > $major )
						return false;

					if( $currentMinor < $minor )
						return true;
					if( $currentMinor > $minor )
						return false;

					if( $currentRevision < $revision )
						return true;
					if( $currentRevision > $revision )
						return false;

					return false;
				});

				$sortWorked = usort($installScripts, function($scriptLeft, $scriptRight) use ($scriptPathPattern)
				{
					// Get versions
					$matchesLeft = [];
					if( !preg_match($scriptPathPattern, $scriptLeft, $matchesLeft) )
						throw new \Exception("Could not sort parse version of installscript '$scriptLeft'");
					$majorLeft    = (int)$matchesLeft["major"];
					$minorLeft    = (int)$matchesLeft["minor"];
					$revisionLeft = (int)$matchesLeft["revision"];

					$matchesRight = [];
					if( !preg_match($scriptPathPattern, $scriptRight, $matchesRight) )
						throw new \Exception("Could not sort parse version of installscript '$scriptRight'");
					$majorRight    = (int)$matchesRight["major"];
					$minorRight    = (int)$matchesRight["minor"];
					$revisionRight = (int)$matchesRight["revision"];

					// Determine which version is higher
					if( $majorLeft < $majorRight )
						return -1;
					if( $majorLeft > $majorRight )
						return 1;

					if( $minorLeft < $minorRight )
						return -1;
					if( $minorLeft > $minorRight )
						return 1;

					if( $revisionLeft < $revisionRight )
						return -1;
					if( $revisionLeft > $revisionRight )
						return 1;

					return 0;
				});

				if( !$sortWorked )
					throw new \Exception("Could not sort found installscripts in '$installScriptsFolder' because of usort error");
			}

			if( !$update )
				array_unshift($installScripts, $initInstallScript);

			foreach($installScripts as $installScript)
			{
				// Read install script
				$installScriptContent = file_get_contents($installScript);
				if( $installScriptContent === false)
					throw new \Exception("Could not read install script '$installScript' in order to create/update '" . strtolower($dbName) . "'");

				// Replace tags in install script
				if( count($replacements) > 0 )
					$installScriptContent = strtr( $installScriptContent, $replacements);

				// Install database
				$installScriptStmt = $mariaDb->prepare($installScriptContent);
				if( $installScriptStmt->execute() === false )
					throw new \Exception("Could not execute install script for '" . strtolower($dbName) . "' in '$dbName' on mariadb host '$dbHostName'" . " because of " . $installScriptStmt->errorCode()  . PHP_EOL . print_r($installScriptStmt->errorInfo(), true) );

				// TODO if not exist next statement fails -> but why?
				$installScriptStmt->closeCursor();
			}

			// create user
			//
			// TODO  instead of [$sql =...'@'localhost'...] use $dbHostName
			// problem now: connection not possible
			$sql = "GRANT SELECT, EXECUTE, SHOW VIEW, DELETE, INSERT, UPDATE ON `{$dbName}`.* TO '$dbUserName'@'localhost' IDENTIFIED BY '$dbPassword';";
			$createUserStmt = $mariaDb->prepare($sql);
			if( $createUserStmt->execute() === false )
				throw new \Exception("Error creating and granting user '$dbUserName' because of " . $createUserStmt->errorCode() . PHP_EOL . print_r($createUserStmt->errorInfo(), true) );

		}
	}
}