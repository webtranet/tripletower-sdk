#!/usr/bin/env php
<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Application\OsNames;
use TripleTowerSDK\Tower\CliTower;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Application\ConsoleGui;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname($rootFolder));

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

try
{
	// Check operating system support
	$currentOperatingSystem = OsNames::getCurrentOsType();
	if( $currentOperatingSystem !== OsNames::OS_TYPE_LINUX && $currentOperatingSystem !== OsNames::OS_TYPE_WINDOWS )
		throw new \Exception("Only Linux and Windows are currently by hanoi deployment");

	// Create tower
	$cliTower = new CliTower($rootFolder, $sdkFolder, $loader, [["p:", "pluginfolder:"], ["s::", "skip::"]]);

	// Load plugins
	// $plugins = loadHanoiPlugins( $cliTower->cmdLineOptions->pluginfolder );
	$consoleGui = new ConsoleGui(50, 25);

	$consoleGui->writeToBuffer(20, 11, "HanoiGui");
	$consoleGui->writeToBuffer(16, 12, "Feature to come", true);
	sleep(1);

	$loop = 0;
	for($running = true; $running === true; )
	{
		if( handleInput($consoleGui) !== null )
			$running = false;
		calcScene($consoleGui);
		drawScene($consoleGui);
		usleep((1/5) * 1000000); // 5 fps
		$loop++;
		if($loop >= 40)
			$running = false;
	}

	echo "Finished deployment".PHP_EOL;
}
catch(\Throwable $t)
{
	echo "ERROR: " . $t->getMessage() . PHP_EOL;
}

function handleInput(ConsoleGui $consoleGui)
{
	$keys = $consoleGui->getKeysPressed();
	return count($keys) > 0 ? $keys[0] : null;
}

function calcScene(ConsoleGui $consoleGui)
{
	$consoleGui->writeToBuffer(random_int(0, 79), random_int(0, 39), chr(random_int(97, 122)));
}

function drawScene(ConsoleGui $consoleGui)
{
	$consoleGui->drawFrame();
}

/**
 * @name loadHanoiPlugins
 * @desc Searches for all plugins which implement the IHanoiPlugin-interface.
 *       Those get loaded and instantiated together with all of their
 *       dependencies.
 * @param string $pluginFolder The folder to search for hanoi plugins
 * @return IHanoiPlugin-array List of instantiated hanoi plugins
 */
function loadHanoiPlugins(string $pluginFolder)
{
	// Use default plugin folder if none specified
	$pluginFolder = ( empty($pluginFolder) ? $_SERVER["TT"]->tower->getBinFolder()."plugins" : $pluginFolder);

	$plugins = [];

	foreach($_SERVER["TT"]->conf as $pluginName => $pluginConfig)
	{
		// Only consider hanoi plugin entries
		if( strpos(strtolower($pluginName), "hanoi_") !== 0)
			continue;

		$pluginFilePath="$pluginFolder/$pluginName.php";
		if( !is_file($pluginFilePath) )
			throw new FsError( FsError::FILE_NOT_FOUND, FsError::ERR, "File '$pluginFilePath' not found.");

		// Load plugin file
		require_once $pluginFilePath;

		// Check if class has been successfully loaded
		$className = strtolower( str_replace([".", "-", " "], ["_", "_", "_"], $pluginName) );
		if( !class_exists($className, false) )
			throw new \Exception("Class plugin '$className' not found but is defined in the configuration file.");

		// Check if class is a real plugin
		if( !is_subclass_of($className, 'TripleTowerSDK\\Hanoi\\IHanoiPlugin') )
			throw new \Exception("Class plugin '$className' is declared but is not derived from IHanoiPlugin.");

	}

	// All available plugins
	return $plugins;
}