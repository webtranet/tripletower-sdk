                       _    _          _   _  ____ _____ 
                      | |  | |   /\   | \ | |/ __ \_   _|
                      | |__| |  /  \  |  \| | |  | || |  
                      |  __  | / /\ \ | . ` | |  | || |  
                      | |  | |/ ____ \| |\  | |__| || |_ 
                      |_|  |_/_/    \_\_| \_|\____/_____|
                 
-------------------------------------------------------------------------------

For installing remotely on linux and windows systems ssh is used. And the
following requirements have to be met:
- Name resolution has to work via dns or Samba has to be installed for broad-
  cast NetBIOS name requests.
- Port 22 (SSH) is not blocked by a firewall.
- Remote system has to support SSH-2
- Remote system's ssh key has to be already in key cache
- Remote system's folder "/tmp" has to be writeable
- A stable internet connection for your package manager in order to install php