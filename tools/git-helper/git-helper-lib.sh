#
# TripleTower SDK - https://webtranet.online/tripletower-sdk
#
# @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
# @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
# @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
#

# Environment setup
# 
# Saves the current directory and sets a cleanup trap
function cleanup
{
	cd ${currentDir}
}
trap cleanup EXIT
currentDir=$(pwd)


# Function to execute git update-server-info on a given path
#
# $1 - path to repository
# return - void
function updateServerInfo
{
	local repoPath=$1
	if [[ ! -d ${repoPath} ]]; then
		echo "ERROR: Script could not find git repository '${repoPath}' to exec git update-server-info"
		exit 2;
	fi
	cd "${repoPath}"

	local updateServerInfoCommand="git update-server-info"
	eval ${updateServerInfoCommand}
	local gitUpdateServerInfoResult=$?

	if [[ $gitUpdateServerInfoResult -ne 0 ]]; then
		echo "ERROR: Could not update-server-info on ${repoPath}"
		echo "Command '${updateServerInfoCommand}' returned '${gitUpdateServerInfoResult}'"
		exit 3;
	fi
	
	local repoRootPath=$(extractRepoRootpath $repoPath)
	echo "Updated $(basename $repoRootPath)"
}


# Function to extract the root path of the repository
#
# Examples:
# /my/path/to/repo.git -> /my/path/to/repo/.git
# /my/path/to/repo/.git -> /my/path/to/repo
# 
# $1 - path to repository
# return - path to path to repository root
function extractRepoRootpath
{
	local repoRootPath=$1
	local repoName=$(basename $repoRootPath)
	if [[ ${repoName,,} == ".git" ]]; then
		echo "$(dirname $repoRootPath)"
	else
		echo $repoRootPath
	fi
}