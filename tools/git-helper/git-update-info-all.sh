#!/bin/sh
#
# TripleTower SDK - https://webtranet.online/tripletower-sdk
#
# @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
# @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
# @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
#

# Include libs and environement setup
. $(dirname $0)/git-helper-lib.sh
currentScriptDir=$(dirname $0)
cd ${currentScriptDir}

# Script switches
verbosity="on"

# Script can only be called if it lies within a tripletower-sdk git repository
tripletowerSdkGitDir=$(git rev-parse --git-dir 2>&1)
gitDirFoundResult=$?
if [[ $gitDirFoundResult -ne 0 ]]; then
  echo "ERROR: Script can only be called if it lies within a tripletower-sdk git repository"
  exit 1;
fi

# Find tripletower 
tripletowerGitDir=$(extractRepoRootpath $tripletowerSdkGitDir)/../tripletower
if [[ ! -d ${tripletowerGitDir} ]]; then
	tripletowerGitDir=$(extractRepoRootpath $tripletowerSdkGitDir)/../tripletower.git
	if [[ ! -d ${tripletowerGitDir} ]]; then
		echo "ERROR: Script could not find folder '${tripletowerGitDir}' to exec git update-server-info"
		exit 2;
	fi
fi

updateServerInfo ${tripletowerSdkGitDir}
updateServerInfo ${tripletowerGitDir}

for wappFolderPath in "${tripletowerGitDir}/wappstower"/*/; do
    updateServerInfo ${wappFolderPath}
done

