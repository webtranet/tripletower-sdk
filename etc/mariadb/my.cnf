# This MariaDB options file has been created for a triple tower instance which
# also usually runs an apache2 webserver with php alongside. Please be aware
# that if you change this options file it will be automatically overwritten by
# the tripletower auto-update feature. To prevent this behaviour add a section
# called tripletower and activate the no_autoupdate_alteration switch which is
# turned off by default.
#
# The instance is assumed to be run on a small sized server like a Raspi 3
# Model B with 1G of memory or above and it is assumed to run in a none
# replicating environment.
# Tripletower uses innodb tables only, so there won't be any options for
# replications or other engines than innodb.
#
# More information can be obtained at:
# https://mariadb.com/kb/en/library/mysqld-options/
# https://mariadb.com/kb/en/library/server-system-variables/
# https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables/

[tripletower]
# Uncomment if you prevent tripletowers' auto update feature to revert your
# changes. Beware that you have to look after the config in future for
# yourself!
#no_autoupdate_alteration

[client]
#password = your_password

[mysql]
no_auto_rehash

[mysqld]
##
# 1. Connections
#
bind_address = $mySqlBindAdress 				# Default: 0.0.0.0
max_connections = 200							# Default: 151


##
# 2. General settings
#
default_storage_engine = InnoDB					# Default: InnoDB
lower_case_table_names = 1						# Default: Linux=0, Windows=1, OSX=2
character_set_server = utf8						# Default: latin1
collation_server = utf8_general_ci				# Default: latin1_swedish_ci
transaction_isolation = REPEATABLE-READ 		# Default: REPEATABLE-READ
event_scheduler = ON							# Default: OFF


##
# 3. Information & Administration
#

# Common
log_basename = '$serverName'					# Default: %hostname%, but no dots and slashes allowed
log_warnings = 1								# Default: 1 < MariaDB 10.2.3 => 2
general_log = $activateLogging					# Default: FALSE

# Slowlog
slow_query_log = $activateLogging				# Default: OFF
long_query_time = 15							# Default: 10
min_examined_row_limit = 0						# Default: 0
log_queries_not_using_indexes = ON				# Default: OFF

# Binlog
#log_bin										# Default: OFF - uncomment to activate
expire_logs_days = 10							# Default: 0 (UBUNTU 10 DAYS)
max_binlog_size = 100M							# Default: 1G (UBUNTU 100M)
binlog_format = mixed							# Default: mixed (good for non replicating server)
binlog_direct_non_transactional_updates = OFF	# Default: OFF (good for non replicating server)


##
# 4. Security
#

# Users & Authentication
secure_auth = TRUE								# Default: FALSE
safe_user_create = TRUE							# Default: FALSE
allow_suspicious_udfs = FALSE					# Default: FALSE

# Import/Export
local_infile = OFF								# Default: ON

# SSL
ssl-key = "$privateKeyPath"						# Default: ""
ssl-cert = "$certificatePath"					# Default: None
ssl-ca = "$caCertificatePath"					# Default: ""


##
# 5. Performance
#

# Common
sort_buffer_size = 16M							# Default: 2M (UBUNTU 16M)
max_allowed_packet = 16M						# Default: 4M < MariaDB 10.2.4 => 16M
table_open_cache = 2000							# Default: 400 < MariaDB 10.1.7 => 2000
net_buffer_length = 16K							# Default: 16K
thread_stack = 290K								# Default: ~290K
thread_cache_size = 256							# Default: 0 < MariaDB 10.2.0 => Auto
query_cache_size = 0							# Default: 0 < MariaDB 10.1.7 => 1M

# InnoDB
innodb_strict_mode = ON							# Default: OFF < MariaDB 10.2.2 => ON
innodb_file_per_table = ON						# Default: ON
innodb_fast_shutdown = 0						# Default: 1
innodb_data_file_path = ibdata1:12M:autoextend	# Default: ibdata1:12M:autoextend, size must not be grater than 12M
innodb_buffer_pool_size = 512M					# Default: 128M - using 50% of the raspie3b memory
innodb_log_buffer_size = 32M					# Default: 8M < MariaDB 10.1.9 => 16M
innodb_log_file_size = 128M						# Default: 48M < MariaDB 10.5 => 96M - using 25% of innodb_buffer_pool_size
innodb_flush_log_at_trx_commit = 1				# Default: 1 - needed for acid compliance
innodb_lock_wait_timeout = 120					# Default: 50

[mysqldump]
#quick											# Default: quick
#quote_names									# Default: quote_names
max_allowed_packet = 1024M						# Default: 24M

