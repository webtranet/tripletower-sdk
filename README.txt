 _______   _       _   _______                            _____ _____  _  __
|__   __| (_)     | | |__   __|                          / ____|  __ \| |/ /
   | |_ __ _ _ __ | | ___| | _____      _____ _ __  ___ | (___ | |  | | ' / 
   | | '__| | '_ \| |/ _ \ |/ _ \ \ /\ / / _ \ '__||___| \___ \| |  | |  <  
   | | |  | | |_) | |  __/ | (_) \ V  V /  __/ |         ____) | |__| | . \ 
   |_|_|  |_| .__/|_|\___|_|\___/ \_/\_/ \___|_|        |_____/|_____/|_|\_\
            | |                                                             
            |_|                                                             

-------------------------------------------------------------------------------

  What is it?
  -----------

  The Tripletower-SDK is an open but not free software development kit which
  enables a developer to create an application for the tripletower web desktop
  software or a tripletower cli application.


  The Latest Version
  ------------------

  Details of the latest version can be found on the Webtranet Affinity Groups's
  Gitlab repository: https://gitlab.com/webtranet/tripletower-sdk


  Documentation
  -------------

  The documentation available as of the date of this release can be found on
  the Webtranet Affinity Group's website in the documentation section:
  https://webtranet.online/wappstower/Webtra.Docs


  Installation
  ------------

  We recommend using the hanoi software installer which is currently available
  for linux and windows in the tripletower-sdk.


  Licensing
  ---------

  Please see the file called LICENSE.


  Contacts
  --------

     o If you would like to purchase support for using TripleTower-SDK please
       write a mail to `sales@webtranet.online´ so we can get in touch with you
       and write you an offering.

     o If you want to participate in actively developing TripleTower-SDK please
       write a mail to `contribute@webtranet.online´ to get information about a
       Contributor License Agreement (CLA).