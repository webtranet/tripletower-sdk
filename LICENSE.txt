
  Open but Not Free Software License Version 1 (ONFSLv1)
  ======================================================

  Copyright (c) 2025 Webtranet Affinity Group

  Licensor: Webtranet Affinity Group

  Software: TripleTower-SDK

  License Grant
  -------------

  Licensor hereby grants to each recipient of the Software (“you”) a non-
  exclusive, non-transferable, royalty-free license, under all of the
  Licensor’s copyright and patent rights, to use, copy and display the
  Software and its Source Code, subject to the Use Limitation and the
  conditions set forth below.


  Use Limitation
  --------------

  The license granted above allows use for non-commercial purposes only. This
  means you can use it for private purposes and for your business, but your
  are not allowed to sell, resell, distribute, redistribute, provide services,
  get fees, donations, or payments via ads for the source code, the Software
  or any derivative work. You are not allowed to make income in any way in
  which the software or the code is involved.


  Conditions
  ----------

  Copies of the source code, the software or other forms must include a copy
  of this license document to be provided in a reasonable manner.
  Any redistribution is prohibited except for copies within your legal entity
  to administer.


  Trademarks
  ----------

  This license does not grant you any right in the trademarks, service marks,
  brand names or logos of Licensor.


  DISCLAIMER
  ----------

  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OR CONDITION, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM ALL
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE.


  Termination
  -----------

  If you violate the terms of this license, your rights will terminate
  automatically and will not be reinstated without the prior written consent of
  Licensor.