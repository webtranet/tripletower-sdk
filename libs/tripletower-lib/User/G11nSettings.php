<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User
{

	use TripleTowerSDK\Error\TripleTowerError;

	class G11nSettings
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $language;
		protected $location;
		protected $timezone;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $g11nSettingsId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$g11nSettingsInfo = $serviceTowerDb->getG11nSettingsInfos($g11nSettingsId);
			if( TripleTowerError::isError($g11nSettingsInfo) )
				throw $g11nSettingsInfo;

			$this->language = $g11nSettingsInfo["languagekey"];
			$_SERVER["TT"]->g11n->changeLanguage( $this->language )->throwIfNotSuccess();

			$this->location = $g11nSettingsInfo["locationkey"];
			$_SERVER["TT"]->g11n->changeLocation( $this->location )->throwIfNotSuccess();

			$this->timezone = $g11nSettingsInfo["timezonekey"];
			$_SERVER["TT"]->g11n->changeTimezone( $this->timezone )->throwIfNotSuccess();
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}