<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\User\UserSpace\InstalledWapp;

	class UserSpace
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $displayName     = "";
		protected $backgroundUrl   = "";
		protected $backgroundColor = "";
		protected $windowColor     = "";
		protected $tileShape       = "";
		protected $tileSize        = 0;

		protected $installedWapps = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $userSpaceId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$userSpacesInfos = $serviceTowerDb->getUserSpaceInfos($userSpaceId);
			if( TripleTowerError::isError($userSpacesInfos) )
				throw $userSpacesInfos;

			$this->displayName     = $userSpacesInfos["displayname"];
			$this->backgroundUrl   = $userSpacesInfos["backgroundurl"];
			$this->backgroundColor = $userSpacesInfos["backgroundcolor"];
			$this->windowColor     = $userSpacesInfos["windowcolor"];
			$this->tileShape       = $userSpacesInfos["tileshape"];
			$this->tileSize        = (int)$userSpacesInfos["tilesize"];

			$installedWappsIds = $serviceTowerDb->getInstalledWappIds($userSpaceId);
			if( TripleTowerError::isError($installedWappsIds) )
				throw $installedWappsIds;

			foreach($installedWappsIds as $installedWappsId)
				$this->installedWapps[] = new InstalledWapp((int)$installedWappsId["installedwapp_id"]);
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}