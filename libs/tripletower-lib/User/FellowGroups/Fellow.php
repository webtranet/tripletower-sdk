<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\FellowGroups
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\User;
	use TripleTowerSDK\Date\DateTime;

	class Fellow
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $userName         = User::DEFAULT_USERNAME;
		protected $displayName      = "";
		protected $avatarUrl        = "";
		protected $lastActive       = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $fellowId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$fellowInfos = $serviceTowerDb->getFellowInfos($fellowId);
			if( TripleTowerError::isError($fellowInfos) )
				throw $fellowInfos;

			$this->userName    = (string)$fellowInfos["username"];
			$this->displayName = (string)$fellowInfos["displayname"];
			$this->avatarUrl   = (string)$fellowInfos["avatarurl"];
			$this->lastActive  = new DateTime($fellowInfos["lastactive"]);
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}