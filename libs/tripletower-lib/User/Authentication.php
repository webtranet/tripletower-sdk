<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User
{

	use TripleTowerSDK\Error\TripleTowerError;

	class Authentication
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $authenticationSystemName = "";
		protected $authenticationName       = "";
		protected $authenticationTrials     = 0;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $authenticationId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$authenticationInfos = $serviceTowerDb->getAuthenticationInfos($authenticationId);
			if( TripleTowerError::isError($authenticationInfos) )
				throw $authenticationInfos;

			$this->authenticationSystemName = $authenticationInfos["authenticationsystemname"];
			$this->authenticationName     = $authenticationInfos["authenticationname"];
			$this->authenticationTrials   = (int)$authenticationInfos["authenticationtrials"];
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}