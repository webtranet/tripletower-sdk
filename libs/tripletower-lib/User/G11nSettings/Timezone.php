<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\G11nSettings
{
	class Timezone
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const G11N_DEFAULT_TIMEZONE = "UTC";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}