<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace\InstalledWapp
{

	use Parsedown;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Tower;

	class WappInfo
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/
		const CHANGELOG_DEFAULT_NAME   = "CHANGELOG";
		const README_DEFAULT_NAME      = "README";
		const WAPP_INFOFILE_ENDINGS    = ["", "html", "md", "markdown", "md", "mdown", "mkd", "mkdn", "php", "txt"];

		const META_INVOCATION_HOOK     = "metaInvocationHook";
		const PRE_AUTHENTICATION_HOOK  = "preAuthenticationHook";
		const POST_AUTHENTICATION_HOOK = "postAuthenticationHook";
		const PRE_AUTHORIZATION_HOOK   = "preAuthorizationHook";
		const POST_AUTHORIZATION_HOOK  = "postAuthorizationHook";
		const PRE_EXECUTION_HOOK       = "preExecutionHook";
		const POST_EXECUTION_HOOK      = "postExecutionHook";

		// Self renderer - if both given, localSelfRenderer preceeds
		const GLOBAL_SELF_RENDERER     = "globalSelfRenderer";
		const LOCAL_SELF_RENDERER      = "localSelfRenderer";

		// Render hooks
		const GLOBAL_PRE_RENDER_HOOK   = "globalPreRenderHook";
		const GLOBAL_POST_RENDER_HOOK  = "globalPostRenderHook";
		const LOCAL_PRE_RENDER_HOOK    = "localPreRenderHook";
		const LOCAL_POST_RENDER_HOOK   = "localPostRenderHook";

		// API hooks
		const GLOBAL_PRE_APICALL_HOOK  = "globalPreApiCallHook";
		const GLOBAL_POST_APICALL_HOOK = "globalPostApiCallHook";
		const LOCAL_PRE_APICALL_HOOK   = "localPreApiCallHook";
		const LOCAL_POST_APICALL_HOOK  = "localPostApiCallHook";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/
		protected $name             = "";                          // string
		protected $version          = "";                          // string
		protected $versionName      = "";                          // string
		protected $versionDate      = "";                          // string
		protected $shortDescription = "";                          // string
		protected $longDescription  = true;                        // bool/string -> turns always to string
		protected $changeLog        = true;                        // bool/string -> turns always to string
		protected $developer        = [ "name" => "", "url" => "", "email" => "" ]; // object containing strings - if url starting with "/", the servername of this tower is prepended
		protected $mediaUrls        = [];                          // array containing strings - if strings starting with "/", the servername of this tower is prepended
		protected $categories       = ["Miscellaneous"];           // array
		protected $requirements     = [];                          // object
		protected $icons            = [];                          // object containing strings - if strings starting with "/", the servername of this tower is prepended
		protected $visible          = true;                        // bool
		protected $permissions      = [];                          // array
		protected $license          = "No license specified";      // string
		protected $manifestVersion  = 1;                           // unsigned int
		protected $startType        = "windowed";                  // string
		protected $startHeight      = 600;                         // unsigned int
		protected $startWidth       = 800;                         // unsigned int
		protected $resizable        = true;                        // bool
		protected $fullscreenable   = false;                       // bool
		protected $appcachePath     = "";                          // string
		protected $dependencies     = [ "css" => [], "js" => [] ]; // array
		protected $views            = [ "default_view" => "" ];    // object
		protected $api              = [];                          // object


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct( array $metaInfos )
		{
			// Check if metaInfos are complete
			$classPropertyNames = array_keys( get_class_vars(static::class) );
			foreach( $classPropertyNames as $propertyName )
				if( !isset($metaInfos[$propertyName]) )
					throw new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Property '$propertyName' is missing in WappInfo");

			foreach($metaInfos as $key => $value)
			{
				switch($key)
				{
					case "developer":
					{
						if( !isset($value["url"]) || !is_string($value["url"]) || !Helper::stringStartsWith($value["url"], "https") )
							$value["url"] = "No website";
					}
					break;

					case "mediaUrls":
					{
						for($loop = 0; $loop < count($value); $loop++)
						{
							if( !empty($value[$loop]) )
								$value[$loop] = Helper::getTowerUrl() . "/" . $metaInfos["name"] . "/" . $value[$loop];
						}
					}
					break;

					case "icons":
					{
						foreach($value as $iconResolution => $iconUrl)
						{
							if( !empty($iconUrl) )
								$value[$iconResolution] = Helper::getTowerUrl() . "/" . $metaInfos["name"] . "/" . $iconUrl;
						}
					}
					break;

					case "views":
					{
						if( !isset($value["default_view"]) )
							throw new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Views must have a 'default_view' property in the root");

						$this->checkViewsProperties($value)->throwIfNotSuccess();
					}
					break;
				}

				$this->$key = Helper::convertHashesToObjects($value);
			}
		}

		public function __get(string $key)
		{
			if( $key === "changeLog" )
			{
				switch($this->$key)
				{
					case false:
						return $this->generateRootFileInfoFromFile("");
					case true:
						return $this->generateRootFileInfoFromFile(self::CHANGELOG_DEFAULT_NAME);
					default:
						return $this->generateRootFileInfoFromFile($this->$key);
				}
			}

			if( $key === "longDescription" )
			{
				switch($this->$key)
				{
					case false:
						return $this->generateRootFileInfoFromFile("");
					case true:
						return $this->generateRootFileInfoFromFile(self::README_DEFAULT_NAME);
					default:
						return $this->generateRootFileInfoFromFile($this->$key);
				}
			}

			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}

		public function getFittingIconUrl(int $favoredSize)
		{
			if(($favoredSize = filter_var($favoredSize, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>1)))) === false)
				return null;

			$bestSize = null;
			foreach(array_keys(get_object_vars($this->icons)) as $availableIconSize)
			{
				$iconSizeInt = filter_var($favoredSize, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>1)));
				if($iconSizeInt !== false && ($bestSize === null || abs($favoredSize-$bestSize) > abs($favoredSize-$iconSizeInt)))
					$bestSize = $availableIconSize;
			}

			return $this->icons->$bestSize;
		}

		/**
		 * This method returns the view object determined by the selected
		 * contentName.
		 *
		 * @param string $contentName
		 * @return stdClass|TripleTowerError
		 */
		public function getViewEntry(string $contentName)
		{
			$contentNameParts = explode("/", $contentName);

			// Find view entry by content name
			$currentView = $this;
			foreach($contentNameParts as $contentNamePart)
			{
				if( !isset($currentView->views->$contentNamePart))
					return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "The requested view '$contentName' in '$contentProvider' is not available.");

				$currentView = $currentView->views->$contentNamePart;
			}

			return $currentView;
		}

		/**
		 * This method returns the api object determined by the selected
		 * contentName.
		 *
		 * @param string $contentName
		 * @return stdClass|TripleTowerError
		 */
		public function getApiEntry(string $contentName)
		{
			$contentNameParts = explode("/", $contentName);

			// Find api by content name
			$currentApi = $this->api;
			foreach($contentNameParts as $contentNamePart)
			{
				if( !isset($currentApi->$contentNamePart))
					return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "The requested api '$contentName' in '{$this->name}' is not available.");

				$currentApi = $currentApi->$contentNamePart;
			}

			// Requested api cannot just be an api category
			if( isset($currentApi->api) )
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "The requested api '$contentName' in '{$this->name}' is just an api category, not an api.");

			return $currentApi;
		}

		/**
		 * This method checks if a given hook is available and corresponding file
		 * is present. The hook file should reside in the appropriate path.
		 * E.g. renderer hooks in /views, api hook in /api, common hooks in /bin, etc.
		 *
		 * @param string $hookName
		 * @return string - path to hook file or emtpy string if hook has not been specified in meta.json
		 */
		public function checkHookFilePresence(string $hookName) : string
		{
			$hookFileName = $this->determineHookFileName($hookName);
			if( empty($hookFileName) )
				return "";

			$hookFileLocation = $this->determineHookFileLocation($hookName);

			$absoluteHookFilePath = Helper::normalizeFilePath("{$_SERVER["TT"]->tower->getRootFolder()}/$hookFileLocation/$hookFileName");
			if( !is_file($absoluteHookFilePath) )
				throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Hook file '$hookName' at '$absoluteHookFilePath' could not be found.");

			return $absoluteHookFilePath;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		/**
		 * This method checks if the views entry is valid or not.
		 *
		 * @param array $views
		 * @return TripleTowerError
		 */
		protected function checkViewsProperties(array $views) : TripleTowerError
		{
			foreach($views as $viewKey => $view)
			{
				if( is_array($view) )
				{
					if( !isset($view["title"]) )
						return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Every view must have a title property");

					if( !isset($view["content"]) && !isset($view["views"]) )
						return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Every view must either have a content property or sub views");

					if( isset($view["content"]) && isset($view["views"]) )
						return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "A view can either have a content property or sub views, but not both");

					if( isset($view["content"]) && !isset($view["accessType"]) )
						return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Every view with a content must have an accessType property");

					if( isset($view["views"]) )
					{
						$subViewsResult = $this->checkViewsProperties($view["views"]);
						if( !$subViewsResult->isSuccess() )
							return $subViewsResult;
					}
				}
			}
			return new ConfigError(ConfigError::SUCCESS);
		}

		/**
		 * This method checks if the views entry is valid or not.
		 *
		 * @param array $apis
		 * @return TripleTowerError
		 */
		protected function checkApiProperties(array $apis) : TripleTowerError
		{
			foreach($apis as $apiKey => $api)
			{
				if( is_array($api) )
				{
					if( !isset($api["description"]) )
						return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Every api must have a description property");

					if( isset($api["api"]) && (isset($api["accessType"]) || isset($api["conditions"])) )
						return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "An api can either have sub apis or an accesstype with conditions, but not both");

					if( !isset($api["api"]) && (!isset($api["accessType"]) || !isset($api["conditions"])) )
						return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "A callable api must have an accessType property and a list of conditions");

					if( isset($api["api"]) )
					{
						$subApiResult = $this->checkApiProperties($api["api"]);
						if( !$subApiResult->isSuccess() )
							return $subApiResult;
					}
				}
			}
			return new ConfigError(ConfigError::SUCCESS);
		}

		/**
		 * This method assumes a changelog file which is found in the wapp's root folder.
		 *
		 * @param string $changeLogFileName
		 * @return string|TripleTowerError
		 */
		protected function generateRootFileInfoFromFile(string $changeLogFileName)
		{
			// Changelog is not available on purpose
			if( empty($changeLogFileName) )
				return "";

			// Changlog entry is a file to look for
			if( strpos($changeLogFileName, "/") !== false || strpos($changeLogFileName, "\\") !== false )
				return new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "The parameter '\$changeLogFileName' can not contain a path or be empty, only file names are allowed");

			$changeFilePath = "";
			foreach(self::WAPP_INFOFILE_ENDINGS as $infoFileEnding)
			{
				$changeFilePath = $_SERVER["TT"]->tower->getRootFolder() . $changeLogFileName . (empty($infoFileEnding) ? "" : ".$infoFileEnding");
				if( is_file($changeFilePath) )
					break;
			}
			if( empty($changeFilePath) )
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Changelog file '$changeLogFileName' could not be found");

			switch( pathinfo($changeLogFileName, PATHINFO_EXTENSION) )
			{
				case "md":
				case "markdown":
				case "md":
				case "mdown":
				case "mkd":
				case "mkdn":
					$changeLogContent = file_get_contents($changeFilePath);
					if( $changeLogContent !== false )
						return Parsedown::instance()->text($changeLogContent);
					break;

				case "html":
					$changeLogContent = file_get_contents($changeFilePath);
					if( $changeLogContent !== false )
						return $changeLogContent;
					break;

				case "":
				case "txt":
				default:
					$changeLogContent = file_get_contents($changeFilePath);
					if( $changeLogContent !== false )
						return "<pre>$changeLogContent</pre>";

					break;
				case "php":
					ob_start();
					if( require($changeFilePath) !== false )
						return ob_get_flush();
					else
						ob_end_clean();
					break;
			}

			return new FsError(FsError::FILE_OPENING_FAILED, FsError::ERR, "Changelog file '$changeLogFileName' found but could not be read");
		}

		/**
		 * This method determines the hooks file name from the meta.json file.
		 *
		 * @param string $hookName
		 * @return string - The specified filename or empty string if no hook got specified.
		 */
		protected function determineHookFileName(string $hookName) : string
		{
			$contentProvider = $_SERVER["TT"]->contentProvider;
			$contentType     = $_SERVER["TT"]->contentType;
			$contentName     = $_SERVER["TT"]->contentName;

			switch( $hookName )
			{
				case self::META_INVOCATION_HOOK:
				case self::PRE_AUTHORIZATION_HOOK:
				case self::POST_AUTHORIZATION_HOOK:
				case self::PRE_AUTHENTICATION_HOOK:
				case self::POST_AUTHENTICATION_HOOK:
				case self::PRE_EXECUTION_HOOK:
				case self::POST_EXECUTION_HOOK:
					if( !isset($_SERVER["TT"]->meta->$hookName) )
						return "";

					return $_SERVER["TT"]->meta->$hookName;

				case self::GLOBAL_SELF_RENDERER:
				case self::GLOBAL_PRE_RENDER_HOOK:
				case self::GLOBAL_POST_RENDER_HOOK:
					if( !isset($_SERVER["TT"]->meta->views->$hookName) )
						return "";

					return $_SERVER["TT"]->meta->views->$hookName;

				case self::LOCAL_SELF_RENDERER:
				case self::LOCAL_PRE_RENDER_HOOK:
				case self::LOCAL_POST_RENDER_HOOK:
					if( $contentType !== Tower::CONTENT_TYPE_VIEW || empty($contentName) )
						throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Can not invoke hook '$hookName' for content type '$contentType'");

					$viewEntry = $this->getViewEntry($contentName);
					if( TripleTowerError::isError($viewEntry) )
						throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Can not invoke hook '$hookName' for content '$contentName'");

					if( !isset($viewEntry->$hookName) )
						return "";

					return $viewEntry->$hookName;

				case self::GLOBAL_PRE_APICALL_HOOK:
				case self::GLOBAL_POST_APICALL_HOOK:
					if( !isset($_SERVER["TT"]->meta->api->$hookName) )
						return "";

					return $_SERVER["TT"]->meta->api->$hookName;

				case self::LOCAL_PRE_APICALL_HOOK:
				case self::LOCAL_POST_APICALL_HOOK:
					if( $contentType !== Tower::CONTENT_TYPE_API || empty($contentName))
						throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Can not invoke hook '$hookName' for content type '$contentType'");
					if( !isset($_SERVER["TT"]->meta->api->$contentName->$hookName) )
						return "";

					return $_SERVER["TT"]->meta->api->$contentName->$hookName;

				default:
					throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Given hook name '$hookName' is not valid.");
			}
		}

		/*
		 * This method determines the folder in which the hook should reside in. This is
		 * especially delicate because the folder setup changes if the wapp context switch
		 * has already happened.
		 *
		 * @param string $hookName
		 * @return string - The approriate hook folder
		 */
		public function determineHookFileLocation(string $hookName) : string
		{
			switch( $hookName )
			{
				case self::META_INVOCATION_HOOK:
				case self::PRE_EXECUTION_HOOK:
					return $_SERVER["TT"]->tower->getTowerInstance() .
							"/{$_SERVER["TT"]->contentProvider}/bin";

				case self::PRE_AUTHORIZATION_HOOK:
				case self::POST_AUTHORIZATION_HOOK:
				case self::PRE_AUTHENTICATION_HOOK:
				case self::POST_AUTHENTICATION_HOOK:
				case self::POST_EXECUTION_HOOK:
					return "/bin";

				case self::GLOBAL_SELF_RENDERER:
				case self::LOCAL_SELF_RENDERER:
				case self::GLOBAL_PRE_RENDER_HOOK:
				case self::GLOBAL_POST_RENDER_HOOK:
				case self::LOCAL_PRE_RENDER_HOOK:
				case self::LOCAL_POST_RENDER_HOOK:
					return "/views";

				case self::GLOBAL_PRE_APICALL_HOOK:
				case self::GLOBAL_POST_APICALL_HOOK:
				case self::LOCAL_PRE_APICALL_HOOK:
				case self::LOCAL_POST_APICALL_HOOK:
					return "/api";

				default:
					throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Given hook name '$hookName' is not valid.");
			}
		}
	}
}