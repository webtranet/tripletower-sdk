<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace\InstalledWapp
{

	use TripleTowerSDK\Error\TripleTowerError;

	class WappsTowerInfo
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $serverName  = "";
		protected $publicKey   = "";
		protected $displayName = "";
		protected $visible     = false;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $wappsTowerInfoId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$wappsTowerInfos = $serviceTowerDb->getWappsTowerInfos($wappsTowerInfoId);
			if( TripleTowerError::isError($wappsTowerInfos) )
				throw $wappsTowerInfos;

			$this->serverName  = (string)$wappsTowerInfos["servername"];
			$this->publicKey   = (string)$wappsTowerInfos["publickey"];
			$this->displayName = (string)$wappsTowerInfos["displayname"];
			$this->visible     = (bool)$wappsTowerInfos["visible"];
		}

		public function __get($key)
		{
			return $this->$key;
		}

		public function __isset($key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}