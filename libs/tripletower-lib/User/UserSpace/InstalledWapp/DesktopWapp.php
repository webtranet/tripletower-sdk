<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace\InstalledWapp
{

	use TripleTowerSDK\Error\TripleTowerError;

	class DesktopWapp
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $positionX   = 0;
		protected $positionY   = 0;
		protected $displayName = "";
		protected $parameters  = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $desktopWappId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$desktopWappInfos = $serviceTowerDb->getDesktopWappInfos($desktopWappId);
			if( TripleTowerError::isError($desktopWappInfos) )
				throw $desktopWappInfos;

			$this->positionX   = (int)$desktopWappInfos["positionx"];
			$this->positionY   = (int)$desktopWappInfos["positiony"];
			$this->displayName = (string)$desktopWappInfos["displayname"];
			$this->parameters  = (string)$desktopWappInfos["parameters"];
		}

		public function __get($key)
		{
			return $this->$key;
		}

		public function __isset($key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}