<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace\InstalledWapp
{

	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Error\CryptoError;
	use TripleTowerSDK\Error\TripleTowerError;

	class WappUserSignature
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const SIGN_DATA_GLUE       = "#";
		const SIGN_ALGORITHM       = OPENSSL_ALGO_SHA256;
		const SIGN_DATEFORMAT      = DateTime::ATOM;
		const SIGN_VALIDITY_PERIOD = "P3M";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $userName              = "";
		protected $fullQualifiedWappName = "";
		protected $signature             = "";
		protected $expirationDate        = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function verifySignature(string $userName, string $fullQualifiedWappName, string $signature, string $expirationDate, string $publicKeyContent) : TripleTowerError
		{
			$signData = strtolower($userName . self::SIGN_DATA_GLUE . $fullQualifiedWappName) . self::SIGN_DATA_GLUE . $expirationDate;
			if( openssl_verify($signData, base64_decode($signature, true), $publicKeyContent, self::SIGN_ALGORITHM) !== 1 )
				return new CryptoError(CryptoError::OPENSSL_SIGNING_FAILED, CryptoError::ERR, "Verification of WappUserSignature failed");

			$currentDateTime = DateTime::createFromFormat(self::SIGN_DATEFORMAT, $expirationDate);
			if( $currentDateTime < new DateTime("now") )
				return new CryptoError(CryptoError::EXPIRATION_REACHED, CryptoError::ERR, "The WappUserSignature is invalid because the expiration date '$expirationDate' has already been reached");

			return new CryptoError(CryptoError::SUCCESS);
		}

		public function verify(string $publicKeyContent) : TripleTowerError
		{
			return self::verifySignature($this->userName, $this->fullQualifiedWappName, $this->signature, $this->expirationDate, $publicKeyContent);
		}

		public function __construct( string $userName, string $fullQualifiedWappName, string $privateKeyContent )
		{
			$expirationDate = date_create("now")->add(new \DateInterval(self::SIGN_VALIDITY_PERIOD))->format(self::SIGN_DATEFORMAT);
			$signData = strtolower($userName . self::SIGN_DATA_GLUE . $fullQualifiedWappName) . self::SIGN_DATA_GLUE . $expirationDate;
			$signature = "";

			if( openssl_sign($signData, $signature, $privateKeyContent, self::SIGN_ALGORITHM) === false )
				throw new CryptoError(CryptoError::OPENSSL_SIGNING_FAILED, CryptoError::ERR, "Could not sign WappUserSignature");

			$this->userName              = $userName;
			$this->fullQualifiedWappName = $fullQualifiedWappName;
			$this->signature             = base64_encode($signature);
			$this->expirationDate        = $expirationDate;
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}