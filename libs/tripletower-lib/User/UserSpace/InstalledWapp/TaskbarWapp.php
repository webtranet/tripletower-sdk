<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace\InstalledWapp
{

	use TripleTowerSDK\Error\TripleTowerError;

	class TaskbarWapp
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $position    = 0;
		protected $displayName = "";
		protected $parameters  = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $taskbarWappId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$taskbarWappInfos = $serviceTowerDb->getTaskbarWappInfos($taskbarWappId);
			if( TripleTowerError::isError($taskbarWappInfos) )
				throw $taskbarWappInfos;

			$this->position    = (int)$taskbarWappInfos["position"];
			$this->displayName = (string)$taskbarWappInfos["displayname"];
			$this->parameters  = (string)$taskbarWappInfos["parameters"];
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}