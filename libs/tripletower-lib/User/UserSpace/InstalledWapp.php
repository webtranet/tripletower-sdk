<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User\UserSpace
{

	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\DesktopWapp;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\StartmenuWapp;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\TaskbarWapp;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\WappsTowerInfo;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\WappUserSignature;

	class InstalledWapp
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $wappName          = "";
		protected $wappInfo          = null;
		protected $wappsTowerInfo    = null;
		protected $wappPermissions   = [];

		protected $desktopWapps      = [];
		protected $startmenuWapps    = [];
		protected $taskbarWapps      = [];

		protected $wappUserSignature = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $installedWappId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$installedWappInfos = $serviceTowerDb->getInstalledWappInfos($installedWappId);
			if( TripleTowerError::isError($installedWappInfos) )
				throw $installedWappInfos;

			$this->wappName = (string)$installedWappInfos["wappname"];
			$this->wappInfo = new \stdClass(); // TODO: Make a WappInfoPlaceHolder
			$this->wappsTowerInfo = new WappsTowerInfo((int)$installedWappInfos["wappstowerinfo_id"]);

			$wappPermissions = $serviceTowerDb->getWappPermissionInfos($installedWappId);
			if( TripleTowerError::isError($wappPermissions) )
				throw $wappPermissions;
			foreach($wappPermissions as $wappPermission)
				$this->wappPermissions[] = $wappPermission["permission"];

			$desktopWappIds = $serviceTowerDb->getDesktopWappIds($installedWappId);
			if( TripleTowerError::isError($desktopWappIds) )
				throw $desktopWappIds;
			foreach($desktopWappIds as $desktopWappId)
				$this->desktopWapps[] = new DesktopWapp((int)$desktopWappId["desktopwapp_id"]);

			$startmenuWappIds = $serviceTowerDb->getStartmenuWappIds($installedWappId);
			if( TripleTowerError::isError($startmenuWappIds) )
				throw $startmenuWappIds;
			foreach($startmenuWappIds as $startmenuWappId)
				$this->startmenuWapps = new StartmenuWapp((int)$startmenuWappId["startmenuwapp_id"]);

			$taskbarWappIds = $serviceTowerDb->getTaskbarWappIds($installedWappId);
			if( TripleTowerError::isError($taskbarWappIds) )
				throw $taskbarWappIds;
			foreach($taskbarWappIds as $taskbarWappId)
				$this->taskbarWapps = new TaskbarWapp((int)$taskbarWappId["taskbarwapp_id"]);
		}

		public function generateWappUserSignature(string $userName)
		{
			$privateKeyContent = file_get_contents($_SERVER["TT"]->conf->privateKeyPath);
			if( $privateKeyContent === false )
				throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "ServiceTower private key file could not be found");

			$this->wappUserSignature = new WappUserSignature($userName, "{$this->wappName}@{$this->wappsTowerInfo->serverName}", $privateKeyContent);
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}