<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\User
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\User\FellowGroups\Fellow;

	class FellowGroup
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $fellowGroupName = "";
		protected $fellowGroupRights = [];
		protected $fellows = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $fellowGroupId)
		{
			$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

			$fellowGroupInfos = $serviceTowerDb->getFellowGroupInfos($fellowGroupId);
			if( TripleTowerError::isError($fellowGroupInfos) )
				throw $fellowGroupInfos;

			$this->fellowGroupName = $fellowGroupInfos["fellowgroupname"];

			$fellowGroupRights = $serviceTowerDb->getFellowGroupRights($fellowGroupId);
			if( TripleTowerError::isError($fellowGroupRights) )
				throw $fellowGroupRights;
			foreach($fellowGroupRights as $fellowGroupRight)
				$this->fellowGroupRights[] = $fellowGroupRight["fellowgroupright"];

			$fellowIds = $serviceTowerDb->getFellowIds($fellowGroupId);
			if( TripleTowerError::isError($fellowIds) )
				throw $fellowIds;
			foreach($fellowIds as $fellowId)
				$this->fellows[] = new Fellow((int)$fellowId["fellow_id"]);
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}