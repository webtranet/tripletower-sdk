<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Serializer
{
	use TripleTowerSDK\Helper\Helper;

	trait PhpSerializer
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function phpSerialize( $object )
		{
			return serialize( $object );
		}

		public static function phpUnserialize( $string )
		{
			return unserialize($string);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}