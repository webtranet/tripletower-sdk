<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Serializer
{
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Serializer;

	trait XmlSerializer
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function xmlSerialize( $object, $echoXmlHeader = false)
		{
			if( $echoXmlHeader )
				Helper::setXmlHeader();

			return json_encode( Serializer::toArray($object), ($readable ? ($options | JSON_PRETTY_PRINT) : $options) );
		}

		public static function xmlUnserialize( $string, string $expectedClass = "", $allowDerivedClasses = false )
		{
			return Serializer::toObject( json_decode(json_encode(simplexml_load_string($string, "SimpleXMLElement", LIBXML_NOCDATA)), true),
					$expectedClass, $allowDerivedClasses );
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected static function arrayToXml( array $array, $simpleXmlElement = null )
		{
			if( $simpleXmlElement === null )
				$simpleXmlElement = new \SimpleXMLElement("<root></root>");

			foreach( $array as $key => $value )
			{
				$nodeValue = is_array($value) ? arrayToXml($value) : $value;

				$simpleXmlElement->addChild($name)->addChild($name, $value);
			}
		}
	}
}