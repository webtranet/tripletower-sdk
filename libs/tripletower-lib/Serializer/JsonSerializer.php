<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Serializer
{
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Serializer\Serializer;

	trait JsonSerializer
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function jsonSerialize( $object, bool $echoJsonHeader = false, bool $formatted = false, int $options = 0, bool $serializeClassIndication = true)
		{
			if( $echoJsonHeader )
				Helper::setJsonHeader();

			$preparedData = null;
			if( is_scalar($object) || is_null($object) )
			{
				$preparedData = $object;
			}
			else
			{
				$preparedData = Serializer::toArray($object);
				if( !$serializeClassIndication )
					unset($preparedData[Serializer::TYPE]);
			}

			$serializedData = json_encode($preparedData , ($formatted ? ($options | JSON_PRETTY_PRINT) : $options) );

			if( !$formatted )
				return $serializedData;

			// Replace spaces to tabs at line start
			$serializedData = preg_replace_callback("/^ +/m", function($match)
			{
				return str_repeat( "\t", (int)(strlen($match[0]) / 4));
			}, $serializedData);

			// moves { and [ to new line and add tabs bevor them
			return preg_replace("/^(\\t*)(\"[\\w ]*\": *)([\\{\\[])$/m", "$1$2" . PHP_EOL . "$1$3", $serializedData);
		}

		public static function jsonUnserialize( $string, string $expectedClass = "", $allowDerivedClasses = false )
		{
			return Serializer::toObject( json_decode($string, true), $expectedClass, $allowDerivedClasses );
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}