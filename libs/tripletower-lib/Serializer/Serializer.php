<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Serializer
{
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;

	class Serializer
	{
		use JsonSerializer;
		use PhpSerializer;

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const TYPE = '#type';


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function toObject( array $data, string $expectedClass = "", $allowDerivedClasses = false )
		{
			if( empty($expectedClass) )
			{
				if( isset($data[self::TYPE]) )
					return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
				return $data;
			}

			if( empty($data[self::TYPE]) )
				$data[self::TYPE] = $expectedClass;

			$class = $data[self::TYPE];
			if( $expectedClass !== $class )
			{
				if( !$allowDerivedClasses )
					return new SysError(SysError::TYPE_MISMATCH, SysError::ERR, "Class '$class' does not match the expected class '$expectedClass'");
				
				$parentClasses = @class_parents($class);
				if( !is_array($parentClasses) || !in_array($expectedClass, $parentClasses) )
					return new SysError(SysError::TYPE_MISMATCH, SysError::ERR, "Class '$class' is not derived from expected class '$expectedClass'");
			}

			$reflectionClass = new \ReflectionClass($data[self::TYPE]);
			$classObject = $reflectionClass->newInstanceWithoutConstructor();
			if( method_exists( $classObject, "__unserialize" ) )
			{
				$classObject->__unserialize($data);
				return $classObject;
			}

			foreach( $reflectionClass->getProperties() as $property )
			{
				$value = null;
				$name = $property->name;
				$type = $property->getType();
				$typeName = !is_null($type) ? $type->getName() : "null";

				if( array_key_exists($name, $data) && !is_null($data[$name]) )
				{
					switch( $typeName )
					{
						case "null": // untyped property
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
							if( !is_scalar($data[$name]) && !is_null($data[$name]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR, "For security reasons untyped class properties can only become scalars or null");
							$value = $data[$name];
							break;

						case "bool":
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
							$value = (bool)$data[$name];
							break;

						case "int":
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
							$value = (int)$data[$name];
							break;

						case "float":
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
							$value = (float)$data[$name];
							break;

						case "string":
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
							$value = (string)$data[$name];
							break;

						case "array":
							if( isset($data[$name][self::TYPE]) )
								return new SysError(SysError::TYPE_MISMATCH, SysError::ERR);
								$value = $data[$name];
							break;

						default:
							$value = self::toObject($data[$name], $typeName, true);
					}
				}
				else if( array_key_exists($name, $data) && is_null($data[$name]) )
				{
					// untyped uninitialized properties can be set to null
					// e.g.: public $myProp;
					if( $typeName === "null" && !isset($reflectionClass->$name) )
						$value = $data[$name];
					
					// typed nullable properties will can be set to null
					// e.g.: public ?int $myProp;
					else if( $typeName !== "null" && $property->getType()->allowsNull() )
						$value = $data[$name];
					else
						return new SysError(SysError::TYPE_MISMATCH, SysError::ERR, "Property '$name' cannot be set to null");
				}
				else // property not found in serialized data
				{
					// untyped uninitialized properties stay uninitialized
					// e.g.: public $myProp;
					if( $typeName === "null" && !isset($reflectionClass->$name) )
						continue;
					
					// typed uninitialized properties stay uninitialized
					// e.g.: public int $myProp;
					$property->setAccessible(true);
					if( $typeName !== "null" && !$property->isInitialized($classObject) )
						continue;

					// typed nullable properties will become null
					// e.g.: public ?int $myProp;
					if( $typeName !== "null" && $property->getType()->allowsNull() )
						continue;
					
					// No further exception - properties should be in serialized data
					return new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "Property '$name' cannot be uninitialized and is not nullable");
				}

				try
				{
					$classObject->$name = $value;
				}
				catch (\Error $err)
				{
					$property->setAccessible(true);
					$property->setValue($classObject, $value);
				}
			}
			return $classObject;
		}

		public static function toArray( $object ) : array
		{
			$rawArray = null;
			$array = [];
			if( is_array($object) )
			{
				$rawArray = $object;
			}
			else
			{
				$rawArray = method_exists( $object, "__serialize" ) ? $object->__serialize() : self::toRawArray( $object );
				$array[self::TYPE] = get_class($object);
			}

			foreach( $rawArray as $key => $value )
			{
				// Key handling
				$newKey = $key;
				if( is_string($key) && is_object($object) )
				{
					// Remove special identifier in front of variable names
					$specialIdentifier =
					[
						"privatIdentifier" => "\0" . get_class($object) ."\0",
						"protectedIdentifier" => "\0*\0"
					];
					foreach($specialIdentifier as $identifier)
						if( substr( $key, 0, strlen($identifier) ) === $identifier )
							$newKey = substr( $key, strlen($identifier) );

					$array[$newKey] = $value;
				}

				// Value handling
				if( is_object($value) || is_array($value) )
					$array[$newKey] = self::toArray($value);
				else
					$array[$newKey] = $value;
			}
			return $array;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected static function toRawArray( $object )
		{
			$className = get_class( $object );
			$rawArray = (array)$object;
			$classNames = array_merge( [$className], class_parents($object) );

			if( !isset($_SERVER["TT"]) || !$_SERVER["TT"]->isInDevelopmentMode() )
			{
				foreach( $classNames as $className )
				{
					switch( $className )
					{
						case "Throwable":
							array_walk( $rawArray["\0Throwable\0trace"], function(&$item)
							{
								unset( $item["args"] );
							});
							break;

						case "Exception":
							array_walk( $rawArray["\0Exception\0trace"], function(&$item)
							{
								unset( $item["args"] );
							});
							break;

						case "PDO":
							unset( $rawArray["passwd"] );
							unset( $rawArray["password"] );
							break;
					}
				}
			}
			return $rawArray;
		}
	}
}