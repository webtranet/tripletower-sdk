<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Authorizor
{
	class WappsTowerAuthorizor extends Authorizor
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct()
		{
			parent::__construct($_SERVER["TT"]->dataBases->wappsTowerDb);
		}

		public function checkAccess(string $userName, string $contentProvider, string $contentType, string $contentName)
		{
			return parent::checkAccess($userName, $contentProvider, $contentType, $contentName);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}