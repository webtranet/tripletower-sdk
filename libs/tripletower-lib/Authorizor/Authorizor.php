<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Authorizor
{
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\DataBase\AccessRightsDBC;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Tower;
	use TripleTowerSDK\User;

	abstract class Authorizor
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const ACCESS_TYPE_PUBLIC        = "public";
		const ACCESS_TYPE_ANONYMOUSONLY = "anonymousOnly";
		const ACCESS_TYPE_AUTHENTICATED = "authenticated";
		const ACCESS_TYPE_RESTRICTED    = "restricted";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $accessRightsDataBase = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function checkAccess(string $userName, string $contentProvider, string $contentType, string $contentName)
		{
			$meta = $_SERVER["TT"]->meta;

			$contentAccess = null;
			switch( $contentType )
			{
				case Tower::CONTENT_TYPE_VIEW:
					$view = $meta->getViewEntry($contentName);
					if( TripleTowerError::isError($view) )
						return $view;
					$contentAccess = $view->accessType;
					break;

				case Tower::CONTENT_TYPE_API:
					$api = $meta->getApiEntry($contentName);
					if( TripleTowerError::isError($api) )
						return $api;
					$contentAccess = $api->accessType;
					break;

				case Tower::CONTENT_TYPE_PUBLIC:
					$contentAccess = self::ACCESS_TYPE_PUBLIC;
					break;

				case Tower::CONTENT_TYPE_TEMP:
					$publicTempDocuments = ["dumpling.jgz", "dumpling.cgz", "styles.less"];
					if( in_array($contentName, $publicTempDocuments) )
						$contentAccess = self::ACCESS_TYPE_PUBLIC;
					else
						$contentAccess = self::ACCESS_TYPE_RESTRICTED;
					break;

				default:
					return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "Unfortunately, the content type '$contentType' is not known.");
			}

			switch( $contentAccess )
			{
				case self::ACCESS_TYPE_PUBLIC:
					return new AuthError(AuthError::SUCCESS);

				case self::ACCESS_TYPE_ANONYMOUSONLY:
					return ($userName === User::DEFAULT_USERNAME) ? new AuthError(AuthError::SUCCESS) : new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "Unfortunately, your access to '$contentName' in '$contentProvider' is denied. Only anonymous access is allowed.");

				case self::ACCESS_TYPE_AUTHENTICATED:
					return ($userName !== User::DEFAULT_USERNAME) ? new AuthError(AuthError::SUCCESS) : new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "Unfortunately, your access to '$contentName' in '$contentProvider' is denied. Only authenticated access is allowed.");

				case self::ACCESS_TYPE_RESTRICTED:
					if( $userName === User::DEFAULT_USERNAME )
						return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "Unfortunately, your access to '$contentName' in '$contentProvider' is denied. Only authenticated users are allowed for having restricted access.");

					list($userName, $serviceTower) = Helper::splitTowerFromName($userName);

					$groups = $this->accessRightsDataBase->getGroupsWithAccess($contentProvider, $contentType, $contentName);
					if( TripleTowerError::isError($groups) )
						return $groups;

					foreach( $groups as $group )
					{
						$authSystemName = $group["authenticationsystemname"];
						if( $_SERVER["TT"]->authSystems->$authSystemName->checkUserInGroup($userName, $group["accessgroupname"], $group["checknestedgroups"])->isSuccess() )
							return new AuthError(AuthError::SUCCESS);
					}

					return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "Unfortunately, your access to '$contentName' in '$contentProvider' is denied. Only authenticated users are allowed for having restricted access.");

				default:
					return new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "There is no or a wrong accessType defined for '$contentName' in '$contentProvider'.");
			}
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function __construct(AccessRightsDBC $accessRightsDataBase)
		{
			$this->accessRightsDataBase = $accessRightsDataBase;
		}
 	}
}