<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\HttpError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Network\HttpRequest;
	use TripleTowerSDK\Network\HttpResponse;


	class AmazonS3SC extends StorageConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const META_INFO_IDENTIFIER = "x-amz-meta-";
		const ACL_FULL_ACCESS = "FULL_ACCESS";
		const ACL_WRITE_ACCESS = "WRITE";
		const ACL_READ_ACCESS = "READ";

		const PROTOCOL = "https://";
		const AWS_REQUEST_TYPE = "aws4_request";
		const AWS4_HASH_ALGORITHM = "sha256";
		const AWS4_HASH_ALGORYTHM_NAME = "AWS4-HMAC-SHA256";

		const AWS_REGION = "";
		const AWS_SERVICE = "";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $host, string $basePath, string $userName, string $password, string $caCertificateFilePath, int $timeOut)
		{
			parent::__construct($host, $basePath, $userName, $password, $caCertificateFilePath, $timeOut);
		}

		public function changeTimes(string $filePath, int $time = null, int $atime = null) : TripleTowerError
		{
			return new FsError(FsError::FILE_TIMECHANGE_FAILED, FsError::ERR, "Could not change time of file '$filePath'");
		}

		public function copyFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$sourceFilePath = Helper::normalizeFilePath($sourceFilePath);
			$destinationFilePath = Helper::normalizeFilePath($destinationFilePath);

			if( TripleTowerError::isError($copyResult = $this->copyObject($sourceFilePath, $destinationFilePath)) )
				return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file '$sourceFilePath' to '$destinationFilePath'");

			return $copyResult;
		}

		public function copyFileOrFolder(string $sourcePath, string $destinationPath) : TripleTowerError
		{
			$sourcePath = Helper::normalizePath($sourcePath);
			$destinationPath = Helper::normalizePath($destinationPath);

			if( TripleTowerError::isError($copyResult = $this->copyObjectRecursive($sourcePath, $destinationPath)) )
				return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file or folder '$sourcePath' to '$destinationPath'");

			return $copyResult;
		}

		public function copyFolder(string $sourceFolderPath, string $destinationFolderPath) : TripleTowerError
		{
			$sourceFolderPath = Helper::normalizeFolderPath($sourceFolderPath);
			$destinationFolderPath = Helper::normalizeFolderPath($destinationFolderPath);

			if( TripleTowerError::isError($copyResult = $this->copyObjectRecursive($sourceFolderPath, $destinationFolderPath)) )
				return new FsError(FsError::FOLDER_COPYING_FAILED, FsError::ERR, "Could not copy folder '$sourceFolderPath' to '$destinationFolderPath'");

			return $copyResult;
		}

		public function createFolder(string $folderPath, int $mode = 0777, bool $recursive = false) : TripleTowerError
		{
			$folderPath = Helper::addFirstCharIfNot(Helper::normalizeFolderPath($folderPath), "/");
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . $folderPath);
			$canonicalQueryString = "";

			$url = self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");

			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, "");
			$headerParts =
			[
				"x-amz-content-sha256" => $hashedRequestPayload,
				"content_type" => "application/x-directory",
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, $httpRequestMethod, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->post();
			if( TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
				return new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "Could not create folder '$folderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFile(string $filePath) : TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);

			if( !$this->isFile($filePath)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file '$filePath' because it does not exist");

			if( !$this->deleteObject( $filePath )->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFileOrFolder(string $fileOrFolderPath, bool $recursive = false) : TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			if( !$this->deleteObjectsByPrefix( $fileOrFolderPath, $recursive )->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file or folder '$fileOrFolderPath' recursively");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFolder(string $folderPath, bool $recursive = false) : TripleTowerError
		{
			$normalizedFolderPath = Helper::normalizeFolderPath($folderPath);

			if( !$this->isFolder($normalizedFolderPath)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete folder '$folderPath' because it does not exist");

			if(!$this->deleteObjectsByPrefix( $folderPath, $recursive )->isSuccess())
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete folder '$folderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function fileExists(string $filePath) : TripleTowerError
		{
			if( !$this->isFile($filePath)->isSuccess())
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File '$filePath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function fileOrFolderExists(string $fileOrFolderPath) : TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			if( TripleTowerError::isError($this->getObjectMetaInfo($fileOrFolderPath)) )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File or Folder '$fileOrFolderPath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function folderExists(string $folderPath) : TripleTowerError
		{
			if( !$this->isFolder($folderPath)->isSuccess())
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "Folder '$folderPath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function getFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$destinationFilePath = Helper::normalizeFilePath($destinationFilePath);

			if( TripleTowerError::isError($fileContent = $this->getFileContents($sourceFilePath)) )
				return $fileContent;

			$destFolderPath = Helper::extractFolderName($destinationFilePath);
			$pathCreationResult = Helper::createPathIfNotExists($destFolderPath);
			if( !$pathCreationResult->isSuccess() )
				return $pathCreationResult;

			if( file_put_contents($destinationFilePath, $fileContent) === false )
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Get file '$sourceFilePath' failed because content could not be written to destination file '$destinationFilePath'.");

			return new FsError(FsError::SUCCESS);
		}

		public function getFileContents(string $sourceFilePath) // : string or TripleTowerError
		{
			$sourceFilePath = Helper::addFirstCharIfNot(Helper::normalizeFilePath($sourceFilePath), "/");
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . $sourceFilePath);
			$canonicalQueryString = "";

			$response = $this->getInformationFromS3($canonicalQueryString, $canonicalURI);
			if( TripleTowerError::isError($response) )
				return $response;

			if( !$response->checkResponseCode([HttpError::HTTP_OK])->isSuccess() )
			{
				 $simpleXmlResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $response->getBody()));
				 $awsErrorCode = (string)($simpleXmlResponse->xpath("Code")[0] ?? "");
				 if( $awsErrorCode === "NoSuchKey" )
					 return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "File '$sourceFilePath' could not be found");

				 return new HttpError($response->getResponseCode(), HttpError::ERR, $response->getBody());
			}

			$hashHeaderName = self::META_INFO_IDENTIFIER . self::AWS4_HASH_ALGORITHM;
			if( $response->checkHeaderExits($hashHeaderName)->isSuccess() )
			{
				$fileHash = hash(self::AWS4_HASH_ALGORITHM, $response->getBody());
				if( !$response->compareHeaderValue($hashHeaderName, $fileHash)->isSuccess())
					return new FsError(FsError::FILE_FORMAT_CORRUPT, FsError::ERR, "Get file '$sourceFilePath' faild because of file hash is different to expected.");
			}

			return $response->getBody();
		}

		public function getFileHash(string $filePath, string $hashAlgorithm) // : string or TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);

			if( $hashAlgorithm !== self::AWS4_HASH_ALGORITHM )
				return new FsError(FsError::FILE_HASH_CREATION_FAILED , SysError::ERR, "The given hash algorithm '$hashAlgorithm' is not suported by the AmazonS3 storage conector");

			$sha256Hash = $this->getObjectMetaInfo($filePath, self::META_INFO_IDENTIFIER . self::AWS4_HASH_ALGORITHM);
			if( TripleTowerError::isError($sha256Hash) )
				return new FsError(FsError::FILE_HASH_CREATION_FAILED, FsError::ERR, "Could not create '$hashAlgorithm' file hash of '$filePath'");

			return $sha256Hash;
		}

		public function getFileType(string $filePath) // : string or TripleTowerError
		{
			if( $this->isFolder($filePath)->isSuccess() )
				return "httpd/unix-directory";

			$filePath = Helper::normalizeFilePath($filePath);

			$contentType = $this->getObjectMetaInfo($filePath, "content-type");

			if( TripleTowerError::isError($contentType) )
				return new FsError(FsError::MIMETYPE_UNKNOWN, FsError::ERR, "Could not determine mime type of file '$filePath'");

			return $contentType;
		}

		public function getFolderContents(string $folderPath) // : array or TripleTowerError
		{
			$folderPath = Helper::removeFirstChar(Helper::normalizeFolderPath($folderPath), "/");
			$folderContent = $this->listObjectsWithPrefix($folderPath);
			if( TripleTowerError::isError($folderContent) )
				return new FsError(FsError::SCANDIR_FAILED, FsError::ERR, "Could not get folder contents of folder '$folderPath'");

			$folderContent = array_map(function($value)
			{
				if( substr($value, -1, 1) === "/" )
					return substr($value, strrpos($value, "/", -2) + 1 , -1);

				return substr($value, strrpos($value, "/") + 1);
			}, $folderContent);

			return $folderContent;
		}

		public function getFreeSpace(string $folderPath) // : int or TripleTowerError
		{
			return new FsError(FsError::FREE_SPACE_DETERMINATION_FAILED, FsError::ERR, "Could not determine space in folder '$folderPath'");
		}

		public function getId(string $fileOrFolderPath)  // : string or TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			$eTag = $this->getObjectMetaInfo($fileOrFolderPath, "eTag");

			if( TripleTowerError::isError($eTag) )
				return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could read inode value of file or folder '$fileOrFolderPath'");

			return $eTag;
		}

		public function getMetaData(string $fileOrFolderPath) // : array or TripleTowerError // Should be php stat compatible
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			$mode = $this->isFolder($fileOrFolderPath)->isSuccess() ? octdec(0040000) :  octdec(0100000);

			$objectMetaData = $this->getObjectMetaInfo($fileOrFolderPath);

			if( TripleTowerError::isError($objectMetaData) || !isset($objectMetaData["content-length"]) || !isset($objectMetaData["x-emc-mtime"]) )
				return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could not retrieve meta data of file or folder '$fileOrFolderPath'");

			return
			[
				"dev" => 1,
				"ino" => 0,
				"mode" => $mode,
				"nlink" => 1,
				"uid" => 0,
				"gid" => 0,
				"rdev" => 1,
				"size" => intval($objectMetaData["content-length"]),
				"atime" => intval($objectMetaData["x-emc-mtime"] / 1000),
				"mtime" => intval($objectMetaData["x-emc-mtime"] / 1000)
				/*"ctime" => "",
				"blksize" => -1,
				"blocks" => -1*/
			];
		}

		public function isCreatable(string $fileOrFolderPath) : TripleTowerError
		{
			if($this->fileOrFolderExists($fileOrFolderPath)->isSuccess())
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::SILENT, "File or folder '$fileOrFolderPath' already exists");

			//check if user has write access to the bucket if yes then file or folder should be createable
			return hasUserWriteAccessToObject("");
		}

		public function isDeletable(string $fileOrFolderPath) : TripleTowerError
		{
			return $this->isWritable($fileOrFolderPath);
		}

		public function isFile(string $filePath) : TripleTowerError
		{
			$filePath = Helper::removeFirstChar(Helper::normalizeFilePath($filePath), "/");

			$objectsWithPrefix = $this->listObjectsWithPrefix(Helper::removeLastChar(Helper::normalizeFolderPath($filePath)));

			if( !in_array($filePath, $objectsWithPrefix) )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::SILENT, "File '$filePath' is no file");

			return new FsError(FsError::SUCCESS);
		}

		public function isFolder(string $folderPath) : TripleTowerError
		{
			$folderPath = Helper::removeFirstChar(Helper::normalizeFolderPath($folderPath), "/");
			$objectsWithPrefix = $this->listObjectsWithPrefix(Helper::removeLastChar($folderPath));

			if( !in_array($folderPath, $objectsWithPrefix) )
				return new FsError(FsError::IS_NOT_A_FOLDER, FsError::SILENT, "Folder '$folderPath' is no folder");

			return new FsError(FsError::SUCCESS);
		}

		public function isReadable(string $fileOrFolderPath) : TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			if( !$this->hasUserReadAccessToObject($fileOrFolderPath)->isSuccess() )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::SILENT, "File or folder '$fileOrFolderPath' is not readable");

			return new FsError(FsError::SUCCESS);
		}

		public function isSharable(string $fileOrFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_SHARING_FAILED, FsError::SILENT, "File or folder '$fileOrFolderPath' can't be shared");
		}

		public function isWritable(string $filePath) : TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);

			if( $this->getObjectRetainUntilDate($filePath) > getdate())
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::SILENT, "File '$filePath' is not writable because it is under retention");

			return $this->hasUserWriteAccessToObject($filePath);
		}

		public function putFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$sourceFilePath = Helper::normalizeFilePath($sourceFilePath);
			$destinationFilePath = Helper::addFirstCharIfNot(Helper::normalizeFilePath($destinationFilePath), "/");

			if( $destinationFilePath === "" )
				return new FsError(FsError::NAME_IS_INVALID);

			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . $destinationFilePath);
			$canonicalQueryString = "";
			$url = self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash_file(self::AWS4_HASH_ALGORITHM, $sourceFilePath);

			$headerParts =
			[
				"x-amz-meta-sha256" => $hashedRequestPayload,
				"x-amz-content-sha256" => $hashedRequestPayload,
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_PUT, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->put($sourceFilePath);

			// Curl error
			if( TripleTowerError::isError($httpResponse) )
				return $httpResponse->raiseError(HttpError::ERR, "File '$destinationFilePath' could not be written");

			// HTTP error
			if( !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::ERR, "File '$destinationFilePath' could not be written");

			return new FsError(FsError::SUCCESS);
		}

		public function putFileContents(string $destinationFilePath, string $contents) : TripleTowerError
		{
			$destinationFilePath = Helper::addFirstCharIfNot(Helper::normalizeFilePath($destinationFilePath), "/");

			if( $destinationFilePath === "" )
				return new FsError(FsError::NAME_IS_INVALID);

			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . $destinationFilePath);
			$canonicalQueryString = "";
			$url = self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, $contents);

			$headerParts =
			[
				"x-amz-meta-sha256" => $hashedRequestPayload,
				"x-amz-content-sha256" => $hashedRequestPayload,
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_PUT, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->putFileContents($contents);

			// Curl error
			if( TripleTowerError::isError($httpResponse) )
				return $httpResponse->raiseError(HttpError::ERR, "File '$destinationFilePath' could not be written");

			// HTTP error
			if( !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::ERR, "File '$destinationFilePath' could not be written");

			return new FsError(FsError::SUCCESS);
		}

		public function renameFile(string $oldFileName, string $newFileName) : TripleTowerError
		{
			$oldFileName = Helper::normalizeFilePath($oldFileName);
			$newFileName = Helper::normalizeFilePath($newFileName);

			if( !$this->isDeletable($oldFileName)->isSuccess() || !$this->isFile($oldFileName)->isSuccess() )
				return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, "File '$oldFileName' could not be renamed to '$newFileName'");

			$copyResult = $this->copyObject($oldFileName, $newFileName);
			if( !$copyResult->isSuccess() )
				return $copyResult;

			return $this->deleteFile($oldFileName);
		}

		public function renameFileOrFolder(string $oldFileOrFolderName, string $newFileOrFolderName) : TripleTowerError
		{
			$oldFileOrFolderName = Helper::normalizePath($oldFileOrFolderName);
			$newFileOrFolderName = Helper::normalizePath($newFileOrFolderName);

			if( !$this->isDeletable($oldFileOrFolderName)->isSuccess() )
				return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, "File or folder '$oldFileOrFolderName' could not be renamed to '$newFileOrFolderName'");

			$copyResult = $this->copyObjectRecursive($oldFileOrFolderName, $newFileOrFolderName);
			if( !$copyResult->isSuccess() )
				return $copyResult;

			return $this->deleteFileOrFolder($oldFileOrFolderName);
		}

		public function renameFolder(string $oldFolderName, string $newFolderName) : TripleTowerError
		{
			$oldFolderName = Helper::normalizeFilePath($oldFolderName);
			$newFolderName = Helper::normalizeFilePath($newFolderName);

			if( !$this->renameFileOrFolder( $newFolderName, $oldFolderName )->isSuccess() )
				return new FsError(FsError::FOLDER_RENAMING_FAILED, FsError::ERR, "Folder '$oldFolderName' could not be renamed to '$newFolderName'");

			return new FsError(FsError::SUCCESS);
		}

		public function openFile(string $filePath, string $mode) // :resource or TripleTowerError
		{
			return new FsError(FsError::FILE_OPENING_FAILED, FsError::ERR, "Could not open file '$filePath' with mode '$mode'");
		}

		public function openFolder(string $folderPath) // :resource or TripleTowerError
		{
			return new FsError(FsError::FOLDER_OPENING_FAILED, FsError::ERR, "Could not open folder '$folderPath'");
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function copyObjectRecursive(string $sourcePath, string $destinationPath)
		{
			$objectsToCopy = $this->listObjectsWithPrefix($sourcePath, true);
			if( TripleTowerError::isError($objectsToCopy) )
				return $objectsToCopy;

			if( count($objectsToCopy) < 1 )
				return new FsError(FsError::FOLDER_COPYING_FAILED, FsError::ERR, "Folder '$sourcePath' could not be found.");

			$lenSourcePath = strlen($sourcePath);
			foreach($objectsToCopy as $objectToCopy)
			{
				$copyResult = $this->copyObject( $objectToCopy, $destinationPath . substr($objectToCopy, $lenSourcePath));
				if(!$copyResult->isSuccess() )
					return $copyResult;
			}
			return new FsError(FsError::SUCCESS);
		}

		protected function copyObject(string $sourcePath, string $destinationPath) : FsError
		{
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($destinationPath, "/"));
			$canonicalQueryString = "";

			$url = self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, "");

			$headerParts =
			[
				"x-amz-copy-source" => self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($sourcePath, "/")),
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_PUT, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->put();
			if( TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
			{
				return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file or folder '$sourcePath' to '$destinationPath'");
			}

			return new FsError(FsError::SUCCESS);
		}

		protected function deleteSingleObject(string $objectPath)
		{
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($objectPath, "/"));
			$canonicalQueryString = "";

			$url = self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, "");

			$headerParts =
			[
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_DELETE, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->delete();
			if( TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete object '$objectPath'");

			return new FsError(FsError::SUCCESS);
		}

		protected function deleteObjectsByPrefix(string $objectPathPrefix, bool $recursive)
		{
			$objectKeys = $this->listObjectsWithPrefix($objectPathPrefix, $recursive);
			if( TripleTowerError::isError($objectKeys))
				return $objectKeys;

			if( count($objectKeys) < 1)
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete object '$objectPathPrefix' because it does not exist");

			if( count($objectKeys) > 1 && $recursive === false )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete object '$objectPathPrefix' because it has sub objects");

			$xmlString = "<Delete>";
			foreach( $objectKeys as $objectKey )
			{
				$xmlString .= "<Object><Key>$objectKey</Key></Object>";
			}
			$xmlString .= "</Delete>";

			$requestPayload = $xmlString;

			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath());
			$canonicalQueryString = "delete=";

			$url = $canonicalQueryString !== "" ? self::PROTOCOL . parent::getHost() . "$canonicalURI?$canonicalQueryString" : self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, $requestPayload);

			$headerParts =
			[
				"x-amz-content-sha256" => $hashedRequestPayload,
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Content-Length" => strlen($requestPayload),
				"Content-MD5" => base64_encode(md5($requestPayload, true)),
				"Content-Type" => "application/xml",
				"Connection" => "close"
			];

			$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_POST, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
			$httpResponse = $httpRequest->post($requestPayload);
			if( TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete object '$objectPath'");

			$simpleXmlDeleteResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $httpResponse->getBody()));
			if( count($simpleXmlDeleteResponse->xpath("Error")) > 0 )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete all objects with prefix '$objectPathPrefix'");

			return new FsError(FsError::SUCCESS);
		}

		protected function getInformationFromS3($canonicalQueryString, $canonicalURI, $noBody = false)
		{
			$url = $canonicalQueryString !== "" ? self::PROTOCOL . parent::getHost() . "$canonicalURI?$canonicalQueryString" : self::PROTOCOL . parent::getHost() . $canonicalURI;

			$dateTime = new \DateTime("UTC");
			$hashedRequestPayload = hash(self::AWS4_HASH_ALGORITHM, "");
			$headerParts =
			[
				"x-amz-date" => $dateTime->format("Ymd\THis\Z"),
				"Connection" => "close"
			];

			if($noBody === true)
			{
				$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_HEAD, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
				$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
				return $httpRequest->head();
			}
			else
			{
				$httpHeaders = $this->genHttpHeader($headerParts, HttpRequest::HTTP_GET, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
				$httpRequest = new HttpRequest($url, $httpHeaders, $this->caCertificateFilePath, $this->timeOutValue);
				 return $httpRequest->get();
			}
		}

		protected function getObjectMetaInfo(string $objectPath, string $metaInfoKey = "")
		{
			try
			{
				$canonicalQueryString = "";
				$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($objectPath, "/"));
				$response = $this->getInformationFromS3($canonicalQueryString, $canonicalURI, true);
				if( TripleTowerError::isError($response) )
					return $response;

				return $metaInfoKey === "" ? $response->getHeaders() : $response->getHeaderValue($metaInfoKey);
			}
			catch(\Exception $ex)
			{
				return new FsError(FsError::UNKNOWN, FsError::ERR, "Could not get S3 meta information of object with path '$objectPath'");
			}
		}

		protected function getObjectRetainUntilDate(string $objectPath)
		{
			$canonicalQueryString = "retention";
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($objectPath, "/"));

			if( TripleTowerError::isError($response = $this->getInformationFromS3($canonicalQueryString,$canonicalURI)) )
				return $response;

			try
			{
				$simpleXmlResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $response->getBody()));
				return \DateTime::createFromFormat("Y-m-d H:i:s", $simpleXmlResponse->xpath("Retention/RetainUntilDate"));

			}
			catch(\Exception $ex)
			{
				return new FsError(FsError::UNKNOWN, FsError::ERR, "Could not get retention until date of object with path '$objectPath'");
			}
		}

		protected function hasUserWriteAccessToObject(string $objectPath)
		{
			if( TripleTowerError::isError($userPermissionsForObject = $this->getUserPermissionsForObject($objectPath)) )
				return $userPermissionsForObject;
			foreach( $userPermissionsForObject as $userPermissionForObject )
			{
				if($userPermissionForObject === self::ACL_FULL_ACCESS || $userPermissionForObject === self::ACL_WRITE_ACCESS)
					return new FsError(FsError::SUCCESS);
			}

			return new FsError(FsError::FILE_WRITING_FAILED, FsError::SILENT, "User '$this->userName' has no write access to '$objectPath'");
		}

		protected function hasUserReadAccessToObject(string $objectPath)
		{
			if( TripleTowerError::isError($userPermissionsForObject = $this->getUserPermissionsForObject($objectPath)) )
				return $userPermissionsForObject;
			foreach( $userPermissionsForObject as $userPermissionForObject )
			{
				if( $userPermissionForObject === self::ACL_FULL_ACCESS || $userPermissionForObject === self::ACL_READ_ACCESS )
					return new FsError( FsError::SUCCESS);
			}

			return new FsError(FsError::UNKNOWN, FsError::SILENT, "User '$this->userName' has no read access to '$objectPath'");
		}

		protected function getUserPermissionsForObject(string $objectPath)
		{
			$canonicalQueryString = "acl";
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath() . Helper::addFirstCharIfNot($objectPath, "/"));

			if( TripleTowerError::isError($response = $this->getInformationFromS3($canonicalQueryString,$canonicalURI)) )
				return $response;

			try
			{
				$simpleXmlResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $response->getBody()));
				return $simpleXmlResponse->xpath("GetObjectAclOutput/AccessControlList/Grant[Grantee/ID=$this->userName]/Permission");

			}
			catch(\Exception $ex)
			{
				return new FsError(FsError::UNKNOWN, FsError::ERR, "Could get ACL for object with path '$objectPath'");
			}
		}

		protected function genHttpHeader($headerParts,$requestMethod,$canonicalURI, $canonicalQueryString,$hashedRequestPayload,$dateTime)
		{
			foreach( $headerParts as $key => $value )
			{
				$htmlHeader[] = "$key: $value";
			}
			$htmlHeader[] = $this->genAuthorizationHeader($headerParts, $requestMethod, $canonicalURI, $canonicalQueryString, $hashedRequestPayload, $dateTime);
			return $htmlHeader;
		}

		protected function genAuthorizationHeader($headerParts,$requestMethod,$canonicalURI, $canonicalQueryString,$hashedRequestPayload,$dateTime)
		{
			ksort($headerParts);
			foreach( $headerParts as $key => $value )
			{
				$lKey = strtolower($key);
				$preparedValue = str_replace("  ", " ", $value);
				$signedHeaders = isset($signedHeaders) === true ? $signedHeaders . ";" . $lKey : $lKey;
				$canonicalHeaders = isset($canonicalHeaders) === true ? "$canonicalHeaders$lKey:$preparedValue\n" : "$lKey:$preparedValue\n";
			}

			$canonicalRequest = "$requestMethod\n" .
								"$canonicalURI\n" .
								"$canonicalQueryString\n".
								"$canonicalHeaders\n" .
								"$signedHeaders\n" .
								$hashedRequestPayload;


			$credentialScope = $dateTime->format("Ymd")."/" . self::AWS_REGION . "/". self::AWS_SERVICE . "/" . self::AWS_REQUEST_TYPE;
			$stringToSign = self::AWS4_HASH_ALGORYTHM_NAME . "\n"
				. $dateTime->format("Ymd\THis\Z") ."\n"
				. "$credentialScope\n"
				. hash(self::AWS4_HASH_ALGORITHM,$canonicalRequest);


			//The data and the key must be swichted in order to the aws documentation
			$dateKey = hash_hmac(self::AWS4_HASH_ALGORITHM, $dateTime->format("Ymd"), "AWS4$this->password" ,true);
			$dateRegionKey = hash_hmac(self::AWS4_HASH_ALGORITHM, self::AWS_REGION , $dateKey ,true);
			$dateRegionServiceKey = hash_hmac(self::AWS4_HASH_ALGORITHM, self::AWS_SERVICE, $dateRegionKey, true);
			$signingKey = hash_hmac(self::AWS4_HASH_ALGORITHM, self::AWS_REQUEST_TYPE, $dateRegionServiceKey, true);

			$signature = hash_hmac(self::AWS4_HASH_ALGORITHM, $stringToSign, $signingKey);

			return "Authorization: ". self::AWS4_HASH_ALGORYTHM_NAME . " Credential=$this->userName/$credentialScope, SignedHeaders=$signedHeaders, Signature=$signature";
		}

		protected function listObjectsWithPrefix($prefix, $recursive = false)
		{
			$canonicalQueryString = $recursive ? "list-type=2&prefix=" . self::awsParamRawUrlEncode($prefix) :
					"delimiter=" . self::awsParamRawUrlEncode("/") . "&list-type=2&prefix=".self::awsParamRawUrlEncode($prefix);
			$canonicalURI = self::awsPathRawUrlEncode($this->getBasePath());

			if( TripleTowerError::isError($response = $this->getInformationFromS3($canonicalQueryString,$canonicalURI)) )
				return $response;

			$simpleXmlResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $response->getBody()));

			$contentsArray = [];
			$commonPrefixeNodes = $simpleXmlResponse->xpath("CommonPrefixes/Prefix");
			if( count($commonPrefixeNodes) > 0)
				$contentsArray = array_merge($contentsArray, self::toStringArray($commonPrefixeNodes));

			$contentKeyNodes = $simpleXmlResponse->xpath("Contents/Key");
			if( count($contentKeyNodes) > 0 )
				$contentsArray = array_merge($contentsArray, self::toStringArray($contentKeyNodes));

			$lastContinuationToken = null;
			$nextContinuationToken = isset($simpleXmlResponse->NextContinuationToken) ? (string)($simpleXmlResponse->NextContinuationToken) : null;
			while( $nextContinuationToken !== $lastContinuationToken && isset($simpleXmlResponse->IsTruncated) && (string)($simpleXmlResponse->IsTruncated) === "true" )
			{
				$canonicalQueryStringExt = "continuation-token=".self::awsParamRawUrlEncode($nextContinuationToken) .
						"&" . $canonicalQueryString;
				if( TripleTowerError::isError($response = $this->getInformationFromS3($canonicalQueryStringExt,	$canonicalURI)) )
					return $response;

				$simpleXmlResponse = new \SimpleXMLElement(str_replace("xmlns=", "xmlns1=", $response->getBody()));
				$responseArray = Helper::xmlToArray($simpleXmlResponse);

				$commonPrefixeNodes = $simpleXmlResponse->xpath("CommonPrefixes/Prefix");
				if( count($commonPrefixeNodes) > 0)
					$contentsArray = array_merge($contentsArray, self::toStringArray($commonPrefixeNodes));

				$contentKeyNodes = $simpleXmlResponse->xpath("Contents/Key");
				if( count($contentKeyNodes) > 0 )
					$contentsArray = array_merge($contentsArray, self::toStringArray($contentKeyNodes));

				$lastContinuationToken = $nextContinuationToken;

				$nextContinuationToken = isset($simpleXmlResponse->NextContinuationToken) ? (string)($simpleXmlResponse->NextContinuationToken) : null;
			}

			$contentsArray = array_unique($contentsArray);
			sort($contentsArray);
			return $contentsArray;
		}

		protected static function awsParamRawUrlEncode(string $urlParameterToEncode)
		{
			// This method is used for AWS parameters only.
			// It rawurl encodes and DOUBLE encodes equal signs (=)
			//
			// See docs for more information (section 3 b):
			// https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html

			return preg_replace_callback("([^A-Za-z0-9_~\-\.])", function($matches)
			{
				$char = $matches[0];

				// Equal symbol "=" -> double encode
				if( $char === "=")
					return "%" . strtoupper(bin2hex( "%".strtoupper(bin2hex( $char )) ));

				// Everything else -> single encode
				return "%" . strtoupper( bin2hex( $char ) );
			}, $urlParameterToEncode);
		}

		protected static function awsPathRawUrlEncode(string $urlPathToEncode)
		{
			// This method is used for AWS parameters only.
			// It rawurl encodes EXCEPT for slashes and adds a slash at the beginning if necessary
			//
			// See docs for more information (section 2):
			// https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
			return preg_replace_callback("([^A-Za-z0-9_~\-\.\/])", function($matches)
			{
				$char = $matches[0];
				return "%" . strtoupper(bin2hex( $char ));
			},
			Helper::addFirstCharIfNot($urlPathToEncode, "/"));
		}

		protected static function toStringArray(array $objectsArray)
		{
			$outArray = [];
			foreach( $objectsArray as $object)
			{
				$outArray[] = (string)$object;
			}
			return $outArray;
		}
	}
}