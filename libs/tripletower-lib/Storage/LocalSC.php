<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;

	class LocalSC extends StorageConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/
		const INVALID_FILESYTEM_CHARACTERS = [ '\\', '/', ':', '*', '?', '"', '<', '>', '|'];

		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $host, string $basePath, string $userName, string $password, string $caCertificateFilePath, int $timeOut)
		{
			parent::__construct($host, $basePath, $userName, $password, $caCertificateFilePath, $timeOut);
		}

		public function changeTimes(string $filePath, int $time = null, int $atime = null) : TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( touch($fullFilePath, $time, $atime) === false )
				return new FsError(FsError::FILE_TIMECHANGE_FAILED, FsError::ERR, "Could not change time of file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		public function copyFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$fullSourceFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $sourceFilePath);
			$fullDestinationFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $destinationFilePath);
			if( copy($fullSourceFilePath, $fullDestinationFilePath) === false )
				return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file '$sourceFilePath' to '$destinationFilePath'");

			return new FsError(FsError::SUCCESS);
		}

		public function copyFileOrFolder(string $sourcePath, string $destinationPath) : TripleTowerError
		{
			$fullSourcePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $sourcePath);
			$fullDestinationPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $destinationPath);
			if( Helper::copy_recursive($fullSourcePath, $fullDestinationPath) === false )
				return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file or folder '$sourcePath' to '$destinationPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function copyFolder(string $sourceFolderPath, string $destinationFolderPath) : TripleTowerError
		{
			$fullSourceFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $sourceFolderPath);
			$fullDestinationFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $destinationFolderPath);
			if( Helper::copy_recursive($fullSourceFolderPath, $fullDestinationFolderPath) === false )
				return new FsError(FsError::FOLDER_COPYING_FAILED, FsError::ERR, "Could not copy folder '$sourceFolderPath' to '$destinationFolderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function createFolder(string $folderPath, int $mode = 0777, bool $recursive = false) : TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $folderPath);
			if( mkdir($fullFolderPath, $mode, $recursive) === false )
				return new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "Could not create folder '$folderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFile(string $filePath) : TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( unlink($fullFilePath) === false )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFileOrFolder(string $fileOrFolderPath, bool $recursive = false) : TripleTowerError
		{
			$fullFileOrFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( is_file($fullFileOrFolderPath) || is_link($fullFileOrFolderPath) )
				return $this->deleteFile($fullFileOrFolderPath);

			if( is_dir($fullFileOrFolderPath) )
				return $this->deleteFolder($fullFileOrFolderPath, $recursive);

			return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "'$fullFileOrFolderPath' is neither a file or link, nor a folder, so it couldn't be deleted");
		}

		public function deleteFolder(string $folderPath, bool $recursive = false) : TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $folderPath);
			if( $recursive )
			{
				return Helper::unlink_recursive($fullFolderPath);
			}
			else
			{
				if( rmdir($fullFolderPath) === false )
					return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete folder '$folderPath'");

				return new FsError(FsError::SUCCESS);
			}
		}

		public function fileExists(string $filePath) : TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( file_exists($fullFilePath) === false )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File '$filePath' does not exist");

			if( is_file($fullFilePath) === false && is_link($fullFilePath) === false)
				return new FsError(FsError::IS_NOT_A_FILE, FsError::ERR, "File '$filePath' is no file or link");

			return new FsError(FsError::SUCCESS);
		}

		public function fileOrFolderExists(string $fileOrFolderPath) : TripleTowerError
		{
			$fullFileOrFolderPath = Helper::normalizePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( file_exists($fullFileOrFolderPath) === false )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File or Folder '$fileOrFolderPath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function folderExists(string $folderPath) : TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFolderPath($this->getBasePathWithHost() . "/" . $folderPath);
			if( file_exists($fullFolderPath) === false )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "Folder '$fullFolderPath' does not exist");

			if( is_dir($fullFolderPath) === false )
				return new FsError(FsError::IS_NOT_A_FOLDER, FsError::ERR, "'$folderPath' exists but is no folder");

			return new FsError(FsError::SUCCESS);
		}

		public function getFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$fullSourceFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $sourceFilePath);
			return Helper::downloadFile($fullSourceFilePath, $destinationFilePath, false);
		}

		public function getFileContents(string $sourceFilePath) // : string or TripleTowerError
		{
			$fullSourceFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $sourceFilePath);
			return Helper::downloadFile($fullSourceFilePath, "", false);
		}

		// Watch out! This is an expensive implementation, since it fully fetches the whole file each time called and
		// then generates the hash
		public function getFileHash(string $filePath, string $hashAlgorithm) // : string or TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( in_array($hashAlgorithm, hash_algos()) === false )
				return new SysError(SysError::INVALID_PARAM, SysError::ERR, "The given hash algorithm '$hashAlgorithm' is unknown");

			$hash = hash_file($hashAlgorithm, $fullFilePath);
			return ($hash !== false ? $hash : new FsError(FsError::FILE_HASH_CREATION_FAILED, FsError::ERR, "Could not create '$hashAlgorithm' file hash of '$filePath'"));
		}

		public function getFileType(string $filePath) // :string or TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( $this->isFolder($fullFilePath)->isSuccess() )
				return "httpd/unix-directory";

			$mimeType = mime_content_type($fullFilePath);
			return ($mimeType !== false ? $mimeType : new FsError(FsError::MIMETYPE_UNKNOWN, FsError::ERR, "Could not determine mime type of file '$filePath'"));
		}

		public function getFolderContents(string $folderPath) // : array or TripleTowerError
		{
			$folderResult = $this->isFolder($folderPath);
			if( $folderResult->isSuccess() === false )
				return $folderResult;

			$fullFolderPath = Helper::normalizeFolderPath($this->getBasePathWithHost() . "/" . $folderPath);
			$folderContent = scandir($fullFolderPath, SCANDIR_SORT_ASCENDING);
			return ($folderContent !== false ? $folderContent : new FsError(FsError::SCANDIR_FAILED, FsError::ERR, "Could not get folder contents of folder '$folderPath'"));
		}

		public function getFreeSpace(string $folderPath) // : int or TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFolderPath($this->getBasePathWithHost() . "/" . $folderPath);
			$freeSpace = disk_free_space($fullFolderPath);
			return ($freeSpace !== false ? $freeSpace : new FsError(FsError::FREE_SPACE_DETERMINATION_FAILED, FsError::ERR, "Could not determine space in folder '$folderPath'"));
		}

		public function getId(string $fileOrFolderPath)  // : string or TripleTowerError
		{
			$fullFileOrFolderPath = Helper::normalizePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( fileinode($fullFileOrFolderPath) === false )
				return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could read inode value of file or folder '$fileOrFolderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function getMetaData(string $fileOrFolderPath) // : array or TripleTowerError // Should be php stat compatible
		{
			$fullFileOrFolderPath = Helper::normalizePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( stat($fullFileOrFolderPath) === false )
				return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could not retrieve meta data of file or folder '$fileOrFolderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function isCreatable(string $fileOrFolderPath) : TripleTowerError
		{
			return $this->isWritable($fileOrFolderPath);
		}

		public function isDeletable(string $fileOrFolderPath) : TripleTowerError
		{
			return $this->isWritable($fileOrFolderPath);
		}

		public function isFile(string $filePath) : TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			if( is_file($fullFilePath) === false )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::ERR, "File '$filePath' is no file");

			return new FsError(FsError::SUCCESS);
		}

		public function isFolder(string $folderPath) : TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFolderPath($this->getBasePathWithHost() . "/" . $folderPath);
			if( is_dir($fullFolderPath) === false )
				return new FsError(FsError::IS_NOT_A_FOLDER, FsError::ERR, "Folder '$folderPath' is no folder");

			return new FsError(FsError::SUCCESS);
		}

		public function isReadable(string $fileOrFolderPath) : TripleTowerError
		{
			$fullFileOrFolderPath = Helper::normalizePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( is_readable($fullFileOrFolderPath) === false )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::ERR, "File or folder '$fileOrFolderPath' is not readable");

			return new FsError(FsError::SUCCESS);
		}

		public function isSharable(string $fileOrFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_SHARING_FAILED, FsError::ERR, "File or folder '$fileOrFolderPath' can't be shared on via Storage '" . __CLASS__ . "'");
		}

		public function isWritable(string $fileOrFolderPath) : TripleTowerError
		{
			$fullFileOrFolderPath = Helper::normalizePath($this->getBasePathWithHost() . "/" . $fileOrFolderPath);
			if( is_writable($fullFileOrFolderPath) === false )
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::ERR, "File or folder '$fileOrFolderPath' is not writable");

			return new FsError(FsError::SUCCESS);
		}

		public function putFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$contents = file_get_contents($sourceFilePath);
			if( TripleTowerError::isError($contents) )
				return $contents;

			return new $this->putFileContents($destinationFilePath);
		}

		public function putFileContents(string $destinationFilePath, string $contents) : TripleTowerError
		{
			$fullDestinationFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $destinationFilePath);

			if( file_put_contents($fullDestinationFilePath, $contents) === false )
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::ERR, "File '$destinationFilePath' could not be written");

			return new FsError(FsError::SUCCESS);
		}

		public function renameFile(string $oldFileName, string $newFileName) : TripleTowerError
		{
			$isFileResult = $this->isFile($oldFileName);
			if( $isFileResult->isSuccess() === false )
				return $isFileResult;

			return $this->renameFileOrFolder($oldFileName, $newFileName);
		}

		public function renameFileOrFolder(string $oldFileOrFolderName, string $newFileOrFolderName) : TripleTowerError
		{
			$fullOldFileOrFolderName = Helper::normalizePath($this->getBasePathWithHost() . "/" . $oldFileOrFolderName);
			$fullNewFileOrFolderName = Helper::normalizePath($this->getBasePathWithHost() . "/" . $newFileOrFolderName);
			if( rename($oldFileOrFolderName, $newFileOrFolderName) === false )
				return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, "File or folder '$oldFileOrFolderName' could not be renamed to '$newFileOrFolderName'");

			return new FsError(FsError::SUCCESS);
		}

		public function renameFolder(string $oldFolderName, string $newFolderName) : TripleTowerError
		{
			$isFolderResult = $this->isFolder($oldFolderName);
			if( $isFolderResult->isSuccess() === false )
				return $isFolderResult;

			return $this->renameFileOrFolder($oldFolderName, $newFolderName);
		}

		public function openFile(string $filePath, string $mode) // :resource or TripleTowerError
		{
			$fullFilePath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $filePath);
			$openFileResult = fopen($fullFilePath, $mode);
			return ($openFileResult !== false ? $openFileResult : new FsError(FsError::FILE_OPENING_FAILED, FsError::ERR, "Could not open file '$filePath' with mode '$mode'"));
		}

		public function openFolder(string $folderPath) // :resource or TripleTowerError
		{
			$fullFolderPath = Helper::normalizeFilePath($this->getBasePathWithHost() . "/" . $folderPath);
			$openFolderResult = opendir($fullFolderPath);
			return ($openFolderResult !== false ? $openFolderResult : new FsError(FsError::FOLDER_OPENING_FAILED, FsError::ERR, "Could not open folder '$folderPath'"));
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function getBasePathWithHost()
		{
			if( $this->getHost() === "" )
				return $this->getBasePath();
			else
				return "\\\\" . $this->getHost() . "/" . $this->getBasePath();
		}
	}
}