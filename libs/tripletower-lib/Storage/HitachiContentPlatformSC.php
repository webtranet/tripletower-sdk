<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\HttpError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Network\HttpRequest;
	use TripleTowerSDK\Network\HttpResponse;

	class HitachiContentPlatformSC extends StorageConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const PROTOCOL = "https://";

		const HCP_HASH_ALGORYTM = "sha256";

		const HCP_HEADER_NAME_HASH = "x-hcp-hash";

		const HCP_HASH_PREFIX = "SHA-256 ";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $host, string $basePath, string $userName, string $password, string $caCertificateFilePath, int $timeOut)
		{
			parent::__construct($host, $basePath, $userName, $password, $caCertificateFilePath, $timeOut);
		}

		public function changeTimes(string $filePath, int $time = null, int $atime = null) : TripleTowerError
		{
			return new FsError(FsError::FILE_TIMECHANGE_FAILED, FsError::ERR, "Could not change time of file '$filePath'");
		}

		public function copyFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file '$sourceFilePath' to '$destinationFilePath'");
		}

		public function copyFileOrFolder(string $sourcePath, string $destinationPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_COPYING_FAILED, FsError::ERR, "Could not copy file or folder '$sourcePath' to '$destinationPath'");
		}

		public function copyFolder(string $sourceFolderPath, string $destinationFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FOLDER_COPYING_FAILED, FsError::ERR, "Could not copy folder '$sourceFolderPath' to '$destinationFolderPath'");
		}

		public function createFolder(string $folderPath, int $mode = 0777, bool $recursive = false) : TripleTowerError
		{
			$folderPath = Helper::normalizeFolderPath($folderPath);
			if($folderPath === '/')
				$folderPath = '';

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . "$folderPath?type=directory";
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->put();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode([HttpError::HTTP_OK, HttpError::HTTP_CREATED, HttpError::HTTP_CONFLICT])->isSuccess() )
				return new FsError(FsError::FOLDER_CREATION_FAILED, FsError::ERR, "Could not create folder '$folderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFile(string $filePath) : TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);
			if( !$this->deleteFileOrFolder($folderPath)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFileOrFolder(string $fileOrFolderPath, bool $recursive = false) : TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);
			if($fileOrFolderPath === '/')
				$fileOrFolderPath = '';

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $fileOrFolderPath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->delete();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode([HttpError::HTTP_OK])->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete file or folder '$fileOrFolderPath' recursively");

			return new FsError(FsError::SUCCESS);
		}

		public function deleteFolder(string $folderPath, bool $recursive = false) : TripleTowerError
		{
			$folderPath = Helper::normalizeFolderPath($folderPath);
			if( !$this->deleteFileOrFolder($folderPath)->isSuccess() )
				return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "Could not delete folder '$folderPath'");

			return new FsError(FsError::SUCCESS);
		}

		public function fileExists(string $filePath) : TripleTowerError
		{
			if( !$this->isFile($filePath)->isSuccess() )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File '$filePath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function fileOrFolderExists(string $fileOrFolderPath) : TripleTowerError
		{
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $filePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->compareHeaderValue("x-hcp-type", ["object", "directory"])->isSuccess() )
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "File or Folder '$fileOrFolderPath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function folderExists(string $folderPath) : TripleTowerError
		{
			if(!$this->isFolder($folderPath)->isSuccess())
				return new FsError(FsError::FILENAME_NOT_EXISTS, FsError::ERR, "Folder '$folderPath' does not exist");

			return new FsError(FsError::SUCCESS);
		}

		public function getFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$destinationFilePath = Helper::normalizeFilePath($destinationFilePath);

			$fileContent = $this->getFileContents($sourceFilePath);

			$destFolderPath = Helper::extractFolderName($destinationFilePath);
			$pathCreationResult = Helper::createPathIfNotExists($destFolderPath);
			if( !$pathCreationResult->isSuccess() )
				return $pathCreationResult;

			if( file_put_contents($destinationFilePath, $fileContent) === false )
				return new FsError(FsError::FILE_CREATION_FAILED);

			return new FsError(FsError::SUCCESS);
		}

		public function getFileContents(string $sourceFilePath) // : string or TripleTowerError
		{
			$sourceFilePath = Helper::normalizeFilePath($sourceFilePath);

			if( $sourceFilePath === "" )
				return new FsError(FsError::NAME_IS_INVALID, FsError::ERR, "SourceFilePath '' is invalid");

			$fileName = Helper::extractFileName($sourceFilePath);
			if( $fileName === "" )
				return new FsError(FsError::NAME_IS_INVALID, FsError::ERR, "FileName '' is invalid");

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $sourceFilePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->get();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess())
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not open file '$sourceFilename' for reading");

			$hcpFileHashExpected = strtoupper(self::HCP_HASH_PREFIX . hash(self::HCP_HASH_ALGORYTM, $httpResponse->getBody()));
			if( !$httpResponse->compareHeaderValue(self::HCP_HEADER_NAME_HASH, $hcpFileHashExpected)->isSuccess() )
				return new FsError(FsError::FILE_HASH_COMPARISON_FAILED, FsError::ERR, "Hash of the donloaded file does not match to the expected hash.");

			return $httpResponse->getBody();
		}

		public function getFileHash(string $filePath, string $hashAlgorithm) // : string or TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);
			if( $hashAlgorithm !== self::HCP_HASH_ALGORYTM )
				return new FsError(FsError::FILE_HASH_CREATION_FAILED , SysError::ERR, "The given hash algorithm '$hashAlgorithm' is not suported by the HCP storage conector");

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $filePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if( TripleTowerError::isError($httpResponse) || TripleTowerError::isError($hash = $httpResponse->getHeaderValue(self::HCP_HEADER_NAME_HASH)))
				return new FsError(FsError::FILE_HASH_CREATION_FAILED, FsError::ERR, "Could not create '$hashAlgorithm' file hash of '$filePath'");

			return strtolower($this->removeHcpHashPrefix($hash));
		}

		public function getFileType(string $filePath) // :string or TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $filePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if( TripleTowerError::isError($httpResponse) || TripleTowerError::isError($fileType = $httpResponse->getHeaderValue("Content-Type")))
				return new FsError(FsError::MIMETYPE_UNKNOWN, FsError::ERR, "Could not determine mime type of file '$filePath'");

			return $fileType;
		}

		public function getFolderContents(string $folderPath) // : array or TripleTowerError
		{
			$folderPath = Helper::normalizeFolderPath($folderPath);
			if($folderPath === '/')
				$folderPath = '';

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $folderPath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->get();
			if( !TripleTowerError::isError($httpResponse) && $httpResponse->checkResponseCode(HttpError::HTTP_OK)->isSuccess() )
			{
				$simpleXmlResponse = simplexml_load_string( $httpResponse->getBody() );
				if($simpleXmlResponse !== false)
				{
					$array = array_map("strval", $simpleXmlResponse->xpath("entry/@utf8Name"));
					sort($array);
					return $array;
				}
			}

			return new FsError(FsError::SCANDIR_FAILED, FsError::ERR, "Could not get folder contents of folder '$folderPath'");
		}

		public function getFreeSpace(string $folderPath) // : int or TripleTowerError
		{
			return new FsError(FsError::FREE_SPACE_DETERMINATION_FAILED, FsError::ERR, "Could not determine space in folder '$folderPath'");
		}

		public function getId(string $fileOrFolderPath) // : string or TripleTowerError
		{
			return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could read inode value of file or folder '$fileOrFolderPath'");
		}

		public function getMetaData(string $fileOrFolderPath) // : array or TripleTowerError // Should be php stat compatible
		{
			//TODO: check header keys and test
			$fileOrFolderPath = Helper::normalizePath($fileOrFolderPath);

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $fileOrFolderPath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if(TripleTowerError::isError($httpResponse))
				return new FsError(FsError::IS_NOT_A_FOLDER, FsError::ERR, "Folder '$folderPath' is no folder");

			if( TripleTowerError::isError($hcpType = $httpResponse->getHeaders("x-hcp-type")) ||
				TripleTowerError::isError($hcpSize = $httpResponse->getHeaders("x-hcp-size")) ||
				TripleTowerError::isError($hcpIgnestTime = $httpResponse->getHeaders("x-hcp-ignesttime")) )
			{
				return new FsError(FsError::ATTRIBUTE_READING_FAILED, FsError::ERR, "Could not retrieve meta data of file or folder '$fileOrFolderPath'");
			}

			$mode = ($hcpType === "directory" ? octdec(0040000) : octdec(0100000));

			return
			[
				"dev" => 1,
				"ino" => 0,
				"mode" => $mode,
				"nlink" => 1,
				"uid" => 0,
				"gid" => 0,
				"rdev" => 1,
				"size" => intval($hcpSize),
				"atime" => intval($hcpIgnestTime),
				"mtime" => intval($hcpIgnestTime)
				/*"ctime" => "",
				"blksize" => -1,
				"blocks" => -1*/
			];

		}

		public function isCreatable(string $fileOrFolderPath) : TripleTowerError
		{
			if( $this->fileOrFolderExists($fileOrFolderPath)->isSuccess() )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::SILENT, "File or folder '$fileOrFolderPath' is not createable");

			return new FsError(FsError::SUCCESS);
		}

		public function isDeletable(string $fileOrFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_DELETION_FAILED, FsError::SILENT, "File or folder '$fileOrFolderPath' is not deleteable");
		}

		public function isFile(string $filePath) : TripleTowerError
		{
			$filePath = Helper::normalizeFilePath($filePath);

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $filePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->compareHeaderValue("x-hcp-type", "object")->isSuccess() )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::SILENT, "File '$filePath' is no file");

			return new FsError(FsError::SUCCESS);

		}

		public function isFolder(string $folderPath) : TripleTowerError
		{
			$folderPath = Helper::normalizeFolderPath($folderPath);

			$url = self::PROTOCOL . parent::getHost() . "/" . parent::getBasePath() . $folderPath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->head();
			if(TripleTowerError::isError($httpResponse) || !$httpResponse->compareHeaderValue("x-hcp-type", "directory")->isSuccess() )
				return new FsError(FsError::IS_NOT_A_FOLDER, FsError::SILENT, "Folder '$folderPath' is no folder");

			return new FsError(FsError::SUCCESS);
		}

		public function isReadable(string $fileOrFolderPath) : TripleTowerError
		{
			if( !$this->fileOrFolderExists($fileOrFolderPath)->isSuccess() )
				return new FsError(FsError::IS_NOT_A_FILE, FsError::SILENT, "File or folder '$fileOrFolderPath' is not readable");

			return new FsError(FsError::SUCCESS);
		}

		public function isSharable(string $fileOrFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_SHARING_FAILED, FsError::SILENT, "File or folder '$fileOrFolderPath' can't be shared");
		}

		public function isWritable(string $fileOrFolderPath) : TripleTowerError
		{
			return new FsError(FsError::FILE_WRITING_FAILED, FsError::SILENT, "File or folder '$fileOrFolderPath' is not writable");
		}

		public function putFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError
		{
			$destinationFilePath = Helper::normalizeFilePath($destinationFilePath);
			$sourceFilePath = Helper::normalizeFilePath( $sourceFilePath );

			$destinationFolderPath = Helper::extractFolderName($destinationFilePath);
			if( $destinationFilePath === "" || $destinationFolderPath === "" )
				return new FsError(FsError::NAME_IS_INVALID);

			if( is_file($sourceFilePath) !== true)
				return new FsError(FsError::FILE_NOT_FOUND);

			$folderCreation = $this->createFolder($destinationFolderPath);
			if( !$folderCreation->isSuccess() )
				return $folderCreation;

			$url = self::PROTOCOL . parent::getHost() ."/" .  parent::getBasePath() . $destinationFilePath;
			$httpRequest = new HttpRequest($url, [], $this->caCertificateFilePath, $this->timeOutValue, $this->getAuthentificationCookie());
			$httpResponse = $httpRequest->put($sourceFilePath);
			$hcpFileHashExpected = strtoupper(self::HCP_HASH_PREFIX . hash_file(self::HCP_HASH_ALGORYTM, $sourceFilePath));

			if(TripleTowerError::isError($httpResponse) ||
				!$httpResponse->checkResponseCode([HttpError::HTTP_OK, HttpError::HTTP_CREATED])->isSuccess() ||
				!$httpResponse->compareHeaderValue(self::HCP_HEADER_NAME_HASH, $hcpFileHashExpected)->isSuccess() )
			{
				return new FsError(FsError::FILE_WRITING_FAILED, FsError::ERR, "File '$destinationFilePath' could not be written");
			}

			return new FsError(FsError::SUCCESS);
		}

		public function putFileContents(string $destinationFilePath, string $contents) : TripleTowerError
		{
			$tmpFile = tmpfile();
			fwrite(tmpfile, $contents);
			$sourceFilePath = stream_get_meta_data($tmpFile)['uri'];

			$response = $this->putFile($sourceFilePath, $destinationFilePath);
			fclose($tmpFile);
			return $response;
		}

		public function renameFile(string $oldFileName, string $newFileName) : TripleTowerError
		{
			return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, "File '$oldFileName' could not be renamed to '$newFileName'");
		}

		public function renameFileOrFolder(string $oldFileOrFolderName, string $newFileOrFolderName) : TripleTowerError
		{
			return new FsError(FsError::FILE_RENAMING_FAILED, FsError::ERR, "File or folder '$oldFileOrFolderName' could not be renamed to '$newFileOrFolderName'");
		}

		public function renameFolder(string $oldFolderName, string $newFolderName) : TripleTowerError
		{
			return new FsError(FsError::FOLDER_RENAMING_FAILED, FsError::ERR, "Folder '$oldFolderName' could not be renamed to '$newFolderName'");
		}

		public function openFile(string $filePath, string $mode) // :resource or TripleTowerError
		{
			return new FsError(FsError::FILE_OPENING_FAILED, FsError::ERR, "Could not open file '$filePath' with mode '$mode'");
		}

		public function openFolder(string $folderPath) // :resource or TripleTowerError
		{
			return new FsError(FsError::FOLDER_OPENING_FAILED, FsError::ERR, "Could not open folder '$folderPath'");
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function removeHcpHashPrefix(string $hcpHash)
		{
			return substr($hcpHash, strlen(self::HCP_HASH_PREFIX));
		}

		protected function getAuthentificationCookie()
		{
			$cookieName = strpos(parent::getHost(), ":9090/") ? "hcp-api-auth" : "hcp-ns-auth";
			return [ $cookieName => base64_encode($this->userName).":".md5($this->password) ];
		}
	}
}