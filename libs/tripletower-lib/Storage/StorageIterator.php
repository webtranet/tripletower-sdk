<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Storage\StorageObject;

	class StorageIterator implements \Iterator
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected StorageConnector $storage;
		protected ?string $baseFolderPath;
		protected ?string $startPath;
		protected ?string $startFilePath;
		protected ?string $startFolderPath;
		protected ?string $currentPath;
		protected ?string $currentFolderPath;
		protected ?array $currentFolderContent;
		protected int $currentFolderContentIndex;
		protected bool $ignoreFolders;
		protected bool $currentIsFolder;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(StorageConnector $storage, string $baseFolderPath = "", bool $ignoreFolders = false, ?string $startPath = "")
		{
			$this->storage       = $storage;
			$this->ignoreFolders = $ignoreFolders;

			$storage->isFolder($baseFolderPath)->throwIfNotSuccess();
			$this->baseFolderPath = $baseFolderPath;

			$this->startPath  = !empty($startPath) ? $startPath : $this->baseFolderPath;


			if( $this->startPath === $this->baseFolderPath || $storage->isFolder($startPath)->isSuccess() )
			{
				$this->startFolderPath = $this->startPath;
				$this->currentIsFolder = true;
				$this->currentPath = $this->startFolderPath;
				$this->currentFolderPath = $this->startFolderPath;
				$this->startFilePath = null;
				if($ignoreFolders)
				{
					$this->next();
					$this->startFilePath = $this->currentPath;
				}
			}
			else
			{
				$storage->isFile($this->startPath)->throwIfNotSuccess();
				$this->startFolderPath = $this->getParrentFolder($this->startPath);
				$this->startFilePath = $this->startPath;
				$this->currentFolderPath = $this->startFolderPath;
				$this->currentPath = $this->startPath;

				$this->currentFolderContent = $storage->getFolderContents($this->currentFolderPath);
				if( TripleTowerError::isError($this->currentFolderContent) )
					throw $this->currentFolderContent;

				$currentFileName = $this->getObjectName($this->currentPath);
				$this->currentFolderContentIndex = array_search($currentFileName, $this->currentFolderContent);
				$this->currentIsFolder = false;
			}
		}

		public function rewind()
		{
			$this->currentFolderContent = $this->storage->getFolderContents($this->startFolderPath);
			$this->currentPath = $this->ignoreFolders ? $this->startFilePath : $this->startPath;
			$this->currentFolderPath = $this->startFolderPath;

		}

		public function current()
		{
			return new StorageObject($this->storage, $this->key());
		}

		public function key()
		{
			return Helper::addFirstCharIfNot( $this->currentPath, "/");
		}

		public function next()
		{
			if( $this->currentIsFolder )
			{
				$folderContent = $this->storage->getFolderContents($this->currentPath);
				if( TripleTowerError::isError($folderContent) )
					throw $this->currentFolderContent;

				$this->currentFolderContent = $folderContent;

				$this->currentFolderPath = Helper::removeLastChar($this->currentPath, "/");
				$this->currentFolderContentIndex = -1;
			}

			$this->currentFolderContentIndex++;

			if( $this->currentFolderContentIndex >= count($this->currentFolderContent) )
			{
				if( $this->currentFolderPath === $this->baseFolderPath || strlen($this->currentFolderPath) <= strlen($this->baseFolderPath) )
				{
					$this->currentFolderContentIndex = -1;
					$this->currentPath = null;
					return;
				}
				else
				{
					$parentFolderPath = $this->getParrentFolder($this->currentFolderPath);
					$parentFolderContent = $this->storage->getFolderContents($parentFolderPath);
					if( TripleTowerError::isError($parentFolderContent) )
						throw $parentFolderContent;


					$this->currentFolderContent = $parentFolderContent;
					$currentFolderName = $this->getObjectName($this->currentFolderPath);
					$this->currentFolderContentIndex = array_search($currentFolderName, $parentFolderContent);
					$this->currentFolderPath = $parentFolderPath;
					$this->next();
				}
			}

			if( isset($this->currentFolderContent[$this->currentFolderContentIndex]) )
				$this->currentPath = $this->currentFolderPath . "/" . $this->currentFolderContent[$this->currentFolderContentIndex];
			else
				$this->currentPath = null;

			$this->currentIsFolder = (!empty($this->currentPath) && $this->storage->isFolder($this->currentPath)->isSuccess());

			if( $this->ignoreFolders && $this->currentIsFolder)
				$this->next();
		}

		public function valid()
		{
			return !empty($this->currentPath);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function getParrentFolder($path)
		{
			$path = Helper::removeLastChar($path, "/");
			return Helper::removeLastChar(Helper::normalizeFolderPath(substr($path, 0, strrpos($path, "/"))), "/");
		}

		protected function getObjectName($folderPath)
		{
			if( empty($folderPath) )
				return "";

			if( substr($folderPath, -1, 1) === "/" )
					return substr($folderPath, strrpos($folderPath, "/", -2) + 1 , -1);

			$poslastSlash = strrpos($folderPath, "/");
			if( $poslastSlash === false)
				return $folderPath;

			return substr($folderPath, strrpos($folderPath, "/") + 1);
		}
	}
}