<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{

	class StorageObject
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected StorageConnector $storage;
		protected string $objectPath;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(StorageConnector $storage, string $objectPath)
		{
			$this->storage = $storage;
			$this->objectPath = $objectPath;
		}

		public function changeTimes(string $filePath, int $time = null, int $atime = null) : TripleTowerError
		{
			$this->storage->changeTimes($this->objectPath, $time, $atime);
		}

		public function delete(bool $recursive = false) : TripleTowerError
		{
			$this->storage->deleteFileOrFolder($this->objectPath, $recursive);
		}

		public function copyObject(string $destinationPath) : TripleTowerError
		{
			return $this->storage->copyFileOrFolder($this->objectPath, $destinationPath);
		}

		public function getObjectPath()
		{
			return $this->objectPath;
		}

		public function getContent()
		{
			return $this->storage->isFile($this->objectPath) ? $this->storage->getFileContents($this->objectPath) : $this->storage->getFolderContents($this->objectPath);
		}

		public function getFileHash(string $hashAlgorithm)
		{
			return $this->storage->getFileHash($this->objectPath, $hashAlgorithm);
		}

		public function getFileType()
		{
			return $this->storage->getFileType($this->objectPath);
		}

		public function getId()
		{
			return $this->storage->getId($this->objectPath);
		}

		public function getMetaData()
		{
			return $this->storage->getMetaData($this->objectPath);
		}

		public function isDeletable() : TripleTowerError
		{
			return $this->storage->isDeletable($this->objectPath);
		}

		public function isFile() : TripleTowerError
		{
			return $this->storage->isFile($this->objectPath);
		}

		public function isFolder() : TripleTowerError
		{
			return $this->storage->isFolder($this->objectPath);
		}

		public function isReadable() : TripleTowerError
		{
			return $this->storage->isReadable($this->objectPath);
		}

		public function isSharable() : TripleTowerError
		{
			return $this->storage->isSharable($this->objectPath);
		}

		public function isWritable() : TripleTowerError
		{
			return $this->storage->isWritable($this->objectPath);
		}

		public function rename(string $newName) : TripleTowerError
		{
			return $this->storage->renameFileOrFolder($this->objectPath, $newName);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}