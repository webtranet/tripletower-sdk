<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Error\DevError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Storage\StorageConnector;

	class StorageManager
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $data = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct()
		{

		}

		public function getAvailableStorages() : array
		{
			$availableStorages = [];
			foreach($_SERVER["TT"]->conf as $configKey => $configValue)
			{
				if( !isset($configValue->scName) || !Helper::isClassRelative($configValue->scName, "TripleTowerSDK\\Storage\\StorageConnector") )
					continue;

				$availableStorages[] = $configKey;
			}
			return $availableStorages;
		}

		public function addStorage(string $linkName, JsonConfig $storageConf)
		{
			if(!$storageConf)
				throw new SysError(SysError::INVALID_PARAM);

			$linkName = strtolower($linkName);
			if($linkName === "")
				throw new SysError(SysError::EMPTY_STRING_ERROR);
			if(array_key_exists($linkName, $this->data))
				throw new SysError(SysError::PROPERTY_ALREADY_EXISTS);

			try
			{
				if(!class_exists($storageConf->scname))
				{
					require_once $storageConf->scName.".php";
					if(!class_exists($storageConf->scName))
						throw new DevError(DevError::CLASS_NOT_FOUND, DevError::NOTICE, "Class '$storageConf->scName' not available.");
				}

				$caCertificateFilePath = ($storageConf->caCertificateFilePath ?? $_SERVER["TT"]->tower->getEtcFolder() . "ssl/certs/ca-bundle.crt");
				$timeOut = ($storageConf->timeOut ?? StorageConnector::DEFAULT_TIMEOUT);
				$sc = new $storageConf->scName($storageConf->hostName, $storageConf->basePath, $storageConf->userName, $storageConf->password, $caCertificateFilePath, $timeOut);
				$this->data[$linkName] = $sc;
				return $sc;
			}
			catch(\Throwable $t)
			{
				throw new FsError(FsError::MOUNTING_FAILED, FsError::ERR, $t->getMessage());
			}
		}

		public function removeStorage(string $linkName)
		{
			$linkName = strtolower($linkName);
			if(!array_key_exists($linkName, $this->data))
				throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The storage '$linkName' could not be removed.");

			unset($this->data[$linkName]);
		}

		public function __get(string $key)
		{
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
			{
				return $this->data[$key];
			}
			else
			{
				// Storage auto-connect
				$storageConf = $_SERVER["TT"]->conf->$key;
				if($storageConf !== null)
					return $this->addStorage($key, $storageConf);
				else
					throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The property '$key' is not available.");

			}
		}

		public function __isset(string $key)
		{
			// Storage should not auto-connect when isset() is used,
			// it should just test if there is an open connection
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
				return true;
			else
				return false;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}