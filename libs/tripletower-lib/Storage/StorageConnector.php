<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Storage
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;

	abstract class StorageConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const STORAGETYPE_UNKNOWN = "unknown";

		const DEFAULT_TIMEOUT = 35;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $storageType  = self::STORAGETYPE_UNKNOWN;

		protected $host                  = "";
		protected $basePath              = "";
		protected $userName              = "";
		protected $password              = "";
		protected $caCertificateFilePath = "";
		protected $timeOutValue          = 0;

		protected $userAgent = "";


		/***********************************
		 * ABSTRACT METHODS                *
		***********************************/

		abstract public function changeTimes(string $filePath, int $time = null, int $atime = null) : TripleTowerError;

		abstract public function copyFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError;

		abstract public function copyFileOrFolder(string $sourcePath, string $destinationPath) : TripleTowerError;

		abstract public function copyFolder(string $sourceFolderPath, string $destinationFolderPath) : TripleTowerError;

		abstract public function createFolder(string $folderPath, int $mode = 0777, bool $recursive = false) : TripleTowerError;

		abstract public function deleteFile(string $filePath) : TripleTowerError;

		abstract public function deleteFileOrFolder(string $fileOrFolderPath, bool $recursive = false) : TripleTowerError;

		abstract public function deleteFolder(string $folderPath, bool $recursive = false) : TripleTowerError;

		abstract public function fileExists(string $filePath) : TripleTowerError;

		abstract public function fileOrFolderExists(string $fileOrFolderPath) : TripleTowerError;

		abstract public function folderExists(string $folderPath) : TripleTowerError;

		abstract public function getFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError;

		abstract public function getFileContents(string $sourceFilePath); // : string or TripleTowerError

		abstract public function getFileHash(string $filePath, string $hashAlgorithm); // : string or TripleTowerError

		abstract public function getFileType(string $filePath); // :string or TripleTowerError // Folders always have to return httpd/unix-directory

		abstract public function getFolderContents(string $folderPath); // : array or TripleTowerError

		abstract public function getFreeSpace(string $folderPath); // : int or TripleTowerError

		abstract public function getId(string $fileOrFolderPath);  // : string or TripleTowerError

		abstract public function getMetaData(string $fileOrFolderPath); // : array or TripleTowerError // Should be php stat compatible

		abstract public function isCreatable(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isDeletable(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isFile(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isFolder(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isReadable(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isSharable(string $fileOrFolderPath) : TripleTowerError;

		abstract public function isWritable(string $fileOrFolderPath) : TripleTowerError;

		abstract public function putFile(string $sourceFilePath, string $destinationFilePath) : TripleTowerError;

		abstract public function putFileContents(string $destinationFilePath, string $contents) : TripleTowerError;

		abstract public function renameFile(string $oldFileName, string $newFileName) : TripleTowerError;

		abstract public function renameFileOrFolder(string $oldFileOrFolderName, string $newFileOrFolderName) : TripleTowerError;

		abstract public function renameFolder(string $oldFolderName, string $newFolderName) : TripleTowerError;


		// Methods using resource
		abstract public function openFile(string $filePath, string $mode); // :resource or TripleTowerError

		abstract public function openFolder(string $folderPath); // :resource or TripleTowerError
		

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $host, string $basePath, string $userName, string $password, string $caCertificateFilePath, int $timeOut)
		{
			$classNameWithNameSpace = get_class($this);
			$this->storageType = substr($classNameWithNameSpace, strrpos($classNameWithNameSpace, "\\")+1, -strlen("SC"));

			$this->host                   = Helper::removeLastChar( strtolower($host), "/" );
			$this->basePath               = Helper::removeFirstChar( Helper::removeLastChar( strtolower($basePath), "/" ), "/");
			$this->userName               = trim($userName);
			$this->password               = $password;
			$this->caCertificateFilePath  = Helper::normalizeFilePath($caCertificateFilePath);
			$this->timeOut                = $timeOut;

			$phpVersion      = phpversion();
			$operatingSystem = strtolower(PHP_OS);
			$this->userAgent = "Mozilla/5.0 (compatible; TripleTower/1.0; php/$phpVersion $operatingSystem)";

			// TODO: TRY TO CONNECT HERE
		}


		public function getStorageType()
		{
			return $this->storageType;
		}

		public function getHost()
		{
			return $this->host;
		}

		public function getBasePath()
		{
			return $this->basePath;
		}

		public function getTimeOutValue()
		{
			return $this->timeOutValue;
		}

		public function getUserAgent()
		{
			return $this->userAgent;
		}

		/***********************************
		* COMPATIBILITY METHODS            *
		***********************************/
		// Alias of copyFileOrFolder
		public function copy(string $filename) : bool
		{
			return $this->copyFileOrFolder($filename)->isSuccess();
		}

		// Alias of fileOrFolderExists
		public function file_exists(string $filename) : bool
		{
			return $this->fileOrFolderExists($filename)->isSuccess();
		}

		// Alias of openFile
		public function fopen(string $filename, string $mode) // : resource or false
		{
			$result = $this->openFile($filename, $mode);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of getFileType
		public function filetype(string $filename) // : string or false
		{
			$result = $this->getFileType($filename);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of getFileContents
		public function file_get_contents(string $filename) // : string or false
		{
			$result = $this->getFileContents($filename);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of putFileContents
		public function file_put_contents(string $filename, $data) // : int or false
		{
			$result = $this->putFileContents($filename, $data);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of getFreeSpace
		public function free_space($path) // : int or false
		{
			$result = $this->getFreeSpace($path);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of isFolder
		public function is_dir(string $filename) : bool
		{
			return $this->isFolder($filename)->isSuccess();
		}

		// Alias of isFile
		public function is_file(string $filename) : bool
		{
			return $this->isFile($filename)->isSuccess();
		}

		// Alias of isReadable
		public function is_readable(string $filename) : bool
		{
			return $this->isReadable($filename)->isSuccess();
		}

		// Alias of isWritable
		public function is_writable(string $filename) : bool
		{
			return $this->isWritable($filename)->isSuccess();
		}

		// Alias of createFolder
		public function mkdir(string $pathname, int $mode = 0777, bool $recursive = false ) : bool
		{
			return $this->createFolder($pathname, $mode, $recursive)->isSuccess();
		}

		// Alias of rename which is an alias of renameFileOrFolder
		public function move(string $oldname , string $newname) : bool
		{
			return $this->rename($oldname, $newname)->isSuccess();
		}

		// Alias of opendir
		public function opendir(string $path) // : resource or false
		{
			$result = $this->openFolder($path);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		// Alias of renameFileOrFolder
		public function rename(string $oldname, string $newname) : bool
		{
			return $this->renameFileOrFolder($oldname, $newname)->isSuccess();
		}

		// Alias of deleteFolder
		public function rmdir(string $dirname) : bool
		{
			return $this->deleteFolder($dirname, false)->isSuccess();
		}

		// Alias of getMetaData
		public function stat(string $filename) // : array or false
		{
			$result = $this->getMetaData($filename);
			return (!TripleTowerError::isError($result) ? $result : false);
		}

		public function touch(string $filename, int $time = null, int $atime = null ) : bool
		{
			return $this->changeTimes($filename, $time, $atime)->isSuccess();
		}

		public function unlink(string $filename) : bool
		{
			return $this->deleteFileOrFolder($filename)->isSuccess();
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

	}
}