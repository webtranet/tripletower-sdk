<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\ApiProvider
{

	use TripleTowerSDK\ApiProvider\Conditions;
	use TripleTowerSDK\ApiProvider\Types;
	use TripleTowerSDK\Error\ApiError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\Version;
	use TripleTowerSDK\Serializer\Serializer;
	use TripleTowerSDK\User;

	class ApiProvider
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		private static $apiFolder  = "";
		private static $action     = "";
		private static $conditions = array();
		private static $params     = array();


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function processApiCall(string $apiFolder, string $action, User $user)
		{
			self::$apiFolder      = Helper::normalizeFolderPath($apiFolder);
			self::$action         = array_slice(explode("/", $action), -1)[0]; // last elements of $action path
			self::$params["user"] = $user;

			if( Helper::isFolderEmpty($apiFolder) )
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Unfortunately the api '$action' is not available.");

			$api = $_SERVER["TT"]->meta->getApiEntry($action);
			if( TripleTowerError::isError($api) )
				return $api;

			$api->requestType ??= "rest"; // REST is the default, eventSource possible
			$responseType = $_SERVER["HTTP_ACCEPT"] ?? "*/*";

			switch( $api->requestType )
			{
				case "rest":
					if( $responseType === "text/event-stream" )
						return new SysError(ApiError::INVALID_REQUEST_TYPE, SysError::ERR, "Unfortunately the requested responseType '$responseType' for '$action' is invalid because a rest api cannot be used as event source.");
					break;

				case "eventSource":
					if( $responseType !== "text/event-stream" )
						return new SysError(ApiError::INVALID_REQUEST_TYPE, SysError::ERR, "Unfortunately the requested responseType '$responseType' for '$action' is invalid because an event source api can't be called via rest.");

					header("X-Accel-Buffering: no");
					header("Content-Type: text/event-stream");
					header("Cache-Control: no-cache");
					set_time_limit(0);
					break;

				default:
					return new SysError(ApiError::INVALID_REQUEST_TYPE, SysError::ERR, "Unfortunately the configured requestType '$api->requestType' for '$action' is invalid.");
			}

			if( !isset($api->conditions) || !is_array($api->conditions) )
				return new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Unfortunately the api configuration for '$action' is invalid because of a missing conditions parameter.");

			foreach($api->conditions as $condition)
			{
				if( empty($condition->variable) || empty($condition->condition) || !isset($condition->value) )
					return new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Unfortunately the api configuration for '$action' is invalid because of an invalid condition parameter.");

				$conditionError = self::addCondition(
					$condition->variable,
					$condition->condition,
					$condition->value,
					isset($condition->default) ? $condition->default : null,
					isset($condition->delimiter) ? $condition->delimiter : (isset($condition->value2) ? $condition->value2 : null)
				);

				if( !$conditionError->isSuccess() )
					return $conditionError;
			}
			return self::verifyConditions();
		}

		private static function addCondition(string $variable, string $conditionType, $param, $default=null, $param2=null)
		{
			$conditionType = strtolower($conditionType);
			switch($conditionType)
			{
				case Conditions::TYPE:
				case Conditions::TYPE_SYMBOL:
					$conditionType = Conditions::TYPE;
					break;
				case Conditions::EQUAL:
				case Conditions::EQUAL_SYMBOL:
					$conditionType = Conditions::EQUAL;
					break;
				case Conditions::GREATER:
				case Conditions::GREATER_SYMBOL:
					$conditionType = Conditions::GREATER;
					break;
				case Conditions::GREATEREQUAL:
				case Conditions::GREATEREQUAL_SYMBOL:
					$conditionType = Conditions::GREATEREQUAL;
					break;
				case Conditions::SMALLER:
				case Conditions::SMALLER_SYMBOL:
					$conditionType = Conditions::SMALLER;
					break;
				case Conditions::SMALLEREQUAL:
				case Conditions::SMALLEREQUAL_SYMBOL:
					$conditionType = Conditions::SMALLEREQUAL;
					break;
				case Conditions::BETWEEN:
				case Conditions::BETWEEN_SYMBOL:
					$conditionType = Conditions::BETWEEN;
					if( $param2 === null )
						return new ApiError(ApiError::INVALID_PARAMETER_COMBINATION, ApiError::ERR, "Variable '$variable' has no second parameter to be in between something.");

					break;
				case Conditions::CALLBACK:
				case Conditions::CALLBACK_SYMBOL:
					$conditionType = Conditions::CALLBACK;
					break;
				case Conditions::REGEX:
				case Conditions::REGEX_SYMBOL:
					$conditionType = Conditions::REGEX;
					break;
				case Conditions::LENGTH:
				case Conditions::LENGTH_SYMBOL:
					$conditionType = Conditions::LENGTH;
					break;
				case Conditions::MINLENGTH:
				case Conditions::MINLENGTH_SYMBOL:
					$conditionType = Conditions::MINLENGTH;
					break;
				case Conditions::MAXLENGTH:
				case Conditions::MAXLENGTH_SYMBOL:
					$conditionType = Conditions::MAXLENGTH;
					break;
				default:
					 return new ApiError(ApiError::UNDEFINED_CONDITION_TYPE, ApiError::ERR, "Undefined ConditionType '$conditionType' for action '" . self::$action . "'");
			}

			if( empty($variable) )
				return new ApiError(ApiError::VARIABLE_NOT_DEFINED, ApiError::ERR, "Undefined variablename '$variable' for condition type $conditionType in action '" . self::$action . "'.");

			if($variable === "user")
				return new ApiError(ApiError::KEYWORD_RESERVED, ApiError::ERR, "The variable name 'user' is a reserved variable name and used internally by the OwnApiProvider.");

			// Those two values are always there
			$conditionArray = array('variableName' => (string)$variable, 'conditionType' => $conditionType);

			// Default is only allowed for Conditions::TYPE
			if( $conditionType !== Conditions::TYPE && $default !== null )
				return new ApiError(ApiError::INVALID_PARAMETER_COMBINATION, ApiError::ERR, "Variable '$variable' can not have a condition type '$conditionType' combined with a default parameter.");
			if( $default !== null )
				$conditionArray['default'] = $default;

			if( $param2 === null )
			{
				if($conditionType === Conditions::TYPE && $param === Types::OBJECT )
					return new ApiError(ApiError::INVALID_PARAMETER_COMBINATION, ApiError::ERR, "Variable '$variable' can not be of type '" . Types::OBJECT . "' but have no definition of allowed classes.");

				array_push(self::$conditions, array_merge($conditionArray, ['param' => $param] ));
			}
			else
			{
				if($conditionType === Conditions::TYPE)
				{
					if( $param === Types::OBJECT )
						$param2Name = "classes";
					else
						$param2Name = "delimiter";
					array_push(self::$conditions, array_merge($conditionArray, ['param' => $param, $param2Name => $param2]));
				}
				else if($conditionType === Conditions::BETWEEN)
				{
					// default is not allowed for Conditions::BETWEEN
					// array_merge overwrites conditionType, that's just fine
					array_push(self::$conditions, array_merge($conditionArray, ['variableName'  => $variable, 'conditionType' => Conditions::GREATEREQUAL, 'param' => $param]));
					array_push(self::$conditions, array_merge($conditionArray, ['variableName'  => $variable, 'conditionType' => Conditions::SMALLEREQUAL, 'param' => $param2]));
				}
				else
				{
					return new ApiError(ApiError::INVALID_PARAMETER_COMBINATION, ApiError::ERR, "Variable '$variable' has a second parameter with value='$param2' specified, but is neither of condition type '" . Conditions::TYPE . "', nor '" . Conditions::BETWEEN . "', but $conditionType.");
				}
			}
			return new ApiError(ApiError::SUCCESS);
		}

		public static function verifyConditions()
		{
			$apiIter = new \AppendIterator();

			$globalApiIter = new \RecursiveDirectoryIterator( $_SERVER["TT"]->tower->getGlobalApiFolder() );
			$apiIter->append( new \RecursiveIteratorIterator($globalApiIter) );

			$wappApiIter = new \RecursiveDirectoryIterator( self::$apiFolder );
			$apiIter->append( new \RecursiveIteratorIterator($wappApiIter) );

			foreach($apiIter as $apiFileInfo)
				if( $apiFileInfo->isFile() && $apiFileInfo->getExtension() === "php" )
					require $apiFileInfo->getRealPath();

			foreach(self::$conditions as $condition)
			{
				$conditionType      = $condition['conditionType'];
				$variableValueArray = array();
				$variableValue      = isset($_REQUEST[$condition['variableName']]) ? $_REQUEST[$condition['variableName']] : null;
				$variableName       = $condition['variableName'];
				$param              = $condition['param'];

				switch( $conditionType )
				{
					case Conditions::TYPE:
					{
						if( isset(self::$params[ $variableName ]) )
							return new ApiError(ApiError::VARIABLE_ALREADY_DEFINED, ApiError::ERR, "Variable '$variableName' is already defined.");

						if( $variableValue === null )
						{
							if( isset($condition['default']) )
								$variableValue = $condition['default'];
							else
								return new ApiError(ApiError::VARIABLE_NOT_DEFINED, ApiError::ERR, "Variable '$variableName' is not defined.");
						}
						if( isset($condition['delimiter']) )
						{
							$variableValueArray = preg_split("/[".$condition['delimiter']."]/", $variableValue );
							if(count($variableValueArray) === 0)
								return new ApiError(ApiError::VARIABLE_NOT_DELIMITERABLE, ApiError::ERR, "Variable '$variableName' is not delimiterable by '" . $condition['delimiter'] . "'.");
						}
						else
						{
							// All values are treated as arrays. Later on if there is no delimiter, they get reconverted into non-arrays
							$variableValueArray  = array($variableValue);
						}

						foreach($variableValueArray as $loop => $singleValue)
						{
							$filteredValue = null;
							switch($param)
							{
								case Types::BOOL:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) === null )
										return new ApiError(ApiError::VARIABLE_NOT_BOOL, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no BOOL.");
									break;

								case Types::INT:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_INT, array("flags"=>FILTER_FLAG_ALLOW_HEX))) === false )
										return new ApiError(ApiError::VARIABLE_NOT_INT, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no INT.");
									break;

								case Types::UINT:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>0), "flags"=>FILTER_FLAG_ALLOW_HEX))) === false )
										return new ApiError(ApiError::VARIABLE_NOT_UINT, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no UINT.");
									break;

								case Types::UINT_POSITIVE:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>1), "flags"=>FILTER_FLAG_ALLOW_HEX))) === false )
										return new ApiError(ApiError::VARIABLE_NOT_UINT_POSITIVE, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no UINT_POSITIVE.");
									break;

								case Types::FLOAT:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_FLOAT)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_FLOAT, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no FLOAT.");
									break;

								case Types::STRING:
									$filteredValue = htmlspecialchars($singleValue);
									if( strlen($singleValue) > 0 && strlen($filteredValue) === "" )
										return new ApiError(ApiError::VARIABLE_NOT_STRING, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no STRING.");
									break;

								case Types::STRING_RAW:
									if( ($filteredValue = filter_var($singleValue, FILTER_UNSAFE_RAW)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_STRING, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no STRING_RAW.");
									break;

								case Types::DATETIME:
									$apiCompatibleDateTimeFormats = array_merge( DateTime::ISO_FORMATS, [DateTime::UNIX_TIMESTAMP] );
									$filteredValue = DateTime::createDateTimeFromFormats( $apiCompatibleDateTimeFormats, htmlspecialchars($singleValue) );
									if( TripleTowerError::isError($filteredValue) )
										return new ApiError(ApiError::VARIABLE_NOT_DATETIME, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no DATETIME.");
									break;

								case Types::TIME_INTERVAL:
									try
									{
										$filteredValue = new \DateInterval(htmlspecialchars($singleValue));
									}
									catch(\Throwable $t)
									{
										return new ApiError(ApiError::VARIABLE_NOT_TIME_INTERVAL, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no TIME_INTERVAL.");
									}
									break;

								case Types::IP:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4|FILTER_FLAG_IPV6)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_IP, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no IP.");
									break;

								case Types::IPV4:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_IPV4, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no IPV4.");
									break;

								case Types::IPV6:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_IPV6, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no IPV6.");
									break;

								case Types::EMAIL:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_EMAIL)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_EMAIL, ApiError::ERR, "Variable '$variableName' (no value logged for security reasons) is no EMAIL.");
									break;

								case Types::URL:
									if( ($filteredValue = filter_var($singleValue, FILTER_VALIDATE_URL)) === false )
										return new ApiError(ApiError::VARIABLE_NOT_URL, ApiError::ERR, "Variable '$variableName' (no value logged for security reasons) is no URL.");
									break;

								case Types::VERSION:
									try
									{
										$filteredValue = new Version($singleValue);
									}
									catch(ApiError $ex)
									{
										return new ApiError(ApiError::VARIABLE_NOT_VERSION, ApiError::ERR, "Variable '$variableName' with value '$singleValue' is no version. The error '" . $ex->getMessage() . "' happened.");
									}
									break;

								case Types::OBJECT:
									try
									{
										if( empty($condition["classes"]) )
											return new ApiError(ApiError::INVALID_PARAMETER_COMBINATION, ApiError::ERR, "Variable '$variableName' can not be of type '" . Types::OBJECT . "' without a definition of allowed classes.");

										// Turn single class param into array
										$allowedClasses = is_array($condition["classes"]) ? $condition["classes"] : [$condition["classes"]];

										// If the #type property is set inside an assoc array,
										// it has to match one of the allowed class names from meta.json
										// If the #type property is NOT set in an assoc array,
										// it gets casted to the first allowed class name from meta.json
										if( empty($singleValue["#type"]) )
										{
											$filteredValue = Serializer::jsonUnserialize($singleValue, $allowedClasses[0], true);
											if( TripleTowerError::isError($filteredValue) )
												return new ApiError(ApiError::COMPARE_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' type '" . $singleValue["#type"] . "' does not match '$param2'.");
										}
										else
										{
											$parentClasses = @class_parents($singleValue["#type"]);
											if( !is_array($parentClasses) )
												return new ApiError(ApiError::COMPARE_CONDITION_NOT_MET, ApiError::ERR, "Class '" . $singleValue["#type"] . "' of variable '$variableName' could not be found.");
											$wantedClasses = array_push($parentClasses, $singleValue["#type"]);

											foreach($allowedClasses as $allowedClass)
											{
												if( !is_string($allowedClass) )
													$allowedClass = (string)$allowedClass;

												if( !in_array($allowedClass, $wantedClasses) )
													continue;

												$filteredValue = Serializer::jsonUnserialize($singleValue, $allowedClass, true);
												if( TripleTowerError::isError($filteredValue) )
													continue;
											}

											if( $filteredValue === null)
												return new ApiError(ApiError::COMPARE_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' type '" . $singleValue["#type"] . "' does not match '$param2'.");
										}
									}
									catch(ApiError $ex)
									{
										return new ApiError(ApiError::VARIABLE_NOT_OBJECT, ApiError::ERR, "Variable '$variableName' (no value logged for security reasons) is no json object. The error '" . $ex->getMessage() . "' happened.");
									}
									break;

								default:
									return new ApiError(ApiError::UNDEFINED_VARIABLE_TYPE, TripleTowerError::ERR, "Undefined variable type '" . $condition['param'] . "' for variable '" . $condition['variableName'] . "'.");
							}
							$variableValueArray[$loop] = $filteredValue;
						}
						self::$params[ $variableName ] = $variableValueArray;
					}
					break;

					case Conditions::EQUAL:
					case Conditions::GREATER:
					case Conditions::GREATEREQUAL:
					case Conditions::SMALLER:
					case Conditions::SMALLEREQUAL:
					case Conditions::LENGTH:
					case Conditions::MINLENGTH:
					case Conditions::MAXLENGTH:
					{
						if( !isset(self::$params[ $variableName ]) || empty(self::$params[ $variableName ]) )
							return new ApiError(ApiError::VARIABLE_NOT_DEFINED, ApiError::ERR, "Variable '$variableName' is not defined, but the condition '" . $conditionType . "' was tried to be applied to it.");

						foreach(self::$params[ $variableName ] as $piece)
						{
							if( self::conditionTest($piece, $param, $conditionType) === false )
								return new ApiError(ApiError::COMPARE_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' with value '$piece' did not meet the compare condition.");
						}
					}
					break;

					case Conditions::CALLBACK:
					{
						if( !isset(self::$params[ $variableName ]) || empty(self::$params[ $variableName ]) )
							return new ApiError(ApiError::VARIABLE_NOT_DEFINED, ApiError::ERR, "Variable '$variableName' is not defined, but the condition '" . $conditionType . "' was tried to be applied to it.");

						if(!is_callable($param))
							return new ApiError(ApiError::ACTION_NOT_CALLABLE, ApiError::ERR, "'$param' is not callable, '$conditionType' needs a callable function which returns true or false.");

						foreach(self::$params[ $variableName ] as $piece)
						{
							try
							{
								if( !$param($piece) )
									return new ApiError(ApiError::CALLBACK_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' did not meet the callback condition");
							}
							catch(\Throwable $t)
							{
								return new ApiError(ApiError::CALLBACK_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' did not meet the callback condition");
							}
						}
					}
					break;

					case Conditions::REGEX:
					{
						if( !isset(self::$params[ $variableName ]) || empty(self::$params[ $variableName ]) )
							return new ApiError(ApiError::VARIABLE_NOT_DEFINED, ApiError::ERR, "Variable '$variableName' is not defined, but the condition '" . $conditionType . "' was tried to be applied to it.");

						foreach(self::$params[ $variableName ] as $piece)
						{
							if( preg_match($param, $piece) !== 1 )
								return new ApiError(ApiError::REGEX_CONDITION_NOT_MET, ApiError::ERR, "Variable '$variableName' with value '$piece' did not meet the regex condition $param");
						}
					}
					break;
				}
			}// conditions foreach

			if( !is_callable(self::$action) )
			{
				return new ApiError(ApiError::ACTION_NOT_CALLABLE, ApiError::ERR, "The action '" . self::$action . "' is not callable");
			}

			// Reconvert single value params back into non-arrays (indicator is the absence of a delimiter)
			foreach(self::$conditions as $condition)
			{
				if( $condition['conditionType'] === Conditions::TYPE && !isset($condition['delimiter']) )
				{
					self::$params[$condition['variableName']] = self::$params[$condition['variableName']][0];
				}
			}

			return call_user_func(self::$action, self::$params);
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/
		private static function conditionTest($variableValue, $param, string $conditionType)
		{
			// Referencing to a another defined variable is preceded over pure value usage
			if( is_string($param) && isset(self::$params[$param]) )
				$param = self::$params[$param][0];

			if(is_numeric($param))
			{
				switch($conditionType)
				{
					case Conditions::EQUAL:        return ($variableValue         == $param);
					case Conditions::GREATER:      return ($variableValue         >  $param);
					case Conditions::GREATEREQUAL: return ($variableValue         >= $param);
					case Conditions::SMALLER:      return ($variableValue         <  $param);
					case Conditions::SMALLEREQUAL: return ($variableValue         <= $param);
					case Conditions::LENGTH:       return (strlen($variableValue) == $param);
					case Conditions::MINLENGTH:    return (strlen($variableValue) >= $param);
					case Conditions::MAXLENGTH:    return (strlen($variableValue) <= $param);
					default: return false;
				}
			}
			else if(is_string($variableValue) && is_string($param))
			{
				switch($conditionType)
				{
					case Conditions::EQUAL:        return ($variableValue         ==        $param);
					case Conditions::GREATER:      return ($variableValue         >         $param);
					case Conditions::GREATEREQUAL: return ($variableValue         >=        $param);
					case Conditions::SMALLER:      return ($variableValue         <         $param);
					case Conditions::SMALLEREQUAL: return ($variableValue         <=        $param);
					case Conditions::LENGTH:       return (strlen($variableValue) == strlen($param));
					case Conditions::MINLENGTH:    return (strlen($variableValue) >= strlen($param));
					case Conditions::MAXLENGTH:    return (strlen($variableValue) <= strlen($param));
					default: return false;
				}
			}
			else if(( Helper::isClassRelative($variableValue, 'DateTimeInterface') || Helper::isClassRelative($variableValue, 'DateInterval')) &&
					( Helper::isClassRelative($param,         'DateTimeInterface') || Helper::isClassRelative($param,         'DateInterval')) || is_string($param) )
			{
				switch($conditionType)
				{
					case Conditions::EQUAL:        return ($variableValue == $param);
					case Conditions::GREATER:      return ($variableValue >  $param);
					case Conditions::GREATEREQUAL: return ($variableValue >= $param);
					case Conditions::SMALLER:      return ($variableValue <  $param);
					case Conditions::SMALLEREQUAL: return ($variableValue <= $param);
					default: return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
}