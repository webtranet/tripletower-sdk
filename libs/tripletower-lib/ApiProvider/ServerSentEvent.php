<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\ApiProvider
{
	use TripleTowerSDK\Serializer\Serializer;

	class ServerSentEvent
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/
		public $data;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/
		protected string $event;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/
		public function __construct(string $event, $data = null )
		{
			$this->event = $event;
			$this->data  = $data;
		}

		public function __toString()
		{
			return "data:" . Serializer::jsonSerialize($this, false, false, 0, false) . "\n\n";
		}

		public function send()
		{
			echo $this;
			if (ob_get_contents())
			{
				ob_end_flush();
			}
			flush();
		}
	}
}