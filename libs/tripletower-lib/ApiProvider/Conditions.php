<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\ApiProvider
{

	final class Conditions
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const TYPE                = "type";
		const TYPE_SYMBOL         = "#";

		const EQUAL               = "equal";
		const EQUAL_SYMBOL        = "==";

		const GREATER             = "greater";
		const GREATER_SYMBOL      = ">";

		const GREATEREQUAL        = "greaterequal";
		const GREATEREQUAL_SYMBOL = ">=";

		const SMALLER             = "smaller";
		const SMALLER_SYMBOL      = "<";

		const SMALLEREQUAL        = "smallerequal";
		const SMALLEREQUAL_SYMBOL = "<=";

		const BETWEEN             = "between";
		const BETWEEN_SYMBOL      = "><";

		const CALLBACK            = "callback";
		const CALLBACK_SYMBOL     = "()";

		const REGEX               = "regex";
		const REGEX_SYMBOL        = "=~";

		const LENGTH              = "length";
		const LENGTH_SYMBOL       = "[]";

		const MINLENGTH           = "minlength";
		const MINLENGTH_SYMBOL    = "[*";

		const MAXLENGTH           = "maxlength";
		const MAXLENGTH_SYMBOL    = "*]";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function __construct()
		{
		}
	}
}