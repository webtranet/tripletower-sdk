<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\ApiProvider
{

	final class Types
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const BOOL           = "bool";
		const INT            = "int";
		const UINT           = "uint";
		const UINT_POSITIVE  = "uint_positive";
		const FLOAT          = "float";
		const STRING         = "string";
		const STRING_RAW     = "string_raw";
		const DATETIME       = "datetime";
		const TIME_INTERVAL  = "time_interval";
		const IP             = "ip";
		const IPV4           = "ipv4";
		const IPV6           = "ipv6";
		const EMAIL          = "email";
		const URL            = "url";
		const VERSION        = "version";
		const OBJECT         = "object";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function __construct()
		{
		}
	}
}


