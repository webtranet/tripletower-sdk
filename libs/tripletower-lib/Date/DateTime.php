<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Date
{
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\ApiError;

	class DateTime extends \DateTimeImmutable
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const DEFAULT_FORMAT			 = self::RFC3339_EXTENDED;
		const DEFAULT_FORMAT_DATE_ONLY	 = "Y-m-d";
		const DEFAULT_FORMAT_TIME_ONLY	 = "H:i:s";
		const DEFAULT_FORMAT_NO_TIMZONE	 = "Y-m-d\TH:i:s.v";
		const DEFAULT_FORMAT_FILEYSTEM   = "Y-m-d_H-i-s.v";

		const NOW            = "now";
		const UNIX_TIMESTAMP = "@U";
		const ISO_FORMATS    =
		[
			"Y-m-d\TH:i:s.uP",
			self::DEFAULT_FORMAT,
			self::W3C,
			"Y-m-d\TH:i:s.u",
			"Y-m-d\TH:i:s.v"
		];

		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		public function __construct(string $datetime = self::NOW, \DateTimeZone $timezone = null, array $formats = null)
		{
			if( empty($formats) || strtolower($datetime) === self::NOW )
			{
				parent::__construct($datetime, $timezone);
			}
			else
			{
				$dateTimeImmutable = false;
				foreach($formats as $format)
				{
					$dateTimeImmutable = \DateTimeImmutable::createFromFormat($format, $datetime, $timezone);
					if($dateTimeImmutable !== false)
						break;
				}
				if( $dateTimeImmutable === false )
					throw new ApiError(ApiError::VARIABLE_NOT_DATETIME, ApiError::ERR, "Variable '$datetime' is no DATETIME.");

				parent::__construct($dateTimeImmutable->format(self::DEFAULT_FORMAT), $timezone);
			}
		}

		public function formatDefaultUTC(bool $includeTimeZoneOffset = true)
		{
			return $this->formatDefault($includeTimeZoneOffset, "UTC");
		}

		public function formatDefault(bool $includeTimeZoneOffset = true, ?string $timezone = null)
		{
			$format = $includeTimeZoneOffset ? self::DEFAULT_FORMAT : self::DEFAULT_FORMAT_NO_TIMZONE;
			return $this->setTimezone(new \DateTimeZone($timezone ?? date_default_timezone_get()))->format($format);
		}

		public function getTimzoneOffsetString()
		{
			return $this->format("P");
		}

		public function __serialize()
		{
			return
			[
				"datetime" => $this->formatDefault( false, $this->getTimezone()->getName() ),
				"timezone" => $this->getTimezone()->getName()
			];
		}

		public function __unserialize( $data )
		{
			parent::__construct($data["datetime"], new \DateTimeZone($data["timezone"]));
		}

		public static function createDateTimeFromFormats(array $formats, string $datetime, string $timezone = "UTC")
		{
			try
			{
				return new self($datetime, new \DateTimeZone($timezone), $formats);
			}
			catch(TripleTowerError $tte)
			{
				return $tte;
			}
		}

		public static function createDateTimeFromISOFormat(string $dateString)
		{
			return self::createDateTimeFromFormats(self::ISO_FORMATS, $dateString);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}


