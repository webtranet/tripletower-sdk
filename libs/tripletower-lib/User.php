<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Application\SapiNames;
	use TripleTowerSDK\User\G11nSettings;
	use TripleTowerSDK\User\Authentication;
	use TripleTowerSDK\User\FellowGroup;
	use TripleTowerSDK\User\UserSpace;
	use TripleTowerSDK\User\G11nSettings\Language;
	use TripleTowerSDK\User\G11nSettings\Location;
	use TripleTowerSDK\User\G11nSettings\Timezone;
	use TripleTowerSDK\Helper\Helper;

	class User
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const INFO_NOINFO   =  0;
		const INFO_BASE     =  1;
		const INFO_PERSONAL =  2;
		const INFO_BROWSER  =  4;
		const INFO_SOCIAL   =  8;
		const INFO_TECH     = 16;
		const INFO_WORK     = 32;
		const INFO_ALL      = 0x7FFFFFFF;

		const DEFAULT_USERNAME = "anonymous";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		// Session related properties
		protected $activeSessions  = 0;
		protected $environment     = "";
		protected $loadingTime     = null;

		// Direct User properties from DB
		protected $userName        = self::DEFAULT_USERNAME;
		protected $displayName     = "Anonymous";
		protected $publicKey       = "";
		protected $privateKey      = "";
		protected $avatarUrl       = "";
		protected $blocked         = false;
		protected $entryTime       = null;
		protected $lastActive      = null;

		// Indirect User properties from DB
		protected $authentications = [];
		protected $fellowGroups    = [];
		protected $g11nSettings    = null;
		protected $userSpaces      = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/*
		 * User can either be constructed by:
		 * new User($userName)
		 * new User($authName, $authSystem)
		 */
		function __construct(string $userNameOrAuthName, string $authSystem = "", int $options = self::INFO_BASE)
		{
			// -- INVALID PARAMETER --
			if( empty($userNameOrAuthName) )
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR);

			// -- DON'T GATHER USER INFORMATION --
			// --  IF PHP RUNS ON COMMAND LINE  --
			if( SapiNames::isCliSapi() )
			{
				// use default information
				return;
			}

			// --   IF NO AUTHSYSTEM GIVEN    --
			// -- IT'S ALL ABOUT THE USERNAME --
			$userName = null;
			if( empty($authSystem) )
			{
				// UserName given
				$userName = $userNameOrAuthName;
			}
			else
			{
				// AuthName and AuthSystem given
				$userName = $_SERVER["TT"]->dataBases->serviceTowerDb->getUserName($userNameOrAuthName, $authSystem);
				if( TripleTowerError::isError($userName) )
					throw $userName;
			}

			// -- GATHER USER INFOS --
			if( $options & self::INFO_BASE )
			{
				$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

				if( strtolower($userName) !== strtolower(self::DEFAULT_USERNAME) )
					$serviceTowerDb->setLastActive($userName)->throwIfNotSuccess();

				$userInfos = $serviceTowerDb->getUserInfos($userName);
				if( TripleTowerError::isError($userInfos) )
					throw $userInfos;

				$this->activeSessions  = (int)$userInfos["activesessions"];
				$this->environment     = $_SERVER["TT"]->conf->environment;
				$this->loadingTime     = new \DateTime("now");

				$this->userName        = (string)$userInfos["username"];
				$this->displayName     = trim((string)$userInfos["displayname"]);
				$this->publicKey       = (string)$userInfos["publickey"];
				$this->privateKey      = (string)$userInfos["privatekey"];
				$this->avatarUrl       = trim((string)$userInfos["avatarurl"]);
				$this->blocked         = (bool)$userInfos["blocked"];
				$this->entryTime       = new \DateTime($userInfos["entrytime"]);
				$this->lastActive      = new \DateTime($userInfos["lastactive"]);


				// Authentications
				$authenticationIds = $serviceTowerDb->getAuthenticationIds($this->userName);
				if( TripleTowerError::isError($authenticationIds) )
					throw $authenticationIds;
				foreach($authenticationIds as $authenticationId)
					$this->authentications[] = new Authentication((int)$authenticationId["authentication_id"]);


				// FellowGroups
				$fellowGroupIds = $serviceTowerDb->getFellowGroupIds($this->userName);
				if( TripleTowerError::isError($fellowGroupIds) )
					throw $fellowGroupIds;
				foreach($fellowGroupIds as $fellowGroupId)
					$this->fellowGroups[] = new FellowGroup((int)$fellowGroupId["fellowgroup_id"]);


				// G11nSettings
				$g11nSettingsId = $serviceTowerDb->getG11nSettingsId($this->userName);
				if( TripleTowerError::isError($g11nSettingsId) )
					throw $g11nSettingsId;
				$this->g11nSettings = new G11nSettings((int)$g11nSettingsId["g11nsetting_id"]);


				// UserSpaces
				$userSpaceIds = $serviceTowerDb->getUserSpaceIds($this->userName);
				if( TripleTowerError::isError($userSpaceIds) )
					throw $userSpaceIds;
				foreach($userSpaceIds as $userSpaceId)
				{
					$this->userSpaces[] = new UserSpace((int)$userSpaceId["userspace_id"]);
					break; // TODO: Currently only a single user space is supported
				}

				// Signatures
				foreach($this->userSpaces[0]->installedWapps as $installedWapp)
				{
					$installedWapp->generateWappUserSignature($this->userName);
				}
			}
			else
			{
				$this->userName     = $userName;
				$this->displayName  = Helper::splitTowerFromName($userName, false)[0];
				$this->loadingTime  = new \DateTime("now");
				$this->g11nSettings = new \stdClass();
				$this->g11nSettings->language = isset( $_REQUEST["language"] ) ? $_REQUEST["language"] : Language::G11N_DEFAULT_LANGUAGE;
				$this->g11nSettings->location = isset( $_REQUEST["location"] ) ? $_REQUEST["location"] : Location::G11N_DEFAULT_LOCATION;
				$this->g11nSettings->timezone = isset( $_REQUEST["timezone"] ) ? $_REQUEST["timezone"] : Timezone::G11N_DEFAULT_TIMEZONE;
				$this->environment = $_SERVER["TT"]->conf->environment;
			}
		}

		public function __get($key)
		{
			// Usual properties
			if( isset($this->$key) )
				return $this->$key;

			throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The requested property '$key' does not exist.");
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}