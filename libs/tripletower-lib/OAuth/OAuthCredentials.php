<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\OAuth
{
	use TripleTowerSDK\Error\OAuthError;
	use TripleTowerSDK\Helper\JsonConfig;

	class OAuthCredentials
	{


		/***********************************
		* PUBLIC ATTRIBUTES                *
		************************************/

		const GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
		const GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
		const GRANT_TYPE_DEVICE_CODE        = "urn:ietf:params:oauth:grant-type:device_code";
		const GRANT_TYPE_ON_BEHALF_OF       = "urn:ietf:params:oauth:grant-type:jwt-bearer";
		const GRANT_TYPE_IMPLICIT           = "implicit";
		const GRANT_TYPE_OWNER_CREDENTIALS  = "password";
		const GRANT_TYPE_OPEN_ID            = "open_id";
		const GRANT_TYPE_REFRESH_TOKEN      = "refresh_token";

		/***********************************
		* PROTECTED ATTRIBUTES             *
		************************************/

		protected string $resourceProvider;
		protected string $authUrl;
		protected string $tokenUrl;
		protected string $grantType;
		protected string $scope;
		protected string $clientId;
		protected string $clientSecret;


		/***********************************
		* PUBLIC METHODS                   *
		************************************/

		public static function getGrantTypes()
		{
			return
			[
				self::GRANT_TYPE_CLIENT_CREDENTIALS,
				self::GRANT_TYPE_AUTHORIZATION_CODE,
				self::GRANT_TYPE_DEVICE_CODE,
				self::GRANT_TYPE_ON_BEHALF_OF,
				self::GRANT_TYPE_IMPLICIT,
				self::GRANT_TYPE_OWNER_CREDENTIALS,
				self::GRANT_TYPE_OPEN_ID,
				self::GRANT_TYPE_REFRESH_TOKEN
			];
		}

		public static function parseConfig( string $resourceProvider )
		{
			$config = $_SERVER["TT"]->conf->$resourceProvider;
			$authUrl = $config->authUrl;
			$tokenUrl = $config->tokenUrl ?? $config->authUrl;
			$grantType = $config->grantType;
			$scope = $config->scope;
			$clientId = $config->clientId;
			$clientSecret = isset( $config->clientSecret ) ? $config->clientSecret : "";

			return new self( $resourceProvider, $authUrl, $tokenUrl, $grantType, $scope, $clientId, $clientSecret );
		}

		public function __construct( string $resourceProvider, string $authUrl, string $tokenUrl, string $grantType, string $scope, string $clientId, string $clientSecret = "" )
		{
			if( empty($resourceProvider) )
				throw new OAuthError( OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR );

			if( empty($authUrl) )
				throw new OAuthError( OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR );

			if( empty($tokenUrl) )
				throw new OAuthError( OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR );

			if( empty($scope) )
				throw new OAuthError( OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR );

			if( empty($clientId) )
				throw new OAuthError( OAuthError::CLIENT_ID_EMPTY, OAuthError::ERR );

			if( array_search($grantType, self::getGrantTypes()) === false )
				throw new OAuthError( OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR );

			$this->resourceProvider = $resourceProvider;
			$this->authUrl = $authUrl;
			$this->tokenUrl = $tokenUrl;
			$this->grantType = $grantType;
			$this->scope = $scope;
			$this->clientId = $clientId;
			$this->clientSecret = $clientSecret;
		}

		public function __isset( $name )
		{
			return isset( $this->$name );
		}

		public function __get($name)
		{
			return $this->$name;
		}


		/***********************************
		* PROTECTED METHODS                *
		************************************/
	}
}