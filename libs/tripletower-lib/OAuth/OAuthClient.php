<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\OAuth
{
	use TripleTowerSDK\Error\OAuthError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Network\HttpRequest;
	use TripleTowerSDK\OAuth\OAuthCredentials;
	use TripleTowerSDK\OAuth\OAuthToken;
	use TripleTowerSDK\Serializer\Serializer;

	class OAuthClient extends HttpRequest
	{


		/***********************************
		* PUBLIC ATTRIBUTES                *
		************************************/

		/***********************************
		* PROTECTED ATTRIBUTES             *
		************************************/
		protected OAuthCredentials $credentials;
		protected OAuthToken $token;
		protected string $proxy                  = "";
		protected string $caCertificatePath      = "";


		/***********************************
		* PUBLIC METHODS                   *
		************************************/

		public function __construct( OAuthCredentials $credentials, ?OAuthToken $token = null, string $caCertificatePath = "", int $timeOutValue = 30, string $proxy = "" )
		{
			$this->credentials = $credentials;
			$this->proxy = $proxy;
			$this->caCertificatePath = $caCertificatePath;

			parent::__construct( $this->credentials->authUrl, [], $this->caCertificatePath, $timeOutValue );

			if( $token !== null )
			{
				$this->token = $token;
			}
			else
			{
				$token = $this->getTokenFromCookie();
				if( !TripleTowerError::isError($token) )
					$this->token = $token;
			}
		}

		public function getToken()
		{
			if( !isset($this->token) || !$this->token->checkToken()->isSuccess() )
			{
				$result = !empty($this->token->refreshToken) ? $this->refreshToken() : $this->requestToken();
				if( !$result->isSuccess() )
					return $result;
			}

			return $this->token;

		}

		public function getAuthorizationHeader()
		{
			$token = $this->getToken();
			if( TripleTowerError::isError($token) )
				return $token;

			return "Authorization: " . $token->tokenType . " " . $token->accessToken;
		}

		public function getAuthorizationUrl( string $redirectUrl )
		{
			$this->token->codeVerifier = $this->createCodeVerifier();
			$this->setTokenCookie();

			$query = http_build_query(
			[
				"client_id" => $this->credentials->clientId,
				"response_type" => "code",// id_token",
				"response_mode" => "form_post",
				"redirect_uri" => $redirectUrl,
				"scope" => $this->credentials->scope,
				"state" => hash("sha256", $this->credentials->clientId),
				"promt" => "select_account",
				"code_challenge" => rtrim(strtr(base64_encode(hash("sha256", $this->token->codeVerifier, true)), "+/", "-_"), "="),
				"code_challenge_method" => "S256"
			]);

			return $this->credentials->authUrl . "?" . $query;
		}

		public function requestToken( array $requestParameter = [] ) : TripleTowerError
		{
			$authBody = [];
			$authBody["client_id"]  = $this->credentials->clientId;
			$authBody["scope"]      = $this->credentials->scope;
			$authBody["grant_type"] = $this->credentials->grantType;

			switch( $authBody["grant_type"] )
			{
				case OAuthCredentials::GRANT_TYPE_CLIENT_CREDENTIALS:
					$authBody["client_secret"] = $this->credentials->clientSecret;
					$this->url = $this->credentials->authUrl;
					break;

				case OAuthCredentials::GRANT_TYPE_AUTHORIZATION_CODE:
					if( $requestParameter["state"] !== hash("sha256", $this->credentials->clientId))
						return new OAuthError(OAuthError::UNAUTHORIZED_CLIENT, OAuthError::ERR);

					$this->url = $this->credentials->tokenUrl;
					$authBody["redirect_uri"]  = $requestParameter["redirectUrl"] ?? "";
					$authBody["code"]          = $requestParameter["code"] ?? "";
					$authBody["code_verifier"] = $this->token->codeVerifier;
					if( !empty($this->credentials->clientSecret) )
						$authBody["client_secret"] = $this->credentials->clientSecret;
					break;

				default:
					return new OAuthError(OAuthError::AUTH_TYPE_NOT_IMPLEMENTED, OAuthError::ERR);
			}

			$curlOpts =
			[
				CURLOPT_POST       => true,
				CURLOPT_POSTFIELDS => $authBody,
				CURLOPT_PROXY      => ( !empty($this->proxy) ? $this->proxy : null )
			];

			$authResult = $this->setBaseOptionsAndExecuteCurlRequest($curlOpts);
			if( TripleTowerError::isError($authResult) )
				return $authResult->raiseError(TripleTowerError::ERR);

			$token = Serializer::jsonUnserialize($authResult->getBody(), OAuthToken::class);
			if( TripleTowerError::isError($token) )
				return $token;

			$this->token = $token;
			if( $this->credentials->grantType !== OAuthCredentials::GRANT_TYPE_CLIENT_CREDENTIALS )
				$this->setTokenCookie();

			return new OAuthError( OAuthError::SUCCESS );
		}

		public function refreshToken( ) : TripleTowerError
		{
			$authBody = [];
			$authBody["client_id"]  = $this->credentials->clientId;
			$authBody["scope"]      = $this->scope;
			$authBody["grant_type"] = self::GRANT_TYPE_REFRESH_TOKEN;
			$authBody["refresh_token"] = $this->token->refreshToken;

			if( $this->credentials->grantType )
				$authBody["client_secret"] = $this->credentials->clientSecret;

			$curlOpts =
			[
				CURLOPT_POST       => true,
				CURLOPT_POSTFIELDS => $authBody,
				CURLOPT_PROXY      => ( !empty($this->proxy) ? $this->proxy : null )
			];

			$authResult = $this->setBaseOptionsAndExecuteCurlRequest($curlOpts);
			if( TripleTowerError::isError($authResult) )
				return $authResult->raiseError(TripleTowerError::ERR);

			$token = OAuthToken::parseJsonString($authResult->getBody());
			if( TripleTowerError::isError($token) )
				return $token;

			$this->token = $token;

			return new $this->setTokenCookie();
		}

		public function setTokenCookie() : TripleTowerError
		{
			$serializedToken = Serializer::JsonSerialize( $this->token );
			if( TripleTowerError::isError($serializedToken) )
				return $serializedToken;

			$cookiePath = "/" . $_SERVER["TT"]->tower->getTowerInstance() . "/" . $_SERVER["TT"]->contentProvider;
			if( !setcookie($this->generateCookieName(), $serializedToken, 0, $cookiePath ) )
				return new OAuthError( OAuthError::SET_TOKEN_COOKIE_FAILED, OAuthError::ERR );

			return new OAuthError( OAuthError::SUCCESS );
		}

		public function getTokenFromCookie()
		{
			$cookieName = $this->generateCookieName();
			if( !empty($_COOKIE[$cookieName]) )
				return Serializer::jsonUnserialize( $_COOKIE[$cookieName], OAuthToken::class );
			else
				return new OAuthToken();
		}

		/***********************************
		* PROTECTED METHODS                *
		************************************/

		protected function createCodeVerifier() : string
		{
			return (string)bin2hex(random_bytes(16));
		}

		protected function generateCookieName() : string
		{
			return preg_replace( "/[^-_0-9A-zA-Z]+/", "-", $this->credentials->resourceProvider . "-oauth-token" );
		}
	}
}