<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\OAuth
{
	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Error\OAuthError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Serializer\Serializer;

	class OAuthToken
	{


		/***********************************
		* PUBLIC ATTRIBUTES                *
		************************************/

		/***********************************
		* PROTECTED ATTRIBUTES             *
		************************************/
		protected string $accessToken    = "";
		protected string $tokenType      = "";
		protected string $refreshToken   = "";
		protected string $idToken        = "";
		protected ?DateTime $tokenExpire = null;
		protected string $codeVerifier   = "";
		protected string $error          = "";


		/***********************************
		* PUBLIC METHODS                   *
		************************************/

		public static function parseJsonString( string $jsonString = "" )
		{
			if( empty($jsonString) )
				return new OAuthError( OAuthError::ACCESS_TOKEN_JSON_EMPTY, OAuthError::ERR );

			$tokenInfo = Serializer::jsonUnserialize( $jsonString );

			if( empty($tokenInfo["expires_in"]) || empty($tokenInfo["access_token"]) || empty($tokenInfo["token_type"]) )
			{
				if( !empty($tokenInfo["error_description"]) )
					return new OAuthError( OAuthError::ACCESS_TOKEN_REQUEST_FAILED, OAuthError::ERR, $tokenInfo["error_description"] );
				else
					return new OAuthError( OAuthError::ACCESS_TOKEN_REQUEST_FAILED, OAuthError::ERR );
			}

			$tokenExpire = ( new DateTime())->add(new \DateInterval("PT" . $tokenInfo["expires_in"] . "S") );

			return new OAuthToken( $tokenInfo["access_token"], $tokenInfo["refresh_token"], $tokenInfo["token_type"], $tokenInfo["id_token"] ?? "", $tokenExpire );
		}

		public function __construct( string $accessToken = "", string $tokenType = "", string $refreshToken = "", string $idToken = "", ?DateTime $tokenExpire = null )
		{
			$this->accessToken  = $accessToken;
			$this->tokenType    = $tokenType;
			$this->refreshToken = $refreshToken;
			$this->tokenExpire  = $tokenExpire;
		}

		public function checkToken() : TripleTowerError
		{
			if( $this->tokenExpire < (new DateTime()) )
				return new OAuthError( OAuthError::ACCESS_TOKEN_EXPIRED, OAuthError::SILENT );

			return new OAuthError( OAuthError::SUCCESS );
		}

		public function __isset( $name )
		{
			return isset( $this->$name );
		}

		public function __get( $name )
		{
			return $this->$name;
		}

		public function __unset( $name )
		{
			unset( $this->$name );
		}

		public function __set( $name, $value )
		{
			$this->$name = $value;
		}

		public function __unserialize(array $data)
		{
			$this->accessToken = $data["access_token"] ?? "";
			$this->tokenType = $data["token_type"] ?? "";
			$this->refreshToken = $data["refresh_token"] ?? "";
			$this->idToken = $data["id_token"] ?? "";
			$this->tokenExpire = isset($data["token_expire"]) ? new DateTime($data["token_expire"]) : ( new DateTime())->add(new \DateInterval("PT" . ($tokenInfo["expires_in"] ?? "0") . "S") );
			$this->error = $data["error_description"] ?? "";
			$this->codeVerifier = $data["code_verifier"] ?? "";
		}

		public function __serialize()
		{
			return
			[
				"access_token"      => $this->accessToken,
				"token_type"        => $this->tokenType,
				"refresh_token"     => $this->refreshToken,
				"id_token"          => $this->idToken,
				"token_expire"      => ($this->tokenExpire ?? new DateTime())->formatDefault(),
				"error_description" => $this->error,
				"code_verifier"     => $this->codeVerifier
			];
		}



		/***********************************
		* PROTECTED METHODS                *
		************************************/
	}
}