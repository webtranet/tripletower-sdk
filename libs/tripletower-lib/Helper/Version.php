<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Error\SysError;

	class Version
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const VERSION_SEPARATOR_DOT    = '.';
		const VERSION_SEPARATOR_DASH   = '-';
		const VERSION_SEPARATOR_SLASH  = '/';
		const VERSION_SEPARATOR_SPACE  = ' ';

		const VERSION_SEPARATOR_MIXED  = '';


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $versionString = "";
		protected $originalVersionString = "";

		protected $versionSeparator = "";

		protected $major    = null;
		protected $minor    = null;
		protected $patch    = null;
		protected $revision = null;
		protected $modifier = null; // e.g. rc, alpha, beta, etc.


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		function __construct(string $versionString, string $versionSeparator = self::VERSION_SEPARATOR_DOT)
		{
			if( empty($versionString) )
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "A version number cannot be empty.");

			$this->originalVersionString = $versionString;
			$this->versionSeparator = $versionSeparator;

			$fixedVersionString = $this->originalVersionString;
			if( $this->versionSeparator === self::VERSION_SEPARATOR_MIXED )
			{
				$separators = array(self::VERSION_SEPARATOR_DOT, self::VERSION_SEPARATOR_DASH, self::VERSION_SEPARATOR_SLASH, self::VERSION_SEPARATOR_SPACE);
				foreach($separators as $separator)
				{
					$fixedVersionString = str_replace($separator, self::VERSION_SEPARATOR_DOT, $fixedVersionString);
				}
			}

			// Filter empty version parts
			$versionArray = array_filter(explode(self::VERSION_SEPARATOR_DOT, $fixedVersionString), function($versionPart)
			{
				return !(is_null($versionPart) || $versionPart === "");
			});

			// Assign version parts to variables
			if( empty($versionArray) || count($versionArray) === 1 )
			{
				throw new SysError(SysError::INVALID_PARAM, SysError::SILENT, "Version $versionString could not be disaggregated into version compontents.");
			}
			else if( count($versionArray) === 2 )
			{
				list($this->major, $this->minor) = $versionArray;
				if( preg_match('/[a-z]/i', $this->minor, $matches, PREG_OFFSET_CAPTURE) === 1 )
				{
					$this->modifier = substr($this->minor, $matches[0][1]);
					$this->minor = substr($this->minor, 0, $matches[0][1]);
				}
			}
			else if( count($versionArray) === 3 )
			{
				list($this->major, $this->minor, $this->revision) = $versionArray;
				if( preg_match('/[a-z]/i', $this->revision, $matches, PREG_OFFSET_CAPTURE) === 1 )
				{
					$this->modifier = substr($this->revision, $matches[0][1]);
					$this->revision = substr($this->revision, 0, $matches[0][1]);
				}
			}
			else if( count($versionArray) === 4 )
			{
				list($this->major, $this->minor, $this->patch, $this->revision) = $versionArray;
				if( preg_match('/[a-z]/i', $this->major, $matches, PREG_OFFSET_CAPTURE) === 1 )
				{
					$this->modifier = substr($this->revision, $matches[0][1]);
					$this->revision = substr($this->revision, 0, $matches[0][1]);
				}
			}
			else
			{
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Version '$versionString' does have too many version components. Only 4 components are supported.");
			}

			$versionStringArray = [];
			$versionStringArray[] = $this->major;
			if( $this->minor !== null )
				$versionStringArray[] = $this->minor;
			if( $this->patch !== null )
				$versionStringArray[] = $this->patch;
			if( $this->revision !== null )
				$versionStringArray[] = $this->revision;

			$this->versionString = implode( ($this->versionSeparator!==self::VERSION_SEPARATOR_MIXED) ? self::VERSION_SEPARATOR_DOT : $this->versionSeparator, $versionStringArray) . ($this->modifier ?? "");
		}

		function bigger(Version $version) : bool
		{
			if( (int)$this->major > (int)$version->major )
				return true;
			if( (int)$this->major === (int)$version->major && (int)$this->minor > (int)$version->minor )
				return true;
			if( (int)$this->major === (int)$version->major && (int)$this->minor === (int)$version->minor  && (int)$this->patch > (int)$version->patch )
				return true;
			if( (int)$this->major === (int)$version->major && (int)$this->minor === (int)$version->minor  && (int)$this->patch === (int)$version->patch && (int)$this->revision > (int)$version->revision )
				return true;
			if( (int)$this->major === (int)$version->major && (int)$this->minor === (int)$version->minor  && (int)$this->patch === (int)$version->patch && (int)$this->revision === (int)$version->revision && empty($this->modifier) && !empty($version->modifier) )
				return true;

			return false;
		}

		function equal(Version $version) : bool
		{
			return ((int)$this->major === (int)$version->major &&
					(int)$this->minor === (int)$version->minor &&
					(int)$this->patch === (int)$version->patch &&
					(int)$this->revision === (int)$version->revision) &&
					$this->modifier === $version->modifier;
		}

		function smaller(Version $version) : bool
		{
			return (!$this->equal($version) && !$this->bigger($version));
		}

		function __toString()
		{
			return $this->versionString;
		}

		function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}