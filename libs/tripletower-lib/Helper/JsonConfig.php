<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\ConfigError;

	class JsonConfig implements \ArrayAccess
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const ENVSECTION_DEVELOPMENT = 1;
		const ENVSECTION_STAGING     = 2;
		const ENVSECTION_PRODUCTION  = 3;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/**
		 * @name loadJsonConfig
		 * Loads a webtra.net json config and accordingly inherits environementSection defined by given environment variable.
		 *
		 * @param string $jsonOrConfigPath    - The path to the json config file which should get loaded.
		 * @param string $environementSection - The environementSection which defines how environementSection should be inherited (production <- staging <- development).
		 * @param string $configKey           - The configKey which is used to decrypt encrypted config json files.
		 */
		public function __construct($jsonOrConfigPath, string $environementSection = "", string $configKey = "")
		{
			$jsonConf = null;
			if( is_string($jsonOrConfigPath) )
			{
				if( is_file($jsonOrConfigPath) || is_file("$jsonOrConfigPath.tte") )
				{
					$jsonConf = empty($configKey) ? Helper::loadJsonFile($jsonOrConfigPath) : Helper::loadJsonFile($jsonOrConfigPath, false, $configKey);
					if( TripleTowerError::isError($jsonConf) )
						throw $jsonConf;

					// Ommited config sections are treated as if their empty
					if(!isset($jsonConf->development))
						$jsonConf->development = new \stdClass();
					if(!isset($jsonConf->staging))
						$jsonConf->staging = new \stdClass();
					if(!isset($jsonConf->production))
						$jsonConf->production = new \stdClass();

					switch(strtolower($environementSection))
					{
						case "development":
							$jsonConf = $jsonConf->development;
							break;

						case "staging":
							$jsonConf = Helper::mergeRecursiveAndDestructive($jsonConf->development, $jsonConf->staging);
							break;

						case "production":
							$jsonConf = Helper::mergeRecursiveAndDestructive($jsonConf->development, $jsonConf->staging, $jsonConf->production);
							break;

						case "":
							break; // Take the whole config as is

						default:
							throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Parameter environementSection '$environementSection' is an unknown section type. It's neither 'production', 'staging' nor 'development'.");
					}
				}
				else
				{
					$jsonConf = json_decode($jsonOrConfigPath);
					if($jsonConf === null)
						throw new ConfigError(ConfigError::CONFIGURATION_FILE_NOT_FOUND, ConfigError::ERR, "Config file '$jsonOrConfigPath' could not be found or parameter 'jsonOrConfigPath' is no valid json.");
				}
			}
			else if(is_object($jsonOrConfigPath))
			{
				$jsonConf = (object)$jsonOrConfigPath;
			}
			else if(is_array($jsonOrConfigPath))
			{
				$jsonConf = (object)$jsonOrConfigPath;
			}
			else
			{
				throw new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "Parameter 'jsonOrConfigPath' is no file path, no object, no array nor a valid json string.");
			}

			foreach($jsonConf as $key => $value)
			{
				if(is_object($value))
					$this->$key = new self($value);
				else
					$this->$key = $value;
			}
		}

		public function __get(string $name) : mixed
		{
			$lowerName = strtolower($name);
			if( !isset($this->$lowerName) )
				throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Could not find parameter '$lowerName' in config file.");

			return $this->$lowerName;
		}

		public function __set(string $name, $value) : void
		{
			$lowerName = strtolower($name);
			$this->$lowerName = $value;
		}

		public function __isset(string $name) : bool
		{
			$lowerName = strtolower($name);
			return isset($this->$lowerName);
		}

		public function __unset(string $name) : void
		{
			$lowerName = strtolower($name);
			unset($this->$lowerName);
		}

		public function offsetExists($offset) : bool
		{
			$offsetString = (string)$offset;
			return isset($this->$offsetString);
		}

		public function offsetGet($offset) : mixed
		{
			$offsetString = (string)$offset;
			return $this->$offsetString;
		}

		public function offsetSet($offset, $value) : void
		{
			$offsetString = (string)$offset;
			$this->$offsetString = $value;
		}

		public function offsetUnset($offset) : void
		{
			$offsetString = (string)$offset;
			unset($this->$offsetString);
		}

		public function getKeys() : array
		{
			$keys = [];
			foreach($this as $key => $value)
				$keys[] = $key;
			return $keys;
		}

		public function getValues() : array
		{
			$values = [];
			foreach($this as $value)
				$values[] = $value;
			return $values;
		}

		public function merge(self $merge)
		{
			$jsonConf = Helper::mergeRecursiveAndDestructive($this, $merge);
			foreach($jsonConf as $key => $value)
			{
				if(is_object($value))
					$this->$key = new self($value);
				else
					$this->$key = $value;
			}
			return $jsonConf;
		}

		public function mergeArray(array $merge) : self
		{
			$mergedObject = (object)array_replace_recursive((array)$this, $merge);
			foreach($mergedObject as $key => $value)
			{
				$this->$key = $value;
			}
			return $this;
		}

		public function toXml(bool $includeHeader = true, array $blacklist = []) : string
		{
			return Helper::arrayToXml($this, get_class($this), $includeHeader, $blacklist);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}