<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\G11nError;
	use TripleTowerSDK\User\G11nSettings\Language;
	use TripleTowerSDK\User\G11nSettings\Location;
	use TripleTowerSDK\User\G11nSettings\Timezone;

	class G11n
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $g11nFolder;

		protected $language;
		protected $translationArray = [];

		protected $location;
		protected $currencyFormat;
		protected $numberFormat;
		protected $dateTimeFormat;
		protected $dateFormat;
		protected $timeFormat;

		protected $timezone;
		protected $timezoneUtcOffsetString;
		protected $timezoneUtcOffsetMinutes;
		protected $timezonePhpName;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $g11nFolder, string $language = Language::G11N_DEFAULT_LANGUAGE, string $location = Location::G11N_DEFAULT_LOCATION, string $timezone = Timezone::G11N_DEFAULT_TIMEZONE)
		{
			$this->changeG11nFolder($g11nFolder, $language, $location, $timezone)->throwIfNotSuccess();
		}

		public function changeG11nFolder(string $g11nFolder, string $language, string $location, string $timezone)
		{
			$this->g11nFolder = Helper::normalizeFolderPath(stream_resolve_include_path($g11nFolder));
			if(!is_dir($this->g11nFolder))
				return new G11nError(G11nError::INVALID_GLOBALIZATION_FOLDER, G11nError::ALERT, "Folder '$g11nFolder' could not be found.");

			return $this->changeG11nSettings($language, $location, $timezone);
		}

		public function changeG11nSettings(string $language, string $location, string $timezone)
		{
			$languageError = $this->changeLanguage($language);
			if(!$languageError->isSuccess())
				return $languageError;

			$locationError = $this->changeLocation($location);
			if(!$locationError->isSuccess())
				return $locationError;

			$timzoneError = $this->changeTimezone($timezone);
			if(!$timzoneError->isSuccess())
				return $timzoneError;

			return new G11nError(G11nError::SUCCESS);
		}

		public function changeLanguage(string $language)
		{
			if( $language === "" )
				return new G11nError(G11nError::INVALID_LANGUAGE_FILE, G11nError::ALERT, "Given language '$language' seems to be empty.");

			$this->language = $language;
			$languageFilePath = $this->g11nFolder.$language.'.language.php';

			if($language === 'key_KEY')
				return new G11nError(G11nError::SUCCESS);

			if( !is_file($languageFilePath) )
			{
				$_SERVER["TT"]->logger->warn("The requested language '$language' is not available, so " . Language::G11N_DEFAULT_LANGUAGE . " is used. If this is requested language the translation keys will used instead.");
				return new G11nError(G11nError::SUCCESS);
			}

			$newLanguageKeys = require $languageFilePath;
			$this->translationArray = array_merge($this->translationArray, $newLanguageKeys);

			return new G11nError(G11nError::SUCCESS);
		}

		public function changeLocation(string $location)
		{
			if( $location === "" )
				return new G11nError(G11nError::INVALID_LOCATION_FILE, G11nError::ALERT, "Given location '$location' seems to be empty.");

			$this->location = $location;
			$locationFilePath = $this->g11nFolder.$location.'.location.php';

			if( !is_file($locationFilePath) )
			{
				// No warning is given, because wapps usually don't provide own location files
				return new G11nError(G11nError::SUCCESS);
			}

			$contentArr = require $locationFilePath;
			$this->currencyFormat = $contentArr["LOCATION.CURRENCYFORMAT"];
			$this->numberFormat   = $contentArr["LOCATION.NUMBERFORMAT"];
			$this->dateTimeFormat = $contentArr["LOCATION.DATETIMEFORMAT"];
			$this->dateFormat     = $contentArr["LOCATION.DATEFORMAT"];
			$this->timeFormat     = $contentArr["LOCATION.TIMEFORMAT"];

			return new G11nError(G11nError::SUCCESS);
		}

		public function changeTimezone(string $timezone)
		{
			if( $timezone === "" )
				return new G11nError(G11nError::INVALID_TIMEZONE_FILE, G11nError::ALERT, "Given location '$timezone' seems to be empty.");

			$this->timezone = $timezone;
			$timezoneFilePath = $this->g11nFolder.$timezone.'.timezone.php';

			if( !is_file($timezoneFilePath) )
			{
				// No warning is given, because wapps usually don't provide own timezone files
				return new G11nError(G11nError::SUCCESS);
			}

			$contentArr = require $timezoneFilePath;
			$this->timezoneUtcOffsetString  = $contentArr["TIMEZONE.UTCOFFSETSTRING"];
			$this->timezoneUtcOffsetMinutes = (int)$contentArr["TIMEZONE.UTCOFFSETMINUTES"];
			$this->timezonePhpName = $contentArr["TIMEZONE.PHPNAME"];

			date_default_timezone_set($this->timezonePhpName);

			return new G11nError(G11nError::SUCCESS);
		}

		public function translate(string $translationKey, ...$phraseParts)
		{
			if($this->language !== 'key_KEY' && isset($this->translationArray[$translationKey]))
				return vsprintf($this->translationArray[$translationKey], $phraseParts);
			else
				return $translationKey . (count($phraseParts) > 0 ? "_[" . implode(", ", $phraseParts) . "]" : "");
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}

// None namespaces global functions
namespace
{
	use TripleTowerSDK\Error\G11nError;
	use TripleTowerSDK\Helper\Helper;
	
	// Translate
	function t(string $translationKey, ...$phraseParts)
	{
		$g11n = $_SERVER["TT"]->g11n;
		if($g11n === null)
			return new G11nError(G11nError::NOT_INITIALIZED);

		return $g11n->translate($translationKey, ...$phraseParts);
	}

	// Translate Upper (first character only)
	function tu(string $translationKey, ...$phraseParts)
	{
		$g11n = $_SERVER["TT"]->g11n;
		if($g11n === null)
			return new G11nError(G11nError::NOT_INITIALIZED);
		$translation = $g11n->translate($translationKey, ...$phraseParts);
		if( strlen($translation) >= 1 )
			return mb_strtoupper($translation[0]) . mb_substr($translation, 1);
		else
			return $translation;
	}

	// Translate Lower (first character only)
	function tl(string $translationKey, ...$phraseParts)
	{
		$g11n = $_SERVER["TT"]->g11n;
		if($g11n === null)
			return new G11nError(G11nError::NOT_INITIALIZED);
		$translation = $g11n->translate($translationKey, ...$phraseParts);
		if( strlen($translation) >= 1 )
			return mb_strtolower($translation[0]) . mb_substr($translation, 1);
		else
			return $translation;
	}

	// Translate Upper All
	function tua(string $translationKey, ...$phraseParts)
	{
		$g11n = $_SERVER["TT"]->g11n;
		if($g11n === null)
			return new G11nError(G11nError::NOT_INITIALIZED);
		$translation = $g11n->translate($translationKey, ...$phraseParts);
		if( strlen($translation) >= 1 )
			return mb_strtoupper($translation);
		else
			return $translation;
	}

	// Translate Lower All
	function tla(string $translationKey, ...$phraseParts)
	{
		$g11n = $_SERVER["TT"]->g11n;
		if($g11n === null)
			return new G11nError(G11nError::NOT_INITIALIZED);
		$translation = $g11n->translate($translationKey, ...$phraseParts);
		if( strlen($translation) >= 1 )
			return mb_strtolower($translation);
		else
			return $translation;
	}

	function c($value, $currencySymbol = '')
	{
		throw new \Exception("Not yet implemented");
	}

	/**
	 * @name d
	 * @param $date as string or object in UTC time
	 *
	 * @desc Deliver a date as string in user timezone and format
	 */
	function d($date)
	{
		if( !Helper::isClassRelative($date, 'DateTime'))
		{
			// No DateTime object
			$date = new \DateTime($date, new \DateTimeZone('UTC'));
		}

		// Convert to user timezone
		$date->setTimezone(new \DateTimeZone(date_default_timezone_get()));

		// ToDo: set user format
		return $date->format("Y-m-d H:i:s");
	}

	function n($number)
	{
		if(is_string($var))
		{

		}

		if(is_int())
		{

		}

		if(is_float($var))
		{

		}

		throw new \Exception("Not yet implemented");
	}
}