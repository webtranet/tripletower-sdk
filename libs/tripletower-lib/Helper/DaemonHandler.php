<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;

	final class DaemonHandler
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const MAX_REACTION_TIME = 30; // seconds


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/



		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function isDaemonInstalled(string $daemonName)
		{
			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					if( extension_loaded("win32service") === false )
						return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Pecl extension 'win32service' not installed but mandatory");

					try
					{
						win32_query_service_status($daemonName);
						return new SysError(SysError::SUCCESS);
					}
					catch( \ValueError | \Win32ServiceException )
					{
						return new SysError(SysError::DAEMON_NOT_FOUND, SysError::SILENT, "Service '$daemonName' could not be found");
					}
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					Helper::exec("systemctl status $daemonName");
					return new SysError(SysError::SUCCESS);
				break;

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}
		}

		public static function startDaemon(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					if( extension_loaded("win32service") === false )
						return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Pecl extension 'win32service' not installed but mandatory");

					$isInstalled = self::isDaemonInstalled($daemonName);
					if( $isInstalled->isSuccess() === false )
						return $isInstalled;

					try
					{
						win32_start_service($daemonName);
						return self::isDaemonRunning($daemonName, $maxReactionTime);
					}
					catch( \ValueError | \Win32ServiceException )
					{
						return new SysError(SysError::DAEMON_START_FAILED, SysError::ERR, "Could not start daemon '$daemonName'");
					}
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					Helper::exec("systemctl start $daemonName");
					return new SysError(SysError::SUCCESS);
				break;

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}
		}

		public static function isDaemonRunning(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					if( extension_loaded("win32service") === false )
						return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Pecl extension 'win32service' not installed but mandatory");

					$isInstalled = self::isDaemonInstalled($daemonName);
					if( $isInstalled->isSuccess() === false )
						return $isInstalled;

					for($sleepLoop = 0; $sleepLoop <= $maxReactionTime; $sleepLoop++)
					{
						try
						{
							$daemonState = win32_query_service_status($daemonName);
							switch( $daemonState["CurrentState"] )
							{
								case WIN32_SERVICE_RUNNING:
									return new SysError(SysError::SUCCESS);
								break;

								case WIN32_SERVICE_START_PENDING:
									sleep(1);
								break;

								default:
									return new SysError(SysError::DAEMON_NOT_RUNNING, SysError::SILENT, "Service '$daemonName' is not running. It is in state '" . $daemonState["CurrentState"] . "' (hex)");
							}
						}
						catch( \ValueError | \Win32ServiceException )
						{
							return new SysError(SysError::DAEMON_QUERY_FAILED, SysError::ERR, "Could not query infos about service '$daemonName' to check if it is started");
						}
					}

					return new SysError(SysError::DAEMON_START_FAILED, SysError::ERR, "Service '$daemonName' could not be started within $maxReactionTime seconds");
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					Helper::exec("systemctl is-active $daemonName");
					return new SysError(SysError::SUCCESS);

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}
		}

		public static function stopDaemon(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			switch(OsNames::getCurrentOsType())
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					if( extension_loaded("win32service") === false )
						return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Pecl extension 'win32service' not installed but mandatory");

					$isInstalled = self::isDaemonInstalled($daemonName);
					if( $isInstalled->isSuccess() === false )
						return $isInstalled;

					$isAlreadyStopped = self::isDaemonStopped($daemonName, $maxReactionTime);
					if( $isAlreadyStopped->isSuccess() )
						return $isAlreadyStopped;

					try
					{
						win32_stop_service($daemonName);
						return self::isDaemonStopped($daemonName, $maxReactionTime);
					}
					catch( \ValueError | \Win32ServiceException )
					{
						return new SysError(SysError::DAEMON_STOP_FAILED, SysError::ERR, "Could not stop daemon '$daemonName'");
					}
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					Helper::exec("systemctl stop $daemonName");
					return new SysError(SysError::SUCCESS);
				break;

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}
		}

		public static function isDaemonStopped(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			switch(OsNames::getCurrentOsType())
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					if( extension_loaded("win32service") === false )
						return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Pecl extension 'win32service' not installed but mandatory");

					$isInstalled = self::isDaemonInstalled($daemonName);
					if( $isInstalled->isSuccess() === false )
						return $isInstalled;

					for($sleepLoop = 0; $sleepLoop <= $maxReactionTime; $sleepLoop++)
					{
						try
						{
							$daemonState = win32_query_service_status($daemonName);
							switch( $daemonState["CurrentState"] )
							{
								case WIN32_SERVICE_STOPPED:
									return new SysError(SysError::SUCCESS);
								break;

								case WIN32_SERVICE_STOP_PENDING:
									sleep(1);
								break;

								default:
									return new SysError(SysError::DAEMON_NOT_STOPPED, SysError::SILENT, "Service '$daemonName' is not stopped. It is in state '" . $daemonState["CurrentState"] . "' (hex)");
							}
						}
						catch( \ValueError | \Win32ServiceException )
						{
							return new SysError(SysError::DAEMON_QUERY_FAILED, SysError::ERR, "Could not query infos about service '$daemonName' to check if it is stopped");
						}
					}

					return new SysError(SysError::DAEMON_QUERY_FAILED, SysError::ERR, "Service '$daemonName' could not be queried within $maxReactionTime seconds");
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					Helper::exec("systemctl is-active $daemonName");

					return new SysError(SysError::NOT_FINISHED, SysError::ERR, "This method is not yet implemented");
				break;

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}
		}

		public static function restartDaemon(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			$stopResult = self::stopDaemon($daemonName, $maxReactionTime);
			if( $stopResult->isSuccess() === false )
				return $stopResult;

			$startResult = self::startDaemon($daemonName, $maxReactionTime);
			if( $startResult->isSuccess() === false )
				return $stopResult;

			return new SysError(SysError::SUCCESS);
		}

		public static function deleteDaemon(string $daemonName, $maxReactionTime = self::MAX_REACTION_TIME)
		{
			if( self::isDaemonInstalled($daemonName)->isSuccess() === false)
				return new SysError(SysError::SUCCESS);

			if( self::isDaemonRunning($daemonName)->isSuccess() )
			{
				$stopResult = self::stopDaemon($daemonName, $maxReactionTime);
				if( $stopResult->isSuccess() === false )
					return $stopResult;
			}

			switch(OsNames::getCurrentOsType())
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					try
					{
						win32_delete_service($daemonName);
					}
					catch( \ValueError | \Win32ServiceException )
					{
						return new SysError(SysError::DAEMON_DELETION_FAILED, SysError::ERR, "Could not uninstall service '$daemonName'");
					}
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					return new SysError(SysError::DAEMON_DELETION_FAILED, SysError::ERR, "You should not delete daemons yourself. Removing a software usually also removes the daemon.");

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}

			return new SysError(SysError::SUCCESS);
		}
		
		public static function installDaemon(string $daemonName, string $daemonPath, array $daemonOptions = [])
		{
			if( self::isDaemonInstalled($daemonName)->isSuccess() )
				return new SysError(SysError::DAEMON_ALREADY_EXISTS, SysError::ERR, "The daemon '$daemonName' already exists");

			switch( OsNames::getCurrentOsType() )
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					$daemonOptions["service"] = $daemonName;
					$daemonOptions["path"]    = $daemonPath;

					if(win32_create_service($daemonOptions) !== WIN32_NO_ERROR )
						return new SysError(SysError::DAEMON_DELETION_FAILED, SysError::ERR, "Could not install service '$daemonName'");
				}
				break;

				case OsNames::OS_TYPE_LINUX:
					return new SysError(SysError::DAEMON_DELETION_FAILED, SysError::ERR, "You should not install daemons yourself. Installing a software usually also installs the daemon.");

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "This OS does not support the method '" . __METHOD__ . "'");
			}

			return new SysError(SysError::SUCCESS);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function __construct()
		{

		}
	}
}