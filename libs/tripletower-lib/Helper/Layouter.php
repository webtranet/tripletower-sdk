<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Tower;
	use Parsedown;

	class Layouter
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const DEFAULT_LAYOUT_NAME     = "default";
		const DEFAULT_SOD_LAYOUT_NAME = "sod";

		const AUTOGENERATION_ON  = true;
		const AUTOGENERATION_OFF = false;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $tempFolder           = "";
		protected $layoutFolder         = "";

		protected $metaInformationTags  = array();
		protected $metaInformationFiles = array();
		protected $headStyleSheetTags   = array();
		protected $headStyleSheetFiles  = array();
		protected $headScriptTags       = array();
		protected $headScriptFiles      = array();
		protected $headScriptSnippets   = array();
		protected $title                = "";
		protected $appcache             = "";
		protected $loadingScreenFile    = null;
		protected $headerFile           = null;
		protected $navigationFile       = null;
		protected $subNavigationFile    = null;
		protected $contentFile          = null;
		protected $sidebarFile          = null;
		protected $footerFile           = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $layoutFolder)
		{
			$this->setLayoutFolder($layoutFolder);
			$this->setAllToDefault();
		}

		/**
		 * SETUP STUFF
		 */
		public function addMetaInformation(string $file, bool $useTag=false)
		{
			if($useTag)
				$this->metaInformationTags[] = $file;
			else if($this->checkForValidity($file) === true)
				$this->metaInformationFiles[] = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid metaInformationFile.");
		}
		public function addHeadStyleSheet(string $file, bool $useTag=false)
		{
			if($useTag)
				$this->headStyleSheetTags[] = $file;
			else if($this->checkForValidity($file) === true)
				$this->headStyleSheetFiles[] = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid headStyleSheetFile.");
		}
		public function addHeadScript(string $file, bool $useTag=false)
		{
			if($useTag)
				$this->headScriptTags[] = $file;
			else if($this->checkForValidity($file) === true)
				$this->headScriptFiles[] = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid headScriptFile.");
		}
		public function addHeadScriptSnippet($snippet)
		{
			$this->headScriptSnippets[] = $snippet;
		}
		public function setTitle(string $title)
		{
			if(!empty($title))
				$this->title = $title;
		}
		public function setAppcache(string $appcache)
		{
			if(!empty($appcache))
				$this->appcache = $appcache;
		}
		public function setLoadingScreen(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->loadingScreenFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid loadingScreenFile.");
		}
		public function setHeader(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->headerFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid headerFile.");
		}
		public function setNavigation(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->navigationFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid navigationFile.");
		}
		public function setSubNavigation(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->subNavigationFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid subNavigationFile.");
		}
		public function setContent(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->contentFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid contentFile.");
		}
		public function setSidebar(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->sidebarFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid sidebarFile.");
		}
		public function setFooter(string $file)
		{
			if($this->checkForValidity($file) === true)
				$this->footerFile = $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid footerFile.");
		}
		public function setAllToDefault()
		{
			$this->unsetAll();

			if(is_file($this->layoutFolder.'metaInformation.html'))
				$this->addMetaInformation($this->layoutFolder.'metaInformation.html');
			if(is_file($this->layoutFolder.'headStyleSheet.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'headStyleSheet.css');
			if(is_file($this->layoutFolder.'headScript.js'))
				$this->addHeadScript($this->layoutFolder.'headScript.js');
			if(is_file($this->layoutFolder.'headScript.esm.js'))
				$this->addHeadScript($this->layoutFolder.'headScript.esm.js');
			if(is_file($this->layoutFolder.'default.appcache'))
				$this->setAppcache($this->layoutFolder.'default.appcache');

			if(is_file($this->layoutFolder.'loadingScreen.html'))
				$this->setLoadingScreen($this->layoutFolder.'loadingScreen.html');
			if(is_file($this->layoutFolder.'loadingScreen.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'loadingScreen.css');
			if(is_file($this->layoutFolder.'loadingScreen.js'))
				$this->addHeadScript($this->layoutFolder.'loadingScreen.js');
			if(is_file($this->layoutFolder.'loadingScreen.esm.js'))
				$this->addHeadScript($this->layoutFolder.'loadingScreen.esm.js');

			if(is_file($this->layoutFolder.'header.html'))
				$this->setHeader($this->layoutFolder.'header.html');
			if(is_file($this->layoutFolder.'header.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'header.css');
			if(is_file($this->layoutFolder.'header.js'))
				$this->addHeadScript($this->layoutFolder.'header.js');
			if(is_file($this->layoutFolder.'header.esm.js'))
				$this->addHeadScript($this->layoutFolder.'header.esm.js');

			if(is_file($this->layoutFolder.'navigation.html'))
				$this->setNavigation($this->layoutFolder.'navigation.html');
			if(is_file($this->layoutFolder.'navigation.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'navigation.css');
			if(is_file($this->layoutFolder.'navigation.js'))
				$this->addHeadScript($this->layoutFolder.'navigation.js');
			if(is_file($this->layoutFolder.'navigation.esm.js'))
				$this->addHeadScript($this->layoutFolder.'navigation.esm.js');

			if(is_file($this->layoutFolder.'subNavigation.html'))
				$this->setSubNavigation($this->layoutFolder.'subNavigation.html');
			if(is_file($this->layoutFolder.'subNavigation.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'subNavigation.css');
			if(is_file($this->layoutFolder.'subNavigation.js'))
				$this->addHeadScript($this->layoutFolder.'subNavigation.js');
			if(is_file($this->layoutFolder.'subNavigation.esm.js'))
				$this->addHeadScript($this->layoutFolder.'subNavigation.esm.js');

			if(is_file($this->layoutFolder.'content.html'))
				$this->setContent($this->layoutFolder.'content.html');
			else if(is_file($this->layoutFolder.'content.md')) // Load md file only when there is no html
				$this->setContent($this->layoutFolder.'content.md');
			if(is_file($this->layoutFolder.'content.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'content.css');
			if(is_file($this->layoutFolder.'content.js'))
				$this->addHeadScript($this->layoutFolder.'content.js');
			if(is_file($this->layoutFolder.'content.esm.js'))
				$this->addHeadScript($this->layoutFolder.'content.esm.js');

			if(is_file($this->layoutFolder.'sidebar.html'))
				$this->setSidebar($this->layoutFolder.'sidebar.html');
			if(is_file($this->layoutFolder.'sidebar.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'sidebar.css');
			if(is_file($this->layoutFolder.'sidebar.js'))
				$this->addHeadScript($this->layoutFolder.'sidebar.js');
			if(is_file($this->layoutFolder.'sidebar.esm.js'))
				$this->addHeadScript($this->layoutFolder.'sidebar.esm.js');

			if(is_file($this->layoutFolder.'footer.html'))
				$this->setFooter($this->layoutFolder.'footer.html');
			if(is_file($this->layoutFolder.'footer.css'))
				$this->addHeadStyleSheet($this->layoutFolder.'footer.css');
			if(is_file($this->layoutFolder.'footer.js'))
				$this->addHeadScript($this->layoutFolder.'footer.js');
			if(is_file($this->layoutFolder.'footer.esm.js'))
				$this->addHeadScript($this->layoutFolder.'footer.esm.js');
		}

		public function renderFileNow(string $file)
		{
			if($this->checkForValidity($file) === true)
				require $file;
			else
				throw new FsError(FsError::LOAD_FILE_FAILED, FsError::CRIT, "'$file' is not a valid renderable file.");
		}

		/**
		 * UNSET STUFF
		 */
		public function unsetMetaInformation()
		{
			$this->metaInformationFiles = array();
		}
		public function unsetHeadStyleSheets()
		{
			$this->headStyleSheetFiles = array();
		}
		public function unsetHeadScripts()
		{
			$this->headScriptFiles = array();
		}
		public function unsetTitle()
		{
			$this->title = "";
		}
		public function unsetAppcache()
		{
			$this->appcache = "";
		}
		public function unsetLoadingScreen()
		{
			$this->loadingScreenFile = null;
		}
		public function unsetHeader()
		{
			$this->headerFile = null;
		}
		public function unsetNavigation()
		{
			$this->navigationFile = null;
		}
		public function unsetSubNavigation()
		{
			$this->subNavigationFile = null;
		}
		public function unsetContent()
		{
			$this->contentFile = null;
		}
		public function unsetSidebar()
		{
			$this->sidebarFile = null;
		}
		public function unsetFooter()
		{
			$this->footerFile = null;
		}
		public function unsetAll()
		{
			$this->unsetMetaInformation();
			$this->unsetHeadStyleSheets();
			$this->unsetHeadScripts();
			$this->unsetTitle();
			$this->unsetAppcache();
			$this->unsetLoadingScreen();
			$this->unsetHeader();
			$this->unsetNavigation();
			$this->unsetSubNavigation();
			$this->unsetContent();
			$this->unsetSidebar();
			$this->unsetFooter();
		}

		public function render()
		{
			if(!empty($this->layoutFolder))
				require $this->layoutFolder.'layout.php';
			else
				throw new FsError(FsError::FILE_NOT_FOUND, FsError::CRIT, "Tried to render '" . $this->layoutFolder.'layout.php' . "', but file does not exist.");
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		/**
		 * SETUP STUFF
		 */
		protected function setLayoutFolder(string $layoutFolder)
		{
			$layoutFolder = Helper::normalizeFolderPath($layoutFolder);
			if(is_dir($layoutFolder) === true)
				$this->layoutFolder = $layoutFolder;
			else
				throw new FsError(FsError::FOLDER_NOT_FOUND, FsError::CRIT, "'$layoutFolder' is not a valid layoutFolder.");
		}

		/**
		 * SHOW STUFF
		 */
		protected function showMetaInformation()
		{
			if(!empty($this->metaInformationTags))
				foreach ($this->metaInformationTags as $metaInformationTag)
					echo "<meta " . $metaInformationTag . " />\n";

			if(!empty($this->metaInformationFiles))
				foreach ($this->metaInformationFiles as $metaInformationFile)
					require $metaInformationFile;
		}
		protected function showHeadStyleSheets()
		{
			if(!empty($this->headStyleSheetTags))
			{
				$containsLess = false;
				foreach($this->headStyleSheetTags as $styleSheetTag)
					if(pathinfo($styleSheetTag, PATHINFO_EXTENSION) === "less")
						$containsLess = true;

				// Optimize less/css
				if($_SERVER["TT"]->conf->optimizationEnabled)
				{
					if($containsLess)
					{

					}
					else
					{
						$requestFirstPart = basename($this->tempFolder);
						echo "<link rel='stylesheet' type='text/css' href='/$requestFirstPart/public/dumpling.cgz' />\n";
						if(is_file("$this->tempFolder/temp/dumpling.cgz") === false)
						{
							$dumplingContent = "";
							foreach ($this->headStyleSheetTags as $styleSheetTag)
							{
								$styleSheet = "";

								// if there is a secure connection, then download it from the remote server,
								// otherwise interpret it as a request to our own server, which is of
								// course https. We can't download from '/css/myFile.css', so we have also
								// to append the whole server name.
								if(Helper::isHttps($styleSheetTag))
									$styleSheet = file_get_contents($styleSheetTag);
								else
									$styleSheet = file_get_contents("https://" . $_SERVER["HTTP_HOST"] . "/" . $styleSheetTag);

								if(!empty($styleSheet))
									$dumplingContent .= $styleSheet;
								else
									throw new FsError(FsError::FILE_NOT_FOUND, FsError::CRIT, "StyleSheet file could not be downloaded from '$styleSheetTag'.");
							}

							$dumplingContent = Helper::uglifyCss($dumplingContent);
							if(TripleTowerError::isError($dumplingContent))
								throw $dumplingContent;

							$dumplingContentCompressed = gzencode($dumplingContent, 9, FORCE_GZIP);
							if(TripleTowerError::isError($dumplingContentCompressed))
								throw $dumplingContentCompressed;

							file_put_contents("$this->tempFolder/temp/dumpling.cgz", $dumplingContentCompressed);
						}
					}
				}
				else
				{
					if($containsLess)
					{
						$towerType = $_SERVER["TT"]->tower->getTowerInstance();
						$binFolder = $_SERVER["TT"]->tower->getBinFolder();
						$tempFolder = $_SERVER["TT"]->tower->getTempFolder();
						$contentProvider = $_SERVER["TT"]->contentProvider;
						echo "<link rel='stylesheet/less' type='text/css' href='/$towerType/$contentProvider/". Tower::CONTENT_TYPE_TEMP . "/styles.less' />\n";

						$tempLessFilePath = Helper::normalizeFilePath("$tempFolder/styles.less");
						$metaJsonFilePath = Helper::normalizeFilePath(dirname($binFolder)."/meta.json");
						if(!file_exists($tempLessFilePath) || filemtime($metaJsonFilePath) >= filemtime($tempLessFilePath))
						{
							$stylesText = "";
							foreach($this->headStyleSheetTags as $styleSheetTag)
								$stylesText .= "@import \"$styleSheetTag\";\n";

							if(file_put_contents($tempLessFilePath, $stylesText) === false)
								new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Could not create less file '$tempLessFilePath'");
						}
					}
					else
					{
						foreach($this->headStyleSheetTags as $styleSheetTag)
							echo "<link rel='stylesheet' type='text/css' href='$styleSheetTag' />\n";
					}

				}
			}

			if(!empty($this->headStyleSheetFiles))
			{
				foreach ($this->headStyleSheetFiles as $styleSheetFile)
				{
					echo "<style type='text/css'>\n";
					require $styleSheetFile;
					echo "\n</style>";
				}
			}
		}
		protected function showHeadScripts()
		{
			if(!empty($this->headScriptTags))
			{
				if(!empty($_SERVER["TT"]->conf->optimizationEnabled) && is_dir($this->tempFolder) && !empty(basename($this->tempFolder)))
				{
					$requestFirstPart = basename($this->tempFolder);
					echo "<script src='/$requestFirstPart/public/dumpling.jgz'></script>";
					if(is_file("$this->tempFolder/temp/dumpling.jgz") === false)
					{
						$dumplingContent = "";
						foreach ($this->headScriptTags as $headScriptTag)
						{
							$script = "";

							// if there is a secure connection, then download it from the remote server,
							// otherwise interpret it as a request to our own server, which is of
							// course https. We can't download from '/css/myFile.css', so we have also
							// to append the whole server name.
							if(Helper::isHttps($headScriptTag))
								$script = file_get_contents ($headScriptTag);
							else
								$script = file_get_contents("https://" . $_SERVER["HTTP_HOST"] . "/" . $headScriptTag);

							if(!empty($script))
								$dumplingContent .= $script;
							else
								throw new FsError(FsError::FILE_NOT_FOUND, FsError::CRIT, "Script file could not be downloaded from '$headScriptTag'.");
						}
						$dumplingContent = Helper::uglifyJs($dumplingContent);
						if(TripleTowerError::isError($dumplingContent))
							throw $dumplingContent;

						$dumplingContentCompressed = gzencode($dumplingContent, 9, FORCE_GZIP);
						if(TripleTowerError::isError($dumplingContentCompressed))
							throw $dumplingContentCompressed;

						file_put_contents("$this->tempFolder/temp/dumpling.jgz", $dumplingContentCompressed);
					}
				}
				else
				{
					foreach($this->headScriptTags as $headScriptTag)
					{
						// ESM modules have to be specified with the type='module' attribute
						$isEsmModule = (substr_compare(strtolower($headScriptTag), ".esm.js", -7) === 0);
						echo "<script" . ($isEsmModule ? " type='module'" : "") . " src='" . $headScriptTag . "'></script>\n";
					}
				}
			}

			if(!empty($this->headScriptFiles))
			{
				foreach ($this->headScriptFiles as $headScriptFile)
				{
					// ESM modules have to be specified with the type='module' attribute
					$isEsmModule = (substr_compare(strtolower($headScriptFile), ".esm.js", -7) === 0);
					echo "<script" . ($isEsmModule ? " type='module'" : "") . ">\n";
					require $headScriptFile;
					echo "\n</script>";
				}
			}
		}
		protected function showHeadScriptSnippets()
		{
			echo implode(PHP_EOL, $this->headScriptSnippets);
		}
		protected function showTitle()
		{
			if($this->title[0] === "#")
				echo t(substr($this->title, 1));
			else
				echo $this->title;

		}
		protected function showAppcache()
		{
			echo $this->appcache;

		}
		protected function showLoadingScreen()
		{
			if(!empty($this->loadingScreenFile))
				require $this->loadingScreenFile;
		}
		protected function showHeader()
		{
			if(!empty($this->headerFile))
				require $this->headerFile;
		}
		protected function showNavigation()
		{
			if(!empty($this->navigationFile))
				require $this->navigationFile;
		}
		protected function showSubNavigation()
		{
			if(!empty($this->subNavigationFile))
				require $this->subNavigationFile;
		}
		protected function showContent()
		{
			if(!empty($this->contentFile))
			{
				if( pathinfo($this->contentFile, PATHINFO_EXTENSION) === "md" )
				{
					ob_start();
					require $this->contentFile;
					$markdown = ob_get_clean();

					$parsedown = new Parsedown();
					echo $parsedown->text($markdown);
				}
				else
				{
					require $this->contentFile;
				}
			}
		}
		protected function showSidebar()
		{
			if(!empty($this->sidebarFile))
				require $this->sidebarFile;
		}
		protected function showFooter()
		{
			if(!empty($this->footerFile))
				require $this->footerFile;
		}

		/**
		 * HELPER STUFF
		 */
		protected function checkForValidity(string $file)
		{
			if(stream_resolve_include_path($file) !== false)
			{
				switch(strrchr($file, '.'))
				{
					case '.html':
					case '.md':
					case '.php':
					case '.txt':
					case '.xml':
					case '.pl':
					case '.js':
					case '.css':
					case '.meta':
						return true;
				}
			}
			return false;
		}
	}
}