<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Error\MailError;

	class EmailAddress
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		private $mailAddress;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		function __construct(string $mailAddress)
		{
			$this->mailAddress = $mailAddress;
		}

		public function getDomainPart()
		{
			$atIndex = strrpos($this->mailAddress, "@");
			return ($atIndex !== false) ? substr($this->mailAddress, $atIndex+1) : "";
		}

		public function getLocalPart()
		{
			$atIndex = strrpos($this->mailAddress, "@");
			return ($atIndex !== false) ? substr($this->mailAddress, 0, $atIndex) : "";
		}

		public function getFullAddress()
		{
			return $this->mailAddress;
		}

		/*
		 * Validation algorithm from Douglas Lovell (http://www.linuxjournal.com/article/9585)
		 */
		public function isValid()
		{
			$atIndex = strrpos($this->mailAddress, "@");
			if($atIndex === false)
			{
				return new MailError(MailError::MAILADDRESS_NO_AT_FOUND);
			}
			else
			{
				$domain = substr($this->mailAddress, $atIndex+1);
				$local = substr($this->mailAddress, 0, $atIndex);
				$localLen = strlen($local);
				$domainLen = strlen($domain);
				if($localLen < 1 || $localLen > 64)
				{
					// local part length exceeded
					return new MailError(MailError::LOCALPART_TOO_LONG);
				}
				else if($domainLen < 1 || $domainLen > 255)
				{
					// domain part length exceeded
					return new MailError(MailError::DOMAIN_TOO_LONG);
				}
				else if($local[0] == '.')
				{
					// local part starts with '.'
					return new MailError(MailError::LOCALPART_START_DOT_ERROR);
				}
				else if($local[$localLen-1] == '.')
				{
					// local part starts or ends with '.'
					return new MailError(MailError::LOCALPART_END_DOT_ERROR);
				}
				else if(preg_match('/\\.\\./', $local))
				{
					// local part has two consecutive dots
					return new MailError(MailError::LOCALPART_CONSECUTIVE_DOTS);
				}
				else if(!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
				{
					// character not valid in domain part
					return new MailError(MailError::DOMAIN_INVALID_CHARS);
				}
				else if(preg_match('/\\.\\./', $domain))
				{
					// domain part has two consecutive dots
					return new MailError(MailError::DOMAIN_CONSECUTIVE_DOTS);
				}
				else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
				{
					// character not valid in local part unless local part is quoted
					if(!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local)))
						return new MailError(MailError::LOCALPART_INVALID_CHARS);
				}

				if(!(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
				{
					// domain not found in DNS
					return new MailError(MailError::DOMAIN_NOT_EXISTING);
				}
			}
			return new MailError(MailError::SUCCESS);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}