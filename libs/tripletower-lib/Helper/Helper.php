<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Logger\TripleTowerLogFormatter;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\CryptoError;
	use TripleTowerSDK\Error\HwError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Network\Proxy;
	use TripleTowerSDK\User;

	final class Helper
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const KEYSIZE_AES128 = 16;
		const KEYSIZE_AES192 = 24;
		const KEYSIZE_AES256 = 32;

		const HTTP_USER_AGENT = "Mozilla/5.0 (" . PHP_OS . "; x64) like Webtra.net";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		private static $lastPackageListRefresh = 0;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/**
		 * @name hasRootPrivileges
		 * @desc Check if script is run with root privileges.
		 * @param void
		 * @return TripleTowerError TripleTowerError:SUCCESS if script is run with root privileges. TripleTowerError with according error code otherwise.
		 */
		public static function hasRootPrivileges() : TripleTowerError
		{
			switch(OsNames::getCurrentOsType())
			{
				case OsNames::OS_TYPE_WINDOWS:
				{
					$output     = array();
					$returnCode = 0;
					exec('net session >nul 2>&1', $output, $returnCode);
					if($returnCode === 0)
						return new AuthError(AuthError::SUCCESS);
					else
						return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "You do not have sufficient privileges");
				}
				break;

				case OsNames::OS_TYPE_LINUX:
				{
					if(posix_getuid() === 0)
						return new AuthError(AuthError::SUCCESS);
					else
						return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "You do not have sufficient privileges");
				}
				break;

				default:
					return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "'hasRootPrivileges' is not compatible with your OS/Distro");
			}
		}

		/**
		 * @name copy_recursive
		 * @desc Copies a file or even a whole folder.
		 * @param string $source The file/folder to copy
		 * @param string $destination The file/folder name to copy to
		 * @param int $permissions linux permissions used for chmod
		 * @return bool True if file/folder was copied successfully. False otherwise.
		 */
		public static function copy_recursive(string $source, string $destination, mixed $context = null, int $permissions = 0755) : bool
		{
			// Check for source
			if(!file_exists($source))
				return false;

			// Check for symlinks
			if (is_link($source))
				return symlink(readlink($source), $destination);

			// Check $context
			if( !is_null($context) && !is_resource($context) )
			   return false;

			// Simple copy for a file
			if (is_file($source))
				return copy($source, $destination, $context);

			// Make destination directory
			if (!is_dir($destination) && mkdir($destination, $permissions, true) !== true)
				return false;

			// Loop through the folder
			$dir = dir($source);
			while(($entry = $dir->read()) !== false)
			{
				// Copy files and directories
				if($entry !== '.' && $entry !== '..' && self::copy_recursive("$source/$entry", "$destination/$entry", $context, $permissions) !== true)
					return false;
			}

			// Clean up
			$dir->close();

			// Everything worked
			return true;
		}

		/**
		 * @name unlink_recursive
		 * @desc Unlinks a file or even a whole folder.
		 * @param string $filename The file/folder to unlink
		 * @param string $context The context to be added to the file stream
		 * @return bool True if file/folder was unlinked successfully. False otherwise.
		 */
		public static function unlink_recursive(string $filename, mixed $context = null) : FsError
		{
			// Check $context
			if( !is_null($context) && !is_resource($context) )
				return new FsError(FsError::IS_NOT_A_RESOURCE, FsError::ERR, "Given context for file '$filename' is not a resource");

			// Check if file or folder is a link, if so remove it
			if(is_link($filename))
			{
				if( !@unlink($filename, $context) && !@rmdir($filename, $context) )
					return new FsError(FsError::UNLINK_FAILED, FsError::ERR, "File or folder '$filename' could no be unlinked");

				return new FsError(FsError::SUCCESS);
			}

			// Check if file exists and resolve relative paths
			if($filename === "" || is_null($filename) || ($filename = realpath($filename)) === false)
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "File or folder '$filename' could no be found");

			// Remove files
			if( !is_dir($filename) )
			{
				if( @unlink($filename, $context) === false )
					return new FsError(FsError::FOLDER_DELETION_FAILED, FsError::ERR, "Could not remove folder '$filename'");

				return new FsError(FsError::SUCCESS);
			}

			// Remove folders recursivly
			$dir = dir($filename);
			try
			{
				while(($entry = $dir->read()) !== false)
				{
					if($entry === '.' || $entry === '..')
						continue;

					$unlinkResult = self::unlink_recursive("$filename/$entry", $context);
					if( !$unlinkResult->isSuccess() )
					{
						// If deletion fails, retry deletion after removing the read-only
						// flag for files on Windows systems
						if( !is_file("$filename/$entry") || !OsNames::isWindows() )
							return $unlinkResult;
						if( self::exec("attrib -R \"$filename/$entry\"", "", true) !== 0 )
							return new FsError(FsError::ATTRIBUTE_ALTERATION_FAILED, FsError::ERR, "Could not remove read-only flag from file '$filename/$entry'");

						$unlinkResult = self::unlink_recursive("$filename/$entry", $context);
						if( !$unlinkResult->isSuccess() )
							return $unlinkResult;
					}
				}
			}
			finally
			{
				// Clean up
				$dir->close();

				// Returns automatically if finally got called from a return-statement
			}

			// Remove folder
			if( @rmdir($filename, $context) === false )
				return new FsError(FsError::FOLDER_DELETION_FAILED, FsError::ERR, "Could not remove folder '$filename'");

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name refreshPackageList
		 * @desc Refreshes the package list if last refresh is older than
		 *       $maxOutdated. The default refresh intervall is 15 minutes. This
		 *       method is well aware of different package managers.
		 * @param int $maxOutdated The maximum number of seconds elapsed since hanoi has performed the last refresh.
		 * @return void
		 */
		public static function refreshPackageList(int $maxOutdated = 900)
		{
			try
			{
				// Refresh only if packages list is outdated
				if(self::$lastPackageListRefresh + $maxOutdated > time())
					return new SysError(SysError::SUCCESS);

				self::$lastPackageListRefresh = time();

				// Windows does not have any package lists which could be refreshed
				if(OsNames::isWindows())
					return new SysError(SysError::SUCCESS);

				// Debian/Ubuntu/Mint
				if(self::exec("hash apt-get", "", true) === 0)
					self::exec("apt-get update");

				//  Installing on SLES/openSuse
				else if(self::exec("hash zypper", "", true) === 0)
					self::exec("zypper refresh");

				//  Installing on RHEL/CentOS/Fedora
				else if(self::exec("hash yum", "", true) === 0)
					self::exec("yum check-update");

				//  Installing on CentOS/Fedora
				else if(self::exec("hash dnf", "", true) === 0)
					self::exec("dnf check-update");

				//  Installing on Arch Linux
				else if(self::exec("hash pacman", "", true) === 0)
					self::exec("pacman -Syu");

				// MacOSX
				else if(self::exec("hash brew", "", true) === 0)
					self::exec("brew update");

				// Unknown package manager
				else
					return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Could not refresh package list because package manager is unknown");
			}
			catch(\Exception $ex)
			{
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Could not refresh package because of: " . $ex->getMessage());
			}

			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name genericLinuxInstall
		 * @desc Installs a package by name using the preinstalled package manager.
		 * @param string $packageName The name of the package. Can consist of several packages separated by a space.
		 * @return void
		 */
		public static function genericLinuxInstall(string $packageName)
		{
			try
			{
				if(self::exec("hash apt-get", "", true) === 0)
					self::exec("apt-get -o Dpkg::Options::=\"--force-confold\" install --assume-yes $packageName");
				else if(self::exec("hash zypper", "", true) === 0)
					self::exec("zypper install mariadb");
				else if(self::exec("hash yum", "", true) === 0)
					self::exec("yum install mariadb");
				else if(self::exec("hash dnf", "", true) === 0)
					self::exec("dnf install mariadb");
				else if(self::exec("hash pacman", "", true) === 0)
					self::exec("pacman -S mariadb");
				else
					return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Could install '$packageName' because package manager is unknown");
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Could not refresh package because of: " . $t->getMessage());
			}

			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name exec
		 * @desc Executes the given command. If return code is unequal
		 *       to 0 an \Exception gets thrown. Also all stderr output
		 *       is redirected to stdout.
		 * @param string $command The command that will be executed
		 * @param string $errorText If no error text is given the output of the command is thrown. If error text is specified it gets thrown instead.
		 * @param bool $returnErrorCode If set to true the error code is returned, if set to false the output is being returned or an exception being thrown if an error occured.
		 * @return void/int
		 */
		public static function exec($command, string $errorText = "", bool $returnErrorCode = false)
		{
			$output     = [];
			$returnCode = 0;
			exec("$command 2>&1", $output, $returnCode);

			if( $returnErrorCode === true )
				return $returnCode;

			if($returnCode !== 0)
			{
				if(empty($errorText))
					throw new \Exception("'$command' with errorcode '$returnCode' caused: " . implode(PHP_EOL, $output));
				else
					throw new \Exception($errorText);
			}
			else
			{
				return $output;
			}
		}

		/**
		 * @name replaceInFile
		 * @desc Replaces a text in a file with given text.
		 * @param string $search What should be replaced
		 * @param string $replace With what should $search be replaced
		 * @param string $filePath The file of which the content is being parsed
		 * @return void
		 */
		public static function replaceInFile(string $search, string $replace, string $filePath)
		{
			$fileContent = file_get_contents($filePath);
			if($fileContent === false)
				return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, "Could not read file '$filePath'");
			if(file_put_contents($filePath, str_replace($search, $replace, $fileContent)) === false)
				return new FsError(FsError::FILE_IS_READ_ONLY, FsError::ERR, "Could not write file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name regexReplaceInFile
		 * @desc Replaces a text in a file with given regex.
		 * @param string $pattern What should be replaced
		 * @param string $replace With what should found $pattern be replaced
		 * @param string $filePath The file of which the content is being parsed
		 * @return void
		 */
		public static function regexReplaceInFile(string $pattern, string $replace, string $filePath)
		{
			$fileContent = file_get_contents($filePath);
			if($fileContent === false)
				return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, "Could not read file '$filePath'");
			if(file_put_contents($filePath, preg_replace($pattern, $replace, $fileContent)) === false)
				return new FsError(FsError::FILE_IS_READ_ONLY, FsError::ERR, "Could not write file '$filePath'");

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name replaceInJsonFile
		 * @desc Replaces a text in a file with given text.
		 * @param string $search What should be replaced
		 * @param string $replace With what should $search be replaced
		 * @param string $jsonFilePath The json file of which the content is being parsed
		 * @return void
		 */
		public static function replaceInJsonFile(string $search, $replace, string $jsonFilePath)
		{
			$fileContent = file_get_contents($jsonFilePath);
			if($fileContent === false)
				return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, "Could not read json file '$jsonFilePath'");

			$jsonEncodedReplace = json_encode($replace, JSON_UNESCAPED_UNICODE);
			if($jsonEncodedReplace === false)
				return new SysError(SysError::MALFORMED_JSON, SysError::ERR, "Could not encode '$replace' to json");

			$regexSearch = "/" . preg_quote($search) . "(\W)/";
			$newContent = preg_replace($regexSearch, str_replace(["\\", "\$"], ["\\\\", "\\\$"], $jsonEncodedReplace).'$1', $fileContent);
			if( $newContent === null )
				return new SysError(SysError::INVALID_EXPANSION, SysError::ERR, "Could not replace variable '$search' by '$replace'");

			if( file_put_contents($jsonFilePath, $newContent) === false )
				return new FsError(FsError::FILE_IS_READ_ONLY, FsError::ERR, "Could not write file '$jsonFilePath'");

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name extractFileName
		 * Separates a file name from a given string which contains
		 * a path and a filename. The path does not have to exist on
		 * file system. Works like the core function basename(), but
		 * does not return something if last character is '/'
		 *
		 * @param string $path    Containing a filepath.
		 * @return string         Extracted filename
		 *
		 * @example extractFileName("/myPath/hello.txt")
		 * @return  "hello.txt"
		 *
		 * @example extractFileName("")
		 * @return  ""
		 *
		 * @example extractFileName("C:/myPath/")
		 * @return  ""
		 *
		 */
		public static function extractFileName(string $path)
		{
			if($path === "")
				return "";
			return (substr($path, -1, 1) === '/' ? '' : self::normalizeFilePath(basename($path)));
		}

		/**
		 * @name extractFolderName
		 * Separates the path from a filename in given string.
		 * The path does not have to exist on the file system.
		 * Works like the core function dirname(), but does
		 * return the whole path if last character is '/'
		 *
		 * @param string $path    Containing a filepath.
		 * @return string         Extracted foldername with path
		 *
		 * @example extractFolderName("/myPath/hello.txt")
		 * @return  "/myPath/"
		 *
		 * @example extractFolderName("")
		 * @return  ""
		 *
		 * @example extractFolderName("C:/myPath/")
		 * @return  "C:/myPath/"
		 *
		 */
		public static function extractFolderName(string $path)
		{
			if($path === "")
				return "";
			return (substr($path, -1, 1) === '/' ? self::normalizeFolderPath($path) : self::normalizeFolderPath(dirname($path)));
		}

		/**
		 * @name normalizePath
		 * Reformats the given path according to the TripleTowerLib
		 * standards (no relative paths and no backslashes).
		 * The path does not have to exist on the file system.
		 * If the last character is a slash, the path is assumed to
		 * to be a folder path.
		 *
		 * @param string $path    Containing a filepath
		 * @return string         formatted path
		 *
		 * @example normalizePath("\myPath/./\\hello.txt")
		 * @return  "/myPath/hello.txt"
		 *
		 * @example normalizePath("")
		 * @return  ""
		 *
		 * @example normalizePath("C:/")
		 * @return  "C:/"
		 *
		 * @example normalizePath("C:")
		 * @return  "C:"
		 *
		 * @example normalizePath("/")
		 * @return  "/"
		 *
		 * @example normalizePath("\\localhost\myShare\mypath")
		 * @return  "\\localhost\myShare/mypath"
		 */
		public static function normalizePath(string $path)
		{
			$path = trim($path);
			if( $path === "" )
				return "";

			$isUncPath = self::stringStartsWith($path, "\\\\");
			$path = str_replace('\\', '/', $path);
			$firstCharIsSlash = $path[0] === '/';
			$lastCharIsSlash = substr($path, -1, 1) === '/';
			$folderParts = array_filter(explode('/', $path), 'strlen');
			$resultFolders = array();
			foreach($folderParts as $folder)
			{
				if ('.'  === $folder)
				{
					continue;
				}
				else if ('..' === $folder)
				{
					if(empty($resultFolders))
						$resultFolders[] = trim($folder);
					else
						array_pop($resultFolders);
				}
				else
				{
					$resultFolders[] = trim($folder);
				}
			}
			if($isUncPath)
			{
				if(count($resultFolders) < 2)
				{
					return new FsError(FsError::NAME_IS_INVALID, FsError::ERR);
				}
				$uncPath = "\\\\".$resultFolders[0]."\\".$resultFolders[1];
				$resultFolders = array_slice($resultFolders, 2);
				return $uncPath . (count($resultFolders) ? "/" : "") . implode("/", $resultFolders) . ($lastCharIsSlash?"/":"");
			}
			else
			{
				// If just a slash has been given
				if(count($resultFolders) === 0 && $firstCharIsSlash && $lastCharIsSlash)
				{
					return "/";
				}
				else
				{
					return ($firstCharIsSlash?"/":"") . implode("/", $resultFolders) . ($lastCharIsSlash?"/":"");
				}
			}
		}

		/**
		 * @name normalizeFilePath
		 * Reformats the given path according to the tripletower-lib
		 * standards (no relative paths and no backslashes). The
		 * path is to be assumed to be the path of a file, not a
		 * folder.
		 * The path does not have to exist on the file system.
		 * If the last character is a slash, it gets removed.
		 *
		 * @param string $path    Containing a filepath
		 * @return string         formatted path
		 *
		 * @example normalizeFilePath("\myPath/./\\hello.txt/")
		 * @return  "/myPath/hello.txt"
		 *
		 * @example normalizePath("")
		 * @return  ""
		 *
		 * @example normalizePath("C:/")
		 * @return  "C:"
		 *
		 * @example normalizePath("C:")
		 * @return  "C:"
		 *
		 * @example normalizePath("/")
		 * @return  ""
		 *
		 * @example normalizePath("\\localhost\myShare\myfile")
		 * @return  "\\localhost\myShare/myfile"
		 */
		public static function normalizeFilePath(string $path)
		{
			$path = self::normalizePath($path);
			return substr($path, -1, 1)==='/' ? substr($path, 0, -1) : $path;
		}

		/**
		 * @name normalizeFolderPath
		 * Reformats the a given path according to the tripletower-lib
		 * standards (no relative paths and no backslashes). The
		 * path is to be assumed to be the path of a directory, not a
		 * file.
		 * The path does not have to exist on the file system.
		 * If the path has no trailing slash, it will get added.
		 *
		 * @param string $path    Containing a filepath
		 * @return string         formatted path
		 *
		 * @example normalizeFilePath("\myPath/./\\hello.txt")
		 * @return  "/myPath/hello.txt/"
		 *
		 * @example normalizePath("")
		 * @return  ""
		 *
		 * @example normalizePath("C:/")
		 * @return  "C:/"
		 *
		 * @example normalizePath("C:")
		 * @return  "C:/"
		 *
		 * @example normalizePath("/")
		 * @return  "/"
		 *
		 * @example normalizePath("\\localhost\myShare\mypath")
		 * @return  "\\localhost\myShare/mypath/"
		 */
		public static function normalizeFolderPath(string $path)
		{
			if($path == "")
				return "";
			$path = self::normalizePath($path);
			return (substr($path, -1, 1) === '/' ? $path : $path.'/');
		}

		/**
		 * @name normalizeFolderPath
		 * Reformats the a given path according to the tripletower-lib
		 * standards (no relative paths and no backslashes). This
		 * function lookes for the file on the local filesystem and
		 * looks up if it's a file or a folder.
		 * The path has to exist on the file system.
		 *
		 * @param string $path    Containing a filepath
		 * @return string         formatted path
		 *
		 * @example normalizePath("")
		 * @return  ""
		 *
		 * @example normalizePath("C:/")
		 * @return  "C:/"
		 *
		 * @example normalizePath("C:")
		 * @return  "C:/"
		 *
		 * @example normalizePath("/")
		 * @return  "/"
		 *
		 * @example normalizePath("\\localhost\myShare\myfile")
		 * @return  "\\localhost\myShare/myfile"
		 */
		public static function normalizePathOnFileSystem(string $path)
		{
			if(is_file($path))
				return self::normalizeFilePath($path);
			if(is_dir($path))
				return self::normalizeFolderPath($path);
			return new FsError(FsError::FILE_NOT_FOUND);
		}

		/**
		 * @name createPathIfNotExists
		 * Creates all folders and parent folder according to the path
		 * specified if they don't exist. If no further parameter are
		 * specified, this function creates a path on the local file
		 * system. The other parameters are callbacks to have this
		 * functionality applied to another "system" which needs to
		 * get folders created. Given callbacks must have the absolutely
		 * same interface like php's native FS functions, which are used
		 * by default callbacks are null, not specified or not callable.
		 *
		 * @param string    $path           Containing a filepath
		 * @param callback  $file_exists    optional
		 * @param callback  $is_dir         optional
		 * @param callback  $is_writeable   optional
		 * @param callback  $mkdir          optional
		 * @return TripleTowerError errorCode
		 *
		 * @example createPathIfNotExists("C:/hallo/")
		 * @return  Error::SUCCESS
		 *
		 *
		 */
		public static function createPathIfNotExists(string $path, $file_exists=null, $is_dir=null, $is_writeable=null, $mkdir=null)
		{
			if($file_exists == null || !is_callable($file_exists))
				$file_exists = 'file_exists';
			if($is_dir == null || !is_callable($is_dir))
				$is_dir = 'is_dir';
			if($is_writeable == null || !is_callable($is_writeable))
				$is_writeable = 'is_writeable';
			if($mkdir == null || !is_callable($mkdir))
				$mkdir = 'mkdir';

			clearstatcache();
			$path = self::normalizeFolderPath($path);
			$pathAsArray = array_filter(explode('/', $path), 'strlen');
			$foldersToCreate = array();

			// search for parent path until a folder exists and no more parents (dirname('C:/') returns 'C:/'))
			for($momFolder=$path; !call_user_func($file_exists, $momFolder) && $momFolder!=dirname($momFolder); $momFolder=dirname($momFolder))
				$foldersToCreate[] = array_pop($pathAsArray);

			$existingFolder = self::normalizeFilePath($momFolder);
			if(!call_user_func($file_exists, $existingFolder))
				return new FsError(FsError::FOLDER_NOT_FOUND);
			if(!call_user_func($is_dir, $existingFolder))
				return new FsError(FsError::FILE_ALREADY_EXISTS);
			if(!call_user_func($is_writeable, $existingFolder))
				return new FsError(FsError::ACCESS_DENIED);

			$foldersToCreate = array_reverse($foldersToCreate);
			foreach($foldersToCreate as $folder)
			{
				$resultFolder = str_replace('//', '/', "$existingFolder/$folder");
				if ($folder == '.'  || $folder == '..')
					return new FsError(FsError::FOLDER_CREATION_FAILED);
				else if(!call_user_func($mkdir, $resultFolder))
					return new FsError(FsError::FOLDER_CREATION_FAILED);
				else
					$existingFolder .= "/$folder";
			}
			return call_user_func($is_dir, $path) ? new FsError(FsError::SUCCESS) : new FsError(FsError::UNKNOWN);
		}

		/**
		 * @name getSize
		 * Determines if given path is a file or folder.
		 *
		 *
		 * @param string    $path           Containing a filepath
		 * @param callback  $file_exists    optional
		 * @param callback  $is_dir         optional
		 * @param callback  $is_writeable   optional
		 * @param callback  $mkdir          optional
		 * @return TripleTowerError errorCode
		 *
		 * @example createPathIfNotExists("C:/hallo/")
		 * @return  Error::SUCCESS
		 *
		 *
		 */
		public static function getSize(string $path)
		{
			return is_dir($path) ? self::getFolderSize($path) : self::getFileSize($path);
		}

		/**
		 * @name stringStartsWith
		 * @desc
		 */
		public static function stringStartsWith(string $string, string $conditionString)
		{
			if(empty($string))
			{
				return ($conditionString === "");
			}

			if(strlen($string) < strlen($conditionString))
			{
				return false;
			}

			return substr($string, 0, strlen($conditionString)) === $conditionString;
		}

		/**
		 * @name stringEndsWith
		 * @desc
		 */
		public static function stringEndsWith(string $string, string $conditionString)
		{
			if(empty($string))
			{
				return ($conditionString === "");
			}

			if(strlen($string) < strlen($conditionString))
			{
				return false;
			}

			return substr($string, -strlen($conditionString)) === $conditionString;
		}

		/**
		 * @name getFileSize
		 * @desc
		 */
		public static function getFileSize(string $filePath)
		{
			if(is_file($filePath) === true)
			{
				if(PHP_INT_SIZE === 4)
				{
					if(OsNames::isWindows())
					{
						$comFile = new \COM("Scripting.FileSystemObject");
						return (float)$comFile->GetFile($filePath)->Size;
					}
					else
					{
						// for unixiod operating systems like Unix, Linux and Mac
						return (float)trim('stat -c%s $file');
					}
				}
				else if(PHP_INT_SIZE === 8)
				{
					return filesize($filePath);
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}

		/**
		 * @name getFolderSize
		 * @desc
		 */
		public static function getFolderSize(string $path, int $sum = 0)
		{
			$path = self::normalizePathOnFileSystem($path);
			$handle = opendir($path);
			while(($file = readdir($handle)) !== false)
			{
				if(is_file($path.$file))
					$sum += self::getFileSize($path.$file);
				if(is_dir($path.$file) && $file!="." && $file!="..") // && !symlink
					$sum += self::getFolderSize($path.$file);
			}
			return $sum;
		}

		/**
		 * @name getSize
		 * Funktion zum Umrechnen auf die dementsprechende Mengeneinheit
		 * @todo: Add an accuracy number
		 */
		public static function getBytesHumanReadable(int $bytes)
		{
			$sizeStrings = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
			$rest = 0;
			for($loop = 0; $bytes >= 1024; $loop++)
			{
				$rest = ($bytes % 1024);
				if($bytes <= PHP_INT_MAX)
					$bytes >>= 10;
				else
					$bytes /= 1024;
			}
			return ($bytes. ".$rest" . ' ' . $sizeStrings[$loop]);
		}

		/**
		 * @name isHttps
		 * @desc
		 */
		public static function isHttps($url=null)
		{
			if($url === null)
			{
				if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === "on")
					return true;
				else
					return false;
			}
			else
			{
				return substr($url, 0, 8) === 'https://';
			}
		}

		/**
		 * @name redirectToHttps
		 * @desc
		 */
		public static function redirectToHttps()
		{
			if(self::isHttps())
			{
				header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
				exit();
			}
		}

		/**
		 * @name setXMLHeader
		 * @desc
		 */
		public static function setXmlHeader(bool $echoXmlHeader=true)
		{
			header("Content-type: text/xml; charset=utf-8");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
			if($echoXmlHeader)
				echo '<?xml version="1.0" encoding="UTF-8"?>';
		}

		/**
		 * @name setPlainTextHeader
		 * @desc
		 */
		public static function setPlainTextHeader()
		{
			header("Content-type: text/plain; charset=utf-8");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		}

		/**
		 * @name setHTMLHeader
		 * @desc
		 */
		public static function setHtmlHeader()
		{
			header("Content-type: text/html; charset=utf-8");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		}

		/**
		 * @name setJSONHeader
		 * @desc
		 */
		public static function setJsonHeader()
		{
			header("Content-type: application/json; charset=utf-8");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		}

		/**
		 * @name ctype_int
		 * @desc
		 */
		public static function ctype_int(int $number)
		{
			return preg_match('/^-?[0-9]+$/', (string)$number) ? true : false;
		}

		/**
		 * @name removeFirstChar
		 * @desc
		 */
		public static function removeFirstChar(string $string, string $conditionChar="")
		{
			if(empty($string))
				return "";
			else if($conditionChar == "")
				return substr($string, 1);
			else
				return $string[0]===$conditionChar ? substr($string, 1) : $string;
		}

		/**
		 * @name removeLastChar
		 * @desc
		 */
		public static function removeLastChar(string $string, string $conditionChar="")
		{
			if(empty($string))
				return "";
			else if($conditionChar == "")
				return substr($string, 0, -1);
			else
				return substr($string, -1, 1)===$conditionChar ? substr($string, 0, -1) : $string;
		}

		/**
		 * @name addFirstCharIfNot
		 * @desc
		 */
		public static function addFirstCharIfNot(string $string, string $conditionChar)
		{
			if(empty($string))
				return "";
			else if(empty($conditionChar))
				return $string;
			else
				return $string[0]===$conditionChar ? $string : $conditionChar.$string;
		}

		/**
		 * @name addLastCharIfNot
		 * @desc
		 */
		public static function addLastCharIfNot(string $string, string $conditionChar)
		{
			if(empty($string))
				return "";
			else if(empty($conditionChar))
				return $string;
			else
				return substr($string, -1, 1)===$conditionChar ? $string : $string.$conditionChar;
		}

		/**
		 * @name removeIdPrefix
		 * @desc
		 */
		public static function removeIdPrefix($mixed, $char)
		{
			if(is_array($mixed))
			{
				$resultArray = array();
				for($loop = 0; $loop < count($mixed); $loop++)
				{
					$resultId = self::removeFirstChar($mixed[$loop], $char);
					if(!empty($resultId) && self::isNaturalNumber($resultId))
						array_push($resultArray, $resultId);
				}
				return $resultArray;
			}
			else
			{
				$resultId = self::removeFirstChar($mixed, $char);
				if(!empty($resultId) && self::isNaturalNumber($resultId))
					return $resultId;
				else
					return 0;
			}
		}

		/**
		 * @name renameFile
		 * @desc
		 */
		public static function renameFile(string $oldFileNamePath, string $newFileNamePath)
		{
			if(!file_exists($oldFileNamePath))
				return new FsError(FsError::FILE_NOT_FOUND);

			if(file_exists($newFileNamePath))
				return new FsError(FsError::FILE_ALREADY_EXISTS);

			if(!is_writable($oldFileNamePath))
			{
				if(!OsNames::isWindows())
					return new FsError(FsError::FILE_IS_READ_ONLY);

				$returnValue = 0;
				$output = [];
				exec("attrib -r " . escapeshellarg($oldFileNamePath), $output, $returnValue);
				if($returnValue !== 0 || !is_writable($oldFileNamePath))
					return new FsError(FsError::FILE_IS_READ_ONLY);
			}

			if(($moveResult = rename($oldFileNamePath, $newFileNamePath)) !== true)
				return new FsError(FsError::UNKNOWN);

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name copyFile
		 * @desc
		 */
		public static function copyFile(string $oldFileNamePath, string $newFileNamePath)
		{
			if(!file_exists($oldFileNamePath))
					return new FsError(FsError::FILE_NOT_FOUND);
			else if(file_exists($newFileNamePath))
					return new FsError(FsError::FILE_ALREADY_EXISTS);
			else if(($copyResult = copy($oldFileNamePath, $newFileNamePath)) !== true)
					return new FsError(FsError::UNKNOWN);

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name strrstr
		 * @desc
		 */
		public static function strrstr(string $haystack, string $needle, bool $before_needle=false)
		{
			//TODO: Implement this function to have a real string $needle, not a char $needle
			if($before_needle)
			{
				$resultString = substr($haystack, 0, strrpos($haystack, $needle));
				return $resultString !== "" ? $resultString : false;
			}
			else
			{
				$resultString = substr($haystack, strrpos($haystack, $needle));
				return $resultString !== "" ? $resultString : false;
			}
		}

		/**
		 * @name strristr
		 * @desc
		 */
		public static function strristr(string $haystack, string $needle, bool $before_needle=false)
		{
			//TODO: Implement this function to have a real string $needle, not a char $needle
			if($before_needle)
			{
				$resultString = substr($haystack, 0, strripos($haystack, $needle));
				return $resultString !== "" ? $resultString : false;
			}
			else
			{
				$resultString = substr($haystack, strripos($haystack, $needle));
				return $resultString !== "" ? $resultString : false;
			}
		}

		/**
		 * @name xmlToSimpleXMLObject
		 * @desc
		 */
		public static function xmlToSimpleXMLObject($xml)
		{
			if(self::isXmlObject($xml))
				return $xml;
			else if(is_file($xml))
				return simplexml_load_file($xml);
			else
				return simplexml_load_string($xml);
		}

		/**
		 * @name bcrypt
		 * @desc
		 */
		public static function bcrypt(string $plaintext, $salt=null)
		{
			if(!defined('CRYPT_BLOWFISH'))
				return new CryptoError(CryptoError::BCRYPT_NO_BLOWFISH_AVAILABLE);

			if($salt===null)
			{
				// Generate a blowfish(2y) with costs of 12 and 22 random characters
				$salt = '$2y$12$'.self::createRandomString(22, './0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
			}
			else
			{
				if(substr($salt, 0, 7) !== '$2y$12$')
					return new CryptoError(CryptoError::BCRYPT_INVALID_SALT_TYPE);
				if(strlen($salt) !== 29)
					return new CryptoError(CryptoError::BCRYPT_INVALID_SALT_LENGTH);
			}
			return crypt($plaintext, $salt);
		}

		/**
		 * @name validateBCryptHash
		 * @desc
		 */
		public static function validateBCryptHash(string $plaintext, string $hash)
		{
			$resultHash = self::bcrypt($plaintext, substr($hash, 0, 29));
			return is_string($resultHash) ? $hash === $resultHash : $resultHash;
		}

		/**
		 * @name aes256Encrypt
		 * @desc
		 */
		public static function aesEncrypt($plaintext, string $password, int $keySize = self::KEYSIZE_AES256)
		{
			// Key preparation
			if($keySize !== self::KEYSIZE_AES128 && $keySize !== self::KEYSIZE_AES192 && $keySize !== self::KEYSIZE_AES256)
				return new CryptoError(CryptoError::AES_UNSUPPORTED_KEYSIZE, CryptoError::ERR, "The key size '$keySize' is not supported by AES.");
			$hashedPassword = substr(hash("sha256", $password, true), 0, $keySize);
			if(empty($hashedPassword) || $keySize !== strlen($hashedPassword))
				return new CryptoError(CryptoError::PASSWORD_PREPARATION_FAILED, CryptoError::ERR, "Could not create hash of length '$keySize' of password.");

			// Initialization vector generation
			$ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
			if(empty($iv) || strlen($iv) !== $ivSize)
				return new CryptoError(CryptoError::IV_CREATION_FAILED, CryptoError::ERR, "Could not create an initialization vector of size '$ivSize'.");

			// Padding preparation (PKCS7)
			$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$padding = $blockSize - strlen($plaintext) % $blockSize;
			$plaintext .= str_repeat(chr($padding), $padding);

			// Actual encryption
			$encryptedData = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $hashedPassword, $plaintext, MCRYPT_MODE_CBC, $iv);
			if(empty($encryptedData))
				return new CryptoError(CryptoError::AES_ENCRYPTION_FAILED, CryptoError::ERR);

			// Make encrypted content readable
			$encryptedBase64Data = base64_encode($iv.$encryptedData);

			return $encryptedBase64Data;
		}

		/**
		 * @name aes256Decrypt
		 * @desc
		 */
		public static function aesDecrypt(string $encryptedBase64Data, string $password, int $keySize = self::KEYSIZE_AES256)
		{
			// Key preparation
			if($keySize !== self::KEYSIZE_AES128 && $keySize !== self::KEYSIZE_AES192 && $keySize !== self::KEYSIZE_AES256)
				return new CryptoError(CryptoError::AES_UNSUPPORTED_KEYSIZE, CryptoError::ERR, "The key size '$keySize' is not supported by AES.");
			$hashedPassword = substr(hash("sha256", $password, true), 0, $keySize);
			if(empty($hashedPassword) || $keySize !== strlen($hashedPassword))
				return new CryptoError(CryptoError::PASSWORD_PREPARATION_FAILED, CryptoError::ERR, "Could not create hash of length '$keySize' of password.");

			// Initialization vector extraction
			$ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$iv = substr(base64_decode($encryptedBase64Data), 0, $ivSize);
			if(empty($iv) || strlen($iv) !== $ivSize)
				return new CryptoError(CryptoError::IV_CREATION_FAILED, CryptoError::ERR, "Could not extract the initialization vector of size '$ivSize'.");

			// Make readable encrypted content binary
			$encryptedData = substr(base64_decode($encryptedBase64Data), $ivSize);

			// Actual dencryption
			$decryptedData = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $hashedPassword, $encryptedData, MCRYPT_MODE_CBC, $iv), "\0"); // also remove padded nulls

			// Padding postprocessing (PKCS7)
			$paddedBytes = ord(substr($decryptedData, -1));
			$plaintext = substr($decryptedData, 0, strlen($decryptedData)-$paddedBytes);

			return $plaintext;
		}

		/**
		 * @name createRandomString
		 * @desc
		 */
		public static function createRandomString(int $sidLen, string $alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
		{
			$string = '';
			for($loop = 0; $loop < $sidLen; $loop++)
			{
				$string .= str_shuffle($alphabet)[0];
			}

			return $string;
		}

		/**
		 * @name getXmlInZip
		 * @desc
		 */
		public static function getXmlInZip(string $zipFilePath, string $filePathInZip)
		{
			$zip = new \ZipArchive();
			$zipResult = $zip->open($zipFilePath, \ZipArchive::CHECKCONS);
			if( $zipResult === true )
			{
				$content = $zip->getFromName($filePathInZip);
				$zip->close();
				if($content !== false)
					return (($xmlObject=simplexml_load_string($content)) !== false) ? $xmlObject : new SysError(SysError::MALFORMED_XML);
				else
					return new SysError(SysError::INVALID_FILE_CONTENT);
			}
			else
			{
				switch($zipResult)
				{
					case \ZipArchive::ER_INCONS:
					case \ZipArchive::ER_NOZIP:
					case \ZipArchive::ER_READ:
						return new SysError(SysError::MALFORMED_ZIPFILE);

					case \ZipArchive::ER_INVAL:
					case \ZipArchive::ER_NOENT:
					case \ZipArchive::ER_OPEN:
						// TODO: CHECK IF INVAL MEANS, THAT FILE HAS MOVED!
						if(is_file($zipFilePath))
							return new FsError(FsError::ACCESS_DENIED);
						else
							return new FsError(FsError::FILE_NOT_FOUND);

					default:
						return new SysError(SysError::UNEXPECTED_ZIP_ERROR);
				}
			}
		}

		/**
		 * @name isXmlObject
		 * @desc
		 */
		public static function isXmlObject($object)
		{
			return self::isClassRelative($object, 'SimpleXMLElement');
		}

		/**
		 * @name isJsonObject
		 * @desc
		 */
		public static function isJsonObject($object)
		{
			return is_a($object, "stdClass");
		}

		/**
		 * @name isClassRelative
		 * @desc
		 */
		public static function isClassRelative($object, string $className) : bool
		{
			return (is_a($object, $className) || is_subclass_of($object, $className));
		}

		/**
		 * @name isMultiDimArray
		 * @desc
		 */
		public static function isMultiDimArray(array &$array)
		{
			foreach($array as $value)
			{
				if(is_array($value) === true)
					return true;
			}
			return false;
		}

		/**
		 * @name getCallingMethodName
		 * @desc
		 */
		public static function getCallingMethodName()
		{
			$trace = array();
			if(PHP_MAJOR_VERSION > 5 || (PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION >= 4))
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
			else
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

			if(isset($trace[1]['function']))
				return $trace[1]['function'];
			else
				return "";
		}

		/**
		 * @name getCallingClassName
		 * @desc
		 */
		public static function getCallingClassName()
		{
			$trace = array();
			if(PHP_MAJOR_VERSION > 5 || (PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION >= 4))
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
			else if(PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION == 3 && PHP_RELEASE_VERSION >= 6)
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
			else
				$trace = debug_backtrace();

			if(isset($trace[2]['class']))
				return $trace[2]['class'];
			else
				return "";
		}

		/**
		 * @name validateIP
		 * @desc
		 */
		public static function validateIp(string $ipAddress)
		{
			return filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4|FILTER_FLAG_IPV6) !== false;
		}

		/**
		 * @name mapErrorOutputToGlobalLogger
		 * @desc
		 */
		public static function mapErrorOutputToGlobalLogger()
		{
			if(isset($_SERVER["TT"]))
			{
				\Zend\Log\Logger::registerErrorHandler($_SERVER["TT"]->logger);
				\Zend\Log\Logger::registerExceptionHandler($_SERVER["TT"]->logger);
			}

			static $shutdownFunctionAlreadyRegistered = false;
			if(!$shutdownFunctionAlreadyRegistered)
			{
				$shutdownFunctionAlreadyRegistered = true;
				register_shutdown_function(function()
				{
					if(($e = error_get_last()) !== null)
					{
						if(isset($_SERVER["TT"]))
						{
							$_SERVER["TT"]->logger->err($e['message'] . " in " . $e['file'] . ' line ' . $e['line']);
							$_SERVER["TT"]->logger->__destruct();
						}
					}
				});
			}
		}

		/**
		 * @name unmapErrorOutputFromGlobalLogger
		 * @desc
		 */
		public static function unmapErrorOutputFromGlobalLogger()
		{
			\Zend\Log\Logger::unregisterErrorHandler();
			\Zend\Log\Logger::unregisterExceptionHandler();
			// You cannot unregister a shutdown function
		}


		/**
		 * @name applyToAllArrayElements
		 * A function which applies a given call back for all elements and sub elements
		 * also from sub arrays. This function helps you to alter multidimensional
		 * arrays.
		 *
		 * @param Array &$myArray    - The array to get altered/iterated
		 * @param Callback $callback - The function to execute for each element (gets 1 parameter - the $element by reference)
		 */
		public static function applyToAllArrayElements(array &$myArray, $callback)
		{
			foreach(array_keys($myArray) as $key)
			{
				$callback($myArray[$key]);
				if(is_array($myArray[$key]))
					self::applyToAllArrayElements($myArray[$key], $callback);
			}
		}

		/**
		 * @name arrayToXml
		 * A function for converting an array to a XML document.
		 * Pass in a multi dimensional array and this recrusively loops through and builds up an XML document.
		 *
		 * @todo Momentarily only the last key is converted in '****'. This means adding 'password' to the blacklist all
		 *       *.passwords are being converted to '****'. It would be better if the config is being copied at the beginning
		 *       and all blacklist keys get converted to '****'.
		 *
		 * @param array $data
		 * @param string $rootNodeName - what you want the root node to be - defaultsto data.
		 * @param SimpleXMLElement $xml - should only be used recursively
		 * @param array $blacklist with keys - this keys should'n be add to the xml object
		 * @return string XML
		 */
		public static function arrayToXml(array $data, string $rootNodeName = 'data', bool $includeDeclaration = true, array $blacklist = [], $xml = null) : string
		{
			// turn off compatibility mode as simple xml throws a wobbly if you don't.
			if (ini_get('zend.ze1_compatibility_mode') == 1)
				ini_set ('zend.ze1_compatibility_mode', 0);

			$firstRun = (bool)($xml==null);
			if($firstRun)
				$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");

			// loop through the data passed in.
			foreach($data as $key => $value)
			{
				// no numeric keys in our xml please!
				if (is_numeric($key))
					$key = "unknownNode_". (string) $key; // make string key...

				// replace anything not alpha numeric
				$key = preg_replace('/[^a-z]/i', '', $key);

				// if there is another array found recrusively call this function
				if (is_array($value) || is_object($value))
				{
					$node = $xml->addChild($key);
					// recrusive call.
					self::arrayToXml($value, $rootNodeName, false, $blacklist, $node);
				}
				else
				{
					if( empty($blacklist) || !in_array($key, $blacklist))
					{
						// add single node.
						$value = htmlentities($value);
						$xml->addChild($key,$value);
					}
					else
					{
						$xml->addChild($key, "****");
					}
				}
			}

			$xmlString = $xml->asXML();
			if($firstRun && $includeDeclaration == false)
				return substr($xmlString, strpos($xmlString, '?>')+2);
			else
				return $xmlString;
		}

		/**
		 * @name xmlToArray
		 * A function for converting a simple XML-Element to an array.
		 * Pass in SimpleXMLObject and encodes it to json and produces an array from this json document.
		 *
		 * @param SimpleXMLElement $xmlObject    - A SimpleXMLElement to be converted to an array
		 */
		public static function xmlToArray(\SimpleXMLElement $xmlObject)
		{
			$array = json_decode(json_encode(new \SimpleXMLElement($xmlObject->asXML(), LIBXML_NOCDATA)), true);
			self::applyToAllArrayElements($array, function(&$element)
			{
				if(is_array($element) && count($element) === 0)
					$element = "";
			});
			return $array;
		}


		/**
		 * @name createFileWriter
		 * A function for creating a zend log writer.
		 * If a mail sender and a mail receiver is specified, mails are sent instead of logging it to a file.
		 *
		 * @param array $data
		 * @param string $rootNodeName - what you want the root node to be - defaultsto data.
		 * @param SimpleXMLElement $xml - should only be used recursively
		 * @return string XML
		 */
		public static function createFileWriter(int $zendLogVerbosity, string $filePath)
		{
			self::createPathIfNotExists(dirname($filePath));
			$zendLogVerbosity = (int)$zendLogVerbosity;
			$writer = new \Zend\Log\Writer\Stream($filePath);
			$writer->setFormatter(new TripleTowerLogFormatter());
			$writer->addFilter($zendLogVerbosity);
			return $writer;
		}

		/**
		 * @name createMailWriter
		 * A function for creating a zend log mailer.
		 * If an event occures and has at least the specified zendLogVerbosity, an email is generated and sent to the mailReceiver.
		 *
		 * @param int $zendLogVerbosity         - Zend_Log verbosity e.g. Zend_Log::DEBUG
		 * @param string $subject               - the mail subject in your email
		 * @param MailAdress $mailSender        - from whom the mail is comming
		 * @param MailAdress $mailReceiverArray - who is to receive the log mails
		 * @return \Zend\Log\Writer\Mail         - the mail writer
		 */
		public static function createMailWriter(int $zendLogVerbosity, string $subject, string $mailSender, $mailReceiverArray)
		{
			$zendLogVerbosity = (int)$zendLogVerbosity;
			$mail = new \Zend\Mail\Message();
			$mail->setFrom($mailSender);

			foreach(is_array($mailReceiverArray) ? $mailReceiverArray : [$mailReceiverArray] as $mailReceiver)
			{
				if($mailReceiver !== null && trim($mailReceiver) !== "")
				{
					$mailReceiver = filter_var(trim($mailReceiver), FILTER_SANITIZE_EMAIL);
					if(filter_var($mailReceiver, FILTER_VALIDATE_EMAIL) !== false)
						$mail->addTo($mailReceiver);
					else
						throw new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::CRIT, "Receiver mail adress '$mailReceiver' is no valid email adress");
				}
			}

			$writer = new \Zend\Log\Writer\Mail($mail);
			$writer->setSubjectPrependText($subject);
			$writer->addFilter($zendLogVerbosity);
			$formatter = new \Zend\Log\Formatter\Simple('%timestamp%-Z %priorityName%: %message%');
			$formatter->setDateTimeFormat("d.m.Y H:i:s");
			$writer->setFormatter($formatter);
			return $writer;
		}

		/**
		 * @name debug_backtrace
		 * A function similar to PHP's debug_backtrace(), but it handles
		 * backward compatibility itself.
		 *
		 * @param int $options     - As of 5.3.6, this parameter is a bitmask for the following options:
		 *                              DEBUG_BACKTRACE_PROVIDE_OBJECT: Whether or not to populate the "object" index.
		 *                              DEBUG_BACKTRACE_IGNORE_ARGS: Whether or not to omit the "args" index, and thus all the function/method arguments, to save memory.
		 *                              Before 5.3.6, the only values recognized are TRUE or FALSE, which are the same as setting or not setting the DEBUG_BACKTRACE_PROVIDE_OBJECT option respectively.
		 * @param string $limit       - As of 5.4.0, this parameter can be used to limit the number of stack frames returned. By default (limit=0) it returns all stack frames.
		 * @return Array $trace       - The resulting backtrace
		 */
		public static function debug_backtrace(int $options = DEBUG_BACKTRACE_PROVIDE_OBJECT, int $limit = 0)
		{
			$trace = array();
			if(PHP_MAJOR_VERSION > 5 || (PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION >= 4))
				$trace = debug_backtrace($options, $limit);
			else if(PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION == 3 && PHP_RELEASE_VERSION >= 6)
				$trace = debug_backtrace($options);
			else
				$trace = debug_backtrace();
			array_shift($trace);
			return $trace;
		}

		/**
		 * @name addIncludePath
		 * A function to add an include path if path exists.
		 *
		 * @param string $path     - path to include folder
		 */
		public static function addIncludeFolder(string $path)
		{
			$path = self::normalizeFolderPath(realpath($path));
			if(is_dir($path))
			{
				$paths = explode(PATH_SEPARATOR, get_include_path());
				if(array_search($path, $paths) === false)
				{
					$paths[] = $path;
					set_include_path(implode(PATH_SEPARATOR, $paths));
				}
			}
		}

		/**
		 * @name requireIncludeFolder
		 * A function to add an include path which also checks if path exists.
		 * If not, an FsError is returned.
		 *
		 * @param string $path     - path to include folder
		 */
		public static function requireIncludeFolder(string $path)
		{
			$path = self::normalizeFolderPath(realpath($path));
			if(!is_dir($path))
				return new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Folder '$path' could not be found.");

			self::addIncludeFolder($path);
			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name removeIncludePath
		 * A function to remove a certain include path.
		 *
		 * @param string $path     - path to remove from include path
		 */
		public static function removeIncludeFolder(string $path)
		{
			$path = self::normalizeFolderPath(realpath($path));
			$paths = explode(PATH_SEPARATOR, get_include_path());
			if (($foundKey = array_search($path, $paths)) !== false)
				unset($paths[$foundKey]);

			if(!count($paths))
			{
				set_include_path('.');
				return new SysError(SysError::EMPTY_INCLUDE_PATH, SysError::Err, "The path seems to be empty by now");
			}

			set_include_path(implode(PATH_SEPARATOR, $paths));
			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name getFullRequestUrl
		 * A function which returns the full url which has been requested by the
		 * user's browser.
		 * E.g. "https://www.myServer.de/myRequestedUri/file.php
		 *
		 */
		public static function getFullRequestUrl()
		{
			if(!isset($_SERVER["HTTPS"]) && !isset($_SERVER["HTTP_HOST"]) && !isset($_SERVER["REQUEST_URI"]))
					return "";
			else
				return 'http' . (self::isHttps() ? 's' : null) . '://' . (isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : null) . (isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : null);
		}

		/**
		 * @name getFqdn
		 * A function which returns the full qualified domain name of the local machine.
		 *
		 * E.g. "www.example.com"
		 *
		 */
		public static function getFqdn(string $hostname = "")
		{
			// gethostname() does not return fqdn when used on CLI SAPI
			if( empty($hostname) && ($hostname = gethostname()) === false )
				return new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "Could not get hostname for local machine.");

			$ip = gethostbyname($hostname);
			if( $ip === $hostname )
				return new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "Could not get ip address for host '$hostname'.");

			$fqdn = gethostbyaddr($ip);
			if( $fqdn === false || $fqdn === $ip )
				return new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "Could not get FQDN for ip address '$ip'.");

			return strtolower($fqdn);
		}

		/**
		 * @name getServerUrl
		 * A function which returns the url of the server including the used protocol.
		 * There is no trailing slash used!
		 *
		 * E.g. "https://www.myServer.de
		 *
		 */
		public static function getServerUrl()
		{
			if(!isset($_SERVER["HTTPS"]) && !isset($_SERVER["HTTP_HOST"]))
				return "";
			else
				return 'http' . (self::isHttps() ? 's' : null) . '://' . (isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : null);
		}

		public static function getTowerUrl()
		{
			return self::getServerUrl() . "/" . $_SERVER["TT"]->tower->getTowerInstance();
		}

		/**
		 * @name forwardToPrimaryIfNecessary
		 * A function which forwards a user to a server specified by $primaryTower
		 * if you're currently on a secondary server. A secondary means every server
		 * not being the same as $primaryTower.
		 *
		 * @param string $primaryTower - A hostname specifying the primary BootTower
		 */
		public static function forwardToPrimaryIfNecessary(string $primaryTower)
		{
			$primaryTowerHost = self::extractHost($primaryTower);
			if($primaryTowerHost !== $_SERVER["HTTP_HOST"])
			{
				if(file_get_contents('http' . (self::isHttps() ? 's' : null) . "://$primaryTowerHost") !== false)
				{
					$primaryTowerUrl = 'http' . (self::isHttps() ? 's' : null) . "://$primaryTowerHost" . (isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : null);
					header("Location: $primaryTowerUrl");
					exit();
				}
			}
		}

		/**
		 * @name removeNewLineFeedsAndTabs
		 * A function which removes all "new lines", "carriage returns" and "tabs"
		 *
		 * @param string $string    - The string which gets stripped
		 * @return string           - The stripped string
		 */
		public static function removeNewLineFeedsAndTabs(string $string)
		{
			return str_replace(array("\n", "\r", "\t"), '', $string);
		}

		/**
		 * @name downloadCertificate
		 * A function which downloads a certificate file from the specified url. If
		 * the parameter url does not start with 'https://' it gets prepended.
		 *
		 * @param string $url                 - The url from which the certificate gets downloaded
		 * @return array|TripleTowerError    - The certificate arrays or TripleTowerError
		 */
		public static function downloadCertificate(string $url) : array|TripleTowerError
		{
			if(substr($url, 0, 8) !== "https://")
				$url = "https://$url";

			$streamContext = stream_context_create(array("ssl" => array("capture_peer_cert" => true)));
			if(($readHandle=fopen($url, "rb", false, $streamContext)) !== false)
			{
				$contextParams = stream_context_get_params($readHandle);
				fclose($readHandle);
				if(isset($contextParams["options"]) && isset($contextParams["options"]["ssl"]) && isset($contextParams["options"]["ssl"]["peer_certificate"]) && !empty($contextParams["options"]["ssl"]["peer_certificate"]))
					return $contextParams["options"]["ssl"]["peer_certificate"];
				else
					return new HwError(HwError::NETWORK_CONNECTION_BROKEN, HwError::ERR, "Could open connection to $url, but could not extract certificate.");
			}
			else
			{
				return new HwError(HwError::NETWORK_CONNECTION_BROKEN, HwError::ERR, "Could not download certificate from $url.");
			}
		}

		/**
		 * @name downloadPublicKey
		 * A function which downloads a certificate and extracts the public key
		 * contained within the file from the specified url. If the parameter url does
		 * not start with 'https://' it gets prepended.
		 *
		 * @param string $url                    - The url from which the public key gets downloaded
		 * @return OpenSSLAsymmetricKey|false  - The public key or HwError
		 */
		public static function downloadPublicKey(string $url) : OpenSSLAsymmetricKey|false
		{
			$certificate = self::downloadCertificate($url);
			if(!TripleTowerError::isError($certificate))
				return openssl_get_publickey($certificate);
			else
				return $certificate;
		}

		/**
		 * @name openSslEncrypt
		 * A function which first signs data with provided public key and afterwards
		 * encrypts it with provided private key. The function also handles the issue
		 * of very long strings. Strings get always padded to fit into key lenght.
		 *
		 * @param string $clearText           - The text to sign and encrypt
		 * @param string $senderPrivateKey   - A private key
		 * @param string $receiverPublicKey  - A public key
		 * @param int $keyLengh (optional)   - Defaults to 2048 bit keys.
		 * @return string                      - The signed and encrypted text
		 */
		public static function openSslEncrypt(string $clearText, string $senderPrivateKey, string $receiverPublicKey, int $keyLengh = 2048)
		{
			$chunkLengh = (int)(floor($keyLengh/8) - 11); // -11 for padding issues in RSA when padding used (default)
			$signed = "";
			while(strlen((string)$clearText))
			{
				$chunkToSign = substr($clearText,0, $chunkLengh);
				$clearText   = substr($clearText, $chunkLengh);
				$signedChunk = "";
				if(openssl_private_encrypt($chunkToSign, $signedChunk, $senderPrivateKey) === false)
					return new CryptoError(CryptoError::OPENSSL_ENCRYPTION_FAILED, CryptoError::ERR);
				$signed .= $signedChunk;
			}

			$crypted = "";
			while(strlen((string)$signed))
			{
				$chunkToEncrypt = substr($signed,0, $chunkLengh);
				$signed         = substr($signed, $chunkLengh);
				$cryptedChunk   = "";
				if(openssl_public_encrypt($chunkToEncrypt, $cryptedChunk, $receiverPublicKey) === false)
					return new CryptoError(CryptoError::OPENSSL_ENCRYPTION_FAILED, CryptoError::ERR);
				$crypted .= $cryptedChunk;
			}
			return $crypted;
		}

		/**
		 * @name openSslDecrypt
		 * A function which first decrypts and then checks the signing of according
		 * data with provided public key and afterwards and private key. The function
		 * also handles the issue of very long strings. Strings provided have to be
		 * padded.
		 *
		 * @param string $cryptedText          - The text to decrypt and verify
		 * @param string $senderPublicKey      - A public key
		 * @param string $receiverPrivateKey   - A private key
		 * @param int $keyLengh (optional)     - Defaults to 2048 bit keys.
		 * @return string|TripleTowerError     - The clear text or an error
		 */
		public static function openSslDecrypt(string $cryptedText, string $senderPublicKey, string $receiverPrivateKey, int $keyLengh = 2048) : string|TripleTowerError
		{
			$chunkLengh = (int)(floor($keyLengh/8)); // No -11 needed for decryption
			$signed = "";
			while(strlen($cryptedText))
			{
				$chunkToDecrypt = substr($cryptedText,0, $chunkLengh);
				$cryptedText    = substr($cryptedText, $chunkLengh);
				$signedChunk    = "";
				if(openssl_private_decrypt($chunkToDecrypt, $signedChunk, $receiverPrivateKey) === false)
				{
					return new CryptoError(CryptoError::OPENSSL_DECRYPTION_FAILED, CryptoError::ERR);
				}
				$signed .= $signedChunk;
			}

			$clearText = "";
			while(strlen($signed))
			{
				$chunkToUnsign  = substr($signed,0, $chunkLengh);
				$signed         = substr($signed, $chunkLengh);
				$cryptedChunk   = "";
				if(openssl_public_decrypt($chunkToUnsign, $cryptedChunk, $senderPublicKey) === false)
				{
					return new CryptoError(CryptoError::OPENSSL_DECRYPTION_FAILED, CryptoError::ERR);
				}
				$clearText .= $cryptedChunk;
			}
			return $clearText;
		}

		/**
		 * @name guid
		 * A function which creates a Microsoft valid GUID.
		 *
		 * @return string    - The newly created GUID
		 */
		public static function guid()
		{
			if(function_exists('com_create_guid') === true)
				return trim(com_create_guid(), '{}');
			else
				return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
		}

		/**
		 * @name addHttps
		 * A function prepending 'https://' to the given url. It also removes
		 * 'http://' if present.
		 *
		 * @param string $url  - The url which is should be https prepended
		 * @return string      - The https url
		 */
		public static function addHttps(string $url)
		{
			$url = trim($url);
			if(substr($url, 0, 8) === 'https://')
				return $url;
			else if(substr($url, 0, 7) === 'http://')
				return 'https://' . substr($url, 7);
			else
				return 'https://' . $url;
		}

		/**
		 * @name addHttpsAndSlash
		 * A function prepending 'https://' to the given url and also adding a
		 * trailing '/'. It also removes 'http://' if present.
		 *
		 * @param string $url  - The url-string which is should be https prepended
		 * @return string      - The https url with trailing slash
		 */
		public static function addHttpsAndSlash(string $url)
		{
			return self::addLastCharIfNot(self::addHttps($url), '/');
		}

		/**
		 * @name removeProtocol
		 * A function which removes the protocol from given url.
		 *
		 * @param string $url  - The url-string which is should be stripped
		 * @return string      - The non-protocol url-string
		 */
		public static function removeProtocol(string $url)
		{
			$protocolIdentifier = '://';
			$url = trim($url);
			$protocolPosition = strpos($url, $protocolIdentifier);
			if($protocolPosition === false)
				return $url; // no protocol found
			else
				return substr($url, $protocolPosition+strlen($protocolIdentifier));
		}

		/**
		 * @name removeProtocolAndSlash
		 * A function which removes the protocol from given url and also removes the
		 * trailing slash if there is one.
		 *
		 * @param string $url  - The url-string which is should be stripped
		 * @return string      - The non-protocol url-string
		 */
		public static function removeProtocolAndSlash(string $url)
		{
			return self::removeLastChar(self::removeProtocol($url), '/');
		}

		/**
		 * @name replaceUrlValue
		 * A function which replaces a value by another value determined by key in
		 * an url-string. If $key is not found, the whole url is returned.
		 *
		 * @param string $url         - The url-string which has the value to be replaced
		 * @param string $urlKey      - The url-key which determines the value being replaced
		 * @param string $replacement - The found value is replaced by this new value
		 * @return string     - The non-protocol url-string
		 */
		public static function replaceUrlValue(string $url, string $urlKey, string $replacement)
		{
			$urlKey = trim((string)$urlKey);
			if(empty($urlKey))
				return $url;

			$urlKey .= '=';
			$start = (strpos($url, '&'.$urlKey) !== false ? strpos($url, '&'.$urlKey) : strpos($url, '?'.$urlKey));
			if($start !== false)
			{
				$keyStart = $start + strlen('&'.$urlKey);
				$keyEnd = strpos($url, '&', $keyStart);
				if($keyEnd === false)
					$keyEnd = strlen($url);

				$url = substr($url, 0, $keyStart) . $replacement . substr($url, $keyEnd);
			}
			return $url;
		}

		/**
		 * @name extractHost
		 * A function which extracts the hostname of a given url.
		 * E.g. https://www.example.com/exampledir/index.php?hi=5
		 * This url returns: www.example.com
		 *
		 * @param string $url   - The url-string which's host gets extracted
		 * @param bool strict   - If the url should be parsed strictly according to RFC 3986
		 * @return string       - The hostname (domainname) of given url
		 */
		public static function extractHost(string $url, bool $strict=false)
		{
			$urlArray = parse_url(trim($url));
			if($strict)
				return trim(isset($urlArray["host"]) ? $urlArray["host"] : "");

			if(isset($urlArray["host"]) && !empty(trim($urlArray["host"])))
			{
				return trim($urlArray["host"]);
			}
			else
			{
				$pathParts = explode('/', $urlArray["path"], 2); // first part is the host name
				return array_shift($pathParts);
			}
		}

		public static function extractTower(string $url, bool $strict=false)
		{
			$urlArray = parse_url(trim($url));

			if(isset($urlArray["path"]) && !empty($urlArray["path"]))
			{
				$pathParts = explode('/', $urlArray["path"], 2);

				if(isset($urlArray["host"]))
				{
					array_shift($pathParts);
					return array_shift($pathParts);
				}

				if(!$strict)
					array_shift($pathParts);

				return array_shift($pathParts);
			}
			else
			{
				return "";
			}
		}

		/**
		 * @name getMimeType
		 * A function which returns the mime type of a given file using FileInfo.
		 *
		 * @param string $filePath - The path to the file which gets investigated
		 * @return string          - The according mime type e.g. text/html
		 */
		public static function getMimeType(string $filePath)
		{
			// There is a mime magic detection bug in php, therefore some mimetypes get
			// hard coded for the follwing file extensions

			// Mime.magic database bugs - finfo(FILEINFO_MIME) - mime_content_type($filePath)
			// $mimeType = mime_content_type($filePath, '/tripletower-stack/tripletower/public/mime/magic');
			// https://bugs.php.net/bug.php?id=51732
			// https://bugs.php.net/bug.php?id=53035
			// https://bugs.php.net/bug.php?id=56999
			// http://php.net/manual/de/function.finfo-open.php#107383

			$mimeType = "text/plain";
			$extension = pathinfo($filePath, PATHINFO_EXTENSION);
			switch($extension)
			{
				case 'css':
					$mimeType = 'text/css';
				break;

				case 'js':
					$mimeType = 'application/javascript';
				break;

				case 'svg':
					$mimeType = 'image/svg+xml';
				break;

				case 'crt':
					$mimeType = 'application/x-x509-ca-cert';
				break;

				case 'key':
					$mimeType = 'application/x-pkcs8';
				break;

				case 'pfx':
					$mimeType = 'application/x-pkcs12';
				break;

				default:
					$finfo = finfo_open(FILEINFO_MIME_TYPE);
					$mimeType = finfo_file($finfo, $filePath);
					finfo_close($finfo);
				break;
			}

			return $mimeType;
		}

		/**
		 * @name getImageResource
		 * A function which opens an image file. You have to close it yourself via
		 * imagedestroy().
		 *
		 * @param string $filePath - The path to the image file which gets opened
		 * @return GdImage          - The image file resource or a TripleTowerError
		 */
		public static function getImageResource(string $filePath) : GdImage|TripleTowerError
		{
			$gdImage = null;
			switch(exif_imagetype($filePath))
			{
				case IMAGETYPE_GIF:
					$gdImage = imagecreatefromgif($filePath);
					break;
				case IMAGETYPE_JPEG:
					$gdImage = imagecreatefromjpeg($filePath);
					break;
				case IMAGETYPE_PNG:
					$gdImage = imagecreatefrompng($filePath);
					break;
				case IMAGETYPE_WBMP:
					$gdImage = imagecreatefromwbmp($filePath);
					break;

				case IMAGETYPE_XBM:
				case IMAGETYPE_BMP:
				case IMAGETYPE_ICO:
				case IMAGETYPE_IFF:
				case IMAGETYPE_JB2:
				case IMAGETYPE_JP2:
				case IMAGETYPE_JPC:
				case IMAGETYPE_JPEG2000:
				case IMAGETYPE_JPX:
				case IMAGETYPE_PSD:
				case IMAGETYPE_SWC:
				case IMAGETYPE_SWF:
				case IMAGETYPE_TIFF_II:
				case IMAGETYPE_TIFF_MM:
				default:
					return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, "The file '$filePath' is no supported image type.");
			}
			if( $gdImage !== false )
				return $gdImage;
			else
				return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, "The file '$filePath' is no supported image type.");
		}

		/**
		 * @name createThumbNailFile
		 * A function which creates a thumb nail file of a given image file.
		 *
		 * @param string $imagePath     - The path to the source file
		 * @param string $thumbNailPath - The path to the file to be created
		 * @param string $maxWidth      - The maximum width for the thumbnail
		 * @param string $maxHeight     - The maximum height for the thumbnail
		 * @return string               - TripleTowerError indicating success or failure
		 */
		public static function createThumbNailFile(string $imagePath, string $thumbNailPath, int $maxWidth=192, int $maxHeight=192)
		{
			if(!is_file($imagePath))
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The image file '$imagePath' was not found.");

			if(!is_dir(dirname($thumbNailPath)))
				return new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "The folder " . dirname($thumbNailPath) . " does not exist.");

			if(empty($maxWidth))
				return new SysError(SysError::INVALID_PARAM, SysError::ERR, "The variable 'maxWidth' has to be an positive integer value.");

			if(empty($maxHeight))
				return new SysError(SysError::INVALID_PARAM, SysError::ERR, "The variable 'maxHeight' has to be an positive integer value.");

			$imageSizes = getimagesize($imagePath);
			if(empty($imageSizes))
				return new SysError(SysError::INVALID_FILE_CONTENT, SysError::ERR, "Could not get image information from file '$imagePath'.");

			$imageWidth      = $imageSizes[0];
			$imageHeight     = $imageSizes[1];
			$thumbNailWidth  = $maxWidth;
			$thumbNailHeight = $maxHeight;

			// Does scaled height fit into rect if stretching along x-axis?
			$scaledY = (int)(($maxWidth/$imageWidth)*$imageHeight);
			if($scaledY <= $maxHeight)
				$thumbNailHeight = $scaledY;

			// Does scaled height fit into rect if stretching along x-axis?
			$scaledX = (int)(($maxHeight/$imageHeight)*$imageWidth);
			if($scaledX <= $maxWidth)
				$thumbNailWidth  = $scaledX;

			$gdImage = self::getImageResource($imagePath);
			if(TripleTowerError::isError($imageGd))
				return $imageGd;

			$thumbnailGd = imagecreatetruecolor($thumbNailWidth, $thumbNailHeight);
			imagecopyresampled($thumbnailGd, $imageGd, 0, 0, 0, 0, $thumbNailWidth, $thumbNailHeight, $imageWidth, $imageHeight);
			imagejpeg($thumbnailGd, $thumbNailPath, 90);
			imagedestroy($imageGd);
			imagedestroy($thumbnailGd);
			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name getClassesFromFile
		 * A function which scanns the file for PHP classes and returns them.
		 *
		 * @param string $filePath    - The path to the source file
		 * @return array              - The found classes within the given file
		 */
		public static function getClassesFromFile(string $filePath)
		{
			$filePath = self::normalizeFilePath($filePath);
			if(is_file($filePath))
				return self::getClassesFromSource(file_get_contents($filePath));
			else
				return [];
		}

		/**
		 * @name getClassesFromSource
		 * A function which scanns the file for PHP classes and returns them.
		 *
		 * @param string $sourceCode  - The source code to be scanned
		 * @return array              - The found classes within the given source code
		 */
		public static function getClassesFromSource(string $sourceCode)
		{
			$classes = array();
			$tokens = token_get_all($sourceCode);
			for($loop = 2; $loop < count($tokens); $loop++)
			{
				if($tokens[$loop-2][0] === T_CLASS && $tokens[$loop-1][0] === T_WHITESPACE && $tokens[$loop][0] === T_STRING)
					$classes[] = $tokens[$loop][1];
			}
			return $classes;
		}

		/**
		 * @name openPipeProcess
		 * This function is used for enabling PHP to access external programs in a function-like manner. The
		 * command has to be capable of reading from stdin and write to stdout. If therefore are
		 * additional command line switches necessary, these have to be given in the cmd parameter.
		 *
		 * @param string $command          - The stdin/stdout capable command (with switches/params)
		 * @param string $stdInString      - The string which gets piped into the specified program
		 * @return string/TripleTowerError - If successfull the string from stdout of the specified program is returned. An TripleTowerError otherwise.
		 */
		public static function openPipeProcess(string $command, string $stdInString) : string|TripleTowerError
		{
			$descriptors =
			[
				0 => ["pipe", "r"], // STDIN  is a pipe that the child will read from
				1 => ["pipe", "w"], // STDOUT is a pipe that the child will write to
				2 => ["pipe", "a"]  // STDERR is a pipe that the child will write to if errors occure
			];

			$pipes = [];
			if( is_resource($process = proc_open($command, $descriptors, $pipes, null, null, ['bypass_shell' => true])) )
			{
				fwrite($pipes[0], $stdInString);
				fclose($pipes[0]);

				$stdOutString = stream_get_contents($pipes[1]);
				fclose($pipes[1]);

				$stdErrorString = stream_get_contents($pipes[2]);
				fclose($pipes[2]);

				$returnCode = proc_close($process);
				if(($returnCode === 0 || $returnCode === 8) && empty($stdErrorString))
					return $stdOutString;
				else
					return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Return code: $returnCode - '$stdErrorString'");
			}
			else
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "The subprocess could not be started. See PHP-Logs for more infos.");
			}
		}

		/**
		 * @name uglifyJs
		 * A function which uglifies javascript code given as a string. Nodejs is required for
		 * this function to work properly. For windows the node.exe of OwnSDK is used, for *nix based
		 * systems node and uglify-js has to be installed via your favourite package manager.
		 * Watch out! The output code does not work on IE8 and below! But therefore the uglified
		 * code is more minified. Screw IE8!
		 *
		 * @param string $jsClear     - JavaScript code which shall be uglified
		 * @return string/TripleTowerError    - If successfull the uglified JavaScript code. An TripleTowerError otherwise.
		 */
		public static function uglifyJs(string $jsClear)
		{
			$uglifyJsCmd = "";
			if(OsNames::isWindows())
			{
				$nodeBasePath = dirname(dirname(__DIR__)).'/tools/windows/nodejs/';
				$nodeBinary   = self::normalizeFilePath("$nodeBasePath/node.exe");
				if(!is_file($nodeBinary))
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file '$nodeBinary' does not exist.");

				$uglifyJsBinary = self::normalizeFilePath("$nodeBasePath/node_modules/uglify-js/bin/uglifyjs");
				if(!is_file($uglifyJsBinary))
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file '$uglifyJsBinary' does not exist.");

				$uglifyJsCmd = '"' . $nodeBinary . '" "' . $uglifyJsBinary . '" --compress --mangle --screw-ie8';
			}
			else
			{
				if(!empty(shell_exec("which uglifyjs")))
					$uglifyJsCmd = 'uglifyjs --compress --mangle --screw-ie8';
				else
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file 'uglifyjs' does not exist.");
			}
			return self::openPipeProcess($uglifyJsCmd, $jsClear);
		}

		/**
		 * @name uglifyCss
		 * A function which uglifies css code. Nodejs is required for this function to work properly.
		 * For windows the node.exe of OwnSDK is used, for *nix based systems node and uglifycss has
		 * to be installed via your favourite package manager.
		 *
		 * @param string $cssClear    - CSS classes as string which shall be uglified
		 * @return string/TripleTowerError    - If successfull the uglified CSS classes. An TripleTowerError otherwise.
		 */
		public static function uglifyCss(string $cssClear)
		{
			$uglifyCssCmd = "";
			if(OsNames::isWindows())
			{
				$nodeBasePath = dirname(dirname(__DIR__)).'/tools/windows/nodejs/';
				$nodeBinary   = self::normalizeFilePath("$nodeBasePath/node.exe");
				if(!is_file($nodeBinary))
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file '$nodeBinary' does not exist.");

				$uglifyCssBinary = self::normalizeFilePath("$nodeBasePath/node_modules/uglifycss/uglifycss");
				if(!is_file($uglifyCssBinary))
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file '$uglifyCssBinary' does not exist.");

				$uglifyCssCmd = '"' . $nodeBinary . '" "' . $uglifyCssBinary . '"';
			}
			else
			{
				if(!empty(shell_exec("which uglifycss")))
					$uglifyCssCmd = 'uglifycss';
				else
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "The file 'uglifycss' does not exist.");
			}

			return self::openPipeProcess($uglifyCssCmd, $cssClear);
		}

		/**
		 * @name loadJsonFile
		 * Loads a json file and makes some basic tests to prove consistancy.
		 *
		 * @param string $jsonFilePath             - The path to the json file which should get loaded.
		 * @param bool $assoc                      - If true, the returned json is not an object but an associative array.
		 * @param string $tripleTowerEncryptionKey - If specified the given json file is treated as encrypted with this parametere as key.
		 * @return stdClass/TripleTowerError               - If successfull a json object. An TripleTowerError otherwise.
		 */
		public static function loadJsonFile(string $jsonFilePath, bool $assoc=false, string $tripleTowerEncryptionKey="")
		{
			$jsonContent = "";
			if(empty($tripleTowerEncryptionKey))
			{
				if(!is_file($jsonFilePath))
					return new FsError(FsError::FILE_NOT_FOUND, SysError::ERR, "'$jsonFilePath' was not found.");
				$jsonContent = file_get_contents($jsonFilePath);
			}
			else
			{
				if(is_file($jsonFilePath))
				{
					$jsonContent = file_get_contents($jsonFilePath);
					$encryptedData = self::aesEncrypt($jsonContent, $tripleTowerEncryptionKey);
					if(TripleTowerError::isError($encryptedData))
						return $encryptedData;
					if(!file_put_contents("$jsonFilePath.tte", $encryptedData))
						return new FsError (FsError::FILE_CREATION_FAILED, FsError::ERR, "File '$jsonFilePath.tte' could not be created.");
					if(!unlink($jsonFilePath))
						return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, "File '$jsonFilePath' could not be deleted.");
				}
				else if(is_file("$jsonFilePath.tte"))
				{
					$jsonContent = self::aesDecrypt(file_get_contents("$jsonFilePath.tte"), $tripleTowerEncryptionKey);
					if(TripleTowerError::isError($jsonContent))
						return $jsonContent;
				}
				else
				{
					return new FsError(FsError::FILE_NOT_FOUND, SysError::ERR, "'$jsonFilePath' was not found.");
				}
			}
			if(empty($jsonContent))
				return new FsError(FsError::LOAD_FILE_FAILED, SysError::ERR, "'$jsonFilePath' is no valid json.");
			$jsonObject = json_decode($jsonContent, (bool)$assoc);
			if($jsonObject === null)
				return new FsError(FsError::FILE_FORMAT_CORRUPT, SysError::ERR, "File '$jsonFilePath' could not be reliably decoded.");
			return $jsonObject;
		}

		/**
		 * @name convertHashesToObjects
		 * Converts all hashes(associative arrays) to objects with according properties.
		 *
		 * @param mixed $hash         - The path to the json file which should get loaded.
		 * @return stdClass/TripleTowerError  - A converted object.
		 */
		public static function convertHashesToObjects($hash)
		{
			return json_decode(json_encode($hash), false);
		}

		/**
		 * @name mergeRecursiveAndDestructive
		 * Merges an arbitrary number of given arrays and objects recursivly and destructs ambiguous
		 * properties if these are neither arrays nor objects. The following paramter defines the
		 * added properties. This means if you call this method with an array and an object
		 * the result will be an object. If you call this method with an object and an array the result
		 * will be an array. So the parameter order really matters.
		 *
		 * @param Array/stdClass $arraysToMerge   - The path to the json file which should get loaded.
		 * @return Array/stdClass                 - The resulting array/stdClass containing the properties of all given objects/arrays.
		 */
		public static function mergeRecursiveAndDestructive()
		{
			$arraysToMerge = func_get_args();
			if(count($arraysToMerge) === 0)
			{
				return []; // return empty array
			}
			else if(count($arraysToMerge) === 1)
			{
				return $arraysToMerge[0]; // return the one and only array
			}
			else if(count($arraysToMerge) === 2)
			{
				$mergedArray = (array)$arraysToMerge[0];
				$addedArray = (array)$arraysToMerge[1];

				// this is where the magic happens
				foreach($addedArray as $key => $value)
				{
					if((is_array($value)||is_object($value)) && isset($mergedArray[$key]) && (is_array($mergedArray[$key])||is_object($mergedArray[$key])))
					{
						$mergedArray[$key] = self::mergeRecursiveAndDestructive($mergedArray[$key], $value);
					}
					else
					{
						$mergedArray[$key] = $value;
					}
				}
				return (is_object($arraysToMerge[1]) ? (object)$mergedArray : $mergedArray); // if objects were given, then objects should be returned
			}
			else
			{
				// iterate over all given arrays
				$mergeResult = array();
				foreach($arraysToMerge as $nextArrayToMerge)
					$mergeResult = self::mergeRecursiveAndDestructive($mergeResult, $nextArrayToMerge);

				return $mergeResult;
			}
		}

		/**
		 * @name parseHeaders
		 * Parses a given header string into an array. If a header is occuring more than once, these are added as a
		 * multi dimensional array.
		 *
		 * @param string $headersString  - The raw headers string.
		 * @return Array                 - The resulting array containing all the headers.
		 */
		public static function parseHeaders(string $headersString)
		{
			$headers = [];
			foreach(explode("\n", $headersString) as $headerLine)
			{
				if(strpos($headerLine, ":") === false)
					continue;

				$headerArray   = explode(":", $headerLine, 2);
				$headerName    = trim($headerArray[0]);
				$headerContent = trim($headerArray[1]);

				if(isset($headerContent))
				{
					if (!isset($headers[$headerName]))
						$headers[$headerName] = $headerContent;
					else if(is_array($headers[$headerName]))
						$headers[$headerName] = array_merge($headers[$headerName], array($headerContent));
					else
						$headers[$headerName] = array_merge(array($headers[$headerName]), array($headerContent));
				}
			}

			return $headers;
		}

		/**
		 * @name mountUncPath
		 * Mounts the given UNC path.
		 *
		 * @param string $uncPath             - The UNC path starting with double backslash (\\).
		 * @param string $user (optional)     - The user used for mounting the uncPath.
		 * @param string $password (optional) - The according user password.
		 * @return TripleTowerError                   - An TripleTowerError if the mounting was successfull or not.
		 */
		public static function mountUncPath(string $uncPath, string $user="", string $password="")
		{
			$command    = 'net use "' . $uncPath . '"' . (!empty($user) ? ' /user:"'.$user.'" "' . $password . '"' : '') . ' /persistent:no';
			$output     = [];
			$returnCode = 0;
			exec("$command 2>&1", $output, $returnCode);

			if($returnCode !== 0)
			{
				return new FsError(FsError::MOUNTING_FAILED, FsError::ERR, "Could not mount '$uncPath' because " . (count($output) ? $output[0] : "Unknown error"));
			}

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name unmountUncPath
		 * Unounts the given UNC path.
		 *
		 * @param string $uncPath             - The UNC path starting with double backslash (\\).
		 * @return TripleTowerError                   - An TripleTowerError if the unmounting was successfull or not.
		 */
		public static function unmountUncPath(string $uncPath)
		{
			$command    = 'net use "' . $uncPath . '" /delete /yes';
			$output     = [];
			$returnCode = 0;
			exec("$command 2>&1", $output, $returnCode);

			if($returnCode !== 0)
			{
				return new FsError(FsError::UNMOUNTING_FAILED, FsError::ERR, "Could not unmount '$uncPath' because of: " . (count($output) ? $output[0] : "Unknown error"));
			}

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name extractMountPoint
		 * Extracts the \\server\path part from given full UNC path.
		 *
		 * @param string $uncPath - The UNC path the mount point should be extracted from.
		 * @return string         - The extracted mount point or an empty string on failure.
		 */
		public static function extractMountPoint(string $uncPath)
		{
			$uncParts = array_values(array_filter(explode("\\", str_replace("/", "\\", $uncPath)), 'strlen'));
			if(count($uncParts) < 2)
			{
				return "";
			}

			return "\\\\" . $uncParts[0] . "\\" . $uncParts[1];
		}

		/**
		 * @name findLatestFile
		 * Find the latest changed file via FileSystemIterator in a directory.
		 *
		 * @param string $path               - Can be a local path, a unc path, ftp, etc...
		 * @param int $newerThan (optional)  - If specified only files new than this time stamp are considered.
		 * @return string                    - The path to the latest file found or an TripleTowerError on failure.
		 */
		public static function findLatestFile(string $path, int $newerThan=0)
		{
			$filePath = "";
			try
			{
				$fsIter = new \FilesystemIterator($path, \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS);
				foreach($fsIter as $versionPath)
				{
					if(filemtime($versionPath) >= $newerThan)
					{
						$newerThan = filemtime($versionPath);
						$filePath = "$path/".basename($versionPath);
					}
				}
			}
			catch(\Exception $ex)
			{
				return new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT, $ex->getMessage());
			}

			if(empty($filePath))
			{
				return new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT, "No files in '$path' found.");
			}

			return $filePath;
		}

		/**
		 * @name convertDateFormatToRegex
		 * Converts a given date formatter to it's according regex.
		 *
		 * @param string $dateFormat         - A date formatter string containing symbols like Y representing a year with four digits.
		 * @return string                    - A converted regular expression.
		 */
		public static function convertDateFormatToRegex(string $dateFormat)
		{
			$recognizedDateFormatters =
			[
				// Day
				"d" => "[0-9]{2}",             // Day of the month, 2 digits with leading zeros
				"D" => "[A-Za-z]{3}",          // A textual representation of a day, three letters
				"j" => "[0-9]{1,2}",           // Day of the month without leading zeros
				// "l" => not supported        // A full textual representation of the day of the week
				"N" => "[1-7]{1}",             // ISO-8601 numeric representation of the day of the week
				//"S" => not supported         // English ordinal suffix for the day of the month, 2 characters
				"w" => "[0-6]{1}",             // Numeric representation of the day of the week
				"z" => "[0-9]{1,3}",           // The day of the year (starting from 0)

				// Week
				"W" => "[0-9]{2}",             // ISO-8601 week number of year, weeks starting on Monday

				// Month
				// "F" => not supported        // A full textual representation of a month, such as January or March
				"m" => "[0-9]{2}",             // Numeric representation of a month, with leading zeros
				"M" => "[A-Za-z]{3}",          // A short textual representation of a month, three letters
				"n" => "[0-9]{1,2}",           // Numeric representation of a month, without leading zeros
				"t" => "[0-9]{2}",             // Number of days in the given month

				// Year
				"L" => "[0-1]{1}",             // Whether it's a leap year
				"o" => "[0-9]{4}",             // ISO-8601 week-numbering year.
				"Y" => "[0-9]{4}",		       // A full numeric representation of a year, 4 digits
				"y" => "[0-9]{2}",             // A two digit representation of a year

				// Time
				"a" => "[a|p]m",               // Lowercase Ante meridiem and Post meridiem
				"A" => "[A|P]M",               // Uppercase Ante meridiem and Post meridiem
				"B" => "[0-9]{3}",             // Swatch Internet time
				"g" => "[0-9]{1,2}",           // 12-hour format of an hour without leading zeros
				"G" => "[0-9]{1,2}",           // 24-hour format of an hour without leading zeros
				"h" => "[0-9]{2}",             // 12-hour format of an hour with leading zeros
				"H" => "[0-9]{2}",             // 24-hour format of an hour with leading zeros
				"i" => "[0-9]{2}",             // Minutes with leading zeros
				"s" => "[0-9]{2}",             // Seconds, with leading zeros
				"u" => "[0-9]{6}",             // Microseconds
				"v" => "[0-9]{3}",             // Milliseconds

				// Timezone
				// "e" => not supported        // Timezone identifier
				"I" => "[0-9]{1}",             // Whether or not the date is in daylight saving time
				"O" => "\+[0-9]{4}",           // Difference to Greenwich time (GMT) in hours
				"P" => "\+[0-9]{2}\:[0-9]{2}", // Difference to Greenwich time (GMT) with colon between hours and minutes
				// "T" => not supported        // Timezone abbreviation
				"Z" => "\-[0-9]{1,5}",         // Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive

				// Full Date/Time
				// "c" => not supported        // ISO 8601 date
				// "r" => not supported        // RFC 2822 formatted date
				"U" => "[0-9]{1,10}",          // Seconds since the Unix Epoch
			];
			return str_replace(array_keys($recognizedDateFormatters), array_values($recognizedDateFormatters), $dateFormat);
		}

		/**
		 * @name zipFilesByRegex
		 * Uses the given file path patterna and puts all matching files into the given zip file path except specified
		 * in the file exception list. The default PCRE delimiter is used in this method. This means you have to escape
		 * '/' yourself if you want to use them as normals strings in $fileNameRegex.
		 *
		 * @param string $path                      - The path which all actions will take place
		 * @param string $zipFileName               - The name of the zip file into which all matching files will be moved to
		 * @param string $fileNameRegex             - The regular expression of the file which has to be matched
		 * @param Array/string $fileNameExceptions  - The file name or a list of files which won't be zipped even the file names were matched by the regex.
		 * @return uint/TripleTowerError                    - The number files which got zipped or TripleTowerError if an error occures.
		 */
		public static function zipFilesByRegex(string $path, string $zipFileName, string $fileNameRegex, $fileNameExceptions=[])
		{
			$path = self::normalizeFolderPath($path);

			$numberOfFilesZipped = 0;
			if(!is_array($fileNameExceptions))
				$fileNameExceptions = [$fileNameExceptions];

			$fileSystemIterator = new \RegexIterator(new \FilesystemIterator($path,
					\FilesystemIterator::CURRENT_AS_PATHNAME |
					\FilesystemIterator::SKIP_DOTS |
					\FilesystemIterator::UNIX_PATHS),
				"/" . preg_quote($path, "/") . $fileNameRegex . "/");

			// If there is a lockfile, stop zip log files, another thread is already doing this
			$lockFile = $path . basename($zipFileName, ".zip") . ".lock";
			if( file_exists($lockFile) )
			{
				// If it's older than a minute, try to delete it
				$fileModificationTime = @filemtime($lockFile);
				if( $fileModificationTime !== false && time()-$fileModificationTime > 60 )
					@unlink($lockFile);
				else
					return 0;
			}

			// If this fails, another thread was faster in getting the lock
			$lockFileHandle = @fopen($lockFile, 'x');
			if( $lockFileHandle === false )
				return 0;

			$zipFile = new \ZipArchive();
			foreach($fileSystemIterator as $filePath)
			{
				if( is_dir($filePath) )
					continue;

				if( in_array(basename($filePath), $fileNameExceptions) )
					continue;

				if($zipFile->open($path . $zipFileName, \ZipArchive::CREATE) !== true)
					return new SysError(SysError::UNEXPECTED_ZIP_ERROR, SysError::CRIT, "Could not open file '$path" . "$zipFileName' for zipping file '$filePath'.");

				if($zipFile->addFile($filePath, basename($filePath)) !== true)
					return new SysError(SysError::UNEXPECTED_ZIP_ERROR, SysError::CRIT, "Could not add file '" . $filePath . "' to zip file '$path" . "$zipFileName'.");

				if( $zipFile->close() !== true )
					return new SysError(SysError::UNEXPECTED_ZIP_ERROR, SysError::CRIT, "Could not close previously opened/created zip file '$path" . "$zipFileName'.");

				if( unlink($filePath) !== true )
					return new FsError(FsError::UNLINK_FAILED, FsError::CRIT, "Could not unlink file '$filePath' but it has been added to zip file '$path" . "$zipFileName'.");

				$numberOfFilesZipped++;
			}

			if( fclose($lockFileHandle) === false )
				return new FsError(FsError::ACCESS_DENIED, FsError::CRIT, "Could not close lock filehandle for '$lockFile' to remove logging barrier for other requests");

			// Try to delete it, but if it fails, it's not too bad because
			// it gets deleted anyway when the next thread comes after 60sec
			@unlink($lockFile);

			return $numberOfFilesZipped;
		}

		/**
		 * @name isFolderEmpty
		 * Tests if given folder is empty.
		 *
		 * @param string $folderPath         - The folder which will be tested
		 * @return bool                      - True if folder is empty, false if not
		 */
		public static function isFolderEmpty(string $folderPath)
		{
			return !(new \FilesystemIterator($folderPath))->valid();
		}

		/**
		 * @name expandVariables
		 * @desc Expands environment variables which are contained in a string.
		 * @param string $stringWithVariables The string which contains the variables and is being explanded
		 * @return mixed Returning the expanded string or false if an error occured
		 */
		public static function expandVariables(string $stringWithVariables)
		{
			// Allowed characters in a variable which have to be embraced by a percentage symbol (%):
			// A-Z a-z 0-9 # _ () { } [ ] $ * + - \ / "  ' , ; . @ ! ? $ ` ~
			$matches = [];
			$result = preg_match_all('/[^%]*%([A-Za-z0-9\_\(\)\{\}\[\]\$\*\+\-\\\\\/\"\#\'\,\;\.\@\!\?\$\`\~]+)%+[^%]*/U', $stringWithVariables, $matches);
			if($result === false)
				return new SysError(SysError::INVALID_EXPANSION, "Could not apply variable extraction regex on '$stringWithVariables'");

			if($result === 0 || count($matches) <= 1)
				return $stringWithVariables;

			foreach($matches[1] as $match)
			{
				$expandedVariable = getenv($match);
				if($expandedVariable !== false)
					$stringWithVariables = str_replace("%$match%", $expandedVariable, $stringWithVariables);
			}

			return $stringWithVariables;
		}

		public static function filterArrayRecursive(array $array, ?callable $callback = null, int $mode = 0) : array
		{
			$filteredArray = [];
			foreach( $array as $key => $value )
			{
				if( $callback === null )
				{
					$newValue = is_array($value) ? self::filterArrayRecursive($value, $callback, $mode) : $value;

					if( !empty($newValue) )
						$filteredArray[$key] = $value;
				}
				else
				{
					switch( $mode )
					{
						case ARRAY_FILTER_USE_KEY:
							if( $callback($key) )
								$filteredArray[$key] = is_array($value) ? self::filterArrayRecursive($value, $callback, $mode) : $value;
						break;

						case ARRAY_FILTER_USE_BOTH:
							if( is_array($value) )
								$filteredArray[$key] = self::filterArrayRecursive($value, $callback, $mode);
							elseif( $callback($key, $value) )
								$filteredArray[$key] = $value;
						break;

						default:
							if( is_array($value) )
								$filteredArray[$key] = self::filterArrayRecursive($value, $callback, $mode);
							else if( $callback($value) )
								$filteredArray[$key] = $value;
					}
				}
			}

			return $filteredArray;
		}

		/**
		 * @name findExecutableInPathVariable
		 * @desc Searches for the spcified executable file in all paths found in
		 *       the path variable and returns the resulting path if found.
		 * @param string $executable The name of the executable file to searched
		 * @return mixed The complete path including the executable if executable has been found or SysError::NO_SUCH_PROPERTY_ERROR otherwise
		 */
		public static function findExecutableInPathVariable(string $executable)
		{
			$executable = self::normalizePath($executable);
			if($executable === "")
				return new SysError(SysError::EMPTY_STRING_ERROR, SysError::ERR, "Empty string is no executable");

			if($executable === "." || $executable === ".." || strpos($executable, "/") !== false)
				return new SysError(SysError::INVALID_FILE_NAME, "A file path can't contain a slash (/) so path variable could not be searched");

			$pathVariable = "";
			try
			{
				$shell = new \COM("WScript.Shell");
				if(!$shell)
					return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to search in path variable");
				$pathVariable = $shell->RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\\Path");
				if($pathVariable === false)
					return new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Could not get path variable from registry");
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not find executable '$executable' in folder of path variable because of: " . $t->getMessage());
			}

			$pathArray = explode(PATH_SEPARATOR, $pathVariable);
			for($loop = 0; $loop < count($pathArray); $loop++)
			{
				$expandedPath = self::expandVariables($pathArray[$loop]);
				if(TripleTowerError::isError($expandedPath))
					return $expandedPath;

				$potentialPath = self::normalizePath($expandedPath);
				if(!is_dir($potentialPath)) // if path in path variable is broken don't bother
					continue;

				$potentialFiles = scandir($potentialPath, SCANDIR_SORT_NONE);
				if($potentialFiles === false)
					return new SysError(SysError::INVALID_PARAM, SysError::ERR, "Path variable content '$potentialPath' could not be searched for executable '$executable'");

				foreach($potentialFiles as $potentialFile)
				{
					if($potentialFile === $executable)
						return "$potentialPath/$executable";
				}
			}

			return new FsError(FsError::FILE_NOT_FOUND, FsError::SILENT, "The executable '$executable' could not be found in path variable");
		}

		/**
		 * @name addPathToPathVariable
		 * @desc Adds given path to the path variable. The path is not added or
		 *       altered if it's already there.
		 * @param string $path The path which is to be added to the path variable
		 * @return TripleTowerError
		 */
		public static function addPathToPathVariable(string $path)
		{
			$path = self::normalizePath($path);
			if($path === "")
				return new SysError(SysError::EMPTY_STRING_ERROR, SysError::ERR, "Empty string can not be added to path variable");

			if(realpath($path) === false)
				return new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Path '$path' could not be found so it could not be added to path variable");

			try
			{
				$shell = new \COM("WScript.Shell");
				if(!$shell)
					return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to search in path variable");
				$pathVariable = $shell->RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\\Path");
				if($pathVariable === false)
					return new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not get path variable from registry");

				$pathArray = explode(PATH_SEPARATOR, $pathVariable);
				for($loop = 0; $loop < count($pathArray); $loop++)
				{
					$expandedPath = self::expandVariables($pathArray[$loop]);
					if(TripleTowerError::isError($expandedPath))
						return $expandedPath;

					// Path is already in path variable
					$potentialPath = self::normalizePath($expandedPath);
					if($potentialPath === $path)
						return new SysError(SysError::SUCCESS);
				}

				// Try to add path to path variable
				$pathArray[] = str_replace("/", "\\", $path); // insert path windows style
				$pathVariable = implode(PATH_SEPARATOR, $pathArray);
				self::addEnvironmentVariable("PATH", $pathVariable);
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not add path '$path' to path variable because of: " . $t->getMessage());
			}

			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name removePathFromPathVariable
		 * @desc Removes a path from the path variable.
		 * @param string $path The name of path to be removed
		 * @return TripleTowerError
		 */
		public static function removePathFromPathVariable(string $path)
		{
			$path = self::normalizePath($path);
			if($path === "")
				return new SysError(SysError::EMPTY_STRING_ERROR, SysError::ERR, "Empty string can not be removed from path variable");

			try
			{
				$shell = new \COM("WScript.Shell");
				if(!$shell)
					return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to remove path from path variable");
				$pathVariable = $shell->RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\\Path");
				if($pathVariable === false)
					return new FsError(FsError::FOLDER_NOT_FOUND, FsError::ERR, "Could not get path variable '$path' from registry");

				$pathRemoved = false;
				$pathArray = explode(PATH_SEPARATOR, $pathVariable);
				for($loop = 0; $loop < count($pathArray); $loop++)
				{
					$expandedPath = self::expandVariables($pathArray[$loop]);
					if(TripleTowerError::isError($expandedPath))
						return $expandedPath;

					$potentialPath = self::normalizePath($expandedPath);
					if($potentialPath === $path)
					{
						array_splice($pathArray, $loop, 1);
						$loop--;
						$pathRemoved = true;
					}
				}

				if($pathRemoved)
				{

					$pathVariable = implode(PATH_SEPARATOR, $pathArray);
					self::addEnvironmentVariable("PATH", $pathVariable);
				}
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not remove path '$path' from path variable because of: " . $t->getMessage());
			}

			return new SysError(SysError::SUCCESS);
		}

		/**
		 * @name getEnvironmentVariable
		 * @desc Adds given variable name to the system variables. If it's already been there it will be overwritten.
		 * @param string $variableName The name of the variable to be used whichs' content should be returned.
		 * @return string The content of the environment variable.
		 */
		public static function getEnvironmentVariable(string $variableName) : string
		{
			$variableContent = "";
			try
			{
				$shell = new \COM("WScript.Shell");
				if(!$shell)
					return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to read environment variable '$variableName'");
				$variableContent = $shell->RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\\$variableName");
				if($variableContent === false)
					return new SysError(SysError::INVALID_PARAM, SysError::ERR, "Could not get environment variable '$variableName' from registry");
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not read environment variable '$variableName' because of: " . $t->getMessage());
			}

			return $variableContent;
		}

		/**
		 * @name addEnvironmentVariable
		 * @desc Adds given variable name to the system variables. If it's already been there it will be overwritten.
		 * @param string $variableName The name of the variable to be used for adding to the system.
		 * @param string $variableContent The content of the variable to be saved in the the variable name specified by $variableName
		 * @return void
		 */
		public static function addEnvironmentVariable(string $variableName, string $variableContent)
		{
			try
			{
				$shell = new \COM("WScript.Shell");
				if(!$shell)
					return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to add environment variable '$variableName' with content '$variableContent'");

				self::exec("powershell [Environment]::SetEnvironmentVariable('$variableName', '$variableContent', [System.EnvironmentVariableTarget]::Machine)");
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not add environment variable '$variableName' with content '$variableContent' because of: " . $t->getMessage());
			}

			return new SysError(FsError::SUCCESS);
		}

		/**
		 * @name createSymlink
		 * @desc Creates symbolic link for file or folder.
		 * @param string $sourcePath The name of the source file/path.
		 * @param string $linkPath The name of the destination file/path
		 * @param string $forceOverwrite (optional) delete file/folder if exists
		 * @return TripleTowerError
		 */
		public static function createSymlink(string $sourcePath, string $linkPath, bool $forceOverwrite=false)
		{
			// Get real path --> also converts / to \ under windows
			$realsourcePath = realpath($sourcePath);

			// Check if path exists
			if($realsourcePath === false)
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "'$sourcePath' does not exist");

			// Check symlink or file exists
			if(is_link($linkPath) || file_exists($linkPath))
			{
				if($forceOverwrite !== true)
					return new FsError(FsError::FILE_ALREADY_EXISTS, FsError::ERR, "File '$linkPath' already exits but forceOverwrite is set to 'false'");
				if( !self::unlink_recursive($linkPath)->isSuccess() )
					return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, "Could not unlink file '$linkPath'");
			}

			// Create symlink
			if(!symlink($realsourcePath, $linkPath))
				return new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Could not create symlink from '$realsourcePath' to '$linkPath'");

			// Everything ok
			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name createShortcut
		 * @desc Creates Windows shortcut for file or folder.
		 * @param string $targetPath The name of the source file/path.
		 * @param string $shortcutPath The name of the destination file/path
		 * @return TripleTowerError
		 */
		public static function createWindowsShortcut(string $targetPath, string $shortcutPath)
		{
			$windowsShortcutFileExtension = ".lnk";
			if( !self::stringEndsWith(strtolower($shortcutPath), $windowsShortcutFileExtension) )
				$shortcutPath = $shortcutPath . $windowsShortcutFileExtension;

			try
			{
				$shell = new \COM("WScript.Shell");
				if( !$shell )
					throw new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Could not initialize COM object 'WScript.Shell'");

				$shortcut = $shell->CreateShortcut($shortcutPath);
				$shortcut->targetpath = $targetPath;
				$shortcut->Save();

				return new FsError(FsError::SUCCESS);
			}
			catch( \Throwable $throwable )
			{
				return new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "Could not create shortcut from '$targetPath' to '$shortcutPath'", $throwable);
			}
		}

		/**
		 * @name remoteFileExists
		 * @desc Tests if remote file or folder exists.
		 * @param string $remoteFile The name of the file to test.
		 * @return TripleTowerError
		 */
		public static function remoteFileExists(string $remoteFile)
		{
			$unsecureContext = stream_context_create(["ssl" => ["verify_peer" => false, "verify_peer_name" => false]]);
			if(($sourceFileHandle = fopen($remoteFile, 'rb', false, $unsecureContext)) === false)
			{
				if(($sourceFileHandle = opendir($remoteFile, $unsecureContext)) === false)
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "File '$remoteFile' could not be found");
				else
					closedir($sourceFileHandle);
			}
			else
			{
				fclose($sourceFileHandle);
			}

			return new FsError(FsError::SUCCESS);
		}

		/**
		 * @name downloadFile
		 * @desc Downloads remote file.
		 * @param string $sourceFilePath The name of the source file/path.
		 * @param string $destFilePath The name of the destination filePath, destination folder or "" to return content directly
		 * @param string $echoProgress (optional) Echo progress of download in 10% steps.
		 * @param Proxy $proxy (optional) The proxy to be used for downloading.
		 * @return string | TripleTowerError
		 *		If $destFilePath is an existing folder the name of file will be guessed and therefore the final path
		 *		to the downloaded file will be returned.
		 *		If $destFilePath is not an existing folder the file will be downloaded to the given filePath.
		 *		If $destFilePath is "" or an error occured TripleTowerError is returned.
		 */
		public static function downloadFile(string $sourceFilePath, string $destFilePath="", bool $echoProgress=false, string $caFileName="", Proxy $proxy=null)
		{
			$saveContentToFile = ($destFilePath === "" ? false : true);
			$detectFilename = ($saveContentToFile && is_dir($destFilePath));
			$sizeTransfered = 0;
			$progressSteps = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10];
			$currentProgressStep = 0;
			$sourceFileHandle = false;

			$contextOptions = [	"http" => [ "user_agent" => self::HTTP_USER_AGENT ] ];
			if( isset($_SERVER["TT"]) )
			{
				if( $caFileName === "" )
					$caFileName = "ca-bundle.crt";
				if( is_file($_SERVER["TT"]->tower->getEtcFolder()."ssl/certs/$caFileName") === false )
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not find CA certificate file '$caFileName' in certs folder.");

				$contextOptions["ssl"] = [ 'cafile' => $_SERVER["TT"]->tower->getEtcFolder()."ssl/certs/$caFileName" ];
			}

			$context = stream_context_create($contextOptions);

			// Use proxy object if given and if valid
			if($proxy !== null && empty($proxy->getProxyUrl()) === false)
			{
				$context = $proxy->addProxyToContext($context);
				$sourceFileHandle = fopen($sourceFilePath, 'rb', false, $context);
				if($sourceFileHandle === false)
					$context = $proxy->removeProxyFromContext($context);
			}

			if($sourceFileHandle === false)
			{
				// Try without proxy
				if(($sourceFileHandle = @fopen($sourceFilePath, 'rb', false, $context)) === false)
				{
					// Try with auto-proxy
					$proxyString = self::getProxyServerAndPortFromRegistry();
					if(!TripleTowerError::isError($proxyString))
					{
						$autoProxy        = new Proxy($proxyString, 0, get_current_user());
						$context          = $autoProxy->addProxyToContext($context);
						$sourceFileHandle = fopen($sourceFilePath, 'rb', false, $context);
					}
				}
			}

			if($sourceFileHandle === false)
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "Could not open file '$sourceFilePath' for reading");

			$sourceFileSize = false;
			if(is_array($sourceFileMetaData = stream_get_meta_data($sourceFileHandle)))
			{
				if($sourceFileMetaData["wrapper_type"] === "plainfile" && strtoupper($sourceFileMetaData['stream_type']) === 'STDIO')
				{
					$sourceFileSize = filesize($sourceFilePath);
					if( $detectFilename )
						$destFilePath = self::normalizeFolderPath($destFilePath) . basename($sourceFilePath);
				}
				else if($sourceFileMetaData["wrapper_type"] === "http")
				{
					$fileHeader = [];
					foreach( $sourceFileMetaData["wrapper_data"] as $headerItemString )
					{
						list($headerKey, $headerValueString) = array_pad( explode(":", $headerItemString, 2), 2, null );
						if( !isset($headerValueString) ) // Only HTTP status has no key => value
						{
							$fileHeader["status"] = $headerKey;
							continue;
						}

						$headerValue = [];
						foreach( explode(";", $headerValueString ) as $headerSubValueString )
						{
							$subValueParts = explode("=", $headerSubValueString );
							if( count($subValueParts) === 1 )
								$headerValue[] = trim($subValueParts[0], " \"\n\r\t\v\x00");
							else
								$headerValue[trim($subValueParts[0])] = trim($subValueParts[1], " \"\n\r\t\v\x00");
						}
						$fileHeader[trim($headerKey, " \"\n\r\t\v\x00")] = $headerValue;
					}

					if( $detectFilename )
					{
						if( isset($fileHeader["Content-Disposition"]["filename"]) )
							$destFilePath = self::normalizeFolderPath($destFilePath) . $fileHeader["Content-Disposition"]["filename"];
					}

					$sourceFileSize = isset( $fileHeader["Content-Length"][0] ) ? (int)$fileHeader["Content-Length"][0] : false;
				}
			}

			if( $detectFilename && is_dir($destFilePath) )
				return new FsError(FsError::NAME_IS_INVALID, FsError::ERR, "Destination file '$destFilePath' could not be saved because a folder with the same name already exists.");

			$downloadContent = "";   // Used if downloaded file should be return be method
			$destFileHandle = null; // Used if downloaded file should be written to file

			if($saveContentToFile)
			{
				// Convert only http url to local/ftp/ssh path
				if( !self::stringStartsWith($destFilePath, "http://") && !self::stringStartsWith($destFilePath, "https://") )
					$destFilePath = str_replace( ['%20', '/'], [' ', '\\'], $destFilePath);

				$destFileHandle = fopen($destFilePath, 'wb');
				if($destFileHandle === false)
					return new FsError(FsError::FILE_LOCK_FAILED, FsError::ERR, "Could not open destination file '$destFilePath' for writing");
			}

			$currentProgressStep = array_pop($progressSteps);
			if($echoProgress)
			{
				if($sourceFileSize !== false)
					echo "downloading: 0% ";
				else
					echo "start copying (filesize could not be read, so no progress)... ";
			}

			$chunkSize = 64 * 1024; // 64 KiB to read at once
			while(!feof($sourceFileHandle))
			{
				$readChunk = fread($sourceFileHandle, $chunkSize);
				if($saveContentToFile)
					fwrite($destFileHandle, $readChunk, $chunkSize);
				else
					$downloadContent .= $readChunk;

				if($echoProgress && $sourceFileSize !== false)
				{
					$sizeTransfered += strlen($readChunk);
					$percentDone = round(($sizeTransfered / $sourceFileSize) * 100, 2);
					if($percentDone > 1 && ($percentDone % $currentProgressStep) === 0 && $currentProgressStep < 100)
					{
						echo "$currentProgressStep% ";
						$currentProgressStep = array_pop($progressSteps);
					}
				}
			}

			if($echoProgress)
				echo "100% ";

			$sourceFileHandleClosed = fclose($sourceFileHandle);
			$destFileHandleClosed = ($saveContentToFile ? fclose($destFileHandle) : true);
			if(!$sourceFileHandleClosed)
				return new FsError(FsError::FILE_UNLOCK_FAILED, FsError::ERR, "Could not close source file handle '$sourceFilePath'");
			if(!$destFileHandleClosed)
				return new FsError(FsError::FILE_UNLOCK_FAILED, FsError::ERR, "Could not close destination file handle '$destFilePath'");

			if($detectFilename)
				return $destFilePath;

			if($saveContentToFile)
				return new FsError(FsError::SUCCESS);

			return $downloadContent;
		}

		/**
		 * @name getProxyServerAndPortFromRegistry
		 * @desc Reads the name and port proxy server of the current logged in user from windows registry.
		 * @return string Name and port of proxy server of the current logged in user.
		 */
		public static function getProxyServerAndPortFromRegistry()
		{
			if(OsNames::getCurrentOsType() !== OsNames::OS_TYPE_WINDOWS)
				return new SysError(SysError::UNKNOWN_OS, SysError::ERR, "The function '" . __FUNCTION__ . "' is windows-only");

			$shell = new \COM("WScript.Shell");
			if(!$shell)
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not open Microsoft registry object (WScript.Shell) to search variable");

			$regPath = "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\ProxyServer";
			try
			{
				return $shell->RegRead($regPath);
			}
			catch(\Throwable $t)
			{
				return new SysError(SysError::INVALID_PARAM, SysError::ERR, "Could not find or access variable '$regPath' in registry");
			}
		}

		public static function passThruFile(string $filePath)
		{
			try
			{
				if(file_exists($filePath) === false)
					throw new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "File '$filePath' not found.");

				$fileName = self::extractFileName($filePath);

				header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
				header("Content-Type: " . self::getMimeType($filePath));
				header("Cache-Control: max-age=604800"); // 60sec * 60min * 24hours * 7days
				header("Content-Length: " . filesize($filePath));
				header("Content-Disposition: attachment; filename=\"$fileName\"");

				ob_clean();
				flush();
				readfile($filePath);
			}
			catch(\Throwable $t)
			{
				header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, $t->getMessage());
			}
			return new FsError(FsError::SUCCESS);
		}

		public static function passThruContent(string $content, string $fileName, string $mimeType)
		{
			try
			{
				header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
				header("Content-Type: " . $mimeType);
				header("Cache-Control: max-age=604800"); // 60sec * 60min * 24hours * 7days
				header("Content-Length: " . strlen($content));
				header("Content-Disposition: attachment; filename=\"$fileName\"");

				ob_clean();
				flush();
				echo $content;
			}
			catch(\Throwable $t)
			{
				header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
				return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, $t->getMessage());
			}
			return new FsError(FsError::SUCCESS);
		}

		public static function addTowerToName(string $name, bool $toLower=true) : string
		{
			// Default user has no towername appendix
			// all other users/wapps are coming from a tower
			if( strtolower($name) === strtolower(User::DEFAULT_USERNAME) )
				return strtolower($name);

			return ($toLower ? strtolower($name) : $name) . "@" . strtolower($_SERVER["SERVER_NAME"]);
		}

		public static function removeTowerFromName(string $fullName, bool $toLower=true) : string
		{
			// Default user has no towername appendix
			// all other users/wapps have to come from a tower
			if( strtolower($fullName) === strtolower(User::DEFAULT_USERNAME) )
				return strtolower($fullName);

			list($name, $towerName) = explode("@", $fullName, 2);
			if( strtolower($towerName) !== strtolower($_SERVER["SERVER_NAME"]) )
				return new AuthError(AuthError::INVALID_USER_NAME, AuthError::ERR, "User/wapp '$name' not found on tower '" . strtolower($_SERVER["SERVER_NAME"]) . "'");

			return ($toLower ? strtolower($name) : $name);
		}

		public static function splitTowerFromName(string $fullName, bool $toLower=true)
		{
			// Default user has no towername appendix
			// all other users/wapps have to come from a tower
			if( strtolower($fullName) === strtolower(User::DEFAULT_USERNAME) )
				return [strtolower(User::DEFAULT_USERNAME), ""];

			// Tower names are always lower case
			list($name, $towerName) = explode("@", $fullName, 2);
			return [$toLower ? strtolower($name) : $name, strtolower($towerName)];
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function __construct()
		{

		}
	}
}