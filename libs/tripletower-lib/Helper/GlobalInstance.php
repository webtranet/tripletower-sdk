<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\AuthSystem\AuthSystemManager;
	use TripleTowerSDK\DataBase\DataBaseManager;
	use TripleTowerSDK\ScheduledJob\ScheduledJobManager;
	use TripleTowerSDK\Storage\StorageManager;
	use TripleTowerSDK\Tower;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Helper\G11n;
	use TripleTowerSDK\Helper\Profiler;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\User;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\WappInfo;
	use Zend\Log\Logger;

	class GlobalInstance
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const ENVIRONMENT_UNKNOWN     = "unknown";
		const ENVIRONMENT_DEVELOPMENT = "development";
		const ENVIRONMENT_STAGING     = "staging";
		const ENVIRONMENT_PRODUCTION  = "production";

		const CONTENT_ACCESS_PUBLIC        = "public";
		const CONTENT_ACCESS_ANONYMOUSONLY = "anonymousOnly";
		const CONTENT_ACCESS_AUTHENTICATED = "authenticated";
		const CONTENT_ACCESS_RESTRICTED    = "restricted";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected ?Tower $tower                       = null;
		protected ?Logger $logger                     = null;
		protected ?JsonConfig $conf                   = null;
		protected string $environment                 = self::ENVIRONMENT_UNKNOWN;
		protected ?WappInfo $meta                     = null;
		protected ?AuthSystemManager $authSystems     = null;
		protected ?DataBaseManager $dataBases         = null;
		protected ?StorageManager $storages           = null;
		protected ?G11n $g11n                         = null;
		protected ?Profiler $profiler                 = null;
		protected ?\Throwable $lastError              = null;
		protected ?User $user                         = null;
		protected string $contentProvider             = "";
		protected string $contentType                 = "";
		protected string $contentName                 = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(Tower $tower, Logger $logger, JsonConfig $conf, G11n $g11n)
		{
			if( isset($_SERVER["TT"]) )
				throw new SysError(SysError::PROPERTY_ALREADY_EXISTS, SysError::ERR, "GlobalInstance can't be set twice!");

			$this->tower        = $tower;
			$this->conf         = $conf;
			$this->g11n         = $g11n;

			$this->changeEnvironment($conf->environment)->throwIfNotSuccess();
			$this->logger       = $logger; // TODO: autogeneration of logger by conf

			$this->authSystems   = new AuthSystemManager();
			$this->dataBases     = new DataBaseManager();
			$this->storages      = new StorageManager();
			$this->profiler      = new Profiler(isset($conf->profilingEnabled) ? (bool)$conf->profilingEnabled : false );
		}

		public function __get($key)
		{
			if( property_exists(get_class($this), $key) )
				return $this->$key;
			else
				throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The property '$key' is not available.");
		}

		public function __isset($key)
		{
			if( property_exists(get_class($this), $key) )
				return true;
			else
				return false;
		}

		public function isInDevelopmentMode() : bool
		{
			return $this->environment === self::ENVIRONMENT_DEVELOPMENT;
		}

		public function isInStagingMode() : bool
		{
			return $this->environment === self::ENVIRONMENT_STAGING;
		}

		public function isInProductionMode() : bool
		{
			return $this->environment === self::ENVIRONMENT_PRODUCTION;
		}

		public function addMeta(WappInfo $meta)
		{
			$this->meta = $meta;
			return $this->meta;
		}

		public function changeLogger(\Zend\Log\Logger $logger)
		{
			$this->logger = $logger;
			return $this->logger;
		}

		public function changeEnvironment(string $environment)
		{
			$validEnvironemt = in_array($environment,
			[
				self::ENVIRONMENT_DEVELOPMENT,
				self::ENVIRONMENT_STAGING,
				self::ENVIRONMENT_PRODUCTION
			]);

			if( $validEnvironemt === false )
				return new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "The given environment mode '$environment' is unknown.");

			$this->environment  = $environment;

			return new SysError(SysError::SUCCESS);
		}

		public function personalizeUserRequest( User $user )
		{
			if( !empty($this->user) )
				return new SysError(SysError::PROPERTY_ALREADY_EXISTS, SysError::ERR, "Impersonation failed because it has already been set to '" . $this->user->userName . "'.");

			$this->user = $user;

			return new SysError(SysError::SUCCESS);
		}

		public function setLastError($TripleTowerError)
		{
			if( TripleTowerError::isError($TripleTowerError) )
			{
				if( !$TripleTowerError->isSuccess() )
					$this->lastError = $TripleTowerError;
			}
			else if( Helper::isClassRelative($TripleTowerError, "Throwable") )
			{
				$this->lastError = $TripleTowerError;
			}
			else
			{
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Given parameter '" . $TripleTowerError . "' is not a Throwable.");
			}
		}

		public function setContentVars($contentProvider, $contentType, $contentName)
		{
			$this->contentProvider = $contentProvider;
			$this->contentType     = $contentType;
			$this->contentName     = $contentName;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}