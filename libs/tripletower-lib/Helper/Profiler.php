<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Helper
{

	class Profiler
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $enabled;
		protected $profileTable = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		function __construct($enabled)
		{
			$this->enabled = $enabled;
		}

		function startProfiling(string $name)
		{
			if($this->enabled)
			{
				$this->profileTable[$name] = array();
				$this->profileTable[$name]["start"] = microtime(true);
			}
		}

		function stopProfiling(string $name)
		{
			if($this->enabled && isset($this->profileTable[$name]) && isset($this->profileTable[$name]["start"]))
				$this->profileTable[$name]["stop"] = microtime(true);
		}


		function startAddProfilingTime(string $name)
		{
			if($this->enabled)
			{
				if( isset($this->profileTable[$name]) &&
					isset($this->profileTable[$name]["start"]) &&
					isset($this->profileTable[$name]["stop"]) &&
					!isset($this->profileTable[$name]["startadd"]) )
				{
					$this->profileTable[$name]["startadd"] = microtime(true);
				}
			}
		}

		function stopAddProfilingTime($name)
		{
			if($this->enabled)
			{
				if( isset($this->profileTable[$name]) &&
					isset($this->profileTable[$name]["start"]) &&
					isset($this->profileTable[$name]["stop"]) &&
					isset($this->profileTable[$name]["startadd"]) )
				{
					$this->profileTable[$name]["stop"] += (microtime(true)-$this->profileTable[$name]["startadd"]);
					unset( $this->profileTable[$name]["startadd"] );
				}
			}
		}

		function printTable()
		{
			foreach ($this->profileTable as $key => $value)
				$this->printItem($key);
		}

		function printItem($name)
		{
			if($this->enabled)
			{
				if( !isset($this->profileTable[$name]["start"]) )
					if( isset($_SERVER["TT"]) )
						$_SERVER["TT"]->logger->debug("Profiling: Start signal of " . $name . " is missing");

				if( !isset($this->profileTable[$name]["stop"]) )
					if( isset($_SERVER["TT"]) )
						$_SERVER["TT"]->logger->debug("Profiling: Stop signal of " . $name . " is missing");

				if( isset($this->profileTable[$name]["start"]) && isset($this->profileTable[$name]["stop"]) )
				{
					$duration = $this->profileTable[$name]["stop"] - $this->profileTable[$name]["start"];
					$output = "Profiling: " . $name . " => " . $duration . " s";

					echo $output . PHP_EOL;
					if( isset($_SERVER["TT"]) )
						$_SERVER["TT"]->logger->debug($output);
				}
			}
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}