<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\FileSystem
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\FileSystem\FileAttribute;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Error\SysError;

	class FileInfoEx extends FileInfo
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $mimeType = "none";
		protected $fileAttributes = FileAttribute::NORMAL;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function getFileInfoListFromHcp(\SimpleXMLElement $xmlFileList)
		{
			$folderPath = $xmlFileList->attributes()->utf8Path;
			$folderPath = Helper::normalizeFolderPath(substr($folderPath, 0, 5) === '/rest' ? substr($folderPath, 5) : $folderPath);
			$fileInfos = array();
			foreach($xmlFileList as $xmlFileEntry => $file)
			{
				if($xmlFileEntry === 'entry')
				{
					$fileAttribs = $file->attributes();
					$fileInfo = new self($folderPath . $fileAttribs->utf8Name, true);
					$fileInfo->fileAttributes |= FileAttribute::READONLY;
					if((string)$fileAttribs->type === "object")
					{
						$fileInfo->fileSize = $fileAttribs->size;
						$fileInfo->lastWriteTime = new \DateTime("@".$fileAttribs->ingestTime);
						$fileInfo->creationTime = new \DateTime("@".$fileAttribs->ingestTime);
					}
					else
					{
						$fileInfo->fileAttributes |= FileAttribute::DIRECTORY;
					}

					if($fileInfo->getFileName() != "" || $fileInfo->getFileExtension() != "")
						$fileInfos[] = $fileInfo;
				}
			}
			return $fileInfos;
		}

		public function __construct(string $filePath, bool $virtual=false)
		{
			parent::__construct($filePath, $virtual);

			if(!$virtual)
			{
				if(file_exists($filePath))
				{
					$this->refetchMetaInformation();
				}
				else
				{
					$this->fileAttributes |= FileAttribute::VIRTUAL;
					throw new FsError(FsError::FILE_NOT_FOUND, TripleTowerError::ERR, "File '$filePath' could not be found on your filesystem and has not been declared virtual.");
				}
			}
			else
			{
				$this->fileAttributes |= FileAttribute::VIRTUAL;
			}
		}

		public function refetchMetaInformation()
		{
			$filePath = $this->getFullFilePath();
			clearstatcache(true, $filePath);
			$this->fileSize = Helper::getFileSize($filePath);

			if(OsNames::isWindows())
				$this->fileAttributes = $this->getWinFileAttributes();
			else if(OsNames::isUnixoid(OsNames::OS_DARWIN))
				$this->fileAttributes = $this->getMacFileAttributes();
			else if(OsNames::isUnixoid())
				$this->fileAttributes = $this->getUnixFileAttributes();
			else
				throw new SysError(SysError::UNKNOWN_OS, TripleTowerError::ERR, "Tried to gather file attributes but the OS is neither a known unixoid os or a mac os nor a recognized windows system.");

			$oldErrorReporting = error_reporting(0);
			$finfo = new \finfo(FILEINFO_MIME_TYPE | FILEINFO_PRESERVE_ATIME);
			$this->mimeType = $this->isDirectory() ? "inode/directory" : $finfo->file($this->getFullFilePath());
			error_reporting($oldErrorReporting);
		}

		public function getFileInfoList(string $pattern = "*")
		{
			return self::getFileInfoListFromFolder($this->getFullFilePath(), $pattern);
		}

		public function getMimeType()
		{
			return $this->mimeType;
		}

		public function getFileAttributes()
		{
			return $this->fileAttributes;
		}

		public function isReadOnly()
		{
			return ($this->fileAttributes & FileAttribute::READONLY) == true;
		}

		public function isHidden()
		{
			return ($this->fileAttributes & FileAttribute::HIDDEN) == true;
		}

		public function isSystem()
		{
			return ($this->fileAttributes & FileAttribute::SYSTEM) == true;
		}

		public function isDirectory()
		{
			return ($this->fileAttributes & FileAttribute::DIRECTORY) == true;
		}

		public function isArchive()
		{
			return ($this->fileAttributes & FileAttribute::ARCHIVE) == true;
		}

		public function isDevice()
		{
			return ($this->fileAttributes & FileAttribute::DEVICE) == true;
		}

		public function isNormal()
		{
			return ($this->fileAttributes & FileAttribute::NORMAL) == true;
		}

		public function isTemporary()
		{
			return ($this->fileAttributes & FileAttribute::TEMPORARY) == true;
		}

		public function isSparseFile()
		{
			return ($this->fileAttributes & FileAttribute::SPARSE_FILE) == true;
		}

		public function isReparsePoint()
		{
			return ($this->fileAttributes & FileAttribute::REPARSE_POINT) == true;
		}

		public function isCompressed()
		{
			return ($this->fileAttributes & FileAttribute::COMPRESSED) == true;
		}

		public function isOffline()
		{
			return ($this->fileAttributes & FileAttribute::OFFLINE) == true;
		}

		public function isNotContentIndexed()
		{
			return ($this->fileAttributes & FileAttribute::NOT_CONTENT_INDEXED) == true;
		}

		public function isEncrypted()
		{
			return ($this->fileAttributes & FileAttribute::ENCRYPTED) == true;
		}

		public function isVirtual()
		{
			return ($this->fileAttributes & FileAttribute::VIRTUAL) == true;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function getWinFileAttributes()
		{
			$pathName = $this->getFullFilePath();
			if(is_dir($pathName))
				return FileAttribute::DIRECTORY;
			// in other languages there might be some compatibility problems.
			$winAttribs = substr(shell_exec('attrib ' . escapeshellarg($pathName)) , 0, 12);
			if(substr($winAttribs, 0, 4) === "File " || substr($winAttribs, 0, 4) === "Datei")
				return FileAttribute::VIRTUAL;

			return
			((strpos($winAttribs,'R') !== false) ? FileAttribute::READONLY  : 0) |
			((strpos($winAttribs,'H') !== false) ? FileAttribute::HIDDEN    : 0) |
			((strpos($winAttribs,'S') !== false) ? FileAttribute::SYSTEM    : 0) |
			((strpos($winAttribs,'A') !== false) ? FileAttribute::ARCHIVE   : 0);
		}

		// Todo: Not implemented yet
		protected function getMacFileAttributes()
		{
			$pathName = $this->getFullFilePath();
			if(is_dir($pathName))
			{
				return FileAttribute::DIRECTORY;
			}
			return 0;
		}

		// Todo: Not implemented yet
		protected function getUnixFileAttributes()
		{
			$pathName = $this->getFullFilePath();
			if(is_dir($pathName))
			{
				return FileAttribute::DIRECTORY;
			}
			return 0;
		}
	}
}