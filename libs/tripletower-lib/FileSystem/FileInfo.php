<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\FileSystem
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\FsError;

	class FileInfo
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		public $fileHandle = null;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $filePath       = "";
		protected $fileName       = "";
		protected $fileExtension  = "";
		protected $fileSize       = 0;
		protected $creationTime   = 0;
		protected $lastAccessTime = 0;
		protected $lastWriteTime  = 0;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function getFileInfoListFromFolder(string $path, string $pattern = "*", bool $includeSubFolders=false, int $flags=0)
		{
			$fileInfos = array();
			if(!is_string($path))
				return new SysError(SysError::INVALID_PARAM);

			$path = Helper::normalizePathOnFileSystem($path);
			if(!is_dir($path))
				return new FsError(FsError::FOLDER_NOT_FOUND);

			foreach(glob($path.$pattern, $flags) as $fileName)
			{
				$fileInfo = new static($fileName);
				if($fileInfo->getFileName() !== "" || $fileInfo->getFileExtension() !== "")
					$fileInfos[] = $fileInfo;
			}

			if($includeSubFolders)
			{
				foreach(glob($path."*", GLOB_ONLYDIR) as $subfolder)
				{
					$fileInfos = array_merge($fileInfos, self::getFileInfoListFromFolder($subfolder, $pattern, $includeSubFolders, $flags));
				}
			}
			return $fileInfos;
		}

		public function __construct(string $filePath, bool $virtual=false)
		{
			if($filePath == "")
				throw new SysError(SysError::INVALID_PARAM, TripleTowerError::ERR, "A completely empty filePath is not allowed.");

			if(!$virtual && !file_exists($filePath))
				throw new FsError(FsError::FILE_NOT_FOUND, TripleTowerError::ERR, "Invalid filePath: $filePath in FileInfo");

			$pathInfos           = pathinfo($filePath);
			$this->filePath      = Helper::normalizeFolderPath($pathInfos['dirname']);
			$this->fileName      = isset($pathInfos["filename"]) ? $pathInfos['filename']  : "";
			$this->fileExtension = isset($pathInfos["extension"]) ? $pathInfos['extension'] : "";

			if(!$virtual)
				$this->refetchMetaInformation();
		}

		public function refetchMetaInformation()
		{
			clearstatcache(true, $this->getFullFilePath());
			$this->fileSize = filesize($this->getFullFilePath());
			if(($stat=stat($this->getFullFilePath())) !== false)
			{
				$this->creationTime   = new \DateTime("@".$stat["ctime"]);
				$this->lastAccessTime = new \DateTime("@".$stat["atime"]);
				$this->lastWriteTime  = new \DateTime("@".$stat["mtime"]);
			}
		}

		public function getFileInfoList(string $pattern = "*")
		{
			return self::getFileInfoListFromFolder($this->getFullFilePath(), $pattern);
		}

		public function create()
		{
			if(!$this->exists())
			{
				//Todo: fill with content
				return true;
			}
			else
			{
				return false;
			}
		}

		public function exists()
		{
			return file_exists($this->getFullFilePath());
		}

		public function open($mode)
		{
			$result = fopen($this->getFullFilePath(), $mode);
			if(is_resource($result))
				$this->fileHandle = $result;
			return $result;
		}

		public function isOpen()
		{
			return is_resource($this->fileHandle);
		}

		public function isDirectory()
		{
			return is_dir($this->getFullFilePath());
		}

		public function close()
		{
			if($this->isOpen())
				fclose($this->fileHandle);
			$this->fileHandle = null;
		}

		public function getFilePath()
		{
			return $this->filePath;
		}

		public function getFileName()
		{
			return $this->fileName;
		}

		public function getFileExtension()
		{
			return $this->fileExtension;
		}

		public function getFullFileName()
		{
			return $this->fileName . ($this->fileExtension!="" ? ".".$this->fileExtension : "");
		}

		public function getFullFilePath()
		{
			return $this->filePath . $this->getFullFileName();
		}

		public function getFileSize(bool $humanReadable = false)
		{
			return $humanReadable ? Helper::getBytesHumanReadable($this->fileSize) : $this->fileSize;
		}

		public function getCreationTime()
		{
			return $this->creationTime;
		}

		public function getLastAccessTime()
		{
			return $this->lastAccessTime;
		}

		public function getLastWriteTime()
		{
			return $this->lastWriteTime;
		}

		public function copy(string $newAbsoluteFilePath)
		{
			if(is_dir($this->getFullFilePath()))
				return new FsError(FsError::FOLDER_COPYING_FAILED);
			else
				$newAbsoluteFilePath = Helper::normalizeFilePath($newAbsoluteFilePath);

			$error = Helper::copyFile($this->getFullFilePath(), $newAbsoluteFilePath);

			return $error;
		}

		public function moveAndRename(string $newAbsoluteFilePath)
		{
			if(is_dir($this->getFullFilePath()))
				$newAbsoluteFilePath = Helper::normalizeFolderPath($newAbsoluteFilePath);
			else
				$newAbsoluteFilePath = Helper::normalizeFilePath($newAbsoluteFilePath);

			$error = Helper::renameFile($this->getFullFilePath(), $newAbsoluteFilePath);
			if($error->isSuccess())
			{
				$pathInfos           = pathinfo($newAbsoluteFilePath);
				$this->filePath      = Helper::normalizeFolderPath($pathInfos['dirname']);
				$this->fileName      = isset($pathInfos["filename"]) ? $pathInfos['filename']  : "";
				$this->fileExtension = isset($pathInfos["extension"]) ? $pathInfos['extension'] : "";
			}
			return $error;
		}

		public function moveToFolder(string $newFolder)
		{
			$newFolder = Helper::normalizeFolderPath($newFolder);
			if(is_dir($newFolder))
			{
				$error = Helper::renameFile($this->getFullFilePath(), $newFolder.$this->getFullFileName());
				if($error->isSuccess())
					$this->filePath = $newFolder;
				return $error;
			}
			else
			{
				return new FsError(FsError::FOLDER_NOT_FOUND);
			}
		}

		public function rename(string $newFileName)
		{
			$error = Helper::renameFile($this->getFullFilePath(), $this->getFilePath() . $newFileName);
			if($error->isSuccess())
			{
				$this->fileName      = Helper::strrstr($newFileName, '.', true);
				$this->fileExtension = Helper::removeFirstChar(Helper::strrstr($newFileName, '.', false), '.');
			}
			return $error;
		}

		public function asSimpleXML()
		{
			// TODO;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}