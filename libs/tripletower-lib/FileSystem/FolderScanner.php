<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\FileSystem
{

	use TripleTowerSDK\Helper\Helper;

	class FolderScanner
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		private $pathToScan;
		private $operator;
		private $factor;
		private $callBack;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $pathToScan, string  $operator = '>', int $factor = 0, $callBack = null)
		{
			$this->pathToScan	= $this->normalizePath($pathToScan);
			$this->operator		= $operator;
			$this->factor		= $factor;
			$this->callBack		= $callBack;
		}

		// Ausgabe der Ordnerliste und der entsprechenden Ordnergröße
		public function scanFolder()
		{
			$foundFileSizes = array();
			$handle = opendir($this->pathToScan);
			if(!empty($handle))
			{
				echo "Files:\n <br />";
				while(($file = readdir($handle)) !== false)
				{
					if($file === '.' || $file === '..')
						continue;

					$fileSize = Helper::getSize($this->pathToScan.$file);

					// Kontrolle ob die Größe der Datei oder des Verzeichnisses der Auswahl entspricht
					if($this->applyCondition($fileSize, $this->operator, $this->factor))
					{
						$humanReadableSize = Helper::getBytesHumanReadable($fileSize);	// Umrechnen auf die dementsprechende Mengeneinheit
						echo "$file: $humanReadableSize<br />";							// Ausgabe Ordner Name und Filegröße
						array_push($foundFileSizes, $fileSize);							// Elementen-Array erzeugen um den Inhalt zu zählen

						 // trigger callBack, which has been set by user
						if($this->callback)
							call_user_func($this->callBack, "$file = $humanReadableSize\n");
					}
				}
				closedir($handle);
			}

			$fileSizeSum = 0;
			for($loop=0; $loop < count($foundFileSizes); $loop++)
				$fileSizeSum += $foundFileSizes[$loop];

			echo "<br />Verzeichnis(se): " . count($foundFileSizes) . "<br /> Speicher gesamt: " . Helper::getBytesHumanReadable($fileSizeSum);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function applyCondition(int $file_size, string $operator, int $factor)
		{
			switch($operator)
			{
				case '<': return ($file_size < $factor);
				case '>': return ($file_size > $factor);
				case '=': return ($file_size == $factor);
				case '<=': return ($file_size <= $factor);
				case '>=': return ($file_size >= $factor);
				default: return true;
			}
		}
	}
	/*
	--------------- Beispiel wie die Klasse zu benutzen ist -----------------------

	$folderScanner	= new FolderScanner('C:/hallo', '>', '0', 'createLog');
	$folderScanner->scanFolder();
	*/
}