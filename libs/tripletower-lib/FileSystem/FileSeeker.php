<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\FileSystem
{

	final class FileSeeker
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function humanReadable(int $sizeInByte)
		{
			$fileSizes = array("B", "KiB", "MiB", "GiB", "TiB", "PiB");

			if($sizeInByte < 0)
				$sizeInByte = sprintf("%u", $sizeInByte);
			$sizeIndex = 0;
			for( ; $sizeInByte >= 1024; $sizeIndex++ )
				$sizeInByte >>= 10;

			return $sizeInByte . " " . $fileSizes[$sizeIndex];
		}

		public static function getContentType(string $filePathAndName)
		{

			switch(substr(strrchr($filePathAndName, '.'), 1))
			{
				// Images
				case 'jpg':	return 'image/jpeg';
				case 'jpeg': return 'image/jpeg';
				case 'png': return 'image/png';
				case 'gif': return 'image/gif';
				case 'bmp': return 'image/bmp';
				case 'ico': return 'image/vnd.microsoft.icon';
				case 'tif': return 'image/tiff';
				case 'tiff': return 'image/tiff';
				case 'tga': return 'image/tga';

				// Audio
				case 'ogg': return 'audio/ogg';
				case 'mp3': return 'audio/mpeg';
				case 'wav': return 'audio/vnd.wave';

				// Video
				case 'vob': return 'video/vob';
				case 'ogg': return 'video/ogg';
				case 'mpg': return 'video/mpeg';
				case 'mpeg': return 'video/mpeg';
				case 'mp4': return 'video/mp4';
				case 'mp4': return 'video/avi';

				default:
				  return 'text/plain';
			}
		}

		public static function returnFile(string $root, string $filepath)
		{
			$filename = $root.$filepath;
			$peclFileInfo = finfo_open(FILEINFO_MIME_TYPE);
			header("Content-Type: ".finfo_file($peclFileInfo, $filename));
			finfo_close($peclFileInfo);
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . filesize($filename));
			header("Accept-Ranges: bytes");
			header("Expires: 0");
			header("Cache-Control: private", false);
			flush();
			ob_clean();
			readfile($filename);
		}

		public static function returnDirectory(string $root, string $dir)
		{
			if($root[ strlen($root)-1] == '/')
				$root = substr($root,0,-1);

			if(($handle = opendir($root . $dir)) !== false)
			{
				header('Content-type: text/xml');
				echo '<?xml version="1.0" encoding="UTF-8"?>';
				echo '<dir path="' . $dir . '">';
				while(($fileName = readdir($handle)) !== false)
				{
					if($fileName == '.')
						continue;

					if(is_file($root.$dir.$fileName))
						echo '<file path="' . $dir . '" name="' . substr($fileName, 0, strrpos($fileName, '.')) . '" type="' . substr($fileName, strrpos($fileName, '.')+1) . '" size="' . self::humanReadable(filesize($root.$dir.$fileName)) . '" />';
					if(is_dir($root.$dir.$fileName))
						echo '<file path="' . $dir . '" name="' . $fileName . '" type="dir" size="' . self::humanReadable(0) . '" />';
				}
				echo '</dir>';
				closedir($handle);
			}
		}

		public static function searchForFile(string $root, string $path, string $pattern)
		{
			$completePath = self::normalizeDir($root.$path);
			$searchString = rtrim(str_replace("\\", "/", $completePath), '/') . '/*';
			foreach(glob($searchString) as $file)
			{
				$fileName = substr($file, strlen($completePath));
				if(preg_match($pattern, $fileName))
				{

					if(is_file($file))
						echo '<file path="' . $path . '" name="' . substr($fileName, 0, strrpos($fileName, '.')) . '" type="' . substr($fileName, strrpos($fileName, '.')+1) . '" size="' . self::humanReadable(filesize($file)) . '" />';
					if(is_dir($file))
						echo '<file path="' . $path . '" name="' . $fileName . '" type="dir" size="0" />';
				}
				if(is_dir($file))
					self::searchForFile($root, self::normalizeDir($path.$fileName), $pattern);
			}
		}

		public static function normalizeDir(string $dir)
		{
			$dir = rtrim($dir);						// remove all trailing whitespaces
			$dir = str_replace('%20', ' ', $dir);	// replace %20 by spaces
			$dir = str_replace('\\', '/', $dir);	// replace all \ by /
			if($dir[0] != '/' && $dir[1] != ':') 	// check if first element is / if not, add it
				$dir = '/' . $dir;

			if($dir[ strlen($dir)-1] != '/')		// check if at the end is a / if not, add it
				$dir .= '/';
			$dir = str_replace('/./', '/', $dir);	// replace all /./ by /
			$dir = str_replace('//', '/', $dir);	// replace all // by /

			// e.g. /folder1/folder2/../folder3/
			while( ($pos = strpos($dir, '/../')) !== false)
			{
				if($pos == 0)		// if there is something like /../ throw it away
					return null;

				$frontPartOfPath = substr($dir, 0, $pos);					// = /folder1/folder2
				$keepTill = strrpos($frontPartOfPath, '/');					// = 8
				$frontPartOfPath = substr($frontPartOfPath, 0, $keepTill);	// = /folder1
				$dir = $frontPartOfPath . substr($dir, $pos+3);				// = /folder1 . /folder3/
			}
			return $dir;
		}

		public static function normalizeFile(string $file)
		{
			$file = self::normalizeDir($file);
			if($file[ strlen($file)-1] == '/')		// check if at the end is a / if so, remove it
				$file = substr($file,0,-1);
			return $file;
		}

		private function __construct()
		{

		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}