<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\FileSystem
{

	final class FileAttribute
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/* A file that is read-only. Applications can read the file, but cannot
		 * write to it or delete it. This attribute is not honored on directories.
		 * For more information, see You cannot view or change the Read-only or
		 * the System attributes of folders in Windows Server 2003, in Windows XP,
		 * in Windows Vista or in Windows 7.
		 */
		const READONLY            = 0x1; // (1)


		/* The file or directory is hidden. It is not included in an ordinary
		 * directory listing.
		 */
		const HIDDEN              = 0x2; // (2)


		/* A file or directory that the operating system uses a part of,
		 * or uses exclusively.
		 */
		const SYSTEM              = 0x4; // (4)


		/* The handle that identifies a directory.
		 */
		const DIRECTORY           = 0x10; // (16)


		/* A file or directory that is an archive file or directory. Applications
		 * typically use this attribute to mark files for backup or removal.
		 */
		const ARCHIVE             = 0x20; // (32)


		/* This value is reserved for system use.
		 */
		const DEVICE              = 0x40; // (64)


		/* A file that does not have other attributes set. This attribute is
		 * valid only when used alone.
		 */
		const NORMAL              = 0x80; // (128)

		/* A file that is being used for temporary storage. File systems
		 * avoid writing data back to mass storage if sufficient cache
		 * memory is available, because typically, an application deletes
		 * a temporary file after the handle is closed. In that scenario,
		 * the system can entirely avoid writing the data. Otherwise, the
		 * data is written after the handle is closed.
		 */
		const TEMPORARY           = 0x100; // (256)

		/* A file that is a sparse file.
		 */
		const SPARSE_FILE         = 0x200; // (512)


		/* A file or directory that has an associated reparse point,
		 * or a file that is a symbolic link.
		 */
		const REPARSE_POINT       = 0x400; // (1024)


		/* A file or directory that is compressed. For a file, all of the data
		 * in the file is compressed. For a directory, compression is the default
		 * for newly created files and subdirectories.
		 */
		const COMPRESSED          = 0x800; // (2048)


		/* The data of a file is not available immediately. This attribute
		 * indicates that the file data is physically moved to offline storage.
		 * This attribute is used by Remote Storage, which is the hierarchical
		 * storage management software. Applications should not arbitrarily
		 * change this attribute.
		 */
		const OFFLINE             = 0x1000; // (4096)


		/* The file or directory is not to be indexed by the content indexing
		 * service.
		 */
		const NOT_CONTENT_INDEXED = 0x2000; // (8192)


		/* A file or directory that is encrypted. For a file, all data streams
		 * in the file are encrypted. For a directory, encryption is the default
		 * for newly created files and subdirectories.
		 */
		const ENCRYPTED           = 0x4000; // (16384)


		/* This value is reserved for system use. But used in the PHP part of
		 * the tripletower-lib to indicate that there is no physical file associtated
		 * with it.
		 */
		const VIRTUAL             = 0x10000; // (65536)


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		/*
		 * Binary respresentation of fileAttributes
		 * V_EIOCRSTNDAD_SHR
		 */
		private function __construct()
		{
		}
	}
}