<?php declare(strict_types=1);
/**
 * @name	ExternalScriptControl  (ESC)
 * @desc	Inter script signal handling via file system
 */

namespace TripleTowerSDK\Application
{
	use TripleTowerSDK\Error\SysError;

	class ConsoleGui
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $buffer = [];
		protected $bufferValid = false;

		protected $bufferSizeX = 0;
		protected $bufferSizeY = 0;
		protected $bufferCodePage = "";

		protected $originalScreenSizeX = 0;
		protected $originalScreenSizeY = 0;
		protected $originalCodePage = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(int $bufferSizeX=80, int $bufferSizeY=40, string $bufferCodePage = "")
		{
			$this->saveScreenSettings()->throwIfNotSuccess();
			$this->bufferSizeX = $bufferSizeX;
			$this->bufferSizeY = $bufferSizeY;
			$this->bufferCodePage = $bufferCodePage;
			$this->initFrameBuffer();
			$this->configureScreenSettings()->throwIfNotSuccess();
			$this->drawFrame();

			// Currently not available on windows because w32 builds are using
			// libedit instead of readline. And libedit is not providing all
			// functions readline does. Maybe NCurses builds will be available
			// in the future.
			if( is_callable(readline_callback_handler_install) )
			{
				readline_callback_handler_install("", function(){});
			}
		}

		public function getBufferSizeX() : int
		{
			return $this->bufferSizeX;
		}

		public function getBufferSizeY() : int
		{
			return $this->bufferSizeY;
		}

		public function readFromBuffer(int $x, int $y, int $length = 1)
		{
			if( $length <= 0 )
				return new SysError(SysError::INVALID_PARAM, SysError::ERR, "Could not get buffer line of length '$length'");

			$bufferLine = "";
			for($loop = 0; $loop < $length; $loop++)
			{
				if( $x+$loop >= $this->bufferSizeX || $y >= $this->bufferSizeY)
					return new SysError(SysError::OUT_OF_RANGE_ERROR, SysError::WARN, "Given frame point '" . $x+$loop . "/$y' lies outside of frameBuffer");
				$bufferLine .= $this->buffer[$y][$x+$loop];
			}
			return $bufferLine;
		}

		public function drawFrame() : void
		{
			if( $this->bufferValid === false )
			{
				$this->clearScreen();
				for($loopY = 0; $loopY < $this->bufferSizeY; $loopY++)
				{
					for($loopX = 0; $loopX < $this->bufferSizeX; $loopX++)
					{
						echo $this->buffer[$loopY][$loopX];
					}

					// Don't draw line ending for last Y line
					if( $loopY < $this->bufferSizeY-1 )
						echo PHP_EOL;
				}
				$this->bufferValid = true;
			}
		}

		public function reinitFrameBuffer(int $bufferSizeX, int $bufferSizeY) : void
		{
			$this->buffer = [];
			$this->bufferSizeX = $bufferSizeX;
			$this->bufferSizeY = $bufferSizeY;
			$this->initFrameBuffer();
			$this->configureScreenSettings();
			$this->drawFrame();
		}

		public function writeToBuffer(int $x, int $y, string $content, bool $forceImmediateRedraw = false)
		{
			if( $content === "" )
				return new SysError(SysError::SUCCESS);

			for($loop = 0; $loop < strlen($content); $loop++)
			{
				if( $x+$loop >= $this->bufferSizeX || $y >= $this->bufferSizeY)
					return new SysError(SysError::OUT_OF_RANGE_ERROR, SysError::WARN, "Given frame point '" . $x+$loop . "/$y' lies outside of frameBuffer");

				$this->buffer[$y][$x+$loop] = $content[$loop];
			}

			$this->bufferValid = false;

			if( $forceImmediateRedraw === true )
				$this->drawFrame();

			return new SysError(SysError::SUCCESS);
		}

		public function getKeysPressed() : array
		{
			$pressedCharacters = [];

			// Variables have to be defined because, stream select takes references
			$readStreams       = [ STDIN ];
			$writeStreams      = null;
			$exceptionStreams = null;
			$changedStreamResources = stream_select($r, $w, $e, 0);
			if($changedStreamResources >= 1 && $changedStreamResources !== false)
				while(($pressedKeyBinValue = stream_get_contents(STDIN, 1)) !== false )
					$pressedCharacters[] = ord($pressedKeyBinValue);

			return $pressedCharacters;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function initFrameBuffer() : void
		{
			$this->buffer = [];

			for($loopY = 0; $loopY < $this->bufferSizeY; $loopY++)
			{
				$this->buffer[$loopY] = [];
				for($loopX = 0; $loopX < $this->bufferSizeX; $loopX++)
				{
					$this->buffer[$loopY][$loopX] = ' ';
				}
			}
			$this->bufferValid = false;
		}

		protected function saveScreenSettings()
		{
			$modeCommandPattern =
				'[^0-9]+(?P<lines>[0-9]+)' .	// lines
				'[^0-9]+(?P<columns>[0-9]+)' .	// columns
				'[^0-9]+[0-9]+' .				// keyboard rate
				'[^0-9]+[0-9]+' .				// keyboard delay
				'[^0-9]+(?P<codepage>[0-9]+)' .	// code page
				'[^0-9]*';						// trailing new lines

			$modeCommandContent = shell_exec("mode con");
			if( $modeCommandContent === null )
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not get current screen sizes");

			// Remove all non-ascii characters (Only keep 0-127)
			$asciiOnlyModeCommandContent = preg_replace('/[^\x00-\x7F]/', "", $modeCommandContent);
			$matches = [];
			if( preg_match("/$modeCommandPattern/s", $asciiOnlyModeCommandContent, $matches) === false )
				return new SysError(SysError::SUBPROCESS_ERROR, SysError::ERR, "Could not parse screen sizes from mode");

			$this->originalScreenSizeX = (int)trim($matches["columns"]);
			$this->originalScreenSizeY = (int)trim($matches["lines"]);
			$this->originalCodePage = trim($matches["codepage"]);

			return new SysError(SysError::SUCCESS);
		}



		protected function configureScreenSettings()
		{
			system("mode con: cols=" . ($this->bufferSizeX + 1) . " lines=".($this->bufferSizeY+1));

			// Leave code page as is if not explicitly given
			if( $this->bufferCodePage !== "" )
				system("mode con: cp select=$this->bufferCodePage");
			return new SysError(SysError::SUCCESS);
		}

		protected function restoreScreenSettings()
		{
			system("mode con: cols=$this->originalScreenSizeX lines=$this->originalScreenSizeY");

			// Leave code page as is if not explicitly given
			if( $this->bufferCodePage !== "" )
				system("mode con: cp select=$this->originalCodePage");
		}

		protected function clearScreen()
		{
			system("cls");
			return new SysError(SysError::SUCCESS);
		}

		public function __destruct()
		{
			$this->restoreScreenSettings();
		}
	}
}