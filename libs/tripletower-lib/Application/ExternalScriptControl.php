<?php declare(strict_types=1);
/**
 * @name	ExternalScriptControl  (ESC)
 * @desc	Inter script signal handling via file system
 */

namespace TripleTowerSDK\Application
{

	class ExternalScriptControl
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PRIVATE ATTRIBUTES               *
		***********************************/

		private $signalPath;   /* Local path to the signal files         */
		private $signalList;   /* Array of signals and associated files  */
		private $lastErrorNr;  /* Error no. of the last call             */
		private $lastErrorMsg; /* Error message of the last call         */


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		function __construct(string $signalPath = '.')
		{
			// Default Path
			$this->signalPath = $signalPath;
			// Default Signals
			$this->addSignal("START" ,"ESC.START" );
			$this->addSignal("STOP"  ,"ESC.STOP"  );
			$this->addSignal("PAUSE" ,"ESC.PAUSE" );
			$this->addSignal("RESUME","ESC.RESUME");
			$this->addSignal("LOAD"  ,"ESC.LOAD"  );
			$this->addSignal("TEST"  ,"ESC.TEST"  ); // TEST  mode signal without acknowledge
			$this->addSignal("DEBUG" ,"ESC.DEBUG" ); // DEBUG mode signal without acknowledge
			// Default Acknowledge Signals
			$this->addSignal("STARTACK" ,"ESC.START.ACK" );
			$this->addSignal("STOPACK"  ,"ESC.STOP.ACK"  );
			$this->addSignal("PAUSEACK" ,"ESC.PAUSE.ACK" );
			$this->addSignal("RESUMEACK","ESC.RESUME.ACK");
			$this->addSignal("LOADACK"  ,"ESC.LOAD.ACK"  );

			$this->lastErrorNr  = 0;
			$this->lastErrorMsg = "SUCCESS";

		}

		public function setSignalPath(string $signalPath)
		{
			$this->signalPath   = $signalPath;

			$this->lastErrorNr  = 0;
			$this->lastErrorMsg = "SUCCESS";

			return true;

		}

		public function addSignal(string $signalName, string $signalFile)
		{
			$this->signalList[$signalName] = $signalFile;

			$this->lastErrorNr  = 0;
			$this->lastErrorMsg = "SUCCESS";

			return true;

		}

		public function setSignal(string $signalName)
		{
			// Check if signal exists
			if ( isset($this->signalList[$signalName]) )
			{
				// Create signal file
				$fn = $this->signalPath . "/" . $this->signalList[$signalName];
				if ( ($fh = fopen($fn,'w')) !== false )
				{
					// Opening a file for write access creates a new file if file does not exist, empties the file otherwise
					fclose($fh); // Close file without content

					$this->lastErrorNr  = 0;
					$this->lastErrorMsg = "SUCCESS";

					return true;
				}
				else
				{
					$this->lastErrorNr  = 1;
					$this->lastErrorMsg = "Signal ".$signalName." (file ".$fn.") could not be created!";

					return false;
				}
			}
			else
			{
				$this->lastErrorNr  = 2;
				$this->lastErrorMsg = "Signal ".$signalName." not defined!";

				return false;
			}
		}

		public function checkSignal(string $signalName)
		{
			// Check if signal exists
			if ( isset($this->signalList[$signalName]) )
			{
				// Check if signal file exists
				$fn = $this->signalPath . "/" . $this->signalList[$signalName];

				if ( file_exists($fn) )
				{
					$this->lastErrorNr  = 0;
					$this->lastErrorMsg = "SUCCESS";

					return true; // File exists = signal is set
				}
				else
				{
					$this->lastErrorNr  = 0;
					$this->lastErrorMsg = "SUCCESS";

					return false; // File does not exist = signal not set
				}
			}
			else
			{
				$this->lastErrorNr  = 2;
				$this->lastErrorMsg = "Signal ".$signalName." not defined!";

				return false;
			}

		}

		public function clearSignal(string $signalName)
		{
			// Check if signal exists
			if ( isset($this->signalList[$signalName]) )
			{
				// Delete signal file
				$fn = $this->signalPath . "/" . $this->signalList[$signalName];
				if (file_exists($fn))
				{
					if ( unlink($fn) )
					{
						$this->lastErrorNr  = 0;
						$this->lastErrorMsg = "SUCCESS";

						return true;
					}
					else
					{
						$this->lastErrorNr  = 3;
						$this->lastErrorMsg = "Signal ".$signalName." (file ".$fn.") could not be deleted!";

						return false;
					}
				}
				else
				{
					$this->lastErrorNr  = 4;
					$this->lastErrorMsg = "Signal ".$signalName." not set!";

					return false;
				}
			}
			else
			{
				$this->lastErrorNr  = 2;
				$this->lastErrorMsg = "Signal ".$signalName." not defined!";

				return false;
			}
		}

		public function getSignalPath()
		{
			$this->lastErrorNr  = 0;
			$this->lastErrorMsg = "SUCCESS";

			return $this->signalPath;

		}

		public function getSignalList()
		{
			$this->lastErrorNr  = 0;
			$this->lastErrorMsg = "SUCCESS";

			return $this->signalList;
		}

		public function getSignalFile(string $signalName)
		{
			if ( isset($this->signalList[$signalName]) )
			{
				$this->lastErrorNr  = 0;
				$this->lastErrorMsg = "SUCCESS";

				return $this->signalList[$signalName];
			}
			else
			{
				$this->lastErrorNr  = 2;
				$this->lastErrorMsg = "Signal ".$signalName." not defined!";

				return NULL;
			}
		}

		public function getLastErrorNr()
		{
			return $this->lastErrorNr;

		}

		public function getLastErrorMsg()
		{
			return $this->lastErrorMsg;
		}


		/***********************************
		* PRIVATE METHODS                  *
		***********************************/
	}
}