<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Application
{

	class OsNames
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/*
		 * Possible OS type values
		 */
		const OS_TYPE_LINUX   = 'Linux';
		const OS_TYPE_MACOSX  = 'MacOSX';
		const OS_TYPE_UNIX    = 'Unix';
		const OS_TYPE_WINDOWS = 'Windows';
		const OS_TYPE_UNKNOWN = 'unknown';

		/*
		 * Possible OS values
		 */
		const OS_AIX                 = 'AIX';
		const OS_CYGWIN_NT_5_1       = 'CYGWIN_NT-5.1';
		const OS_CYGWIN_NT_6_1_WOW64 = 'CYGWIN_NT-6.1-WOW64';
		const OS_DARWIN              = 'Darwin';
		const OS_DRAGONFLY           = 'DragonFly';
		const OS_FREEBSD             = 'FreeBSD';
		const OS_GNU_KFREEBSD        = 'GNU/kFreeBSD';
		const OS_HAIKU               = 'Haiku';
		const OS_HP_UX               = 'HP-UX';
		const OS_LINUX               = 'Linux';
		const OS_MINGW32_NT_6_0      = 'MINGW32_NT-6.0';
		const OS_MINIX               = 'Minix';
		const OS_NETBSD              = 'NetBSD';
		const OS_OPENBSD             = 'OpenBSD';
		const OS_SUNOS               = 'SunOS';
		const OS_UNIX                = 'Unix';
		const OS_UWIN_W7             = 'UWIN-W7';
		const OS_WIN32               = 'WIN32';
		const OS_WINNT               = 'WINNT';
		const OS_WINDOWS             = 'Windows';

		/*
		 * Linux OS releases
		 */
		const OS_RELEASE_LINUX_ANNVIX         = 'Annvix';
		const OS_RELEASE_LINUX_ARCH           = 'Arch';
		const OS_RELEASE_LINUX_ARKLINUX       = 'Arklinux';
		const OS_RELEASE_LINUX_AUROX          = 'Aurox';
		const OS_RELEASE_LINUX_BLACKCAT       = 'BlackCat';
		const OS_RELEASE_LINUX_CENTOS         = 'CentOS';
		const OS_RELEASE_LINUX_COBALT         = 'Cobalt';
		const OS_RELEASE_LINUX_CONECTIVA      = 'Conectiva';
		const OS_RELEASE_LINUX_DEBIAN         = 'Debian';
		const OS_RELEASE_LINUX_FEDORA         = 'Fedora';
		const OS_RELEASE_LINUX_FREEOS         = 'FreeEOS';
		const OS_RELEASE_LINUX_GENTOO         = 'Gentoo';
		const OS_RELEASE_LINUX_HLFS           = 'HLFS';
		const OS_RELEASE_LINUX_IMMUNIX        = 'Immunix';
		const OS_RELEASE_LINUX_KNOPPIX        = 'Knoppix';
		const OS_RELEASE_LINUX_FROM_SCRATCH   = 'Linux-From-Scratch';
		const OS_RELEASE_LINUX_PPC            = 'PPC';
		const OS_RELEASE_LINUX_MAGEIA         = 'Mageia';
		const OS_RELEASE_LINUX_MANDRAKE       = 'Mandrake';
		const OS_RELEASE_LINUX_MANDRIVA       = 'Mandriva';
		const OS_RELEASE_LINUX_MKLINUX        = 'MkLinux';
		const OS_RELEASE_LINUX_NOVELL         = 'Novell';
		const OS_RELEASE_LINUX_PLD            = 'PLD';
		const OS_RELEASE_LINUX_REDHAT         = 'Redhat';
		const OS_RELEASE_LINUX_RUBIX          = 'Rubix';
		const OS_RELEASE_LINUX_SLACKWARE      = 'Slackware';
		const OS_RELEASE_LINUX_SME            = 'SME';
		const OS_RELEASE_LINUX_SOLARIS        = 'Solaris';
		const OS_RELEASE_LINUX_SUNJDS         = 'SunJDS';
		const OS_RELEASE_LINUX_SUSE           = 'SuSE';
		const OS_RELEASE_LINUX_SYNOLOGY       = 'Synology';
		const OS_RELEASE_LINUX_TINY_SOFA      = 'Tiny-Sofa';
		const OS_RELEASE_LINUX_TRUSTIX        = 'Trustix';
		const OS_RELEASE_LINUX_TURBOLINUX     = 'TurboLinux';
		const OS_RELEASE_LINUX_UBUNTU         = 'Ubuntu';
		const OS_RELEASE_LINUX_ULTRAPENGUIN   = 'UltraPenguin';
		const OS_RELEASE_LINUX_UNITEDLINUX    = 'UnitedLinux';
		const OS_RELEASE_LINUX_VA             = 'VA';
		const OS_RELEASE_LINUX_YELLOWDOG      = 'YellowDog';

		/*
		 * MacOSX releases
		 */
		const OS_RELEASE_MACOSX_KODIAK        =  "3";
		const OS_RELEASE_MACOSX_CHEETAH       =  "4";
		const OS_RELEASE_MACOSX_PUMA          =  "5";
		const OS_RELEASE_MACOSX_JAGUAR        =  "6";
		const OS_RELEASE_MACOSX_PANTHER       =  "7";
		const OS_RELEASE_MACOSX_TIGER         =  "8";
		const OS_RELEASE_MACOSX_LEOPARD       =  "9";
		const OS_RELEASE_MACOSX_SNOW_LEOPARD  = "10";
		const OS_RELEASE_MACOSX_LION          = "11";
		const OS_RELEASE_MACOSX_MOUNTAIN_LION = "12";
		const OS_RELEASE_MACOSX_MAVERICKS     = "13";
		const OS_RELEASE_MACOSX_YOSEMITE      = "14";
		const OS_RELEASE_MACOSX_EL_CAPITAN    = "15";
		const OS_RELEASE_MACOSX_SIERRA        = "16";
		const OS_RELEASE_MACOSX_HIGH_SIERRA   = "17";
		const OS_RELEASE_MACOSX_MOJAVE        = "18";
		const OS_RELEASE_MACOSX_CATALINA      = "19";
		const OS_RELEASE_MACOSX_BIG_SUR       = "20";
		const OS_RELEASE_MACOSX_MONTEREY      = "21";
		const OS_RELEASE_MACOSX_VENTURA       = "22";

		/*
		 * Windows OS releases
		 */
		const OS_RELEASE_WINDOWS_1            = "1.4";
		const OS_RELEASE_WINDOWS_2_0          = "2.3";
		const OS_RELEASE_WINDOWS_2_1          = "2.11";
		const OS_RELEASE_WINDOWS_3_0          = "3.0";
		const OS_RELEASE_WINDOWS_3_11         = "3.11";
		const OS_RELEASE_WINDOWS_95           = "4.0";
		const OS_RELEASE_WINDOWS_98           = "4.1";
		const OS_RELEASE_WINDOWS_ME           = "4.9";
		const OS_RELEASE_WINDOWS_2000         = "5.0";
		const OS_RELEASE_WINDOWS_XP           = "5.1";
		const OS_RELEASE_WINDOWS_XP64         = "5.2";
		const OS_RELEASE_WINDOWS_VISTA        = "6.0";
		const OS_RELEASE_WINDOWS_7            = "6.1";
		const OS_RELEASE_WINDOWS_8            = "6.2";
		const OS_RELEASE_WINDOWS_8_1          = "6.3";
		const OS_RELEASE_WINDOWS_10           = "10.0";

		/*
		 * Unix OS releases and generics
		 */
		const OS_RELEASE_UNKNOWN              = "unknown";




		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function getOsTypes() : array
		{
			return
			[
				self::OS_TYPE_LINUX,
				self::OS_TYPE_MACOSX,
				self::OS_TYPE_UNIX,
				self::OS_TYPE_WINDOWS,
				self::OS_TYPE_UNKNOWN
			];
		}

		public static function getOsValues() : array
		{
			return
			[
				self::OS_AIX,
				self::OS_CYGWIN_NT_5_1,
				self::OS_CYGWIN_NT_6_1_WOW64,
				self::OS_DARWIN,
				self::OS_DRAGONFLY,
				self::OS_FREEBSD,
				self::OS_GNU_KFREEBSD,
				self::OS_HAIKU,
				self::OS_HP_UX,
				self::OS_LINUX,
				self::OS_MINGW32_NT_6_0,
				self::OS_MINIX,
				self::OS_NETBSD,
				self::OS_OPENBSD,
				self::OS_SUNOS,
				self::OS_UNIX,
				self::OS_UWIN_W7,
				self::OS_WIN32,
				self::OS_WINNT,
				self::OS_WINDOWS
			];
		}

		public static function getLinuxReleases() : array
		{
			return
			[
				self::OS_RELEASE_LINUX_ANNVIX,
				self::OS_RELEASE_LINUX_ARCH,
				self::OS_RELEASE_LINUX_ARKLINUX,
				self::OS_RELEASE_LINUX_AUROX,
				self::OS_RELEASE_LINUX_BLACKCAT,
				self::OS_RELEASE_LINUX_CENTOS,
				self::OS_RELEASE_LINUX_COBALT,
				self::OS_RELEASE_LINUX_CONECTIVA,
				self::OS_RELEASE_LINUX_DEBIAN,
				self::OS_RELEASE_LINUX_FEDORA,
				self::OS_RELEASE_LINUX_FREEOS,
				self::OS_RELEASE_LINUX_GENTOO,
				self::OS_RELEASE_LINUX_HLFS,
				self::OS_RELEASE_LINUX_IMMUNIX,
				self::OS_RELEASE_LINUX_KNOPPIX,
				self::OS_RELEASE_LINUX_FROM_SCRATCH,
				self::OS_RELEASE_LINUX_PPC,
				self::OS_RELEASE_LINUX_MAGEIA,
				self::OS_RELEASE_LINUX_MANDRAKE,
				self::OS_RELEASE_LINUX_MANDRIVA,
				self::OS_RELEASE_LINUX_MKLINUX,
				self::OS_RELEASE_LINUX_NOVELL,
				self::OS_RELEASE_LINUX_PLD,
				self::OS_RELEASE_LINUX_REDHAT,
				self::OS_RELEASE_LINUX_RUBIX,
				self::OS_RELEASE_LINUX_SLACKWARE,
				self::OS_RELEASE_LINUX_SME,
				self::OS_RELEASE_LINUX_SOLARIS,
				self::OS_RELEASE_LINUX_SUNJDS,
				self::OS_RELEASE_LINUX_SUSE,
				self::OS_RELEASE_LINUX_SYNOLOGY,
				self::OS_RELEASE_LINUX_TINY_SOFA,
				self::OS_RELEASE_LINUX_TRUSTIX,
				self::OS_RELEASE_LINUX_TURBOLINUX,
				self::OS_RELEASE_LINUX_UBUNTU,
				self::OS_RELEASE_LINUX_ULTRAPENGUIN,
				self::OS_RELEASE_LINUX_UNITEDLINUX,
				self::OS_RELEASE_LINUX_VA,
				self::OS_RELEASE_LINUX_YELLOWDOG
			];
		}

		public static function getMaxOSXReleases() : array
		{
			return
			[
				self::OS_RELEASE_MACOSX_KODIAK,
				self::OS_RELEASE_MACOSX_CHEETAH,
				self::OS_RELEASE_MACOSX_PUMA,
				self::OS_RELEASE_MACOSX_JAGUAR,
				self::OS_RELEASE_MACOSX_PANTHER,
				self::OS_RELEASE_MACOSX_TIGER,
				self::OS_RELEASE_MACOSX_LEOPARD,
				self::OS_RELEASE_MACOSX_SNOW_LEOPARD,
				self::OS_RELEASE_MACOSX_LION,
				self::OS_RELEASE_MACOSX_MOUNTAIN_LION,
				self::OS_RELEASE_MACOSX_MAVERICKS,
				self::OS_RELEASE_MACOSX_YOSEMITE,
				self::OS_RELEASE_MACOSX_EL_CAPITAN,
				self::OS_RELEASE_MACOSX_SIERRA,
				self::OS_RELEASE_MACOSX_HIGH_SIERRA,
				self::OS_RELEASE_MACOSX_MOJAVE,
				self::OS_RELEASE_MACOSX_CATALINA,
				self::OS_RELEASE_MACOSX_BIG_SUR,
				self::OS_RELEASE_MACOSX_MONTEREY,
				self::OS_RELEASE_MACOSX_VENTURA
			];
		}

		public static function getUnixReleases() : array
		{
			return
			[
				// Dirty hack, treat unixes as if each has only one release/variant
				self::OS_AIX,
				self::OS_DRAGONFLY,
				self::OS_FREEBSD,
				self::OS_GNU_KFREEBSD,
				self::OS_HP_UX,
				self::OS_NETBSD,
				self::OS_OPENBSD,
				self::OS_SUNOS,
				self::OS_UNIX
			];
		}

		public static function getWindowsReleases() : array
		{
			return
			[
				self::OS_RELEASE_WINDOWS_1,
				self::OS_RELEASE_WINDOWS_2_0,
				self::OS_RELEASE_WINDOWS_2_1,
				self::OS_RELEASE_WINDOWS_3_0,
				self::OS_RELEASE_WINDOWS_3_11,
				self::OS_RELEASE_WINDOWS_95,
				self::OS_RELEASE_WINDOWS_98,
				self::OS_RELEASE_WINDOWS_ME,
				self::OS_RELEASE_WINDOWS_2000,
				self::OS_RELEASE_WINDOWS_XP,
				self::OS_RELEASE_WINDOWS_XP64,
				self::OS_RELEASE_WINDOWS_VISTA,
				self::OS_RELEASE_WINDOWS_7,
				self::OS_RELEASE_WINDOWS_8,
				self::OS_RELEASE_WINDOWS_8_1,
				self::OS_RELEASE_WINDOWS_10
			];
		}

		public static function getCurrentOsType() : string
		{
			if( self::isLinux() )
				return self::OS_TYPE_LINUX;
			if( self::isMacOSX() )
				return self::OS_TYPE_MACOSX;
			if( self::isUnixoid() )
				return self::OS_TYPE_UNIX;
			if( self::isWindows() )
				return self::OS_TYPE_WINDOWS;

			return self::OS_TYPE_UNKNOWN;
		}

		public static function getCurrentOsRelease() : string
		{
			switch( self::getCurrentOsType() )
			{
				case self::OS_TYPE_LINUX:
				{
					$releaseFiles =
					[
						"/etc/annvix-release"       => self::OS_RELEASE_LINUX_ANNVIX,
						"/etc/arch-release"         => self::OS_RELEASE_LINUX_ARCH,
						"/etc/arklinux-release"     => self::OS_RELEASE_LINUX_ARKLINUX,
						"/etc/aurox-release"        => self::OS_RELEASE_LINUX_AUROX,
						"/etc/blackcat-release"     => self::OS_RELEASE_LINUX_BLACKCAT,
						"/etc/centos-release"       => self::OS_RELEASE_LINUX_CENTOS,
						"/etc/cobalt-release"       => self::OS_RELEASE_LINUX_COBALT,
						"/etc/conectiva-release"    => self::OS_RELEASE_LINUX_CONECTIVA,
						"/etc/debian_version"       => self::OS_RELEASE_LINUX_DEBIAN,
						"/etc/debian_release"       => self::OS_RELEASE_LINUX_DEBIAN,
						"/etc/fedora-release"       => self::OS_RELEASE_LINUX_FEDORA,
						"/etc/eos-version"          => self::OS_RELEASE_LINUX_FREEOS,
						"/etc/gentoo-release"       => self::OS_RELEASE_LINUX_GENTOO,
						"/etc/hlfs-release"         => self::OS_RELEASE_LINUX_HLFS,
						"/etc/hlfs_version"         => self::OS_RELEASE_LINUX_HLFS,
						"/etc/immunix-release"      => self::OS_RELEASE_LINUX_IMMUNIX,
						"knoppix_version"           => self::OS_RELEASE_LINUX_KNOPPIX,
						"/etc/lfs-release"          => self::OS_RELEASE_LINUX_FROM_SCRATCH,
						"/etc/lfs_version"          => self::OS_RELEASE_LINUX_FROM_SCRATCH,
						"/etc/linuxppc-release"     => self::OS_RELEASE_LINUX_PPC,
						"/etc/mageia-release"       => self::OS_RELEASE_LINUX_MAGEIA,
						"/etc/mandrake-release"     => self::OS_RELEASE_LINUX_MANDRAKE,
						"/etc/mandakelinux-release" => self::OS_RELEASE_LINUX_MANDRAKE,
						"/etc/mandriva-release"     => self::OS_RELEASE_LINUX_MANDRIVA,
						"/etc/mklinux-release"      => self::OS_RELEASE_LINUX_MKLINUX,
						"/etc/nld-release"          => self::OS_RELEASE_LINUX_NOVELL,
						"/etc/pld-release"          => self::OS_RELEASE_LINUX_PLD,
						"/etc/redhat-release"       => self::OS_RELEASE_LINUX_REDHAT,
						"/etc/redhat_version"       => self::OS_RELEASE_LINUX_REDHAT,
						"/etc/rubix-version"        => self::OS_RELEASE_LINUX_RUBIX,
						"/etc/slackware-version"    => self::OS_RELEASE_LINUX_SLACKWARE,
						"/etc/slackware-release"    => self::OS_RELEASE_LINUX_SLACKWARE,
						"/etc/e-smith-release"      => self::OS_RELEASE_LINUX_SME,
						"/etc/release"              => self::OS_RELEASE_LINUX_SOLARIS,
						"/etc/sun-release"          => self::OS_RELEASE_LINUX_SUNJDS,
						"/etc/SuSE-release"         => self::OS_RELEASE_LINUX_SUSE,
						"/etc/novell-release"       => self::OS_RELEASE_LINUX_SUSE,
						"/etc/sles-release"         => self::OS_RELEASE_LINUX_SUSE,
						"/etc/synoinfo.conf"        => self::OS_RELEASE_LINUX_SYNOLOGY,
						"/etc/tinysofa-release"     => self::OS_RELEASE_LINUX_TINY_SOFA,
						"/etc/trustix-release"      => self::OS_RELEASE_LINUX_TRUSTIX,
						"/etc/trustix-version"      => self::OS_RELEASE_LINUX_TRUSTIX,
						"/etc/turbolinux-release"   => self::OS_RELEASE_LINUX_TURBOLINUX,
						"/etc/lsb-release"          => self::OS_RELEASE_LINUX_UBUNTU,
						"/etc/ultrapenguin-release" => self::OS_RELEASE_LINUX_ULTRAPENGUIN,
						"/etc/UnitedLinux-release"  => self::OS_RELEASE_LINUX_UNITEDLINUX,
						"/etc/va-release"           => self::OS_RELEASE_LINUX_VA,
						"/etc/yellowdog-release"    => self::OS_RELEASE_LINUX_YELLOWDOG
					];

					// Search in etc for a folder with the distro name
					foreach( array_keys($releaseFiles) as $releaseFile)
						if( file_exists($releaseFile) )
							return $releaseFiles[$releaseFile];

					return self::OS_RELEASE_UNKNOWN;
				}

				case self::OS_TYPE_MACOSX:
				{
					// This has to be verified on some Macs
					// but hardware is currently not available
					$darwinRelease = php_uname("r");
					return in_array($darwinRelease, self::getMaxOSXReleases()) ? $darwinRelease : self::OS_RELEASE_UNKNOWN;
				}

				case self::OS_TYPE_UNIX;
				{
					// Dirty hack, treat unixes as if each has only one release/variant
					return PHP_OS;
				}

				case self::OS_TYPE_WINDOWS:
				{
					$windowsRelease = php_uname("r");
					return in_array($windowsRelease, self::getWindowsReleases()) ? $windowsRelease : self::OS_RELEASE_UNKNOWN;
				}

				default:
					return self::OS_RELEASE_UNKNOWN;
			}
		}

		public static function isCurrentOs(string $osName) : bool
		{
			return $osName === PHP_OS;
		}

		public static function isCurrentOsType(string $osType) : bool
		{
			return $osType === self::getCurrentOsType();
		}

		public static function isLinux(string $osName = "") : bool
		{
			$osName = empty($osName) ? PHP_OS : $osName;
			switch( $osName )
			{
				case self::OS_LINUX:
					return true;

				default:
					return false;
			}
		}

		public static function isLinuxRelease(string $osRelease) : bool
		{
			return self::isLinux() && in_array( $osRelease, self::getLinuxReleases() );
		}

		public static function isMacOSX(string $osName = "") : bool
		{
			$osName = empty($osName) ? PHP_OS : $osName;
			switch( $osName )
			{
				case self::OS_DARWIN:
					return true;

				default:
					return false;
			}
		}

		public static function isMaxOSXRelease(string $osRelease) : bool
		{
			return self::isMacOSX() && in_array( $osRelease, self::getMaxOSXReleases() );
		}

		public static function isUnixoid(string $osName = "", string $osRelease = "") : bool
		{
			$osName = empty($osName) ? PHP_OS : $osName;
			$osRelease = $osName; // Dirty hack, treat unixes as if each has only one release/variant
			switch( $osName )
			{
				case self::OS_AIX:
				case self::OS_DRAGONFLY:
				case self::OS_FREEBSD:
				case self::OS_GNU_KFREEBSD:
				case self::OS_HP_UX:
				case self::OS_NETBSD:
				case self::OS_OPENBSD:
				case self::OS_SUNOS:
				case self::OS_UNIX:
					return true;

				default:
					return false;
			}
		}

		public static function isUnixRelease(string $osRelease) : bool
		{
			return self::isUnixoid() && in_array( $osRelease, self::getUnixReleases() );
		}

		public static function isWindows(string $osName = "") : bool
		{
			$osName = empty($osName) ? PHP_OS : $osName;
			switch( $osName )
			{
				case self::OS_CYGWIN_NT_5_1:
				case self::OS_CYGWIN_NT_6_1_WOW64:
				case self::OS_MINGW32_NT_6_0:
				case self::OS_UWIN_W7:
				case self::OS_WIN32:
				case self::OS_WINNT:
				case self::OS_WINDOWS:
					return true;

				default:
					return false;
			}
		}

		public static function isWindowsRelease(string $osRelease) : bool
		{
			return self::isWindows() && in_array( $osRelease, self::getWindowsReleases() );
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}