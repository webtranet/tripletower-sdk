<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Application
{

	class SapiNames
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/*
		 * Possible SAPI values
		 */
		const SAPI_AOLSERVER      = 'aolserver';
		const SAPI_APACHE         = 'apache';
		const SAPI_APACHE2FILTER  = 'apache2filter';
		const SAPI_APACHE2HANDLER = 'apache2handler';
		const SAPI_CAUDIUM        = 'caudium';
		const SAPI_CGI            = 'cgi';
		const SAPI_CGI_FCGI       = 'cgi-fcgi';
		const SAPI_CLI            = 'cli';
		const SAPI_CONTINUITY     = 'continuity';
		const SAPI_EMBED          = 'embed';
		const SAPI_ISAPI          = 'isapi';
		const SAPI_LIGHTSPEED     = 'litespeed';
		const SAPI_MILTER         = 'milter';
		const SAPI_NSAPI          = 'nsapi';
		const SAPI_PHTTPD         = 'phttpd';
		const SAPI_PI3WEB         = 'pi3web';
		const SAPI_ROXEN          = 'roxen';
		const SAPI_THTTPD         = 'thttpd';
		const SAPI_TUX            = 'tux';
		const SAPI_WEBJAMES       = 'webjames';


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function isCurrentSapi(string $sapiName)
		{
			return php_sapi_name() === $sapiName;
		}

		public static function isCliSapi(string $sapiName = "")
		{
			$sapiName = empty($sapiName) ? php_sapi_name() : $sapiName;
			return $sapiName === self::SAPI_CLI;
		}

		public static function getCurrentSapi()
		{
			return php_sapi_name();
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}