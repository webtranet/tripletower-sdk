<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{
	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\LdapError;
	use TripleTowerSDK\Error\SysError;

	class OpenLdapASC extends AuthSystemConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const LDAP_CONNECTION_UNENCRYPTED = "unencrypted";
		const LDAP_CONNECTION_LDAPS       = "ldaps";
		const LDAP_CONNECTION_STARTTLS    = "starttls";
 		const LDAP_CONNECTION_SASL        = "sasl";
 		const LDAP_CONNECTION_GC          = "gc";
		const LDAP_CONNECTION_GCSECURE    = "gcsecure";

		const LDAP_DEBUG_OFF        = 0x000;
		const LDAP_DEBUG_TRACE      = 0x001;
		const LDAP_DEBUG_PACKETS    = 0x002;
		const LDAP_DEBUG_ARGS       = 0x004;
		const LDAP_DEBUG_CONNS      = 0x008;
		const LDAP_DEBUG_BER        = 0x010;
		const LDAP_DEBUG_FILTER     = 0x020;
		const LDAP_DEBUG_CONFIG     = 0x040;
		const LDAP_DEBUG_ACL        = 0x080;
		const LDAP_DEBUG_STATS      = 0x100;
		const LDAP_DEBUG_STATS2     = 0x200;
		const LDAP_DEBUG_SHELL      = 0x400;
		const LDAP_DEBUG_PARSE      = 0x800;
		const LDAP_DEBUG_ANY        = 0xffff;

		const DEFAULT_LDAP_GROUP_IDENTIFIER = "GID";
		const DEFAULT_LDAP_USER_IDENTIFIER  = "UID";

		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected string $iconUrl = "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/openldap.png";

		protected $ldapConnection = null;
		protected bool $ldapBound = false;

		protected string $connectionType    = self::LDAP_CONNECTION_UNENCRYPTED;
		protected int $ldapDebugLevel       = self::LDAP_DEBUG_OFF;
		protected int $ldapProtocolVersion  = 3;
		protected bool $ldapFollowReferrals = true;
		protected string $anonymousUser     = "anonymous";
		protected string $baseDn            = "";

		protected $userBuffer = [];
		protected $groupBuffer = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/
		public static function convertServerNameToBaseDn(string $serverName)
		{
			// Converts my.example.com to dc=my,dc=example,dc=com
			$domainParts = array_map(function($domainPart)
			{
				return "dc=$domainPart";
			}, explode(".", trim( strtolower($serverName) )));
			return implode(",", $domainParts);
		}

		public static function convertBaseDnToServerName($baseDn)
		{
			// Converts dc=my,dc=example,dc=com to my.example.com
			$domainParts = array_map(function($domainPart)
			{
				$trimmedDomainPart = trim($domainPart);
				return (strpos($trimmedDomainPart, "dc=")===0 ? substr($trimmedDomainPart, 3) : $trimmedDomainPart);
			}, explode(",", strtolower($baseDn)));
			return implode(".", $domainParts);
		}

		public function __construct(string $hostName, string $userName, string $password, array $options)
		{
			parent::__construct($hostName, $userName, $password, $options);
			if( empty($hostName) )
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Hostname can not be empty");

			if( !empty($options["connectiontype"]) )
				$this->connectionType = (string)$options["connectiontype"];

			if( !empty($options["ldapprotocolversion"]) )
				$this->ldapProtocolVersion = (int)$options["ldapprotocolversion"];

			if( !empty($options["ldapfollowreferrals"]) )
				$this->ldapFollowReferrals = (bool)$options["ldapfollowreferrals"];

			if( !empty($options["anonymoususer"]) )
				$this->anonymousUser = (string)$options["anonymoususer"];

			$this->groupIdentifier = $options["groupidentifier"] ?? self::DEFAULT_LDAP_GROUP_IDENTIFIER;
			$this->userIdentifier  = $options["useridentifier"] ?? self::DEFAULT_LDAP_USER_IDENTIFIER;

			if( !empty($options["ldapdebuglevel"]) )
				$this->ldapDebugLevel = (string)$options["ldapdebuglevel"];
			if( $this->connectionType !== self::LDAP_CONNECTION_UNENCRYPTED && $this->connectionType !== self::LDAP_CONNECTION_GC )
				putenv( "LDAPTLS_CACERT=" . ($options["caCertificateFilePath"] ?? ($_SERVER["TT"]->tower->getEtcFolder() . "ssl/certs/ca-bundle.crt")) );

			$this->baseDn = (string)($options["basedn"] ?? self::convertServerNameToBaseDn($hostName));

			// If user or password is empty, set both to null for anonymous binding
			if( $userName === "" || $password === "" )
			{
				$this->userName = "";
				$this->password = "";
			}
			else
			{
				$this->userName = strtolower($userName);
				$this->password = $password;
			}

			$this->connect()->throwIfNotSuccess();
			$this->setOptions()->throwIfNotSuccess();
		}

		public function authenticateUser(string &$domainAndUserName, string $password)
		{
			// Check for SingleSignOn if username is empty
			if( $domainAndUserName === "" )
			{
				// Check and get SingleSignOn user
				$ssoUser = $this->checkSingleSignOnUser();
				if( TripleTowerError::isError($ssoUser) && !$ssoUser->isSameError(new AuthError(AuthError::USER_NOT_FOUND, AuthError::SILENT)) )
					return $ssoUser;

				// WATCH OUT!
				// This alters the username for the caller
				if( is_string($ssoUser) )
					$domainAndUserName = $ssoUser;

				return new AuthError(AuthError::SUCCESS);
			}

			try
			{
				// Test if you could bind with user's password
				$openLdapAS = new $this($this->hostName, $domainAndUserName, $password, $this->options);
				$openLdapAS->bind()->throwIfNotSuccess();
				unset($openLdapAS);
				return new AuthError(AuthError::SUCCESS);
			}
			catch(\Exception $ex)
			{
				return new AuthError(AuthError::WRONG_USERNAME_OR_PASSWORD, AuthError::ERR, "Could not authenticate with user '$domainAndUserName' to '$this->hostName'");
			}
		}

		public function checkUserEnrollment(string &$authName, string $password)
		{
			// For LDAP/AD -> To check the user's availability in
			// the directory is the same as authenticating him
			return $this->authenticateUser($authName, $password);
		}

		public function checkUserInGroup(string $authName, string $accessGroupName) : TripleTowerError
		{
			$groups = $this->getGroupsByUser( $authName, false );
			if( TripleTowerError::isError($groups) )
				return $groups;

			foreach( $groups as $group )
			{
				if( strtolower($group) === strtolower($accessGroupName) )
					return new AuthError(AuthError::SUCCESS);
			}

			return new AuthError(AuthError::ACCESS_DENIED, AuthError::SILENT);
		}

		public function checkSingleSignOnUser()
		{
			// Check if there is a directory user
			if( !isset($_SERVER["REMOTE_USER"]) )
				return new AuthError(AuthError::USER_NOT_FOUND, AuthError::SILENT, "No remote user detected");

			// If it's just anonymous
			$domainAndUserName = strtolower(mb_convert_encoding($_SERVER["REMOTE_USER"], "UTF-8", "ASCII"));
			if( $domainAndUserName === strtolower($this->anonymousUser) )
				return new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT, "Remote user is anonymous");

			// Split username if a domain is prepended
			list($userDomain, $userName) = ( strpos($domainAndUserName, "\\") !== false ? explode("\\", $domainAndUserName) : ["", $domainAndUserName] );

			// If you have a single domain - you may SSPIOmitDomain to true in apache's servicetower.conf to remove domain part
			$searchResult = $this->searchBySamAccountName($userName, true);
			if( TripleTowerError::isError($searchResult) )
				return new AuthError(AuthError::INVALID_USER_NAME, AuthError::SILENT, "User '$domainAndUserName' tried single sign on, but user could not be found in directory '$this->hostName'");

			return $domainAndUserName;
		}

		public function isBound() : bool
		{
			return $this->ldapBound;
		}

		public function isUnbound() : bool
		{
			return !$this->ldapBound;
		}

		public function searchByFilter(string $filter, array $attributes = ["*"], int $limit = 0)
		{
			$bindResult = $this->bind();
			if( !$bindResult->isSuccess() )
				return $bindResult;


			if( ($searchQuery = @ldap_search($this->ldapConnection, $this->baseDn, $filter, $attributes, 0, $limit)) === false )
				return new LdapError(LdapError::SEARCH_FAILED, LdapError::ERR, "Could not search within '$this->baseDn' with user '$this->userName' and filter '$filter' because of: " . ldap_error($this->ldapConnection));

			if( ($results = @ldap_get_entries($this->ldapConnection, $searchQuery)) === false || (int)$results["count"] === 0 )
				return new LdapError(LdapError::NO_ENTRIES_FOUND, LdapError::SILENT, "No entry found within '$this->baseDn' for filter '$filter'");

			return $results;
		}

		// Available in core.schema
		public function searchByCountry(string $country, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $country : "*$country*");
			return $this->searchByFilter("c=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByCommonName(string $commonName, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $commonName : "*$commonName*");
			return $this->searchByFilter("cn=$filterParam", $attributes, $limit);
		}

		public function searchByDisplayName(string $displayName, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $displayName : "*$displayName*");
			return $this->searchByFilter("displayname=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByDomainComponent(string $domainComponent, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $domainComponent : "*$domainComponent*");
			return $this->searchByFilter("dc=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByFax(string $faxNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $faxNumber : "*$faxNumber*");
			return $this->searchByFilter("facsimileTelephoneNumber=$filterParam", $attributes, $limit);
		}

		// Available in cosine.schema
		public function searchByFriendlyCountryName(string $domainComponent, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $domainComponent : "*$domainComponent*");
			return $this->searchByFilter("co=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByGivenName(string $givenName, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $givenName : "*$givenName*");
			return $this->searchByFilter("gn=$filterParam", $attributes, $limit);
		}

		// Available in cosine.schema
		public function searchByHomePhone(string $phoneNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $phoneNumber : "*$phoneNumber*");
			return $this->searchByFilter("homePhone=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByLocality(string $locality, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $locality : "*$locality*");
			return $this->searchByFilter("l=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByMail(string $mail, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $mail : "*$mail*");
			return $this->searchByFilter("mail=$filterParam", $attributes, $limit);
		}

		// Available in cosine.schema
		public function searchByMobilePhone(string $phoneNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $phoneNumber : "*$phoneNumber*");
			return $this->searchByFilter("mobile=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByOrganization(string $organization, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $organization : "*$organization*");
			return $this->searchByFilter("o=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByOrganisationalUnit(string $organisationalUnit, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $organisationalUnit : "*$organisationalUnit*");
			return $this->searchByFilter("ou=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByOwner(string $owner, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $owner : "*$owner*");
			return $this->searchByFilter("owner=$filterParam", $attributes, $limit);
		}

		// Available in cosine.schema
		public function searchByPager(string $pagerNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $pagerNumber : "*$pagerNumber*");
			return $this->searchByFilter("pager=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByPostalAddress(string $postalAddress, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $postalAddress : "*$postalAddress*");
			return $this->searchByFilter("postalAddress=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByPostalCode(string $postalCode, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $postalCode : "*$postalCode*");
			return $this->searchByFilter("postalCode=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchBySamAccountName(string $samAccountName, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $samAccountName : "*$samAccountName*");
			return $this->searchByFilter("sAMAccountName=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchBySerialNumber(string $serialNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $serialNumber : "*$serialNumber*");
			return $this->searchByFilter("serialNumber=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByStateOrProvince(string $stateOrProvince, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $stateOrProvince : "*$stateOrProvince*");
			return $this->searchByFilter("st=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByStreetAddress(string $streetAddress, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $streetAddress : "*$streetAddress*");
			return $this->searchByFilter("street=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchBySurname(string $surname, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $surname : "*$surname*");
			return $this->searchByFilter("sn=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByTelephone(string $phoneNumber, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $phoneNumber : "*$phoneNumber*");
			return $this->searchByFilter("telephoneNumber=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByTitle(string $title, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $title : "*$title*");
			return $this->searchByFilter("title=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByUserId(string $userId, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $userId : "*$userId*");
			return $this->searchByFilter("uid=$filterParam", $attributes, $limit);
		}

		// Available in core.schema
		public function searchByUserPassword(string $password, bool $exact=true, array $attributes = ["*"], int $limit = 0)
		{
			$filterParam = ($exact ? $password : "*$password*");
			return $this->searchByFilter("userPassword=$filterParam", $attributes, $limit);
		}

		public function getGroupsByUser( string $userName, bool $recursive = false )
		{
			$userName = $this->extractUserNameFromDomainUser($userName);
			$lowerUserName = strtolower($userName);

			if( isset($this->userBuffer[$lowerUserName]["groups"]) )
			{
				if( !$recursive )
					return $this->userBuffer[$lowerUserName]["groups"];

				if( isset($this->userBuffer[$lowerUserName]["nestedGroups"]) )
					return array_merge($this->userBuffer[$lowerUserName]["groups"], $this->userBuffer[$lowerUserName]["nestedGroups"]);
			}

			$result = $this->searchBySamAccountName($userName, true, ["distinguishedName"]);

			if( TripleTowerError::isError($result) )
				return $result;

			$distinguishedName = $result[0]["distinguishedname"][0];

			$filter = "member=$distinguishedName";
			$result = $this->searchByFilter($filter, ["distinguishedName", "samAccountName"]);

			if( TripleTowerError::isError($result) )
				return $result;

			unset($result["count"]);
			$memberDNs = [];

			$groups = [];
			foreach( $result as $resultEntrie )
			{
				$groups[] = $resultEntrie["samaccountname"][0];
				$memberDNs[] = $resultEntrie["distinguishedname"][0];
			}

			$this->userBuffer[$lowerUserName]["groups"] = $groups;

			if( !$recursive )
				return $this->userBuffer[$lowerUserName]["groups"];

			$nestedGroups = [];
			$memberDNsToIgnore = [];
			while( !empty($memberDNs) )
			{
				array_push($memberDNsToIgnore, ...$memberDNs);

				$filter = "(|(member=" . implode(")(member=", $memberDNs) . "))";
				$result = $this->searchByFilter($filter, ["distinguishedName", "samAccountName"]);

				if( TripleTowerError::isError($result) )
				{
					if( $result->isSameError(new LdapError(LdapError::NO_ENTRIES_FOUND, LdapError::SILENT)) )
						break;

					return $result;
				}

				unset($result["count"]);
				$memberDNs = [];

				foreach( $result as $resultEntrie )
				{
					$nestedGroups[] = $resultEntrie["samaccountname"][0];
					$memberDNs[] = $resultEntrie["distinguishedname"][0];
				}

				$memberDNs = array_diff($memberDNs, $memberDNsToIgnore);
			}

			$this->userBuffer[$lowerUserName]["nestedGroups"] = $nestedGroups;

			return array_merge($this->userBuffer[$lowerUserName]["groups"], $this->userBuffer[$lowerUserName]["nestedGroups"]);
		}

		public function getUsersByGroup( string $groupName, bool $recursive = true )
		{
			$lowerGroupName = strtolower($groupName);
			if( isset($this->groupBuffer[$lowerGroupName]["users"]) )
			{
				if( !$recursive )
					return $this->groupBuffer[$lowerGroupName]["users"];

				if( isset($this->groupBuffer[$lowerGroupName]["nestedUsers"]) )
					return array_merge($this->groupBuffer[$lowerGroupName]["users"], $this->groupBuffer[$lowerGroupName]["nestedUsers"]);
			}

			$result = $this->searchBySamAccountName($groupName, true, ["distinguishedName"]);

			if( TripleTowerError::isError($result) )
				return $result;

			$distinguishedName = $result[0]["distinguishedname"][0];

			$filter = "memberOf=$distinguishedName";
			$result = $this->searchByFilter($filter, ["distinguishedName", "samAccountName", "objectClass"]);

			if( TripleTowerError::isError($result) )
				return $result;

			unset($result["count"]);
			$groupDNs = [];
			$users = [];

			foreach( $result as $resultEntrie )
			{
				if( array_search("user", $result[0]["objectclass"]) )
					$users[] = $resultEntrie["samaccountname"][0];
				else
					$groupDNs[] = $resultEntrie["distinguishedname"][0];
			}

			$this->groupBuffer[$lowerGroupName]["users"] = $users;

			if( !$recursive )
				return $this->groupBuffer[$lowerGroupName]["users"];

			$nestedUsers = [];
			$groupDNsToIgnore = [];
			while( !empty($groupDNs) )
			{
				array_push($groupDNsToIgnore, ...$groupDNs);

				$filter = "(|(memberOf=" . implode(")(memberOf=", $groupDNs) . "))";
				$result = $this->searchByFilter($filter, ["distinguishedName", "samAccountName", "objectClass"]);

				if( TripleTowerError::isError($result) )
				{
					if( $result->isSameError(new LdapError(LdapError::NO_ENTRIES_FOUND, LdapError::SILENT)) )
						break;

					return $result;
				}

				unset($result["count"]);
				$groupDNs = [];

				foreach( $result as $resultEntrie )
				{
					if( array_search("user", $result[0]["objectclass"]) )
						$nestedUsers[] = $resultEntrie["samaccountname"][0];
					else
						$groupDNs[] = $resultEntrie["distinguishedname"][0];
				}

				$groupDNs = array_diff($groupDNs, $groupDNsToIgnore);
			}

			$this->groupBuffer[$lowerGroupName]["nestedUsers"] = $nestedUsers;

			return array_merge($this->groupBuffer[$lowerGroupName]["users"], $this->groupBuffer[$lowerGroupName]["nestedUsers"]);
		}

		public function __destruct()
		{
			$this->disconnect();
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function extractUserNameFromDomainUser( string $domainUser ) : string
		{
			$parts = explode( "\\", $domainUser );
			return array_pop( $parts );
		}

		protected function extractGroupIdsFromDistinguishedNames( string ...$distinguishedNames ) : array
		{
			$groupIds = [];

			foreach( $distinguishedNames as $distinguishedName )
			{
				if(preg_match("/.*?" . $this->groupIdentifier . "=(?<groupIds>.*?),.*/", $distinguishedName, $matches))
					$groupIds[] = $matches["groupIds"];
			}
			return $groupIds;
		}

		protected function extractUserIdsFromDistinguishedNames( string ...$distinguishedNames ) : array
		{
			$userIds = [];

			foreach( $distinguishedNames as $distinguishedName )
			{
				if(preg_match("/.*?" . $this->userIdentifier . "=(?<userIds>.*?),.*/", $distinguishedName, $matches))
					$userIds[] = $matches["userIds"];
			}
			return $userIds;
		}

		protected function connect()
		{
			if( !empty($this->ldapConnection) || $this->isBound() )
				return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not connect to ldap host '$this->hostName' while already connected");

			switch( $this->connectionType )
			{
				case self::LDAP_CONNECTION_UNENCRYPTED:
				{
					if( ($this->ldapConnection = @ldap_connect($this->hostName)) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not unencryptedly connect to ldap host '$this->hostName'");
				}
				break;

				case self::LDAP_CONNECTION_LDAPS:
				{
					// For this to work there has to be a "tls_reqcert never" in C:\openldap\sysconf\ldap.conf on windows
					if( ($this->ldapConnection = @ldap_connect("ldaps://$this->hostName:636")) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not ldaps connect to 'ldaps://$this->hostName:636'");
				}
				break;

				case self::LDAP_CONNECTION_STARTTLS:
				{
					if( ($this->ldapConnection = @ldap_connect($this->hostName, 389)) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not unencryptedly connect to '$this->hostName:389'");
				}
				break;

				case self::LDAP_CONNECTION_SASL:
				{
					if( ($this->ldapConnection = @ldap_connect($this->hostName, 389)) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not unencryptedly connect to '$this->hostName:389'");
				}
				break;

				case self::LDAP_CONNECTION_GC:
				{
					if( ($this->ldapConnection = @ldap_connect("$this->hostName", 3268)) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not ldaps connect globally to '$this->hostName:3268'");
				}
				break;

				case self::LDAP_CONNECTION_GCSECURE:
				{
					if( ($this->ldapConnection = @ldap_connect("ldaps://$this->hostName", 3269)) === false )
						return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Could not ldaps connect globally to '$this->hostName:3269'");
				}
				break;

				default:
				{
					return new LdapError(LdapError::CONNECT_FAILED, LdapError::ERR, "Unknown ldap connection type '$this->connectionType' given to connect to '$this->hostName'");
				}
			}

			return new LdapError(LdapError::SUCCESS);
		}

		protected function setOptions()
		{
			if( $this->isBound() )
				return new LdapError( LdapError::SETOPTION_FAILED, LdapError::ERR, "Cannot set option when already bound to host '$this->hostName'.");

			if( ldap_set_option($this->ldapConnection, LDAP_OPT_DEBUG_LEVEL, $this->ldapDebugLevel) === false )
				return new LdapError( LdapError::SETOPTION_FAILED, LdapError::ERR, "Could not set ldap option 'LDAP_OPT_DEBUG_LEVEL' to '$this->ldapDebugLevel' for connection with host '$this->hostName'");

			if( ldap_set_option($this->ldapConnection, LDAP_OPT_PROTOCOL_VERSION, $this->ldapProtocolVersion) === false )
				return new LdapError( LdapError::SETOPTION_FAILED, LdapError::ERR, "Could not set ldap option 'LDAP_OPT_PROTOCOL_VERSION' to '$this->ldapProtocolVersion' for connection with host '$this->hostName'");

			if( ldap_set_option($this->ldapConnection, LDAP_OPT_REFERRALS, $this->ldapFollowReferrals) === false )
				return new LdapError( LdapError::SETOPTION_FAILED, LdapError::ERR, "Could not set ldap option 'LDAP_OPT_REFERRALS' to '" . ($this->ldapFollowReferrals?"true":"false") . "' for connection with host '$this->hostName'");

			return new LdapError(LdapError::SUCCESS);
		}

		protected function bind()
		{
			if( $this->isUnbound() )
			{
				switch( $this->connectionType )
				{
					case self::LDAP_CONNECTION_UNENCRYPTED:
					{
						if( @ldap_bind($this->ldapConnection, $this->userName, utf8_encode($this->password)) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not bind to unencrypted connection '$this->hostName' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					case self::LDAP_CONNECTION_LDAPS:
					{
						if( @ldap_bind($this->ldapConnection, $this->userName, $this->password) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not bind to ldaps connection 'ldaps://$this->hostName:636' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					case self::LDAP_CONNECTION_STARTTLS:
					{
						if( @ldap_start_tls($this->ldapConnection) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not start_tls to connection '$this->hostName:389' because of: " . ldap_error($this->ldapConnection));

						if( @ldap_bind($this->ldapConnection, $this->userName, $this->password) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not bind to start_tls-connection '$this->hostName:389' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					case self::LDAP_CONNECTION_SASL:
					{
						if( @ldap_sasl_bind($this->ldapConnection, null, $this->password, null, null, $this->userName) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not sasl-bind to connection '$this->hostName:389' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					case self::LDAP_CONNECTION_GC:
					{
						if( @ldap_bind($this->ldapConnection, $this->userName, $this->password) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not bind to global connection '$this->hostName:3268' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					case self::LDAP_CONNECTION_GCSECURE:
					{
						if( @ldap_bind($this->ldapConnection, $this->userName, $this->password) === false )
							return new LdapError( LdapError::BIND_FAILED, LdapError::ERR, "Could not bind to secure global connection '$this->hostName:3269' with user '$this->userName' because of: " . ldap_error($this->ldapConnection));
					}
					break;

					default:
					{
						return new LdapError(LdapError::BIND_FAILED, LdapError::ERR, "Unknown ldap connection type '$this->connectionType' given to bind to '$this->hostName'");
					}
				}
				$this->ldapBound = true;
			}

			return new LdapError(LdapError::SUCCESS);
		}

		protected function unbind()
		{
			if( $this->isUnbound() )
				return new LdapError(LdapError::SUCCESS);

			$this->ldapBound = (@ldap_unbind($this->ldapConnection) !== true);
			if( $this->isBound() )
				return new LdapError(LdapError::DISCONNECT_FAILED, LdapError::ERR, "Could not unbind from ldap host '$this->hostName'");

			return new LdapError(LdapError::SUCCESS);
		}

		protected function disconnect()
		{
			$unbindResult = $this->unbind();
			if( !$unbindResult->isSuccess() )
				return $unbindResult;

			if( !empty($this->ldapConnection) )
				$this->ldapConnection = null;

			return new LdapError(LdapError::SUCCESS);
		}
	}
}
