<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{

	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\User;

	class TripleTowerASC extends AuthSystemConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/
		const SESSION_COOKIE_EXPIRED = 1; // 1970-01-01 00:00:01

		const SESSION_HEADER_SESSION_SOURCE        = "TT-Auth-Session-Source";
		const SESSION_HEADER_CRYPTED_USERNAME      = "TT-Auth-Crypted-Username";
		const SESSION_HEADER_CRYPTED_SESSION_TOKEN = "TT-Auth-Crypted-Session-Token";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected string $iconUrl = "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/tripletower_auth.png";

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/
		public function __construct(string $hostName, string $userName, string $password, array $options)
		{
			parent::__construct($hostName, $userName, $password, $options);

			$towerDb = $_SERVER["TT"]->dataBases->{$_SERVER["TT"]->tower->getTowerInstance() . "Db"};
			if( TripleTowerError::isError($towerDb) )
				throw $towerDb;
		}

		public function authenticateUser(string &$authName, string $password)
		{
			$userHash = $_SERVER["TT"]->dataBases->serviceTowerDb->getUserHash($authName, $this->getAuthSystemName());
			if( Helper::validateBCryptHash($password, $userHash) !== true )
				return new AuthError(AuthError::WRONG_USERNAME_OR_PASSWORD, AuthError::ERR, "You entered a wrong password");

			return new AuthError(AuthError::SUCCESS);
		}

		public function checkUserEnrollment(string &$authName, string $password)
		{
			return new AuthError(AuthError::SUCCESS);
		}
		
		public function checkUserInGroup( string $authName, string $groupName ) : TripleTowerError
		{
			$towerDb = $_SERVER["TT"]->dataBases->{$_SERVER["TT"]->tower->getTowerInstance() . "Db"};
			return $towerDb->checkUserInGroup( $authName, $groupName );
		}

		
		// For authenticated users applies:
		// ServiceTower: $userName is <username>
		// Wappstower: $userName is <username>@<servicetower>
		public function getAuthenticatedUser()
		{
			$userSession = $this->getUserSession();

			// No authentication provided
			if( TripleTowerError::isError($userSession) ) 
			{
				if( $_SERVER["TT"]->tower->isServiceTower() )
					return new User( User::DEFAULT_USERNAME );
				else
					return new User(User::DEFAULT_USERNAME, "", User::INFO_NOINFO);
			}
			
			// Get public/private keys
			if( !($privateKey = file_get_contents($_SERVER["TT"]->conf->privateKeyPath)) )
				return new SysError(SysError::INVALID_FILE_NAME, SysError::ALERT, "Could not read privateKey from privateKeyPath '" . $_SERVER["TT"]->conf->privateKeyPath . "'");

			if( !($publicKey = file_get_contents($_SERVER["TT"]->conf->certificatePath)) )
				return new SysError(SysError::INVALID_FILE_NAME, SysError::ALERT, "Could not read certificate from certificatePath '" . $_SERVER["TT"]->conf->certificatePath . "'");

			// Decryption of content
			$userName = Helper::openSslDecrypt(base64_decode($userSession["cryptedUserName"]), $publicKey, $privateKey);
			if( TripleTowerError::isError($userName) )
			{
				$this->removeCookieSession();
				return $_SERVER["TT"]->tower->isServiceTower() ? new User( User::DEFAULT_USERNAME ) : $userName;
			}

			$serviceTowerServer = "";
			if( $_SERVER["TT"]->tower->isWappsTower() )
				list($userName, $serviceTowerServer) = Helper::splitTowerFromName($userName); // Split userName@servicetower.com

			$sessionToken = Helper::openSslDecrypt(base64_decode($userSession["cryptedSessionToken"]), $publicKey, $privateKey);
			if( TripleTowerError::isError($sessionToken) )
			{
				$this->removeCookieSession();
				return $_SERVER["TT"]->tower->isServiceTower() ? new User( User::DEFAULT_USERNAME ) : $sessionToken;
			}

			// Validation of authentication
			$authentication = null;
			if( $_SERVER["TT"]->tower->isServiceTower() )
				$authentication = $_SERVER["TT"]->dataBases->serviceTowerDb->isUserAuthenticated($userName, $sessionToken, $userSession["remoteAddress"]);
			else
				$authentication = $_SERVER["TT"]->dataBases->wappsTowerDb->isUserAuthenticated($serviceTowerServer, $userName, $sessionToken, $_SERVER["REMOTE_ADDR"]);

			if( !$authentication->isSuccess() )
			{
				$this->removeCookieSession();
				return $_SERVER["TT"]->tower->isServiceTower() ? new User(User::DEFAULT_USERNAME) : $authentication;
			}

			// Successful authentication
			if( $_SERVER["TT"]->tower->isServiceTower() )
				return new User($userName);
			else
				return new User("$userName@$serviceTowerServer", "", User::INFO_NOINFO);
		}

		public function getUserSession()
		{
			$userSession = new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT);
			if( $_SERVER["TT"]->isInDevelopmentMode() )
				$userSession = $this->readUrlSession();
			
			if( TripleTowerError::isErrorAndSame($userSession, new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT)) )
				$userSession = $this->readHeaderSession();
			
			if( TripleTowerError::isErrorAndSame($userSession, new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT)) )
				$userSession = $this->readCookieSession();

			return $userSession;
		}

		/***********************************
		* PROTECTED METHODS                  *
		***********************************/

		protected function readUrlSession()
		{
			$sessionSource = $_REQUEST["sessionSource"] ?? null;
			$cryptedUserName = $_REQUEST["cryptedUserName"] ?? null;
			$cryptedSessionToken = $_REQUEST["cryptedSessionToken"] ?? null;

			if( empty($sessionSource) && empty($cryptedUserName) && empty($cryptedSessionToken) )
				return new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT );

			if( empty($sessionSource) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "sessionSource was not found." );
			if( empty($cryptedUserName) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedUserName was not found." );
			if( empty($cryptedSessionToken) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedSessionToken was not found." );

			$session =
			[
				"towerType" => $_SERVER["TT"]->tower->getTowerInstance(),
				"towerName" => $_SERVER["TT"]->tower->getDisplayName(),
				"towerUrl" => $_SERVER["SERVER_NAME"] . "/" . $_SERVER["TT"]->tower->getTowerInstance(),
				"sessionSource" => $_REQUEST["sessionSource"],
				"cryptedUserName" => $_REQUEST["cryptedUserName"],
				"cryptedSessionToken" => $_REQUEST["cryptedSessionToken"],
				"remoteAddress" => $_SERVER["REMOTE_ADDR"]
			];

			if($_SERVER["REQUEST_METHOD"] !== "GET")
				array_walk($session, 'rawurldecode');

			return $session;
		}
		
		protected function readHeaderSession()
		{
			$sessionSourceHeaderName = strtoupper( str_replace("-", "_", self::SESSION_HEADER_SESSION_SOURCE) );
			$sessionSource = $_SERVER["HTTP_$sessionSourceHeaderName"] ?? $_SERVER["HTTP_X_$sessionSourceHeaderName"] ?? null;
			
			$cryptedUserNameHeaderName = strtoupper( str_replace("-", "_", self::SESSION_HEADER_CRYPTED_USERNAME) );
			$cryptedUserName = $_SERVER["HTTP_$cryptedUserNameHeaderName"] ?? $_SERVER["HTTP_X_$cryptedUserNameHeaderName"] ?? null;
			
			$cryptedSessionTokenHeaderName = strtoupper( str_replace("-", "_", self::SESSION_HEADER_CRYPTED_SESSION_TOKEN) );
			$cryptedSessionToken = $_SERVER["HTTP_$cryptedSessionTokenHeaderName"] ?? $_SERVER["HTTP_X_$cryptedSessionTokenHeaderName"] ?? null;
			
			if( empty($sessionSource) && empty($cryptedUserName) && empty($cryptedSessionToken) )
				return new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT );

			if( empty($sessionSource) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "sessionSource was not found." );
			if( empty($cryptedUserName) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedUserName was not found." );
			if( empty($cryptedSessionToken) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedSessionToken was not found." );

			$session =
			[
				"towerType" => $_SERVER["TT"]->tower->getTowerInstance(),
				"towerName" => $_SERVER["TT"]->tower->getDisplayName(),
				"towerUrl" => $_SERVER["SERVER_NAME"] . "/" . $_SERVER["TT"]->tower->getTowerInstance(),
				"sessionSource" => $sessionSource,
				"cryptedUserName" => $cryptedUserName,
				"cryptedSessionToken" => $cryptedSessionToken,
				"remoteAddress" => $_SERVER["REMOTE_ADDR"]
			];

			return $session;
		}

		protected function readCookieSession()
		{
			$sessionCookieName = $_SERVER["TT"]->tower->getTowerInstance() . "-session";

			if( !isset($_COOKIE[$sessionCookieName]) )
				return new AuthError(AuthError::NO_SESSION_SPECIFIED, AuthError::SILENT );

			$sessionCookie = json_decode($_COOKIE[$sessionCookieName]);

			if( !isset($sessionCookie->sessionSource) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "sessionSource was not found." );
			if( !isset($sessionCookie->cryptedUserName) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedUserName was not found." );
			if( !isset($sessionCookie->cryptedSessionToken) )
				return new AuthError(AuthError::INVALID_SESSION, AuthError::NOTICE, "cryptedSessionToken was not found." );

			$session =
			[
				"towerType" => $_SERVER["TT"]->tower->getTowerInstance(),
				"towerName" => $_SERVER["TT"]->tower->getDisplayName(),
				"towerUrl" => $_SERVER["SERVER_NAME"] . "/" . $_SERVER["TT"]->tower->getTowerInstance(),
				"sessionSource" => $sessionCookie->sessionSource,
				"cryptedUserName" => $sessionCookie->cryptedUserName,
				"cryptedSessionToken" => $sessionCookie->cryptedSessionToken,
				"remoteAddress" => $_SERVER["REMOTE_ADDR"]
			];

			return $session;
		}

		protected function removeCookieSession()
		{
			$sessionCookieName = $_SERVER["TT"]->tower->getTowerInstance() . "-session";
			$sessionCookiePath = "/".$_SERVER["TT"]->tower->getTowerInstance();
			if( $_SERVER["TT"]->tower->isWappsTower() )
				$sessionCookiePath .= "/".$_SERVER["TT"]->contentProvider;

			setcookie( $sessionCookieName, "", self::SESSION_COOKIE_EXPIRED, $sessionCookiePath, "", true );
		}
	}
}
