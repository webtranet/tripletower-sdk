<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{

	use TripleTowerSDK\Error\AuthError;

	class NoAuthASC extends AuthSystemConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/
		protected string $iconUrl = "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/lock_open.png";

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/
		public function __construct(string $hostName, string $userName, string $password, array $options)
		{
			parent::__construct($hostName, $userName, $password, $options);
		}

		public function authenticateUser(string &$authName, string $password)
		{
			return new AuthError(AuthError::SUCCESS);
		}

		public function checkUserEnrollment(string &$authName, string $password)
		{
			return new AuthError(AuthError::INVALID_AUTHSYSTEM, AuthError::ERR, "You cannot check the user's enrollment for AuthSystem '" . get_class($this));
		}
		
		public function checkUserInGroup(string $authName, string $group) : TripleTowerError
		{
			return new AuthError(AuthError::ACCESS_DENIED, AuthError::ERR, "You cannot check if user '$authName' is in group '$group' for AuthSystem '" . get_class($this));
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/

	}
}
