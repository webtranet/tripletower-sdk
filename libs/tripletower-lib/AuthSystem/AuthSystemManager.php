<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{
	use TripleTowerSDK\AuthSystem\AuthSystemConnector;
	use TripleTowerSDK\Error\DevError;
	use TripleTowerSDK\Error\NetworkError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;

	class AuthSystemManager
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $data = [];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct()
		{

		}

		public function getAvailableAuthSystems() : array
		{
			$availableAuthSystems = [];
			foreach($_SERVER["TT"]->conf as $configKey => $configValue)
			{
				if( !isset($configValue->ascName) || !Helper::isClassRelative($configValue->ascName, "TripleTowerSDK\\AuthSystem\\AuthSystemConnector") )
					continue;

				$authSystemName = AuthSystemConnector::extractAuthSystemName($configValue->ascName);
				if( TripleTowerError::isError($authSystemName) )
					continue;

				$availableAuthSystems[] = $authSystemName;
			}
			return $availableAuthSystems;
		}

		public function addAuthSystem(string $linkName, JsonConfig $asConf)
		{
			if(!$asConf)
				throw new SysError(SysError::INVALID_PARAM);

			$linkName = strtolower($linkName);
			if($linkName === "")
				throw new SysError(SysError::EMPTY_STRING_ERROR);
			if(array_key_exists($linkName, $this->data))
				throw new SysError(SysError::PROPERTY_ALREADY_EXISTS);

			try
			{
				if( !class_exists($asConf->ascName ))
				{
					require_once $asConf->ascName.".php";
					if(!class_exists($asConf->ascName))
						throw new DevError(DevError::CLASS_NOT_FOUND, DevError::NOTICE, "Class '$asConf->ascName' not available.");
				}

				$options = $this->readAuthSystemOptions($asConf);
				$as = new $asConf->ascName($asConf->hostName, $asConf->userName, $asConf->password, $options);
				$this->data[$linkName] = $as;
				return $as;
			}
			catch(\Throwable $t)
			{
				throw new NetworkError(NetworkError::CONNECTION_FAILED, NetworkError::ERR, $t->getMessage());
			}
		}

		public function removeAuthSystem(string $linkName)
		{
			$linkName = strtolower($linkName);
			if(!array_key_exists($linkName, $this->data))
				throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The authSystem '$linkName' could not be removed.");

			unset($this->data[$linkName]);
		}

		public function __get(string $key)
		{
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
			{
				return $this->data[$key];
			}
			else
			{
				// AuthSystem auto-connect
				$authSystemConf = $_SERVER["TT"]->conf->$key;
				if($authSystemConf !== null)
					return $this->addAuthSystem($key, $authSystemConf);
				else
					throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The property '$key' is not available.");

			}
		}

		public function __isset(string $key)
		{
			// AuthSystem should not auto-connect when isset() is used,
			// it should just test if there is an open connection
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
				return true;
			else
				return false;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function readAuthSystemOptions($asConf)
		{
			$ignoreList = ["ascname", "hostname", "username", "password"];
			$options = [];
			foreach($asConf as $key => $value)
			{
				if( in_array(strtolower($key), $ignoreList) )
					continue;

				$options[$key] = $value;
			}

			return $options;
		}

	}
}