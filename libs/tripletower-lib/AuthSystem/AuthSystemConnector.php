<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\TripleTowerError;

	abstract class AuthSystemConnector
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/
		const ALLOWED_CHARACTERS_AUTHNAME = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' .
											'!$%&=ß+#-_.,;@ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüÿ¢£€¥ƒ¤©®™•§¶Çç«»€œ¿¡' .
											'\\'; // FS unallowed characters, but necessary for AD authentication

		const ALLOWED_CHARACTERS_PASSWORD = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' .
											'!$%&=ß+#-_.,;@ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüÿ¢£€¥ƒ¤©®™•§¶Çç«»€œ¿¡' .
											'²³\'°^()[]{}' . // Authname unallowed characters
											'\\/:*?"<>|';    // FS unallowed characters

		const MINLENGTH_AUTHNAME = 3;
		const MAXLENGTH_AUTHNAME = 255;
		const MINLENGTH_PASSWORD = 8;
		const MAXLENGTH_PASSWORD = 255;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected string $hostName = "";
		protected string $userName = "";
		protected string $password = "";
		protected array $options  = [];
		protected string $iconUrl = "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/emoticons/emotion_question.png";


		/***********************************
		 * ABSTRACT METHODS                *
		***********************************/

		abstract public function authenticateUser(string &$authName, string $password);
		abstract public function checkUserEnrollment(string &$authName, string $password);
		abstract public function checkUserInGroup(string $authName, string $group) : TripleTowerError;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/
		
		public static function extractAuthSystemName(string $ascClassName)
		{
			$classNameParts = explode("\\", $ascClassName);
			$className = end( $classNameParts );

			// If class has no ASC at the end, append AS
			if( strtoupper(substr($className, -3)) !== "ASC" )
				return "$className.AS";

			// If class consists only of the three letters ASC
			if( empty(substr($className, 0, -3)) )
				return new AuthError(AuthError::INVALID_AUTHSYSTEM, AuthError::ERR, "AuthSystem can't just consist of the letters ASC");

			// If everything ok with the AuthSystem remove the "C" from "ASC"
			return substr($className, 0, -1);
		}
		
		public static function extractAuthSystemDisplayName(string $ascClassName)
		{
			$authSystemName = self::extractAuthSystemName($ascClassName);
			if( TripleTowerError::isError($authSystemName) )
				return $authSystemName;

			// Just remove AS appendix
			return substr( $authSystemName, 0, -2 );
		}
		
		/**
		 * @name __construct
		 * Constructor for the ASCs.
		 *
		 * @param string $hostName
		 * @param string $userName
		 * @param string $password
		 * @param array $options
		 * @return ASC Object
		 *
		 */
		public function __construct(string $hostName, string $userName, string $password, array $options)
		{
			$this->hostName = trim($hostName);
			$this->userName = trim($userName);
			//$this->password = $password; // By default passwords are not stored for security reasons
			$this->options  = $options;
		}

		public function getHostName() : string
		{
			return $this->hostName;
		}

		public function getUserName() : string
		{
			return $this->userName;
		}

		public function getIconUrl() : string
		{
			return $this->iconUrl;
		}

		public function getOptions() : array
		{
			return $this->options;
		}

		public function getOption(string $option)
		{
			$loweredOption = strtolower($option);
			if( !isset($this->options[$loweredOption]) )
				return new ConfigError(ConfigError::PARAMETER_NOT_FOUND, ConfigError::ERR, "Paramter '$loweredOption' seems to be no option for authSystem '" . get_class($this) . "'");

			return $this->options[$loweredOption];
		}

		public function getAuthSystemName()
		{
			return self::extractAuthSystemName(get_class($this));
		}

		public function getAuthSystemDisplayName()
		{
			return self::extractAuthSystemDisplayName(get_class($this));
		}

		/**********************************
		* PROTECTED METHODS               *
		**********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/


	}
}