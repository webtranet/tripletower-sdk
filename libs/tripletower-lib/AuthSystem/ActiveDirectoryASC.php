<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\AuthSystem
{

	use TripleTowerSDK\Error\ConfigError;

	class ActiveDirectoryASC extends OpenLdapASC
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/
		protected string $iconUrl = "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/active_directory.png";

		protected string $connectionType    = self::LDAP_CONNECTION_GCSECURE;
		protected int $ldapProtocolVersion  = 3;
		protected bool $ldapFollowReferrals = false;
		protected string $anonymousUser     = "NT AUTHORITY\\ANONYMOUS LOGON";

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $hostName, string $userName, string $password, array $options)
		{
			parent::__construct($hostName, $userName, $password, $options);

			if( isset($options["ldapProtocolVersion"]) )
				throw new ConfigError(ConfigError::INVALID_CONFIGURATION, ConfigError::ERR, "ldapProtocolVersion cannot be set in config for ActiveDirectoryASC");
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}
