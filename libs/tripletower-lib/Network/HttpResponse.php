<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Network
{

	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\HttpError;
	use TripleTowerSDK\Error\TripleTowerError;

	class HttpResponse
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $body = null;
		protected $headers = null;
		protected $responseCode = null;
		protected $certChain = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(array $headers, $body, int $responseCode, array $certChain = null)
		{
			$this->body = $body;
			$this->headers = $headers;
			$this->responseCode = $responseCode;
			$this->certChain = $certChain;
		}

		public function getBody() : string
		{
			return $this->body;
		}

		public function getCertInfo()
		{
			return $this->certInfo;
		}

		public function getHeaders() : array
		{
			return $this->headers;
		}

		public function checkHeaderExits( string $headerName ) : TripleTowerError
		{
			if( !isset($this->headers[$headerName]) )
				return new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::SILENT, "Header with name '$headerName' does not exist.");

			return new SysError(SysError::SUCCESS);
		}

		public function getHeaderValue(string $headerName) // : string or TripleTowerError
		{
			$result = $this->checkHeaderExits( $headerName );
			if( !$result->isSuccess() )
				return $result;

			return $this->headers[$headerName];
		}

		public function compareHeaderValue(string $headerName, $compareValues) : TripleTowerError
		{
			if( TripleTowerError::isError($headerValue = $this->getHeaderValue($headerName)) )
				return $headerValue;

			if( is_array($compareValues) ? (array_search($headerValue, $compareValues) === false) : ($headerValue !== $compareValues) )
				return new SysError(SysError::OUT_OF_RANGE_ERROR, SysError::SILENT, "Value '$headerValue' from header '$headerName' does not match the compare values.");

			return new SysError(SysError::SUCCESS);
		}

		public function getResponseCode() : int
		{
			return $this->responseCode;
		}

		public function checkResponseCode($httpResponseCodesForSuccess) : TripleTowerError
		{
			if( !is_array($httpResponseCodesForSuccess) )
				$httpResponseCodesForSuccess = [$httpResponseCodesForSuccess];

			if( array_search($this->responseCode, $httpResponseCodesForSuccess) === false )
			{
				if( is_string($this->body) && $this->body !== "")
					return new HttpError($this->responseCode, HttpError::ERR, $this->body);

				return new SysError(SysError::OUT_OF_RANGE_ERROR, SysError::ERR, "Response code '$this->responseCode' does not meet values for success.");
			}
			return new SysError(SysError::SUCCESS);
		}

		public function getCertChain()
		{
			return $this->certChain;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}