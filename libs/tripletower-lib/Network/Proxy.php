<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Network
{

	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;

	class Proxy
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $proxyUrl  = "";
		protected $proxyPort = 0;
		protected $proxyUserName = "";
		protected $proxyPassword = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $proxyUrl="", int $proxyPort=8080, string $proxyUserName="", string $proxyPassword="")
		{
			if( $proxyUrl === "" )
			{
				$proxyUrlFromRegistry = Helper::getProxyServerAndPortFromRegistry();
				if( TripleTowerError::isError($proxyUrlFromRegistry) === false )
					$proxyUrl = $proxyUrlFromRegistry;
			}

			$proxyParts = explode(":", $proxyUrl);
			if( count($proxyParts) === 2 )
			{
				$this->proxyUrl = $proxyParts[0];
				$this->proxyPort = $proxyParts[1];
			}
			else
			{
				$this->proxyUrl = $proxyUrl;
				$this->proxyPort = $proxyPort;
			}

			$this->proxyUserName = $proxyUserName;
			$this->proxyPassword = $proxyPassword;
		}

		function getProxyUrl()
		{
			return (string)$this->proxyUrl;
		}

		function getProxyPort()
		{
			return (string)$this->proxyPort;
		}

		function getEncodedAuthString()
		{
			return base64_encode( "$this->proxyUserName:$this->proxyPassword" );
		}

		function getProxyString()
		{
			return "$this->proxyUrl:$this->proxyPort";
		}

		function addProxyToContext($context)
		{
			// Context has to be destroyed because it's a reference
			$oldContextOptions = is_resource($context) ? stream_context_get_options($context) : [];
			$context = null;
			unset($context);

			$httpOptions["proxy"] = "$this->proxyUrl:$this->proxyPort";
			if( empty($this->proxyUserName) === false )
				$httpOptions["header"] = "Proxy-Authorization: Basic " . $this->getEncodedAuthString();

			$newContextOptions = array_merge($oldContextOptions,
			[
				"http" => $httpOptions
			]);

			return stream_context_create($newContextOptions);
		}

		function removeProxyFromContext($context)
		{
			// Context has to be destroyed because it's a reference
			$contextOptions = is_resource($context) ? stream_context_get_options($context) : [];
			$context = null;
			unset($context);

			if( isset($contextOptions["http"]) )
			{
				if( isset($contextOptions["http"]["proxy"]) )
					unset($contextOptions["http"]["proxy"]);
				if( isset($contextOptions["http"]["header"]) )
					unset($contextOptions["http"]["header"]);
			}

			if( empty($contextOptions["http"]) )
				unset($contextOptions["http"]);

			return stream_context_create($contextOptions);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}
