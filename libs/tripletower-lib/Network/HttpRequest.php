<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Network
{
	use TripleTowerSDK\Error\HttpError;
	use TripleTowerSDK\Network\HttpResponse;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;

	class HttpRequest
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const CURLAUTH_CLIENTCERT = 0;

		const HTTP_GET		= "GET";
		const HTTP_HEAD		= "HEAD";
		const HTTP_POST		= "POST";
		const HTTP_PUT		= "PUT";
		const HTTP_DELETE	= "DELETE";
		const HTTP_CONNECT	= "CONNECT";
		const HTTP_OPTIONS	= "OPTIONS";
		const HTTP_TRACE	= "TRACE";
		const HTTP_PATCH	= "PATCH";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $url = "";
		protected $headers = [];
		protected $caCertificateFilePath = "";
		protected $timeOutValue = 0;
		protected $cookies = null;
		protected $getCertChainInfo;
		protected $useNewCurlHandle;
		protected $identity;
		protected $password;
		protected $httpAuthType;

		protected static $curlHandle = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $url, array $headers, string $caCertificateFilePath, int $timeOutValue, array $cookies = null, bool $getCertChainInfo = false, bool $useNewCurlHandle = false,
			?string $identity = null, ?string $password = null, int|string $httpAuthType = CURLAUTH_NTLM)
		{
			$this->url                   = $url;
			$this->headers               = $headers;
			$this->caCertificateFilePath = $caCertificateFilePath;
			$this->timeOutValue          = $timeOutValue;
			$this->cookies               = $cookies;
			$this->getCertChainInfo      = $getCertChainInfo;
			$this->useNewCurlHandle      = $useNewCurlHandle;
			$this->identity              = $identity;
			$this->password              = $password;
			$this->httpAuthType          = is_int($httpAuthType) ? $httpAuthType : constant($httpAuthType);
		}

		public function setUrl($url)
		{
			$this->url = $url;
		}

		public function get( string $urlExtension = "" )
		{
			return $this->setBaseOptionsAndExecuteCurlRequest([], $urlExtension);
		}

		public function head( string $urlExtension = "" )
		{
			return $this->setBaseOptionsAndExecuteCurlRequest([CURLOPT_NOBODY => true], $urlExtension);
		}

		public function put(string $filePath = null, string $urlExtension = "" )
		{
			// If option CURLOPT_PUT is set then CURLOPT_INFILE must be set to a file handle
			$curlOpts =
			[
				CURLOPT_PUT => true,
				CURLOPT_BINARYTRANSFER => true
			];

			if( $filePath !== null )
			{
				if( !is_file($filePath) )
					return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, "File for upload not found under '$filePath'.");

				$fileHandle = fopen($filePath, "rb");
				if( $fileHandle === false )
					return new FsError(FsError::FILE_OPENING_FAILED, FsError::ERR, "File for upload '$filePath' could not be opened.");

				$curlOpts[CURLOPT_INFILE] = $fileHandle;
			}

			$httpResponse = $this->setBaseOptionsAndExecuteCurlRequest($curlOpts, $urlExtension);
			if( isset($fileHandle) )
				fclose($fileHandle);

			return $httpResponse;
		}

		public function putFileContents(string $contents = "", string $urlExtension = "")
		{
			// If option CURLOPT_PUT is set then CURLOPT_INFILE must be set to a file handle
			$curlOpts =
			[
				CURLOPT_CUSTOMREQUEST => "PUT",
				CURLOPT_BINARYTRANSFER => true,
				CURLOPT_POSTFIELDS => $contents
			];

			$httpResponse = $this->setBaseOptionsAndExecuteCurlRequest($curlOpts, $urlExtension);

			return $httpResponse;
		}

		public function post($postFields = "", string $urlExtension = "")
		{
			$curlOpts =
			[
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postFields
			];

			return $this->setBaseOptionsAndExecuteCurlRequest($curlOpts, $urlExtension);
		}

		public function delete( string $urlExtension = "" )
		{
			$curlOpts =
			[
				CURLOPT_CUSTOMREQUEST => self::HTTP_DELETE
			];

			return $this->setBaseOptionsAndExecuteCurlRequest($curlOpts, $urlExtension);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function setBaseOptionsAndExecuteCurlRequest(array $curlOpts = [], string $urlExtension = "")
		{
			$responseHeaders = [];

			$url = preg_match("/\w+:\/\//", $urlExtension) ? $urlExtension : $this->url . $urlExtension;

			$curlOpts +=
			[
				CURLOPT_URL => $url,
				CURLOPT_VERBOSE => true,
				CURLOPT_RETURNTRANSFER => true,
				CURLINFO_HEADER_OUT => true,
				CURLOPT_FRESH_CONNECT => $this->useNewCurlHandle,
				CURLOPT_FORBID_REUSE => $this->useNewCurlHandle,
				CURLOPT_CONNECTTIMEOUT => $this->timeOutValue,
				CURLOPT_TIMEOUT => $this->timeOutValue,
				CURLOPT_HTTPHEADER => $this->headers,
				CURLOPT_HEADERFUNCTION => function($ch, $header) use (&$responseHeaders)
				{
					$length = strlen($header);
					$headerParts = explode(":", $header, 2);

					if( count($headerParts) < 2 )
						return $length;

					$key = strtolower( trim($headerParts[0]) );
					$value = trim( $headerParts[1] );

					$responseHeaders[$key] = $value;
					return $length;
				}
			];

			if( !empty($this->identity) )
			{
				if( $this->httpAuthType !== HttpRequest::CURLAUTH_CLIENTCERT )
				{
					$curlOpts[CURLOPT_USERNAME]  = $this->identity;
					$curlOpts[CURLOPT_PASSWORD]  = $this->password;
					$curlOpts[CURLOPT_HTTPAUTH]  = $this->httpAuthType;
					$curlOpts[CURLOPT_USERAGENT] = Helper::HTTP_USER_AGENT;
				}
				else
				{
					if( file_exists($this->identity . ".crt") &&  file_exists($this->identity . ".key") )
					{
						$curlOpts[CURLOPT_SSLCERT] = $this->identity . ".crt";
						$curlOpts[CURLOPT_SSLKEY] = $this->identity . ".key";
						if( !empty($this->password) )
							$curlOpts[CURLOPT_SSLKEYPASSWD] = $this->password;
					}
					if( file_exists($this->identity . ".pfx") )
					{
						$curlOpts[CURLOPT_SSLCERT] = $this->identity . ".pfx";
						$curlOpts[CURLOPT_SSLCERTTYPE] = "P12";
						if( !empty($this->password) )
							$curlOpts[CURLOPT_SSLKEYPASSWD] = $this->password;
					}
				}
			}

			if( empty($this->caCertificateFilePath) )
			{
				$curlOpts[CURLOPT_SSL_VERIFYPEER] = false;
				$curlOpts[CURLOPT_SSL_VERIFYHOST] = 0;
				$curlOpts[CURLOPT_CERTINFO]       = false;
			}
			else
			{
				$curlOpts[CURLOPT_CERTINFO]       = $this->getCertChainInfo;
				$curlOpts[CURLOPT_SSL_VERIFYPEER] = true;
				$curlOpts[CURLOPT_CAINFO]         = $this->caCertificateFilePath;
				$curlOpts[CURLOPT_SSL_VERIFYHOST] = 2;
			}

			if( is_array($this->cookies) )
			{
				$cookieString = "";
				foreach($this->cookies as $cookieKey => $cookieValue)
				{
					$cookieString .= $cookieString !== "" ? ";$cookieKey=$cookieValue" : "$cookieKey=$cookieValue";
				}
				$curlOpts[CURLOPT_COOKIE] = $cookieString;
			}

			$ch = ($this->useNewCurlHandle === true ? curl_init() : self::getCurlHandle());

			if( curl_setopt_array($ch, $curlOpts) === false )
			{
				if( $this->useNewCurlHandle )
					curl_close($ch);
				return new SysError(SysError::INITIALIZATION_ERROR, SysError::ERR, "Curl request set options failed");
			}

			$responseBody = curl_exec($ch);

			$responseErrorText = curl_error($ch);
			$responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

			$certChain = ($curlOpts[CURLOPT_CERTINFO] === true) ? curl_getinfo($ch, CURLINFO_CERTINFO) : null;

			if( $this->useNewCurlHandle )
				curl_close($ch);

			if( $responseErrorText !== "" )
			{
				if( $responseCode === HttpError::HTTP_NO_CONNECTION )
					$responseCode = HttpError::UNKNOWN;

				return new HttpError( $responseCode, HttpError::ERR, $responseErrorText);
			}

			return new HttpResponse($responseHeaders, $responseBody, $responseCode, $certChain);
		}

		protected static function getCurlHandle()
		{
			if( isset(self::$curlHandle) === false )
				self::$curlHandle = curl_init();
			else
				curl_reset(self::$curlHandle);

			return self::$curlHandle;
		}
	}
}
