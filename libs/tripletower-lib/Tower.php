<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK
{

	use TripleTowerSDK\ApiProvider\ApiProvider;
	use TripleTowerSDK\Application\SapiNames;
	use TripleTowerSDK\Error\ApiError;
	use TripleTowerSDK\Error\ConfigError;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\GlobalInstance;
	use TripleTowerSDK\Helper\G11n;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Helper\Layouter;
	use TripleTowerSDK\Serializer\Serializer;
	use TripleTowerSDK\User\G11nSettings\Language;
	use TripleTowerSDK\User\G11nSettings\Location;
	use TripleTowerSDK\User\G11nSettings\Timezone;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\WappInfo;
	use Zend\Loader\ClassMapAutoloader;

	abstract class Tower
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const UNDEFINED_TOWER = "undefinedtower";
		const SERVICE_TOWER   = "servicetower";
		const WAPPS_TOWER     = "wappstower";
		const CLI_TOWER       = "clitower";

		const CONTENT_TYPE_VIEW   = "views";
		const CONTENT_TYPE_API    = "api";
		const CONTENT_TYPE_PUBLIC = "public";
		const CONTENT_TYPE_TEMP   = "temp";

		const LOG_DATE_FORMAT         = "Y-m-d";
		const LOG_ARCHIVE_DATE_FORMAT = "Y-m";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $instance       = self::UNDEFINED_TOWER;
		protected $displayName    = "";
		protected $configKey      = "";
		protected $classMapLoader = null;

		// Base folders
		protected $rootFolder     = "";
		protected $etcFolder      = "";
		protected $libsFolder     = "";
		protected $toolsFolder    = "";

		// Common folders
		protected $binFolder      = "";
		protected $confFolder     = "";
		protected $g11nFolder     = "";
		protected $installFolder  = "";
		protected $publicFolder   = "";
		protected $tempFolder     = "";
		protected $testsFolder    = "";

		// Special folders
		protected $globalApiFolder   = ""; // Used by tower only
		protected $apiFolder         = ""; // Used by contentProvider only
		protected $globalViewsFolder = ""; // Used by tower only
		protected $viewsFolder       = ""; // Used by contentProvider only
		protected $layoutsFolder     = ""; // Used by tower only

		/***********************************
		 * ABSTRACT METHODS                *
		***********************************/
		abstract public function checkAuthentication();
		abstract public function checkAuthorization();
		abstract protected function removeTowerInfos();


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $rootFolder, string $sdkFolder, ClassMapAutoloader $loader, string $instance)
		{
			// --- INSTANCE DETERMINATION ---
			$this->instance = $instance;

			// --- AUTOLOADING ---
			$this->classMapLoader = $loader;

			// --- FILES & FOLDERS ---
			// Base folders
			$this->rootFolder    = Helper::normalizeFolderPath( $rootFolder );
			$this->etcFolder     = Helper::normalizeFolderPath( $sdkFolder . '/etc/' );
			$this->libsFolder    = Helper::normalizeFolderPath( $sdkFolder . '/libs/' );
			$this->toolsFolder   = Helper::normalizeFolderPath( $sdkFolder . '/tools/' );

			// Common folders
			$this->binFolder         = Helper::normalizeFolderPath( $rootFolder . '/bin/' );
			$this->confFolder        = Helper::normalizeFolderPath( $rootFolder . '/conf/' );
			$this->g11nFolder        = Helper::normalizeFolderPath( $rootFolder . '/g11n/' );
			$this->installFolder     = Helper::normalizeFolderPath( $rootFolder . '/install/' );
			$this->publicFolder      = Helper::normalizeFolderPath( $rootFolder . '/public/' );
			$this->tempFolder        = Helper::normalizeFolderPath( $rootFolder . '/temp/' );
			$this->testsFolder       = Helper::normalizeFolderPath( $rootFolder . '/tests/' );

			// Special folders
			$this->globalApiFolder   = Helper::normalizeFolderPath( $rootFolder . '/api/' );
			$this->globalViewsFolder = Helper::normalizeFolderPath( $rootFolder . '/views/' );
			$this->layoutsFolder     = Helper::normalizeFolderPath( $rootFolder . '/bin/Layouts/' );

			// Check folders
			$this->checkDefaultFolders()->throwIfNotSuccess();

			// --- TIME ZONE ---
			$this->setTimeZone('UTC');

			// --- CONFIG ---
			$conf = $this->loadGlobalConfig();
			if( TripleTowerError::isError($conf) )
				throw $conf->raiseError(TripleTowerError::EMERG);

			$instanceConf = $this->loadInstanceConfig($this->instance, $conf->environment);
			if( TripleTowerError::isError($instanceConf) )
				throw $instanceConf->raiseError(TripleTowerError::EMERG);

			$conf->merge($instanceConf);

			// --- LOGGING ---
			$logger = $this->setupLogging($conf, $this->instance);

			// --- G11n ---
			$g11n = new G11n($this->g11nFolder);

			// --- GLOBAL REGISTRATION ---
			$_SERVER["TT"] = new GlobalInstance($this, $logger, $conf, $g11n);

			// --- MAP ALL ERRORS TO LOGGING FACILITY ---
			Helper::mapErrorOutputToGlobalLogger();

			// --- TOWER DISPLAY NAME ---
			$this->displayName = ( $this->isCliTower() ? $this->instance : $conf->displayName);

			// --- REGISTER PROGRAMFILES ---
			if( file_exists($classMapFile = $this->binFolder . ".classmap.php") === true )
				$this->classMapLoader->registerAutoloadMap($classMapFile);
		}

		public function getTowerInstance()
		{
			return $this->instance;
		}

		public function isWappsTower()
		{
			return $this->instance === self::WAPPS_TOWER;
		}

		public function isServiceTower()
		{
			return $this->instance === self::SERVICE_TOWER;
		}

		public function isCliTower()
		{
			return SapiNames::isCliSapi();
		}

		public function setTimeZone($timeZone)
		{
			return date_default_timezone_set($timeZone);
		}

		public function getTimeZone()
		{
			return date_default_timezone_get();
		}

		public function getDisplayName()
		{
			return $this->displayName;
		}

		public function getEtcFolder()
		{
			return $this->etcFolder;
		}

		public function getToolsFolder()
		{
			return $this->toolsFolder;
		}

		public function getRootFolder()
		{
			return $this->rootFolder;
		}

		public function getBinFolder()
		{
			return $this->binFolder;
		}

		public function getConfFolder()
		{
			return $this->confFolder;
		}

		public function getG11nFolder()
		{
			return $this->g11nFolder;
		}

		public function getInstallFolder()
		{
			return $this->installFolder;
		}

		public function getPublicFolder()
		{
			return $this->publicFolder;
		}

		public function getTempFolder()
		{
			return $this->tempFolder;
		}

		public function getTestsFolder()
		{
			return $this->testsFolder;
		}

		public function getGlobalApiFolder()
		{
			return $this->globalApiFolder;
		}

		public function getApiFolder()
		{
			return $this->apiFolder;
		}

		public function getGlobalViewsFolder()
		{
			return $this->globalViewsFolder;
		}

		public function getViewsFolder()
		{
			return $this->viewsFolder;
		}

		public function getLayoutsFolder()
		{
			return $this->layoutsFolder;
		}

		public function exec()
		{
			// Cli applications don't have wapps
			if( $this->isCliTower() )
				return;

			// --- Remove tower parts which should not be available to the contentProvider ---
			$this->removeTowerInfos();

			// --- Loading request specific g11n files ---
			$_SERVER["TT"]->g11n->changeG11nFolder($this->g11nFolder, Language::G11N_DEFAULT_LANGUAGE, Location::G11N_DEFAULT_LOCATION, Timezone::G11N_DEFAULT_TIMEZONE);
			$_SERVER["TT"]->g11n->changeG11nSettings($_SERVER["TT"]->user->g11nSettings->language, $_SERVER["TT"]->user->g11nSettings->location, $_SERVER["TT"]->user->g11nSettings->timezone);

			switch($_SERVER["TT"]->contentType)
			{
				case self::CONTENT_TYPE_VIEW:
				{
					Helper::requireIncludeFolder($this->viewsFolder)->throwIfNotSuccess();

					// Make page work in corporate intranets
					$userAgent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
					if(preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent))
						header("X-UA-Compatible: IE=edge");
					
					// Don't allow caching for view documents
					header("Cache-Control: no-cache, no-store, must-revalidate");
					header("Pragma: no-cache");
					header("Expires: 0");

					// Start layouting
					$layoutName = (isset($_SERVER["TT"]->conf->layoutName) ? $_SERVER["TT"]->conf->layoutName : Layouter::DEFAULT_LAYOUT_NAME);
					$layout = new Layouter($this->layoutsFolder.$layoutName);

					$meta = $_SERVER["TT"]->meta;
					if(!empty($meta->dependencies))
					{
						if(!empty($meta->dependencies->js))
						{
							foreach($meta->dependencies->js as $jsFile)
								$layout->addHeadScript($jsFile, Helper::stringStartsWith($jsFile, "/") || stream_resolve_include_path($jsFile)===false);
						}

						if(!empty($meta->dependencies->css))
						{
							foreach($meta->dependencies->css as $cssFile)
								$layout->addHeadStyleSheet($cssFile, Helper::stringStartsWith($cssFile, "/") || stream_resolve_include_path($cssFile)===false);
						}
					}

					$view = $meta->getViewEntry($_SERVER["TT"]->contentName);
					if( TripleTowerError::isError($view) )
						throw $view;

					// Global preRenderHook
					if( !empty($globalPreRenderHook = $meta->checkHookFilePresence(WappInfo::GLOBAL_PRE_RENDER_HOOK)) )
						require $globalPreRenderHook;

					// Local preRenderHook
					if( !empty($localPreRenderHook = $meta->checkHookFilePresence(WappInfo::LOCAL_PRE_RENDER_HOOK)) )
						require $localPreRenderHook;

					// Check for selfRenderer - if set in meta json
					$selfRenderer = $meta->checkHookFilePresence(WappInfo::LOCAL_SELF_RENDERER);
					if( empty($selfRenderer) )
						$selfRenderer = $meta->checkHookFilePresence(WappInfo::GLOBAL_SELF_RENDERER);

					// Rendering
					if( empty($selfRenderer) )
					{
						// Include meta information into Layouter
						if( !empty($view->metaInformation) )
						{
							if(($metaFound = stream_resolve_include_path("$view->metaInformation.meta")) !== false)
							{
								if($metaFound !== false)
									$layout->addMetaInformation("$view->metaInformation.meta");
							}
							else
							{
								throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "File '$view->metaInformation.meta' could not be found");
							}
						}

						// Include appcachePath into Layouter
						if( !empty($meta->appcachePath) )
							$layout->setAppcache($meta->appcachePath);

						// Include title into Layouter
						if( !empty($view->title) )
							$layout->setTitle($view->title);

						// Include loadingScreen into Layouter
						if(isset($view->loadingScreen) && $view->loadingScreen !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->loadingScreen) )
							{
								$layout->unsetLoadingScreen(); // Don't show any loadingScreen
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->loadingScreen.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->loadingScreen.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->loadingScreen.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->loadingScreen.esm.js")) !== false )
								{
									if( $htmlFound !== false )
										$layout->setLoadingScreen("$view->loadingScreen.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->loadingScreen.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->loadingScreen.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->loadingScreen.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for loadingScreen '$view->loadingScreen' in meta.json could not be found");
								}
							}
						}

						// Include header into Layouter
						if(isset($view->header) && $view->header !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->header) )
							{
								$layout->unsetHeader(); // Don't show any header
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->header.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->header.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->header.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->header.esm.js")) !== false )
								{
									if( $htmlFound !== false )
										$layout->setHeader("$view->header.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->header.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->header.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->header.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for header '$view->header' in meta.json could not be found");
								}
							}
						}

						// Include navigation into Layouter
						if(isset($view->navigation) && $view->navigation !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->navigation) )
							{
								$layout->unsetNavigation(); // Don't show any navigation
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->navigation.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->navigation.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->navigation.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->navigation.esm.js")) !== false)
								{
									if( $htmlFound !== false )
										$layout->setNavigation("$view->navigation.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->navigation.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->navigation.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->navigation.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for navigation '$view->navigation' in meta.json could not be found");
								}
							}
						}

						// Include subNavigation into Layouter
						if( isset($view->subNavigation) && $view->subNavigation !== Layouter::AUTOGENERATION_ON )
						{
							if( empty($view->subNavigation) )
							{
								$layout->unsetSubNavigation(); // Don't show any subNavigation
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->subNavigation.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->subNavigation.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->subNavigation.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->subNavigation.esm.js")) !== false)
								{
									if( $htmlFound !== false )
										$layout->setSubNavigation("$view->subNavigation.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->subNavigation.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->subNavigation.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->subNavigation.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for subNavigation '$view->subNavigation' in meta.json could not be found");
								}
							}
						}

						// Include content into Layouter
						if(isset($view->content) && $view->content !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->content) )
							{
								$layout->unsetContent(); // Don't show any content
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->content.html")) !== false |
									($mdFound    = stream_resolve_include_path("$view->content.md")) !== false |
									($cssFound   = stream_resolve_include_path("$view->content.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->content.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->content.esm.js")) !== false)
								{
									if( $htmlFound !== false )
										$layout->setContent("$view->content.html");
									else if($mdFound !== false ) // Load md file only when there is no html
										$layout->setContent("$view->content.md");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->content.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->content.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->content.esm.js");
								}
								else if( ($htmlFound = stream_resolve_include_path("$this->globalViewsFolder$view->content.html")) !== false |
										($mdFound    = stream_resolve_include_path("$this->globalViewsFolder$view->content.md")) !== false |
										($cssFound   = stream_resolve_include_path("$this->globalViewsFolder$view->content.css")) !== false |
										($jsFound    = stream_resolve_include_path("$this->globalViewsFolder$view->content.js")) !== false |
										($esmJsFound = stream_resolve_include_path("$this->globalViewsFolder$view->content.esm.js")) !== false )
								{
									if( $htmlFound !== false )
										$layout->setContent("$this->globalViewsFolder$view->content.html");
									else if($mdFound !== false ) // Load md file only when there is no html
										$layout->setContent("$this->globalViewsFolder$view->content.md");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$this->globalViewsFolder$view->content.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$this->globalViewsFolder$view->content.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$this->globalViewsFolder$view->content.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for content '$view->content' in meta.json could not be found");
								}
							}
						}

						// Include sidebar into Layouter
						if(isset($view->sidebar) && $view->sidebar !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->sidebar) )
							{
								$layout->unsetSidebar(); // Don't show any sidebar
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->sidebar.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->sidebar.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->sidebar.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->sidebar.esm.js")) !== false)
								{
									if( $htmlFound !== false )
										$layout->setSidebar("$view->sidebar.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->sidebar.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->sidebar.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->sidebar.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for sidebar '$view->sidebar' in meta.json could not be found");
								}
							}
						}

						// Include footer into Layouter
						if(isset($view->footer) && $view->footer !== Layouter::AUTOGENERATION_ON)
						{
							if( empty($view->footer) )
							{
								$layout->unsetFooter(); // Don't show any footer
							}
							else
							{
								// Use the specified file
								if( ($htmlFound  = stream_resolve_include_path("$view->footer.html")) !== false |
									($cssFound   = stream_resolve_include_path("$view->footer.css")) !== false |
									($jsFound    = stream_resolve_include_path("$view->footer.js")) !== false |
									($esmJsFound = stream_resolve_include_path("$view->footer.esm.js")) !== false)
								{
									if( $htmlFound !== false )
										$layout->setFooter("$view->footer.html");
									if( $cssFound !== false )
										$layout->addHeadStyleSheet("$view->footer.css");
									if( $jsFound !== false )
										$layout->addHeadScript("$view->footer.js");
									if( $esmJsFound !== false )
										$layout->addHeadScript("$view->footer.esm.js");
								}
								else
								{
									throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "Specified files for footer '$view->footer' in meta.json could not be found");
								}
							}
						}

						// Let Layouter render the whole template
						$layout->render();
					}
					else
					{
						// SelfRendering
						require $selfRenderer;
					}

					// Global postRenderHook
					if( !empty($globalPostRenderHook = $meta->checkHookFilePresence(WappInfo::GLOBAL_POST_RENDER_HOOK)) )
						require $globalPostRenderHook;

					// Local postRenderHook
					if( !empty($localPostRenderHook = $meta->checkHookFilePresence(WappInfo::LOCAL_POST_RENDER_HOOK)) )
						require $localPostRenderHook;
				}
				break;

				case self::CONTENT_TYPE_API:
				{
					try
					{
						// Don't allow caching for view documents
						header("Cache-Control: no-cache, no-store, must-revalidate");
						header("Pragma: no-cache");
						header("Expires: 0");

						// Process API call
						$error = ApiProvider::processApiCall($this->apiFolder, $_SERVER["TT"]->contentName, $_SERVER["TT"]->user);
						if( !TripleTowerError::isError($error) )
							throw new ApiError(ApiError::ACTION_NOT_CALLABLE, ApiError::CRIT, "The " . $_SERVER["TT"]->contentProvider . "-api '" . $_SERVER["TT"]->contentName . "' did not return an TripleTowerError object to the calling interface.");

						if(!$error->isSuccess())
							echo Serializer::jsonSerialize($error, true);
					}
					catch(TripleTowerError $ex)
					{
						echo Serializer::jsonSerialize($ex, true);
					}
					catch(\Throwable $t)
					{
						$apiError = null;
						if( $_SERVER["TT"]->isInDevelopmentMode() )
							$apiError = new ApiError(ApiError::UNKNOWN, ApiError::ERR, (string)$t);
						else
							$apiError = new ApiError(ApiError::UNKNOWN, ApiError::ERR, $t->getMessage());
						echo Serializer::jsonSerialize($apiError, true);
					}
				}
				break;

				case self::CONTENT_TYPE_TEMP:
				{
					$tempFile = Helper::normalizeFilePath($this->tempFolder . $_SERVER["TT"]->contentName);
					if(is_file($tempFile))
					{
						header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
						$tempFileType = pathinfo($tempFile, PATHINFO_EXTENSION);
						if($tempFileType === "jgz")
						{
							header("Cache-Control: max-age=21600"); // 60sec * 60min * 6hours
							header("Content-Type: application/javascript; charset=utf-8");
							header("Content-Encoding: gzip");
						}
						else if($tempFileType === "cgz")
						{
							header("Cache-Control: max-age=21600"); // 60sec * 60min * 6hours
							header("Content-Type: text/css; charset=utf-8");
							header("Content-Encoding: gzip");
						}
						else
						{
							header("Content-Type: " . Helper::getMimeType($tempFile));
							header("Cache-Control: max-age=604800"); // 60sec * 60min * 24hours * 7days
						}

						header("Content-Length: " . filesize($tempFile));
						ob_clean();
						flush();
						readfile($tempFile);
					}
					else
					{
						header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
					}
				}
				break;
			}

			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::POST_EXECUTION_HOOK)) )
				require $hookFile;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function checkDefaultFolders()
		{
			// Base tower folders
			if( !is_dir($this->rootFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->rootFolder . " is no valid rootfolder");
			if( !is_dir($this->etcFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->etcFolder . " is no valid etcFolder");
			if( !is_dir($this->libsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->libsFolder . " is no valid libsfolder");
			if( !is_dir($this->toolsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->toolsFolder . " is no valid toolsFolder");

			// Common tower folders
			if( !is_dir($this->binFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->binFolder . " is no valid binFolder");
			if( !is_dir($this->confFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->confFolder . " is no valid confFolder");
			if( !is_dir($this->g11nFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->g11nFolder . " is no valid g11nFolder");
			if( !is_dir($this->publicFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->publicFolder . " is no valid publicFolder");
			if( !is_dir($this->tempFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->tempFolder . " is no valid tempFolder");
			if( !is_dir($this->testsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->testsFolder . " is no valid testsFolder");

			// Special tower folders
			if( !$this->isCliTower() && !is_dir($this->globalApiFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->globalApiFolder . " is no valid globalApiFolder");
			if( !$this->isCliTower() && !is_dir($this->globalViewsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->globalViewsFolder . " is no valid globalViewsFolder");
			if( !$this->isCliTower() && !is_dir($this->layoutsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->layoutsFolder . " is no valid layoutsFolder");

			return new FsError(FsError::SUCCESS);
		}

		protected function checkContentProviderFolders()
		{
			// CLI applications don't have content providers
			if( $this->isCliTower() )
				return new FsError(FsError::SUCCESS);

			// Common ContentProvider folders
			$folderCheck = $this->checkDefaultFolders();
			if( !$folderCheck->isSuccess() )
				return $folderCheck;

			// Specific contentProvider folders
			if( !is_dir($this->apiFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->apiFolder . " is no valid apiFolder");
			if( !is_dir($this->viewsFolder) )
				return new FsError(FsError::FOLDER_NOT_FOUND, SysError::EMERG, $this->viewsFolder . " is no valid viewsFolder");

			return new FsError(FsError::SUCCESS);
		}

		protected function loadGlobalConfig()
		{
			try
			{
				// Look for global config file
				if( !is_file("$this->confFolder/global.json") && !is_file("$this->confFolder/global.json.tte") )
					return new ConfigError(ConfigError::CONFIGURATION_FILE_NOT_FOUND, ConfigError::SILENT);

				// Load environment variable from global config
				$environmentConfig = new JsonConfig("$this->confFolder/global.json", "", $this->configKey);
				$loadedEnvironment = strtolower($environmentConfig->environment);
				if( !in_array($loadedEnvironment, [GlobalInstance::ENVIRONMENT_DEVELOPMENT, GlobalInstance::ENVIRONMENT_STAGING, GlobalInstance::ENVIRONMENT_PRODUCTION]) )
					return new ConfigError(ConfigError::COULD_NOT_PARSE_PARAMETER, ConfigError::ERR, "Config 'environment' property '$loadedEnvironment' in '$this->confFolder/global.json' is not one of the standard GlobalInstance::ENVIRONMENT's.");

				// Load global config
				$globalConf = new JsonConfig("$this->confFolder/global.json", $loadedEnvironment, $this->configKey);

				// Merge environment variable into global config
				$globalConf->mergeArray(array("environment" => $loadedEnvironment));

				return $globalConf;

			}
			catch(TripleTowerError $ex)
			{
				return $ex;
			}
		}

		protected function loadInstanceConfig(string $instanceName, string $environment)
		{
			try
			{
				// Look for instance config file
				if( !is_file("$this->confFolder/$instanceName.json") && !is_file("$this->confFolder/$instanceName.json.tte") )
					return new ConfigError(ConfigError::CONFIGURATION_FILE_NOT_FOUND, ConfigError::SILENT);

				// Load instance config
				return new JsonConfig("$this->confFolder/$instanceName.json", $environment, $this->configKey);
			}
			catch(TripleTowerError $ex)
			{
				return $ex;
			}
		}

		protected function setupLogging(JsonConfig $conf, string $contentProvider)
		{
			// Take care of error reporting
			$debugMode = ($conf->environment === GlobalInstance::ENVIRONMENT_DEVELOPMENT) ? "1" : "0";
			if($debugMode)
				error_reporting(E_ALL);
			else
				error_reporting(null);
			ini_set("display_errors", $debugMode);
			ini_set("display_startup_errors", $debugMode);

			// Setup logging facility
			$logger = new \Zend\Log\Logger();

			// Create a new log file and zip all old log files which match the pattern, except the newly created log file
			$currentLogFileName = $contentProvider . "_" . date(self::LOG_DATE_FORMAT) . ".log";
			$oldLogFilesRegex = preg_quote($contentProvider . "_", "/") . Helper::convertDateFormatToRegex(self::LOG_DATE_FORMAT) . preg_quote(".log", "/");
			$zipResult = Helper::zipFilesByRegex($this->tempFolder, "logarchive_" . date(self::LOG_ARCHIVE_DATE_FORMAT) . ".zip", $oldLogFilesRegex, $currentLogFileName);
			if(TripleTowerError::isError($zipResult))
				throw $zipResult;

			$fileWriter = Helper::createFileWriter($conf->verbosity, "$this->tempFolder/$currentLogFileName");
			if(filesize("$this->tempFolder/$currentLogFileName")) // append if not empty
				file_put_contents("$this->tempFolder/$currentLogFileName", PHP_EOL, FILE_APPEND);
			$logger->addWriter($fileWriter);

			$mailReceiverValid = (!empty($conf->mailReceiver) && is_array($conf->mailReceiver) && !empty($conf->mailReceiver[0]));
			if($mailReceiverValid && $conf->mailVerbosity > TripleTowerError::SILENT)
			{
				$mailWriter = Helper::createMailWriter($conf->mailVerbosity, "$contentProvider error: ", "$contentProvider@".($_SERVER["SERVER_NAME"]??Helper::getFqdn()), $conf->mailReceiver);
				$logger->addWriter($mailWriter);
			}

			$requestUrl = Helper::getFullRequestUrl();

			// Replace security relevant information, so they don't show up in logs
			if($contentProvider === self::SERVICE_TOWER)
			{
				$authUrl    = Helper::replaceUrlValue($requestUrl, 'yang', '********');
				$requestUrl = Helper::replaceUrlValue($authUrl, 'taiji', '********');
			}

			$logger->debug(PHP_EOL .
						"--- " . ($_SERVER["REQUEST_METHOD"]??"APPLICATION") . "-REQUEST ---------------------------" . PHP_EOL .
						$requestUrl);

			return $logger;
		}

		protected function routeRequest()
		{
			// CLI applications can't route requests
			if($this->isCliTower())
				return new SysError(SysError::SUCCESS);

			// --- contentProvider, contentType and contentName analysis ---
			$urlParts = array_filter(explode("/", rawurldecode(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH))), "strlen");
			array_shift($urlParts); // Removes the tower part

			// --- contentProvider analysis ---
			$contentProvider = array_shift($urlParts);
			if( !is_null($contentProvider) && strpos($contentProvider, "..") !== false)
				$contentProvider = null;

			if( empty($contentProvider) )
			{
				if( empty($_SERVER["TT"]->conf->defaultContentProvider) )
					return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "No content provider given and there is no default content provider defined.");

				header("Location: /{$this->instance}/{$_SERVER["TT"]->conf->defaultContentProvider}");
				exit();
			}

			if( !is_dir(Helper::normalizeFolderPath("$this->rootFolder/$this->instance/$contentProvider")) )
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Unfortunately, the requested content provider '$contentProvider' is unavailable.");

			// --- contentType analysis ---
			$contentType = array_shift($urlParts);
			if( empty($contentType) )
				$contentType = self::CONTENT_TYPE_VIEW;
			$availableContentTypes = array(self::CONTENT_TYPE_VIEW, self::CONTENT_TYPE_API, self::CONTENT_TYPE_PUBLIC, self::CONTENT_TYPE_TEMP);
			if( in_array($contentType, $availableContentTypes, true) === false )
				return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Unfortunately, the requested content type '$contentType' is unavailable.");

			// --- contentName analysis ---
			$contentName = array_shift($urlParts); // meta data necessary for contentName analysis

			// Loading global meta data
			$globalMetaFilePath = Helper::normalizeFilePath("$this->rootFolder/meta.json");
			$globalMetaInfo = Helper::loadJsonFile($globalMetaFilePath, true);
			if(TripleTowerError::isError($globalMetaInfo))
				return $globalMetaInfo;

			// Loading wapp meta data
			$metaFilePath = Helper::normalizeFilePath("$this->rootFolder/$this->instance/$contentProvider/meta.json");
			$metaInfo = Helper::loadJsonFile($metaFilePath, true);
			if(TripleTowerError::isError($metaInfo))
				return $metaInfo;

			// Merge meta data
			foreach( $globalMetaInfo as $metaInfoKey => $metaInfoValue )
				if( in_array($metaInfoKey, ["dependencies", "views", "api"]) )
					foreach( $metaInfoValue as $metaInfoSubKey => $metaInfoSubValue )
						if( !array_key_exists($metaInfoSubKey , $metaInfo[$metaInfoKey]) )
							$metaInfo[$metaInfoKey][$metaInfoSubKey] = $metaInfoSubValue;

			if( $contentType === self::CONTENT_TYPE_VIEW )
			{
				// Default view
				if( empty($contentName))
					$contentName = $metaInfo["views"]["default_view"];

				// Sub navigation selection
				if( !empty($urlParts))
					$contentName .= "/" . implode("/", $urlParts);

				// Too short sub navigation path, search for the first none-menu recursivly
				$targetView = $metaInfo;
				foreach(explode("/", $contentName) as $viewName)
				{
					if( !isset($targetView["views"][$viewName]) )
						return new SysError(SysError::SERVICE_UNAVAILABLE, SysError::ERR, "Unfortunately, the requested view '$contentName' is unavailable.");

					$targetView = $targetView["views"][$viewName];
				}
				while( isset($targetView["views"]) )
				{
					$viewIterator = new \ArrayIterator($targetView["views"]);
					$contentName .= "/" . $viewIterator->key();
					$targetView = $viewIterator->current();
				}
			}

			if( $contentType === self::CONTENT_TYPE_API )
			{
				// No content name given
				if( empty($contentName) )
					return new SysError(SysError::INVALID_TOWER_CALL, SysError::ERR, "Invalid tower call due to missing contentName for api call.");
				
				// Sub api selection
				if( !empty($urlParts) )
					$contentName .= "/" . implode("/", $urlParts);
			}
			
			if( $contentType === self::CONTENT_TYPE_PUBLIC )
			{
				// Sub folder selection
				if( !empty($urlParts))
					$contentName .= "/" . implode("/", $urlParts);
			}

			$_SERVER["TT"]->setContentVars($contentProvider, $contentType, $contentName);

			// Setting meta.json
			$wappInfo = new WappInfo($metaInfo);
			$_SERVER["TT"]->addMeta($wappInfo);

			// Applying meta invocation hook if present
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::META_INVOCATION_HOOK)) )
				require $hookFile;

			return new SysError(SysError::SUCCESS);
		}

		protected function prepareRequest()
		{
			if($this->isCliTower())
				return new SysError(SysError::SUCCESS);

			// --- Applying pre exection hook if present ---
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::PRE_EXECUTION_HOOK)) )
				require $hookFile;

			// --- Setting content provider folders ---
			$this->rootFolder    = Helper::normalizeFolderPath("$this->rootFolder/$this->instance/" .  $_SERVER["TT"]->contentProvider);
			// $this->etcFolder     = ""; Not necessary to change
			// $this->libFolder     = ""; Not necessary to change
			// $this->toolsFolder   = ""; Not necessary to change

			// Common folders
			$this->binFolder     = Helper::normalizeFolderPath($this->rootFolder . "bin");
			$this->confFolder    = Helper::normalizeFolderPath($this->rootFolder . "conf");
			$this->g11nFolder    = Helper::normalizeFolderPath($this->rootFolder . "g11n");
			$this->installFolder = Helper::normalizeFolderPath($this->rootFolder . "install");
			$this->publicFolder  = Helper::normalizeFolderPath($this->rootFolder . "public");
			$this->tempFolder    = Helper::normalizeFolderPath($this->rootFolder . "temp");
			$this->testsFolder   = Helper::normalizeFolderPath($this->rootFolder . "tests");
			
			// Special folders
			// $this->globalApiFolder = ""; Not necessary to change
			$this->apiFolder     = Helper::normalizeFolderPath($this->rootFolder . "api");
			// $this->globalViewsFolder = ""; Not necessary to change
			$this->viewsFolder   = Helper::normalizeFolderPath($this->rootFolder . "views");
			// $this->layoutsFolder = ""; Not necessary to change

			// Check folders
			$this->checkContentProviderFolders()->throwIfNotSuccess();

			// --- Adding request specific configs ---
			// global.json if present
			$globalConf = $this->loadGlobalConfig();
			if( TripleTowerError::isError($globalConf) )
				$globalConf->throwIfNotSuccessOrSameAs(new ConfigError(ConfigError::CONFIGURATION_FILE_NOT_FOUND, ConfigError::SILENT));
			else
				$_SERVER["TT"]->conf->merge($globalConf);

			// wappconfig.json if present
			$wappConf = $this->loadInstanceConfig("wappconfig", $_SERVER["TT"]->conf->environment);
			if( TripleTowerError::isError($wappConf) )
				$wappConf->throwIfNotSuccessOrSameAs(new ConfigError(ConfigError::CONFIGURATION_FILE_NOT_FOUND, ConfigError::SILENT));
			else
				$_SERVER["TT"]->conf->merge($wappConf);

			// --- Rerouting logging to content provider logs ---
			Helper::unmapErrorOutputFromGlobalLogger();
			$newLogger = $this->setupLogging($_SERVER["TT"]->conf, $_SERVER["TT"]->contentProvider);
			$_SERVER["TT"]->changeLogger($newLogger);
			Helper::mapErrorOutputToGlobalLogger();

			if($this->instance === self::SERVICE_TOWER && $_SERVER["TT"]->contentProvider  === 'Authenticator')
			{
				$authUrl      = Helper::replaceUrlValue(Helper::getFullRequestUrl(), 'yang', '********');
				$resultingUrl = Helper::replaceUrlValue($authUrl, 'taiji', '********');
				$_SERVER["TT"]->logger->debug(PHP_EOL.PHP_EOL."--- NEW " . $_SERVER["REQUEST_METHOD"] . "-REQUEST ---------------------------".PHP_EOL.$resultingUrl);
			}
			else
			{
				$_SERVER["TT"]->logger->debug(PHP_EOL.PHP_EOL."--- NEW " . $_SERVER["REQUEST_METHOD"] . "-REQUEST ---------------------------".PHP_EOL.Helper::getFullRequestUrl());
			}

			// --- Executing specified request ---
			Helper::requireIncludeFolder($this->binFolder)->throwIfNotSuccess();

			// --- Register classmap ---
			if(file_exists($classMapFile = $this->binFolder . ".classmap.php") === true)
				$this->classMapLoader->registerAutoloadMap($classMapFile);

			return new SysError(SysError::SUCCESS);
		}
	}
}