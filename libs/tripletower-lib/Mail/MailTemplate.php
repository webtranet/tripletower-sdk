<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Mail
{
	use TripleTowerSDK\Error\MailError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Storage\LocalSC;

	class MailTemplate
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		************************************/

		/***********************************
		* PROTECTED ATTRIBUTES             *
		************************************/

		protected string $boundary        = "";
		protected string $templateFile    = "";
		protected string $mailSender      = "";
		protected string $mailReplyTo     = "";
		protected string $mailSubject     = "";
		protected string $contentTitle    = "";
		protected string $contentLogo     = "";
		protected string $contentBody     = "";
		protected string $contentFarewell = "";
		protected array $attachments      = [];

		public function __construct(string $sender, string $replyTo = null, string $templateFilePath = __DIR__ . "/mail-template.html", string $logoFilePath = __DIR__ . "/mail-template-content-logo.png")
		{
			$this->boundary = "--==_mimepart_" . md5( uniqid( (string)time() ) );
			if( $replyTo === null )
				$replyTo = $sender;

			$this->setMailSender( $sender )->throwIfNotSuccess();
			$this->setMailReplyTo( $replyTo )->throwIfNotSuccess();
			$this->setTemplateFile( $templateFilePath )->throwIfNotSuccess();
			$this->setContentLogo( $logoFilePath )->throwIfNotSuccess();
		}

		public function setTemplateFile( string $templateFilePath ) : TripleTowerError
		{
			$templateFile = Helper::downloadFile($templateFilePath);
			if( TripleTowerError::isError($templateFile) )
				return $templateFile;
			$this->templateFile = $templateFile;
			return new MailError( MailError::SUCCESS );
		}
		
		public function setMailSender( string $sender ) : TripleTowerError
		{
			if( filter_var($sender, FILTER_VALIDATE_EMAIL) === false )
				return new MailError( MailError::INVALID_MAIL_ADDRESS, MailError::ERR, "Mail sender '$sender' is no valid mail address." );
			$this->mailSender = $sender;
			return new MailError( MailError::SUCCESS );
		}

		public function setMailReplyTo( string $replyTo ) : TripleTowerError
		{
			if( filter_var($replyTo, FILTER_VALIDATE_EMAIL) === false )
				return new MailError( MailError::INVALID_MAIL_ADDRESS, MailError::ERR, "Mail replyTo '$replyTo' is no valid mail address." );
			$this->mailSender = $replyTo;
			return new MailError( MailError::SUCCESS );
		}

		public function setMailSubject( string $subject ) : TripleTowerError
		{
			$this->mailSubject = $subject;
			return new MailError( MailError::SUCCESS );
		}

		public function setContentTitle( string $title ) : TripleTowerError
		{
			$this->contentTitle = $title;
			return new MailError( MailError::SUCCESS );
		}

		public function setContentLogo( string $logoFilePath ) : TripleTowerError
		{
			$contentLogo = Helper::downloadFile($logoFilePath);
			if( TripleTowerError::isError($contentLogo) )
				return $contentLogo;
			$this->contentLogo = $contentLogo;
			return new MailError( MailError::SUCCESS );
		}

		public function setContentBodyText( string $bodyText ) : TripleTowerError
		{
			$this->contentBody = "<p>$bodyText<p>";
			return new MailError( MailError::SUCCESS );
		}

		public function setContentBodyHtml( string $bodyHtml ) : TripleTowerError
		{
			$this->contentBody = $bodyHtml;
			return new MailError( MailError::SUCCESS );
		}

		public function setContentFarewell( string $farewell ) : TripleTowerError
		{
			$this->contentFarewell = $farewell;
			return new MailError( MailError::SUCCESS );
		}

		public function addAttachment( string $key, string $type, string $content ) : TripleTowerError
		{
			$this->attachments[$key] =
			[
				"type"    => $type,
				"content" => $content
			];
			return new MailError( MailError::SUCCESS );
		}

		public function removeAttachment( string $key ) : TripleTowerError
		{
			unset($this->attachments[$key]);
			return new MailError( MailError::SUCCESS );
		}
		
		public function send( string ...$receivers ) : TripleTowerError
		{
			$renderedMail = $this->renderMailContent();
			foreach( $receivers as $receiver )
			{
				if( filter_var($receiver, FILTER_VALIDATE_EMAIL) === false )
				{
					// NO RETURN! Just create log entry
					new MailError( MailError::INVALID_MAIL_ADDRESS, MailError::ERR, "Mail receiver '$receiver' is no valid mail address." );
					continue;
				}

				if( empty($renderedMail) )
					return new MailError( MailError::MAIL_SENDING_FAILED, MailError::ERR, "Could not send mail because mail content is empty.");
					
				if( $_SERVER["TT"]->isInProductionMode() )
				{
					$mailHeaders = "From: $this->mailSender\r\n" .           // Do NOT! use PHP_EOL
									"Reply-To: $this->mailReplyTo\r\n" .     // because \r\n is the
									"MIME-Version: 1.0\r\n" .                // standard in mails
									"Content-Type: multipart/related; boundary=$this->boundary; charset=UTF-8\r\n";
			
					if( !mail($receiver, $this->mailSubject, $renderedMail, $mailHeaders) )
						return new MailError( MailError::MAIL_SENDING_FAILED, MailError::ERR, "Sending mail to address '$receiver' failed." );
				}
				else
				{
					$fsCompatibleReceiver = str_replace(LocalSC::INVALID_FILESYTEM_CHARACTERS, '_', $receiver);
					if( file_put_contents( $_SERVER["TT"]->tower->getTempFolder() . "$fsCompatibleReceiver.html", $renderedMail ) === false )
						return new MailError( MailError::MAIL_SENDING_FAILED, MailError::ERR, "Sending mail to address '$receiver' failed." );
				}
			}

			return new MailError( MailError::SUCCESS );
		}


		/***********************************
		* ABSTRACT METHODS                 *
		************************************/


		/***********************************
		* PUBLIC METHODS                   *
		************************************/


		/***********************************
		* PROTECTED METHODS                *
		************************************/

		/**
		 * @todo Check line size for word wrapping
		 * @todo MIME type of logo should be determined - currently hard coded to png
		 */
		protected function renderMailContent() : string
		{
			// Load template
			$template = new \DOMDocument();
			$template->loadHTML( $this->templateFile );
			
			// Pre-render title
			$template->getElementById("mail-title-text")->nodeValue = $this->contentTitle;

			// Pre-render logo
			$contentLogoName   = "mail-template-content-logo.png";
			$contentLogoString = chunk_split( base64_encode($this->contentLogo) );

			// Pre-render content
			$contentElement = $template->getElementById("mail-content");
			foreach( $contentElement->childNodes as $childNode )
				$contentElement->removeChild( $childNode );
			$contentFragment = $template->createDocumentFragment();
			$contentFragment->appendXML($this->contentBody);
			$contentElement->appendChild($contentFragment);

			// Pre-render Farewell
			$template->getElementById("mail-farewell-sender")->nodeValue = $this->contentFarewell;

			// Render mail content
			$preparedMailContent = "--$this->boundary\n" .
									"Content-Type: text/html; charset=UTF-8\n" .
									"Content-Transfer-Encoding: 7-bit\n\n" .
									$template->saveHTML() . "\n\n" .
									"--$this->boundary\n" .
									"Content-Type: image/png; file_name = \"$contentLogoName\"\n" .
									"Content-ID: <$contentLogoName>\n" .
									"Content-Transfer-Encoding: base64\n" .
									"Content-Disposition: inline; filename = \"$contentLogoName\"\n\n" .
									"$contentLogoString\n" .
									"--$this->boundary--\n";

			// Render mail attachments
			foreach( $this->attachments as $key => $attachment )
			{
				$preparedMailContent .=
					"--$this->boundary\n" .
					"Content-Type: $attachment->type; file_name = \"$key\"\n" .
					"Content-Transfer-Encoding: base64\n" .
					"Content-Disposition: attachment; filename = \"$key\"\n\n" .
					chunk_split(base64_encode($attachment->content)) . "\n" .
					"--$this->boundary--\n";
			}
			
			return $preparedMailContent;
		}
	}
}