<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DashBoard
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\SysError;

	abstract class DashBoard
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		private $conf;
		private $fileHandle = null;
		private $errors = array();

		protected $filePath;
		protected $dashBoardType;
		protected $dashBoardVersion = 'undefined';

		protected $instanceName;
		protected $moduleName;
		protected $instanceVersion;
		protected $maxErrors = 5;

		protected $blacklist = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $filePath, string $instanceName, string $instanceVersion, string $moduleName, int $maxErrors = 5)
		{
			$this->filePath = Helper::normalizeFilePath($filePath);
			$this->instance = $instanceName;
			$this->instanceVersion = $instanceVersion;
			$this->moduleName = $moduleName;
			$this->maxErrors = $maxErrors;
			$this->conf = $_SERVER["TT"]->conf;
			$this->dashBoardType = get_class($this);

			if( is_file($this->filePath) )
				$this->readXml();
		}

		public function addError(TripleTowerError $error)
		{
			if(!$error->isSuccess())
			{
				$add = true;
				foreach ($this->errors as $err)
				{
					if($error->isSameError($err))
					{
						$add = false;
						break;
					}
				}

				if($add)
					$this->errors[] = $error;
			}
		}

		public function __set(string $key, $value)
		{
			if(property_exists(get_class($this), $key))
			{
				$this->$key = $value;
				$this->flushToFile();
			}
		}

		public function __unset(string $key)
		{
			unset($this->$key);
		}

		public function __get(string $key)
		{
			return $this->$key;
		}

		public function __isset(string $key)
		{
			return isset($this->$key);
		}

		public function init(string $key, $value, bool $flush = false)
		{
			if(property_exists(get_class($this), $this->$key))
			{
				$this->$key = $value;
				if($flush)
					$this->flushToFile();
			}
			else
			{
				return new SysError(SysError::NO_SUCH_PROPERTY_ERROR);
			}
		}

		final public function flushToFile()
		{
			$folderPath = dirname($this->filePath);
			if( !is_dir($folderPath) )
			{
				$error=Helper::createPathIfNotExists($folderPath);
				if( $error->isSuccess() )
					return $error;
			}

			/*
			 * getmypid ( void ) Liefert die Prozess-ID unter der das Skript ausgeführt wird.
			 * getmygid() - Get PHP script owner's GID
			 * getmyuid() - Zeigt die User-ID des Besitzers eines PHP-Scripts
			 * get_current_user() - Liefert den Benutzernamen des Besitzers des aktuellen PHP-Skripts
			 * getmyinode() - Ermittelt den Inode eines Skripts
			 * getlastmod() - Uhrzeit der letzten Änderung eines Scripts
			 */
			if(($this->fileHandle = fopen($this->filePath, 'w')) !== false )
			{
				$_ = $this;
				$_->putLine('<?xml version="1.0" encoding="UTF-8"?>');
				$_->putLine('<DashBoard type="' . $this->dashBoardType . '" version="' . $this->dashBoardVersion . '">');
				$_->putLine('<Activity>');
				$_->flushToFileExt();
				$_->putLine('</Activity>');
				$_->putLine('<Errors>');
				foreach($this->errors as $error)
					$_->putLine( $error->toXml(false) );
				$_->putLine('</Errors>');
				$_->putLine($this->conf->toXml(false, $this->blacklist));
				$_->putLine('<Modul>');
				$_->putLine('<Name>' . $this->moduleName . '</Name>');
				$_->putLine('<Version>' . $this->instanceVersion . '</Version>');
				$_->putLine('<Instance>' . $this->instance . '</Instance>');
				$_->putLine('</Modul>');
				$_->putLine('<Info>');
				$_->putLine('<PhpVersion>' . PHP_VERSION . '</PhpVersion>');
				$_->putLine('<OperatingSystem>' . PHP_OS . '</OperatingSystem>');
				$_->putLine('</Info>');
				$_->putLine('</DashBoard>');
			}
			else
			{
				return new SysError(SysError::INVALID_HANDLE);
			}
		}

		public abstract function flushToFileExt();

		/***********************************
		* PROTECTED METHODS                *
		***********************************/
		protected function putLine(string $line)
		{
			if(!is_resource($this->fileHandle) || get_resource_type($this->fileHandle) !== 'stream')
				return new SysError( SysError::INVALID_HANDLE );
			else
				fwrite($this->fileHandle, $line);
			return new SysError(SysError::SUCCESS );
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		private function readXml()
		{
			$xml = simpleXML_load_file($this->filePath);
			if($xml)
			{
				foreach ($xml->Activity->children() as $child)
				{
					$propName  = $child->getName();
					$propValue = $xml->Activity->{$propName};
					$this->$propName = $propValue;
				}
				foreach ($xml->Errors->children() as $errorXML)
				{
					$this->addError(TripleTowerError::fromXML($errorXML->asXML()));
				}
			}
		}
	}
}