<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\DbError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Tower;
	use TripleTowerSDK\User\G11nSettings\Language;
	use TripleTowerSDK\User\G11nSettings\Location;
	use TripleTowerSDK\User\G11nSettings\Timezone;

	class ServiceTowerDBC extends AccessRightsDBC
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function getGroupsWithAccess(string $contentProvider, string $contentType, string $contentName)
		{
			$sql = "WITH RECURSIVE accessgroupswithaccess AS
					(
						SELECT accessgroups.accessgroup_id, accessgroups.accessgroupname, accessgroups.authenticationsystem_id, contentaccess.checknestedgroups
						FROM contentaccess
						INNER JOIN accessgroups ON accessgroups.accessgroup_id = contentaccess.accessgroup_id
						WHERE LOWER(contentaccess.contentprovider) = LOWER(:contentProvider)
						AND LOWER(contentaccess.contenttype) = LOWER(:contentType)
						AND LOWER(contentaccess.contentname) = LOWER(:contentName)
						AND contentaccess.access = 1

						UNION ALL

						SELECT memberaccessgroups.accessgroup_id, memberaccessgroups.accessgroupname, memberaccessgroups.authenticationsystem_id, accessgroupswithaccess.checknestedgroups
						FROM accessgroupswithaccess
						INNER JOIN accessgroup_accessgroup_relations ON accessgroup_accessgroup_relations.accessgroup_id = accessgroupswithaccess.accessgroup_id
						INNER JOIN accessgroups AS memberaccessgroups ON memberaccessgroups.accessgroup_id = accessgroup_accessgroup_relations.accessgroupmember_id
						WHERE accessgroupswithaccess.accessgroup_id = accessgroup_accessgroup_relations.accessgroup_id
					)
					SELECT accessgroupswithaccess.accessgroupname, authenticationsystems.authenticationsystemname, accessgroupswithaccess.checknestedgroups
					FROM accessgroupswithaccess
					INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = accessgroupswithaccess.authenticationsystem_id";

			$binding = new Bindings();
			$binding->addBinding(":contentProvider", $contentProvider, \PDO::PARAM_STR);
			$binding->addBinding(":contentType", $contentType, \PDO::PARAM_STR);
			$binding->addBinding(":contentName", $contentName, \PDO::PARAM_STR);

			return $this->bindAndExec($sql, $binding, true);
		}

		public function getUserName(string $authName, string $authSystem)
		{
			$stmt = $this->prepare("SELECT users.username
									FROM users
									INNER JOIN authentications ON authentications.user_id = users.user_id
									INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = authentications.authenticationsystem_id
									WHERE authentications.authenticationname = :authName
									AND authenticationsystems.authenticationsystemname = :authSystem");
			$stmt->bindValue(':authName', $authName, \PDO::PARAM_STR);
			$stmt->bindValue(':authSystem', $authSystem, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				if($stmt->rowCount() !== 1)
					return new AuthError(AuthError::USER_NOT_FOUND, AuthError::ERR, "It seems, the user '$authName' does not exist.");

				$row = $stmt->fetch(\PDO::FETCH_ASSOC);
				return Helper::addTowerToName($row["username"]);
			}
			catch(\PDOException $ex)
			{
				return new DbError(AuthError::USER_NOT_FOUND, DbError::ERR, $ex->getMessage());
			}
		}

		public function getUserHash(string $authName, string $authSystemName)
		{
			$stmt = $this->prepare("SELECT authentications.authhash AS authhash
									FROM authentications
									INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = authentications.authenticationsystem_id
									WHERE authentications.authenticationname = :authName
									AND authenticationsystems.authenticationsystemname = :authSystem");
			$stmt->bindValue(':authName', $authName, \PDO::PARAM_STR);
			$stmt->bindValue(':authSystem', $authSystemName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();

				if($result !== false
					&& ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
					&& isset($row['authhash']))
				{
					return $row['authhash'];
				}
				else
				{
					return new AuthError(AuthError::WRONG_USERNAME, AuthError::ERR, "It seems, the user '$authName' with authSystem '$authSystemName' does not exists.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function insertUserSession(string $authName, string $authSystem, string $sessionToken, string $ipAddress)
		{
			$stmt = $this->prepare("INSERT INTO sessions
										(sessions.authentication_id,
										sessions.sessiontoken,
										sessions.remoteaddress,
										sessions.sessionstart,
										sessions.lastactive)
									VALUES
										((SELECT authentications.authentication_id
											FROM authentications
											INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = authentications.authenticationsystem_id
											WHERE authentications.authenticationname = :authName AND authenticationsystems.authenticationsystemname = :authSystem),
										:sessionToken,
										:ipAddress,
										NOW(),
										NOW())");
			$stmt->bindValue(':authName',     $authName,     \PDO::PARAM_STR);
			$stmt->bindValue(':authSystem',     $authSystem,     \PDO::PARAM_STR);
			$stmt->bindValue(':sessionToken', $sessionToken, \PDO::PARAM_STR);
			$stmt->bindValue(':ipAddress',    $ipAddress,    \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					return new DbError(DbError::SUCCESS);
				}
				else
				{
					if($stmt->errorCode() === 1062)
						return new AuthError(AuthError::SESSION_DUPLICATE_FOUND, AuthError::ERR, "Could not insert session for user '$authName' with ip '$ipAddress' because there was a session duplicate.");
					else
						return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert session for user '$authName' with ip '$ipAddress'.");
				}
			}
			catch(\PDOException $ex)
			{
				if($ex->getCode() === 1062)
					return new AuthError(AuthError::SESSION_DUPLICATE_FOUND, AuthError::ERR, $ex->getMessage());
				else
					return new DbError(DbError::INSERT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function isUserAuthenticated(string $userName, string $sessionToken, string $remoteAddress) : TripleTowerError
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT count(*) AS isauthenticated
									FROM users
									INNER JOIN authentications ON authentications.user_id = users.user_id
									INNER JOIN sessions on sessions.authentication_id = authentications.authentication_id
									WHERE users.username = :userName
										AND sessions.sessiontoken = :sessionToken
										AND sessions.remoteaddress = :remoteAddress");
			$stmt->bindValue(':userName',      $userName,      \PDO::PARAM_STR);
			$stmt->bindValue(':sessionToken',  $sessionToken,  \PDO::PARAM_STR);
			$stmt->bindValue(':remoteAddress', $remoteAddress, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();

				if($result !== false
					&& ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
					&& isset($row['isauthenticated'])
					&& $row['isauthenticated'] > 0)
				{
					return new AuthError(AuthError::SUCCESS);
				}
				else
				{
					return new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, "It seems, the user '$userName' is not authenticated.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function removeSession(string $userName, string $sessionToken, string $remoteAddress)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("DELETE FROM sessions
									WHERE sessions.sessiontoken = :sessionToken
										AND sessions.remoteaddress = :remoteAddress
										AND sessions.authentication_id IN
											(SELECT authentications.authentication_id
											 FROM users
											 INNER JOIN authentications on authentications.user_id = users.user_id
											 WHERE users.username = :userName)");

			$stmt->bindValue(':userName',      $userName,      \PDO::PARAM_STR);
			$stmt->bindValue(':sessionToken',  $sessionToken,  \PDO::PARAM_STR);
			$stmt->bindValue(':remoteAddress', $remoteAddress, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					if($stmt->rowCount() === 1)
					{
						return new AuthError(AuthError::SUCCESS);
					}
					else
					{
						if($stmt->rowCount() < 1)
							return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database for user '$userName'. No session found.");
						else
							return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ALERT, "Deleted several sessions, even there should have been only one session deleted for user '$userName'.");
					}
				}
				else
				{
					return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database for user '$userName' because: " . $stmt->errorInfo()[2]);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function removeAllSessions(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

		   $stmt = $this->prepare("DELETE FROM sessions
									WHERE sessions.authentication_id IN
											(SELECT authentications.authentication_id
											 FROM users
											 INNER JOIN authentications on authentications.user_id = users.user_id
											 WHERE users.username = :userName)");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					if($stmt->rowCount() >= 1)
						return new AuthError(AuthError::SUCCESS);
					else
						return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database for user '$userName'. No session found.");
				}
				else
				{
					return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database for user '$userName' because: " . $stmt->errorInfo()[2]);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function isUserRegistered(string $authName, string $authSystem)
		{
			$stmt = $this->prepare("SELECT count(*) AS isRegistered
									FROM authentications
									INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = authentications.authenticationsystem_id
									WHERE authentications.authenticationname = :authName
										AND authenticationsystems.authenticationsystemname = :authSystem");
			$stmt->bindValue(':authName', $authName, \PDO::PARAM_STR);
			$stmt->bindValue(':authSystem', $authSystem, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();

				if($result !== false && ($row = $stmt->fetch(\PDO::FETCH_ASSOC)))
				{
					if(isset($row['isRegistered']) && $row['isRegistered'] > 0)
						return new AuthError(AuthError::SUCCESS);
					else
						return new AuthError(AuthError::WRONG_USERNAME);
				}
				else
				{
					return new DbError(DbError::SELECT_ERROR, DbError::ERR);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function insertNewUser(string $authName, string $authSystem, string $authHash, string $publicKey, string $privateKey, string $displayName = "", bool $permanent = false)
		{
			$this->beginTransaction();
			try
			{
				$userName = $authName;
				$displayName = empty($displayName) ? $authName : $displayName;

				// TODO: It's currently not allowed to have the same user from two separate authentications
				$userStmt = $this->prepare("INSERT INTO users
												(users.username, users.displayname, users.publickey, users.privatekey)
											VALUES
												(:userName, :displayName, :publickey, :privatekey)");
				$userStmt->bindValue(':userName',    $userName,    \PDO::PARAM_STR);
				$userStmt->bindValue(':displayName', $displayName, \PDO::PARAM_STR);
				$userStmt->bindValue(':publickey',   $publicKey,   \PDO::PARAM_STR);
				$userStmt->bindValue(':privatekey',  $privateKey,  \PDO::PARAM_STR);
				$userStmt->execute();
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert new user '$authName' because of " . $ex->getMessage());
			}

			try
			{
				$userId = (int)$this->lastInsertId();
				$authStmt = $this->prepare(
					"INSERT INTO authentications
						(authentications.user_id,
						authentications.authenticationname,
						authentications.authenticationsystem_id,
						authentications.authhash)
					VALUES
						(:userId,
						:authName,
						(SELECT authenticationsystems.authenticationsystem_id FROM authenticationsystems WHERE authenticationsystems.authenticationsystemname = :authSystem),
						:authHash)");
				$authStmt->bindValue(':userId',    $userId,   \PDO::PARAM_INT);
				$authStmt->bindValue(':authName',  $authName, \PDO::PARAM_STR);
				$authStmt->bindValue(':authSystem',  $authSystem, \PDO::PARAM_STR);
				$authStmt->bindValue(':authHash',  $authHash, \PDO::PARAM_STR);
				$authStmt->execute();
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert user id '$userId' with authName '$authName' and authSystem '$authSystem' because of " . $ex->getMessage());
			}

			try
			{
				$g11nStmt = $this->prepare(
					"INSERT INTO g11nsettings
						(g11nsettings.user_id,
						g11nsettings.language_id,
						g11nsettings.location_id,
						g11nsettings.timzeone_id)
					VALUES
						(:userId,
						(SELECT languages.language_id FROM languages WHERE languages.languagekey = :language),
						(SELECT locations.location_id FROM locations WHERE locations.locationkey = :location),
						(SELECT timezones.timezone_id FROM timezones WHERE timezones.timezonekey = :timezone))");
				$g11nStmt->bindValue(':userId',   $userId, \PDO::PARAM_INT);
				$g11nStmt->bindValue(':language', Language::G11N_DEFAULT_LANGUAGE, \PDO::PARAM_STR);
				$g11nStmt->bindValue(':location', Location::G11N_DEFAULT_LOCATION, \PDO::PARAM_STR);
				$g11nStmt->bindValue(':timezone', Timezone::G11N_DEFAULT_TIMEZONE,   \PDO::PARAM_STR);
				$g11nStmt->execute();
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert g11n settings for new user '$authName' because of " . $ex->getMessage());
			}

			try
			{
				$spaceStmt = $this->prepare(
					"INSERT INTO userspaces
						(userspaces.user_id)
					VALUES
						(:userId)");
				$spaceStmt->bindValue(':userId', $userId, \PDO::PARAM_INT);
				$spaceStmt->execute();
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert user space for new user '$authName' because of " . $ex->getMessage());
			}

			$this->commit();
			return new DbError(DbError::SUCCESS);
		}

		public function getAuthSystemBySessionToken(string $userName, string $sessionToken)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT authenticationsystems.authenticationsystemname AS authenticationsystemname
									FROM users
									INNER JOIN authenticationsystems on authenticationsystems.user_id = users.user_id
									INNER JOIN authentications ON authentications.authenticationsystem_id = authenticationsystems.authenticationsystem_id
									INNER JOIN sessions ON sessions.authentication_id = authentications.authentication_id
									WHERE users.username = :userName
									AND sessions.sessiontoken = :sessionToken");
			$stmt->bindValue(':userName',     $userName,     \PDO::PARAM_STR);
			$stmt->bindValue(':sessionToken', $sessionToken, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false && ($row = $stmt->fetch(\PDO::FETCH_ASSOC)))
				{
					if(isset($row['authenticationsystemname']))
						return $row['authenticationsystemname'];
					else
						return new AuthError(AuthError::INVALID_SESSION, DbError::ERR, "There is no user with id '$userName' with given token.");
				}
				else
				{
					return new DbError(DbError::SELECT_ERROR, DbError::ERR);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function grantAccess(string $authName, string $authSystem, int $bootloader, $views = null, $spaceName = null, $wappTowerId = null)
		{
			// Get userId
			$userId = $this->getUserId($authName, $authSystem);
			if(TripleTowerError::isError($userId))
				return $userId;

			// Insert bootloader access
			$bootloaderStmt = $this->prepare(
					"INSERT INTO bootloaderaccess
						(bootloaderaccess.user_id,
						bootloaderaccess.bootloader_id,
						bootloaderaccess.access)
					VALUES
						(:userId,
						(SELECT bootloader.bootloader_id
							FROM bootloader
							WHERE bootloader.name = :bootloader
							LIMIT 1),
						1)
					ON DUPLICATE KEY UPDATE bootloaderaccess.access = 1");

			$stmt->bindValue(':userId',     $userId  ,       \PDO::PARAM_INT);
			$stmt->bindValue(':bootloader', $bootloaderName, \PDO::PARAM_STR);

			try
			{
				$result = $bootloaderStmt->execute();
				if($result === false)
					return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not grant access to '$authName' for bootloader '$bootloader'.");

				// Insert default user space
				if(!empty($spaceName))
				{
					$userSpaceStmt = $this->prepare(
							"INSERT INTO userspaces
								(userspaces.user_id,
								 userspaces.displayname)
							 VALUES
								(:userId,
								 :displayName)");

					$userSpaceStmt->bindValue(":userId",      $userId,    \PDO::PARAM_INT);
					$userSpaceStmt->bindValue(":displayName", $spaceName, \PDO::PARAM_STR);

					$result = $userSpaceStmt->execute();
					if($result === false)
						return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert user space for user id '$userId'.");
				}

				// Insert default user wapptower
				if(!empty($wappTowerId))
				{
					$wappTowerStmt = $this->prepare(
							"INSERT INTO userwappstowers
								(userwappstowers.user_id,
								 userwappstowers.wappstower_id,
								 userwappstowers.name)
							 VALUES
								(:userId,
								 :wappstowerId,
								 'Default Catalog')");

					$wappTowerStmt->bindValue(":userId",       $userId,      \PDO::PARAM_INT);
					$wappTowerStmt->bindValue(":wappstowerId", $wappTowerId, \PDO::PARAM_INT);

					$result = $wappTowerStmt->execute();
					if($result === false)
						return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert wapptower (with the id '$wappTowerId') for the user id '$userId'.");
				}

				// Insert view access
				if(!empty($views))
				{
					$stmt = "INSERT INTO viewaccess
							(viewaccess.user_id,
							viewaccess.view_id,
							viewaccess.access)
							VALUES ";

					for($viewLoop = 0; $viewLoop < count($views); $viewLoop++)
					{
						if($viewLoop > 0)
							$stmt .= ", ";

						$stmt .= "(:userId,
							(SELECT views.view_id
									 FROM views
									 WHERE views.bootloader_id = :bootloaderId
											AND views.name = :view$viewLoop
								LIMIT 1),
									1)";
					}

					$stmt .= " ON DUPLICATE KEY UPDATE viewaccess.access = 1";

					$viewStmt = $this->prepare($stmt);

					$viewStmt->bindValue(':userId',       $userId,       \PDO::PARAM_INT);
					$viewStmt->bindValue(':bootloaderId', $bootloaderId, \PDO::PARAM_INT);

					$viewLoop = 0;
					foreach($views as $view)
					{
						$viewStmt->bindValue(":view$viewLoop", trim($view), \PDO::PARAM_STR);
						$viewLoop++;
					}

					$result = $viewStmt->execute();
					if($result === false)
						return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not grant access to user '$authName' for views '" . implode("', '", $views) . "' in bootloader '$bootloader'.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, $ex->getMessage());
			}
			return new DbError(DbError::SUCCESS);
		}

		public function setLastActive(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("UPDATE users SET lastactive = NOW() WHERE users.username = :userName");
			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				return new DbError(DbError::SUCCESS);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::UPDATE_ERROR, DbError::ERR, "Last active time could not be set because: " . $ex->getMessage());
			}
		}

		public function getLastActive(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT lastactive FROM users WHERE users.username = :userName");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();

				if($result === true
					&& ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
					&& isset($row['lastactive']))
				{
					return $row['lastactive'];
				}
				else
				{
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve last active time for user ID '$userId'.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getUserInfos(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT
										(SELECT count(sessions.session_id)
											FROM sessions
											INNER JOIN authentications ON authentications.authentication_id = sessions.authentication_id
											INNER JOIN users ON users.user_id = authentications.user_id
											WHERE users.username = :userName
										) as activesessions,

										users.username,
										users.displayname,
										users.publickey,
										users.privatekey,
										users.avatarurl,
										users.blocked,
										users.entrytime,
										users.lastactive

									FROM users
									WHERE users.username = :userName");
			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				if(($row = $stmt->fetch(\PDO::FETCH_ASSOC)))
				{
					$row["username"] = Helper::addTowerToName($row["username"]);
					return $row;
				}
				else
				{
					return new AuthError(AuthError::USER_NOT_FOUND, AuthError::ERR, "It seems, the user '$userName' does not exists.");
				}
			}
			catch(\PDOException $ex)
			{
				return new AuthError(AuthError::USER_NOT_FOUND, AuthError::ERR, $ex->getMessage());
			}
		}

		public function getAuthenticationIds(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT authentications.authentication_id
									FROM users
									INNER JOIN authentications ON authentications.user_id = users.user_id
									WHERE users.username = :userName");
			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getAuthenticationInfos(int $authenticationId)
		{
			$stmt = $this->prepare("SELECT authenticationsystems.authenticationsystemname, authentications.authenticationname,
										(SELECT COUNT(authenticationtrials.authenticationtrial_id)
											FROM authenticationtrials
											WHERE authenticationtrials.authentication_id = authentications.authentication_id
										) AS authenticationtrials

									FROM authenticationsystems
									INNER JOIN authentications ON authentications.authenticationsystem_id = authenticationsystems.authenticationsystem_id
									WHERE authentications.authentication_id = :authenticationId");
			$stmt->bindValue(':authenticationId', $authenticationId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getFellowGroupIds(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT fellowgroups.fellowgroup_id
									FROM users
									INNER JOIN fellowgroups ON fellowgroups.user_id = users.user_id
									WHERE users.username = :userName");
			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getFellowGroupInfos(int $fellowGroupId)
		{
			$stmt = $this->prepare("SELECT fellowgroups.fellowgroupname
									FROM fellowgroups
									WHERE fellowgroups.fellowgroup_id = :fellowgroupId");
			$stmt->bindValue(':fellowgroupId', $fellowGroupId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getFellowGroupRights(int $fellowGroupId)
		{
			$stmt = $this->prepare("SELECT fellowgrouprights.fellowgroupright
									FROM fellowgrouprights
									WHERE fellowgrouprights.fellowgroup_id = :fellowgroupId");
			$stmt->bindValue(':fellowgroupId', $fellowGroupId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getFellowIds(int $fellowGroupId)
		{
			$stmt = $this->prepare("SELECT fellows.fellow_id
									FROM fellows
									WHERE fellows.fellowgroup_id = :fellowGroupId");
			$stmt->bindValue(':fellowGroupId', $fellowGroupId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getFellowInfos(int $fellowId)
		{
			$stmt = $this->prepare("SELECT users.username, users.displayname, users.avatarurl, users.lastactive
									FROM users
									WHERE users.user_id = :fellowId");
			$stmt->bindValue(':fellowId', $fellowId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				$row = $stmt->fetch(\PDO::FETCH_ASSOC);
				$row["username"] = Helper::addTowerToName($row["username"]);
				return $row;
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getG11nSettingsId(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT g11nsettings.g11nsetting_id
									FROM users
									INNER JOIN g11nsettings ON g11nsettings.user_id = users.user_id
									WHERE users.username = :userName");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getG11nSettingsInfos(int $g11nSettingsId)
		{
			$stmt = $this->prepare("SELECT languages.languagekey, locations.locationkey, timezones.timezonekey
									FROM g11nsettings
									INNER JOIN languages ON languages.language_id = g11nsettings.language_id
									INNER JOIN locations ON locations.location_id = g11nsettings.location_id
									INNER JOIN timezones ON timezones.timezone_id = g11nsettings.timzeone_id

									WHERE g11nsettings.g11nsetting_id = :g11nSettingsId");

			$stmt->bindValue(':g11nSettingsId', $g11nSettingsId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getUserSpaceIds(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT userspaces.userspace_id
									FROM users
									INNER JOIN userspaces ON userspaces.user_id = users.user_id
									WHERE users.username = :userName");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getUserSpaceInfos(int $userSpaceId)
		{
			$stmt = $this->prepare("SELECT userspaces.displayname,
											userspaces.backgroundurl,
											userspaces.backgroundcolor,
											userspaces.windowcolor,
											userspaces.tileshape,
											userspaces.tilesize
									FROM userspaces
									WHERE userspaces.userspace_id = :userSpaceId");

			$stmt->bindValue(':userSpaceId', $userSpaceId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getInstalledWappIds(int $userSpaceId)
		{
			$stmt = $this->prepare("SELECT installedwapps.installedwapp_id
									FROM installedwapps
									WHERE installedwapps.userspace_id = :userSpaceId");

			$stmt->bindValue(':userSpaceId', $userSpaceId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getInstalledWappInfos(int $installedWappId)
		{
			$stmt = $this->prepare("SELECT installedwapps.wappname, installedwapps.wappstowerinfo_id
									FROM installedwapps
									WHERE installedwapps.installedwapp_id = :installedWappId");

			$stmt->bindValue(':installedWappId', $installedWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getWappsTowerIds()
		{
			$stmt = $this->prepare("SELECT wappstowerinfos.wappstowerinfo_id
									FROM wappstowerinfos
									WHERE wappstowerinfos.visible = 1");
			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getWappsTowerInfos(int $wappsTowerInfoId)
		{
			$stmt = $this->prepare("SELECT wappstowerinfos.servername,
											wappstowerinfos.publickey,
											wappstowerinfos.displayname,
											wappstowerinfos.visible
									FROM wappstowerinfos
									WHERE wappstowerinfos.wappstowerinfo_id = :wappsTowerInfoId");

			$stmt->bindValue(':wappsTowerInfoId', $wappsTowerInfoId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getWappPermissionInfos(int $installedWappId)
		{
			$stmt = $this->prepare("SELECT wapppermissions.permission
									FROM wapppermissions
									WHERE wapppermissions.installedwapp_id = :installedWappId");

			$stmt->bindValue(':installedWappId', $installedWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getDesktopWappIds(int $installedWappId)
		{
			$stmt = $this->prepare("SELECT desktopwapps.desktopwapp_id
									FROM desktopwapps
									WHERE desktopwapps.installedwapp_id = :installedWappId");

			$stmt->bindValue(':installedWappId', $installedWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getDesktopWappInfos(int $desktopWappId)
		{
			$stmt = $this->prepare("SELECT desktopwapps.positionx,
											desktopwapps.positiony,
											desktopwapps.displayname,
											desktopwapps.parameters
									FROM desktopwapps
									WHERE desktopwapps.desktopwapp_id = :desktopWappId");

			$stmt->bindValue(':desktopWappId', $desktopWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getStartmenuWappIds(int $installedWappId)
		{
			$stmt = $this->prepare("SELECT startmenuwapps.startmenuwapp_id
									FROM startmenuwapps
									WHERE startmenuwapps.installedwapp_id = :installedWappId");

			$stmt->bindValue(':installedWappId', $installedWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getStartmenuWappInfos(int $startmenuWappId)
		{
			$stmt = $this->prepare("SELECT startmenuwapps.position,
											startmenuwapps.displayname,
											startmenuwapps.parameters
									FROM startmenuwapps
									WHERE startmenuwapps.startmenuwapp_id = :startmenuWappId");

			$stmt->bindValue(':startmenuWappId', $startmenuWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getTaskbarWappIds(int $installedWappId)
		{
			$stmt = $this->prepare("SELECT taskbarwapps.taskbarwapp_id
									FROM taskbarwapps
									WHERE taskbarwapps.installedwapp_id = :installedWappId");

			$stmt->bindValue(':installedWappId', $installedWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getTaskbarWappInfos(int $taskbarWappId)
		{
			$stmt = $this->prepare("SELECT taskbarwapps.position,
											taskbarwapps.displayname,
											taskbarwapps.parameters
									FROM taskbarwapps
									WHERE taskbarwapps.taskbarwapp_id = :taskbarWappId");

			$stmt->bindValue(':taskbarWappId', $taskbarWappId, \PDO::PARAM_INT);

			try
			{
				$stmt->execute();
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, __METHOD__ . " failed because '" . $ex->getMessage() . "'");
			}
		}

		public function getUserWappsTowers(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$towerType = Tower::WAPPS_TOWER;

			$stmt = $this->prepare("SELECT :towerType AS type, userwappstowers.name, wappstowers.url AS towerUrl, wappstowers.publickey, userwappstowers.userwappstower_id
									FROM users
									INNER JOIN userwappstowers on userwappstowers.user_id = users.user_id
									INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE users.username = :userName");

			$stmt->bindValue(":userName", $userName, \PDO::PARAM_STR);
			$stmt->bindValue(":towerType", $towerType, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve any WappsTower information for user '$userName'.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getDesktopWapps(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT desktopwapps.desktopwapp_id,
											userspaces.displayname AS spacename,
											desktopwapps.displayname, desktopwapps.positionx, desktopwapps.positiony,
											installedwapps.wappname,
											wappstowerinfos.displayname AS towername,
											wappstowerinfos.servername AS towerurl
									FROM users
									INNER JOIN userspaces on userspaces.user_id = users.user_id
									INNER JOIN installedwapps on installedwapps.userspace_id = userspaces.userspace_id
									INNER JOIN desktopwapps ON desktopwapps.installedwapp_id = installedwapps.installedwapp_id
									INNER JOIN wappstowerinfos ON wappstowerinfos.wappstowerinfo_id = installedwapps.wappstowerinfo_id
									WHERE users.username = :userName");
			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve desktop wapps for user '$userName'.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function checkInstalledDesktopWapp(int $userSpaceId, string $wappName)
		{
			$stmt = $this->prepare("SELECT userdesktopwapps.userdesktopwapp_id
									FROM userdesktopwapps
									INNER JOIN userinstalledwapps ON userinstalledwapps.userinstalledwapp_id = userdesktopwapps.userinstalledwapp_id
									WHERE userdesktopwapps.userspace_id = :userSpaceId
										AND userinstalledwapps.wappname = :wappName");

			$stmt->bindValue(':userSpaceId', $userSpaceId,  \PDO::PARAM_INT);
			$stmt->bindValue(':wappName',    $wappName,     \PDO::PARAM_STR);

			try
			{
				if($stmt->rowCount() === 0)
					return new DbError(DbError::SUCCESS);
				else
					return new DbError(DbError::DUPLICATE_ENTRY, DbError::ERR);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getDesktopWappCoordinatesByUserSpaceId(int $userDesktopId)
		{
			$stmt = $this->prepare("SELECT userdesktopwapps.positionx, userdesktopwapps.positiony
									FROM userdesktopwapps
									WHERE userdesktopwapps.userspace_id = :userSpaceId");

			$stmt->bindValue(':userSpaceId',  $userSpaceId, \PDO::PARAM_INT);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError (DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve any coordinate information for desktop id '$userSpaceId'.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function deleteWappRequest(int $requestId)
		{
			$stmt = $this->prepare("DELETE FROM userwapprequests
									WHERE userwapprequest_id = :userwapprequestId");

			$stmt->bindValue(':userwapprequestId', $requestId, \PDO::PARAM_INT);

			try
			{
				$result = $stmt->execute();
				if($result === true)
				{
					if($stmt->rowCount() === 1)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Could not delete request with ID $requestId");
				}
				else
				{
					return new DbError(DbError::DELETE_ERROR, DbError::ERR, "Could not delete request with ID $requestId");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function addWappRequest(string $userName, int $spaceId, string $wappStoreUrl, string $wappName, string $reason)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("INSERT INTO userwapprequests
										(userwapprequests.user_id, userwapprequests.userspace_id, userwapprequests.userwappstower_id, userwapprequests.wappname, userwapprequests.reason, userwapprequests.requesttime)
									SELECT users.user_id, userspaces.userspace_id, userwappstowers.userwappstower_id, :wappName, :reason, NOW()
									FROM userspaces
										INNER JOIN users ON users.user_id = userspaces.user_id
										INNER JOIN userwappstowers ON userwappstowers.user_id = users.user_id
										INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE users.username = :userName
										AND userspaces.userspace_id = :spaceId
										AND wappstowers.url = :wappStoreUrl
									ON DUPLICATE KEY UPDATE userwapprequests.user_id = :userId");

			$stmt->bindValue(':userName',     $userName,     \PDO::PARAM_STR);
			$stmt->bindValue(':spaceId',      $spaceId,      \PDO::PARAM_INT);
			$stmt->bindValue(':wappStoreUrl', $wappStoreUrl, \PDO::PARAM_STR);
			$stmt->bindValue(':wappName',     $wappName,     \PDO::PARAM_STR);
			$stmt->bindValue(':reason',       $reason,       \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($stmt->rowCount() === 1)
					return new DbError(DbError::SUCCESS);
				elseif($stmt->rowCount() === 0)
					return new DbError(DbError::DUPLICATE_ENTRY, DbError::ERR);
				else
					return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Wapp $wappName from wapptower $wappStoreUrl could not be added to request table for user $userId. This can happen because:<br />1. The wapptower is unknown.<br />2. The user is unknown.<br />3. The desktop does not exist.<br />4. The desktop tile is already occupied.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function addWapp( int $userSpaceId, string $wappsTowerServerName, string $wappName)
		{
			$sql = "INSERT INTO installedwapps
						(userspace_id, wappstowerinfo_id, wappname)
					SELECT :userSpaceId, wappstowerinfos.wappstowerinfo_id, :wappName FROM wappstowerinfos
						WHERE wappstowerinfos.servername = :wappsTowerServerName";

			$bindings = new Bindings();
			$bindings->addBinding(':userSpaceId',          $userSpaceId,          self::PARAM_INT);
			$bindings->addBinding(':wappsTowerServerName', $wappsTowerServerName, self::PARAM_STR);
			$bindings->addBinding(':wappName',             $wappName,             self::PARAM_STR);

			$result = $this->bindAndExec( $sql, $bindings, false );

			if( !$result->isSuccess() )
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not add wapp $wappName from wapptower $wappsTowerServerName to userspace with id $userSpaceId because: " . $result->getMessage());

			return $result;
		}

		public function addDesktopWapp( int $userSpaceId, string $wappsTowerServerName, string $wappName, int $x, int $y)
		{
			$sql = "INSERT INTO desktopwapps
						( installedwapp_id, displayname, positionx, positiony, parameters )
					SELECT installedwapps.installedwapp_id, installedwapps.wappname, :x, :y, ''
						FROM userspaces
						INNER JOIN installedwapps ON installedwapps.userspace_id = userspaces.userspace_id
						INNER JOIN wappstowerinfos ON wappstowerinfos.wappstowerinfo_id = installedwapps.wappstowerinfo_id
					WHERE userspaces.userspace_id = :userSpaceId
						AND installedwapps.wappname = :wappName
						AND wappstowerinfos.servername = :wappsTowerServerName";

			$bindings = new Bindings();
			$bindings->addBinding(':userSpaceId',          $userSpaceId,          \PDO::PARAM_INT);
			$bindings->addBinding(':wappsTowerServerName', $wappsTowerServerName, \PDO::PARAM_STR);
			$bindings->addBinding(':wappName',             $wappName,             \PDO::PARAM_STR);
			$bindings->addBinding(':x',                    $x,                    \PDO::PARAM_INT);
			$bindings->addBinding(':y',                    $y,                    \PDO::PARAM_INT);

			$result = $this->bindAndExec( $sql, $bindings, false );

			if( !$result->isSuccess() )
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not add desktop wapp for $wappName from wapptower $wappsTowerServerName to userspace with id $userSpaceId because: " . $result->getMessage());

			return $result;
		}

		public function deleteDesktopWapp(string $userName, int $spaceId, string $wappName, int $userDesktopWappId)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("DELETE userdesktopwapps
									FROM userdesktopwapps
									INNER JOIN userinstalledwapps on userinstalledwapps.userinstalledwapp_id = userdesktopwapps.userinstalledwapp_id
									WHERE userdesktopwapps.userdesktopwapp_id = :userDesktopWappId
										AND userdesktopwapps.userspace_id = :spaceId
										AND userinstalledwapps.wappname = :wappName");

			$stmt->bindValue(':userDesktopWappId', $userDesktopWappId, \PDO::PARAM_INT);
			$stmt->bindValue(':spaceId',           $spaceId,           \PDO::PARAM_INT);
			$stmt->bindValue(':wappName',          $wappName,          \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
				{
					if($stmt->rowCount() === 1)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Wapp '$wappName' could not be deleted from space '$spaceId' for user '$userName'. This can happen because:<br />1. The wapptower is unknown.<br />2. The user is unknown.<br />3. The space does not exist.<br />4. The space tile is already occupied.");
				}
				else
				{
					return new DbError(DbError::DELETE_ERROR, DbError::ERR, "Could not delete wapp '$wappName' for user with id $userName because: " . $stmt->errorInfo());
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function addWappPermission(string $userName, string $wappsTowerUrl, string $wappName, string $permission)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("INSERT INTO wapppermissions
										(wapppermissions.user_id, wapppermissions.userwappstower_id, wapppermissions.wappname, wapppermissions.permission)
									SELECT users.user_id, userwappstowers.userwappstower_id, :wappName, :permission
									FROM users
										INNER JOIN userwappstowers ON userwappstowers.user_id = users.user_id
										INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE users.username = :userName
										AND wappstowers.url = :wappsTowerUrl");

			$stmt->bindValue(':userName',      $userName,      \PDO::PARAM_STR);
			$stmt->bindValue(':wappsTowerUrl', $wappsTowerUrl, \PDO::PARAM_STR);
			$stmt->bindValue(':wappName',      $wappName,      \PDO::PARAM_STR);
			$stmt->bindValue(':permission',    $permission,    \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
				{
					if($stmt->rowCount() === 1)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Wapp '$wappName' could not be granted permission '$permission' for user $userName. This can happen because:<br />1. The wapptower is unknown.<br />2. The user is unknown.<br />3. The permission is already granted.");
				}
				else
				{
					return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Wapp '$wappName' could not be granted permission '$permission' for user $userName because: " . $stmt->errorInfo());
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Wapp '$wappName' could not be granted permission '$permission' for user $userName because: " . $ex->getMessage());
			}
		}

		public function getAllWappPermissions(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT wapppermissions.user_id, wappstowers.url AS towerUrl, wapppermissions.wappname, wapppermissions.permission
									FROM wapppermissions
									INNER JOIN userwappstowers ON userwappstowers.userwappstower_id = wapppermissions.userwappstower_id
									INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE wapppermissions.username = :userName");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError (DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve wapp permissions for user '$userName'.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve wapp permissions for user '$userName' because: " . $ex->getMessage());
			}
		}

		public function removeWappPermission(string $userName, string $wappsTowerUrl, string $wappName, string $permission)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("DELETE wapppermissions
									FROM wapppermissions
										INNER JOIN userwappstowers ON userwappstowers.userwappstower_id = wapppermissions.userwappstower_id
										INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE wapppermissions.username = :userName
										AND wappstowers.url = :wappsTowerUrl
										AND wapppermissions.wappname = :wappName
										AND wapppermissions.permission = :permission");

			$stmt->bindValue(':userName',      $userName,      \PDO::PARAM_STR);
			$stmt->bindValue(':wappsTowerUrl', $wappsTowerUrl, \PDO::PARAM_STR);
			$stmt->bindValue(':wappName',      $wappName,      \PDO::PARAM_STR);
			$stmt->bindValue(':permission',    $permission,    \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
				{
					if($stmt->rowCount() === 1)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "$wappName's permission '$permission' could not be deleted for user with id $userName because it was not found. It may has already been deleted or was never granted.");
				}
				else
				{
					return new DbError(DbError::DELETE_ERROR, DbError::ERR, "$wappName's permission '$permission' could not be deleted for user with id $userName because: " . $stmt->errorInfo());
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, "$wappName's permission '$permission' could not be deleted for user with id $userName because: " . $ex->getMessage());
			}
		}

		public function removeAllWappPermissions(string $userName, string $wappsTowerUrl, string $wappName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("DELETE wapppermissions
									FROM wapppermissions
										INNER JOIN userwappstowers ON userwappstowers.userwappstower_id = wapppermissions.userwappstower_id
										INNER JOIN wappstowers ON wappstowers.wappstower_id = userwappstowers.wappstower_id
									WHERE wapppermissions.username = :userName
										AND wappstowers.url = :wappsTowerUrl
										AND wapppermissions.wappname = :wappName");

			$stmt->bindValue(':userName',      $userName,      \PDO::PARAM_STR);
			$stmt->bindValue(':wappsTowerUrl', $wappsTowerUrl, \PDO::PARAM_STR);
			$stmt->bindValue(':wappName',      $wappName,      \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return new DbError(DbError::SUCCESS);
				else
					return new DbError(DbError::DELETE_ERROR, DbError::ERR, "$wappName's permissions could not be deleted for user with id $userName because: " . $stmt->errorInfo());
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, "$wappName's permissions could not be deleted for user with id $userName because: " . $ex->getMessage());
			}
		}

		public function updateDesktopWappPosition(int $userSpaceId, string $wappName, int $oldX, int $oldY, int $newX, int $newY)
		{
			$sql = "UPDATE desktopwapps
					INNER JOIN installedwapps ON installedwapps.installedwapp_id = desktopwapps.installedwapp_id
					SET desktopwapps.positionx = :newX, desktopwapps.positiony = :newY
					WHERE installedwapps.userspace_id = :userSpaceId
						AND installedwapps.wappname = :wappName
						AND desktopwapps.positionx = :oldX
						AND desktopwapps.positiony = :oldY";

			$bindings = new Bindings();
			$bindings->addBinding(':userSpaceId', $userSpaceId, self::PARAM_INT);
			$bindings->addBinding(':wappName',    $wappName,    self::PARAM_STR);
			$bindings->addBinding(':oldX',        $oldX,        self::PARAM_INT);
			$bindings->addBinding(':oldY',        $oldY,        self::PARAM_INT);
			$bindings->addBinding(':newX',        $newX,        self::PARAM_INT);
			$bindings->addBinding(':newY',        $newY,        self::PARAM_INT);


			$result = $this->bindAndExec( $sql, $bindings, false );

			if( !$result->isSuccess() )
				return new DbError(DbError::INSERT_ERROR, DbError::ERR, "DesktopWapp $wappName could not be saved to $newX/$newY on Desktop $spaceId from user $userName because: " . $result->getMessage());

			return $result;
		}

		public function getAppearanceData(string $userName, int $spaceId)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT userspaces.backgroundurl AS backgroundurl,
										   userspaces.backgroundcolor AS backgroundcolor,
										   userspaces.tileSize AS tilesize,
										   userspaces.tileShape AS tileshape,
										   userspaces.windowColor AS windowcolor
									FROM users
									INNER JOIN userspaces on userspaces.user_id = users.user_id
									WHERE userspaces.userspace_id = :userspaceId
										AND userspaces.user_id = :userId");

			$stmt->bindValue(':userspaceId', $spaceId,  \PDO::PARAM_STR);
			$stmt->bindValue(':userId',      $userName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError (DbError::SELECT_ERROR, DbError::ERR, "Could not retrieve any appearance information for desktop ID $spaceId");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}



		public function updateAppearanceData(string $userName, int $spaceId, string $backgroundurl, string $backgroundcolor, int $tilesize, string $tileshape, string $windowcolor)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("UPDATE userspaces
									INNER JOIN users on users.user_id = userspaces.user_id
									SET userspaces.backgroundurl = :backgroundurl,
										userspaces.backgroundcolor = :backgroundcolor,
										userspaces.tileSize = :tilesize,
										userspaces.tileShape = :tileshape,
										userspaces.windowColor = :windowcolor
									WHERE users.username = :userName
										AND userspaces.userspace_id = :spaceId");

			$stmt->bindValue(':backgroundurl',	 $backgroundurl,   \PDO::PARAM_STR);
			$stmt->bindValue(':tilesize',		 $tilesize,        \PDO::PARAM_INT);
			$stmt->bindValue(':tileshape',		 $tileshape,       \PDO::PARAM_STR);
			$stmt->bindValue(':windowcolor',	 $windowcolor,     \PDO::PARAM_STR);
			$stmt->bindValue(':backgroundcolor', $backgroundcolor, \PDO::PARAM_STR);
			$stmt->bindValue(':userName',		 $userName,        \PDO::PARAM_STR);
			$stmt->bindValue(':spaceId',		 $spaceId,         \PDO::PARAM_INT);

			try
			{
				$result = $stmt->execute();
				if($result === true)
				{
					if($stmt->rowCount() === 1)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Appearance data could not be saved. This can happen because:<br />1. The user is unknown.<br />2. The desktop does not belong to the user.<br />3. The desktop does not exist.");
				}
				else
				{
					return new DbError(DbError::UPDATE_ERROR, DbError::ERR, "Appearance data could not be saved because: " . $stmt->errorInfo());
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::UPDATE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getInstalledWapps(string $userName)
		{
			$userName = Helper::removeTowerFromName($userName);
			if( TripleTowerError::isError($userName) )
				return $userName;

			$stmt = $this->prepare("SELECT users.displayname, users.avatarurl, users.lastactive, relationshiptypes.relationshipname
									FROM users
									INNER JOIN relationships ON relationships.fellow_id = users.user_id
									INNER JOIN relationshiptypes ON relationshiptypes.relationshiptype_id = relationships.relationshiptype_id
									WHERE relationships.username = :userName");

			$stmt->bindValue(':userName', $userName, \PDO::PARAM_STR);

						try
			{
				$result = $stmt->execute();

				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not get user spaces for user ID '$userId'.");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}

		}

		protected function getUserId(string $authName, string $authSystem)
		{
			$stmt = $this->prepare("SELECT authentications.user_id
									FROM authentications INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = authentications.authenticationsystem_id
									WHERE authentications.authenticationname = :authName
									AND authenticationsystems.authenticationsystemname = :authSystem");
			$stmt->bindValue(':authName', $authName, \PDO::PARAM_STR);
			$stmt->bindValue(':authSystem', $authSystem, \PDO::PARAM_STR);

			try
			{
				$stmt->execute();
				if($stmt->rowCount() !== 1)
					return new AuthError(AuthError::USER_NOT_FOUND, AuthError::INFO, "It seems, the user '$authName' does not exist.");

				return (int)$stmt->fetch(\PDO::FETCH_ASSOC)["user_id"];
			}
			catch(\PDOException $ex)
			{
				return new DbError(AuthError::USER_NOT_FOUND, DbError::ERR, $ex->getMessage());
			}
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}