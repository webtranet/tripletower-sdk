<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{

	final class SQLiteConstants
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const SQLITE_OK                      =       0;
		const SQLITE_ERROR                   =       1;
		const SQLITE_INTERNAL                =       2;
		const SQLITE_PERM                    =       3;
		const SQLITE_ABORT                   =       4;
		const SQLITE_BUSY                    =       5;
		const SQLITE_LOCKED                  =       6;
		const SQLITE_NOMEM                   =       7;
		const SQLITE_READONLY                =       8;
		const SQLITE_INTERRUPT               =       9;
		const SQLITE_IOERR                   =      10;
		const SQLITE_CORRUPT                 =      11;
		const SQLITE_NOTFOUND                =      12;
		const SQLITE_FULL                    =      13;
		const SQLITE_CANTOPEN                =      14;
		const SQLITE_PROTOCOL                =      15;
		const SQLITE_EMPTY                   =      16;
		const SQLITE_SCHEMA                  =      17;
		const SQLITE_TOOBIG                  =      18;
		const SQLITE_CONSTRAINT              =      19;
		const SQLITE_MISMATCH                =      20;
		const SQLITE_MISUSE                  =      21;
		const SQLITE_NOLFS                   =      22;
		const SQLITE_AUTH                    =      23;
		const SQLITE_FORMAT                  =      24;
		const SQLITE_RANGE                   =      25;
		const SQLITE_NOTADB                  =      26;
		const SQLITE_NOTICE                  =      27;
		const SQLITE_WARNING                 =      28;
		const SQLITE_ROW                     =     100;
		const SQLITE_DONE                    =     101;
		const SQLITE_IOERR_READ              =     266; //  (self::SQLITE_IOERR      | (1<<8));
		const SQLITE_IOERR_SHORT_READ        =     522; //  (self::SQLITE_IOERR      | (2<<8));
		const SQLITE_IOERR_WRITE             =     778; //  (self::SQLITE_IOERR      | (3<<8));
		const SQLITE_IOERR_FSYNC             =    1034; //  (self::SQLITE_IOERR      | (4<<8));
		const SQLITE_IOERR_DIR_FSYNC         =    1290; //  (self::SQLITE_IOERR      | (5<<8));
		const SQLITE_IOERR_TRUNCATE          =    1546; //  (self::SQLITE_IOERR      | (6<<8));
		const SQLITE_IOERR_FSTAT             =    1802; //  (self::SQLITE_IOERR      | (7<<8));
		const SQLITE_IOERR_UNLOCK            =    2058; //  (self::SQLITE_IOERR      | (8<<8));
		const SQLITE_IOERR_RDLOCK            =    2314; //  (self::SQLITE_IOERR      | (9<<8));
		const SQLITE_IOERR_DELETE            =    2570; //  (self::SQLITE_IOERR      | (10<<8));
		const SQLITE_IOERR_BLOCKED           =    2826; //  (self::SQLITE_IOERR      | (11<<8));
		const SQLITE_IOERR_NOMEM             =    3082; //  (self::SQLITE_IOERR      | (12<<8));
		const SQLITE_IOERR_ACCESS            =    3338; //  (self::SQLITE_IOERR      | (13<<8));
		const SQLITE_IOERR_CHECKRESERVEDLOCK =    3594; //  (self::SQLITE_IOERR      | (14<<8));
		const SQLITE_IOERR_LOCK              =    3850; //  (self::SQLITE_IOERR      | (15<<8));
		const SQLITE_IOERR_CLOSE             =    4106; //  (self::SQLITE_IOERR      | (16<<8));
		const SQLITE_IOERR_DIR_CLOSE         =    4362; //  (self::SQLITE_IOERR      | (17<<8));
		const SQLITE_IOERR_SHMOPEN           =    4618; //  (self::SQLITE_IOERR      | (18<<8));
		const SQLITE_IOERR_SHMSIZE           =    4874; //  (self::SQLITE_IOERR      | (19<<8));
		const SQLITE_IOERR_SHMLOCK           =    5130; //  (self::SQLITE_IOERR      | (20<<8));
		const SQLITE_IOERR_SHMMAP            =    5386; //  (self::SQLITE_IOERR      | (21<<8));
		const SQLITE_IOERR_SEEK              =    5642; //  (self::SQLITE_IOERR      | (22<<8));
		const SQLITE_IOERR_DELETE_NOENT      =    5898; //  (self::SQLITE_IOERR      | (23<<8));
		const SQLITE_IOERR_MMAP              =    6154; //  (self::SQLITE_IOERR      | (24<<8));
		const SQLITE_IOERR_GETTEMPPATH       =    6410; //  (self::SQLITE_IOERR      | (25<<8));
		const SQLITE_IOERR_CONVPATH          =    6666; //  (self::SQLITE_IOERR      | (26<<8));
		const SQLITE_IOERR_VNODE             =    6922; //  (self::SQLITE_IOERR      | (27<<8));
		const SQLITE_LOCKED_SHAREDCACHE      =     262; //  (self::SQLITE_LOCKED     | (1<<8));
		const SQLITE_BUSY_RECOVERY           =     261; //  (self::SQLITE_BUSY       | (1<<8));
		const SQLITE_BUSY_SNAPSHOT           =     517; //  (self::SQLITE_BUSY       | (2<<8));
		const SQLITE_CANTOPEN_NOTEMPDIR      =     270; //  (self::SQLITE_CANTOPEN   | (1<<8));
		const SQLITE_CANTOPEN_ISDIR          =     526; //  (self::SQLITE_CANTOPEN   | (2<<8));
		const SQLITE_CANTOPEN_FULLPATH       =     782; //  (self::SQLITE_CANTOPEN   | (3<<8));
		const SQLITE_CANTOPEN_CONVPATH       =    1038; //  (self::SQLITE_CANTOPEN   | (4<<8));
		const SQLITE_CORRUPT_VTAB            =     267; //  (self::SQLITE_CORRUPT    | (1<<8));
		const SQLITE_READONLY_RECOVERY       =     264; //  (self::SQLITE_READONLY   | (1<<8));
		const SQLITE_READONLY_CANTLOCK       =     520; //  (self::SQLITE_READONLY   | (2<<8));
		const SQLITE_READONLY_ROLLBACK       =     776; //  (self::SQLITE_READONLY   | (3<<8));
		const SQLITE_READONLY_DBMOVED        =    1032; //  (self::SQLITE_READONLY   | (4<<8));
		const SQLITE_ABORT_ROLLBACK          =     516; //  (self::SQLITE_ABORT      | (2<<8));
		const SQLITE_CONSTRAINT_CHECK        =     275; //  (self::SQLITE_CONSTRAINT | (1<<8));
		const SQLITE_CONSTRAINT_COMMITHOOK   =     531; //  (self::SQLITE_CONSTRAINT | (2<<8));
		const SQLITE_CONSTRAINT_FOREIGNKEY   =     787; //  (self::SQLITE_CONSTRAINT | (3<<8));
		const SQLITE_CONSTRAINT_FUNCTION     =    1043; //  (self::SQLITE_CONSTRAINT | (4<<8));
		const SQLITE_CONSTRAINT_NOTNULL      =    1299; //  (self::SQLITE_CONSTRAINT | (5<<8));
		const SQLITE_CONSTRAINT_PRIMARYKEY   =    1555; //  (self::SQLITE_CONSTRAINT | (6<<8));
		const SQLITE_CONSTRAINT_TRIGGER      =    1811; //  (self::SQLITE_CONSTRAINT | (7<<8));
		const SQLITE_CONSTRAINT_UNIQUE       =    2067; //  (self::SQLITE_CONSTRAINT | (8<<8));
		const SQLITE_CONSTRAINT_VTAB         =    2323; //  (self::SQLITE_CONSTRAINT | (9<<8));
		const SQLITE_CONSTRAINT_ROWID        =    2579; //  (self::SQLITE_CONSTRAINT | (10<<8));
		const SQLITE_NOTICE_RECOVER_WAL      =     283; //  (self::SQLITE_NOTICE     | (1<<8));
		const SQLITE_NOTICE_RECOVER_ROLLBACK =     539; //  (self::SQLITE_NOTICE     | (2<<8));
		const SQLITE_WARNING_AUTOINDEX       =     284; //  (self::SQLITE_WARNING    | (1<<8));
		const SQLITE_AUTH_USER               =     279; //  (self::SQLITE_AUTH       | (1<<8));


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}