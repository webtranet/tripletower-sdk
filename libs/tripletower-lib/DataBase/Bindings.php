<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{
	use TripleTowerSDK\Error\DbError;

	class Bindings implements \ArrayAccess, \Iterator
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const BINDINGTYPE_VALUE   = "value";
		const BINDINGTYPE_PARAM   = "param";

		const DATA_BINDINGNAME    = "bindingName";
		const DATA_BINDINGVALUE   = "bindingValue";
		const DATA_PARAMTYPE      = "pdoParamType";
		const DATA_BINDINGTYPE    = "bindingType";
		const DATA_OUTPARAMLENGTH = "outParamLength";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $bindings = [];
		protected $currentKey = null;


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $bindingName = "", $bindingValue = null, int $pdoParamType = 0, string $bindingType = self::BINDINGTYPE_VALUE)
		{
			$this->clearBindings();

			if( $bindingName !== "" )
				$this->addBinding($bindingName, $bindingValue, $pdoParamType, $bindingType)->throwIfNotSuccess();
		}

		public function existsBinding(string $bindingName)
		{
			if( isset($this->bindings[$bindingName]) )
				return new DbError(DbError::SUCCESS);

			return new DbError(DbError::NO_ENTRY_FOUND, DbError::SILENT, "The binding with name '$bindingName' does not exist.");
		}

		public function addBinding(string $bindingName, $bindingValue, int $pdoParamType)
		{
			if( strlen($bindingName) <= 1 || $bindingName[0] !== ":" )
				return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding name '$bindingName' has to start with a colon ':' and be at least 1 character long (without the colon).");

			if( $this->existsBinding($bindingName)->isSuccess() )
				return new DbError(DbError::DUPLICATE_ENTRY, DbError::ERR, "The binding name '$bindingName' is already set.");

			$this->bindings[$bindingName] =
			[
				self::DATA_BINDINGNAME    => $bindingName,
				self::DATA_BINDINGVALUE   => $bindingValue,
				self::DATA_PARAMTYPE      => $pdoParamType,
				self::DATA_BINDINGTYPE	  => self::BINDINGTYPE_VALUE,
				self::DATA_OUTPARAMLENGTH => null
			];

			return new DbError(DbError::SUCCESS);
		}

		public function addBindingReference(string $bindingName, &$bindingReference, int $pdoParamType, int $outParamLength = 0)
		{
			if( strlen($bindingName) <= 1 || $bindingName[0] !== ":" )
				return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding name '$bindingName' has to start with a colon ':' and be at least 1 character long (without the colon).");

			if( $this->existsBinding($bindingName)->isSuccess() )
				return new DbError(DbError::DUPLICATE_ENTRY, DbError::ERR, "The binding name '$bindingName' is already set.");

			$this->bindings[$bindingName] =
			[
				self::DATA_BINDINGNAME    => $bindingName,
				self::DATA_BINDINGVALUE   => &$bindingReference,
				self::DATA_PARAMTYPE      => $pdoParamType,
				self::DATA_BINDINGTYPE	  => self::BINDINGTYPE_PARAM,
				self::DATA_OUTPARAMLENGTH => $outParamLength > 0 ? $outParamLength : null
			];

			return new DbError(DbError::SUCCESS);
		}

		public function getBinding(string $bindingName)
		{
			if( !isset($this->bindings[$bindingName]) )
				return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "The binding with name '$bindingName' does not exist.");

			return $this->bindings[$bindingName];
		}

		public function removeBinding(string $bindingName)
		{
			if( !isset($this->bindings[$bindingName]) )
				return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "The binding with name '$bindingName' does not exist.");

			unset( $this->bindings[$bindingName] );
			return new DbError(DbError::SUCCESS);
		}

		public function clearBindings() : void
		{
			$this->bindings = [];
			$this->rewind();
		}

		public function getCount() : int
		{
			return count($this->bindings);
		}
		//
		// ArrayAccess interface methdos
		//
		public function offsetExists($bindingName) : bool
		{
			return $this->existsBinding($bindingName)->isSuccess();
		}

		public function offsetGet($bindingName) : mixed
		{
			return $this->getBinding($bindingName);
		}

		public function offsetSet($bindingName, $binding) : void
		{
			// Check bindingName
			if( !is_string($bindingName) || !is_array($binding) )
				throw new DbError(DbError::BINDING_FAILED, DbError::ERR, "Parameter binding name '$bindingName' has to be a string and the binding itself has to be an array, but is " . gettype($binding) . ".");

			if( isset($binding[self::DATA_BINDINGNAME]) && $binding[self::DATA_BINDINGNAME] !== $bindingName )
				throw new DbError(DbError::BINDING_FAILED, DbError::ERR, "If parameter given the binding's bindingName '" . $binding[self::DATA_BINDINGNAME] . "' and the bindingName '$bindingName' have to be equal.");

			// Check binding itself
			if( !isset($binding[self::DATA_BINDINGVALUE]) || !isset($binding[self::DATA_PARAMTYPE]) )
				throw new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding with bindingName '$bindingName' is not complete.");

			// Use default parameter for bindingType if not given
			if( isset($binding[self::DATA_BINDINGTYPE]) )
				$this->addBinding($bindingName, $binding[self::DATA_BINDINGVALUE], $binding[self::DATA_PARAMTYPE], $binding[self::DATA_BINDINGTYPE])->throwIfNotSuccess();
			else
				$this->addBinding($bindingName, $binding[self::DATA_BINDINGVALUE], $binding[self::DATA_PARAMTYPE])->throwIfNotSuccess();
		}

		public function offsetUnset($bindingName) : void
		{
			$this->removeBinding($bindingName);
		}

		//
		// Iterator interface methdos
		//
		public function rewind() : void
		{
			$keys = array_keys($this->bindings);
			$this->currentKey = (count($keys) ? $keys[0] : null);
		}

		public function current() : mixed
		{
			return $this->bindings[$this->currentKey];
		}

		public function key() : mixed
		{
			return $this->currentKey;
		}

		public function next() : void
		{
			$keys = array_keys($this->bindings);
			$keyPosition = array_search($this->currentKey, $keys, true);
			if( $keyPosition !== false && $keyPosition < count($keys)-1 )
				$this->currentKey = $keys[$keyPosition+1];
			else
				$this->currentKey = null;
		}

		public function valid() : bool
		{
			return $this->currentKey !== null && isset($this->bindings[$this->currentKey]);
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}