<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{
	use TripleTowerSDK\Error\AuthError;
	use TripleTowerSDK\Error\DbError;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\DataBase\Bindings;

	class WappsTowerDBC extends AccessRightsDBC
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function getGroupsWithAccess( string $contentProvider, string $contentType, string $contentName)
		{
			$sql = "WITH RECURSIVE accessgroupswithaccess AS
					(
						SELECT accessgroups.accessgroup_id, accessgroups.accessgroupname, accessgroups.authenticationsystem_id, contentaccess.checknestedgroups
						FROM contentaccess
						INNER JOIN accessgroups ON accessgroups.accessgroup_id = contentaccess.accessgroup_id
						INNER JOIN wapps ON wapps.wapp_id = contentaccess.wapp_id
						WHERE LOWER(wapps.name) = LOWER(:contentProvider)
						AND LOWER(contentaccess.contenttype) = LOWER(:contentType)
						AND LOWER(contentaccess.contentname) = LOWER(:contentName)
						AND contentaccess.access = 1

						UNION ALL

						SELECT memberaccessgroups.accessgroup_id, memberaccessgroups.accessgroupname, memberaccessgroups.authenticationsystem_id, accessgroupswithaccess.checknestedgroups
						FROM accessgroupswithaccess
						INNER JOIN accessgroup_accessgroup_relations ON accessgroup_accessgroup_relations.accessgroup_id = accessgroupswithaccess.accessgroup_id
						INNER JOIN accessgroups AS memberaccessgroups ON memberaccessgroups.accessgroup_id = accessgroup_accessgroup_relations.accessgroupmember_id
						WHERE accessgroupswithaccess.accessgroup_id = accessgroup_accessgroup_relations.accessgroup_id
					)
					SELECT accessgroupswithaccess.accessgroupname, authenticationsystems.authenticationsystemname, accessgroupswithaccess.checknestedgroups
					FROM accessgroupswithaccess
					INNER JOIN authenticationsystems ON authenticationsystems.authenticationsystem_id = accessgroupswithaccess.authenticationsystem_id";

			$binding = new Bindings();
			$binding->addBinding(":contentProvider", $contentProvider, \PDO::PARAM_STR);
			$binding->addBinding(":contentType", $contentType, \PDO::PARAM_STR);
			$binding->addBinding(":contentName", $contentName, \PDO::PARAM_STR);

			return $this->bindAndExec($sql, $binding, true);
		}

		public function getServiceTowerInfos(string $serverName)
		{
			$serverName = Helper::removeProtocolAndSlash($serverName);

			$sql = "SELECT servicetowerinfos.servername, servicetowerinfos.publickey
					FROM servicetowerinfos
					WHERE servicetowerinfos.servername = :serverName";

			$binding = new Bindings(":serverName", $serverName, \PDO::PARAM_STR);
			return $this->bindAndExec($sql, $binding);
		}

		public function isUserAuthenticated(string $serviceTowerServer, string $userName, string $sessionToken, string $remoteAddress) : TripleTowerError
		{
			$userSession = $this->getUserSession($serviceTowerServer, $userName, $remoteAddress);
			if( TripleTowerError::isError($userSession) )
				return $userSession;

			if( count($userSession) === 0 || $userSession[0]["sessiontoken"] !== $sessionToken)
				return new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, "The provided session information seem to be expired" );

			return new AuthError(AuthError::SUCCESS);
		}

		public function insertUserSession(string $serverName, string $userName, string $wappName, string $sessionToken, string $remoteAddress)
		{
			$sql = "INSERT INTO
						sessions(user_id, wapp_id, sessiontoken, remoteaddress, sessionstart, lastactive)
					VALUES
						((SELECT users.user_id
						FROM users
							INNER JOIN servicetowerinfos ON servicetowerinfos.servicetowerinfo_id = users.servicetowerinfo_id
							WHERE users.username = :userName AND servicetowerinfos.servername = :serverName),
						(SELECT wapps.wapp_id
							FROM wapps
							WHERE wapps.name = :wappName),
							:sessionToken,
							:remoteAddress,
							NOW(),
							NOW())";
			$bindings = new Bindings();
			$bindings->addBinding(":userName", $userName, \PDO::PARAM_STR);
			$bindings->addBinding(":serverName", $serverName, \PDO::PARAM_STR);
			$bindings->addBinding(":wappName", $wappName, \PDO::PARAM_STR);
			$bindings->addBinding(":sessionToken", $sessionToken, \PDO::PARAM_STR);
			$bindings->addBinding(":remoteAddress", $remoteAddress, \PDO::PARAM_STR);

			$result = $this->bindAndExec($sql, $bindings, false);
			if( !$result->isSuccess() )
				return $result->raiseError(DbError::ERR, "Sessiontoken could not be inserted for user '$userName' from ServiceTower '$serverName' of wapp '$wappName'. The user, the service tower or the wapp seem to be unknown to the system.");

			return $result;
		}

		public function getUserSession(string $serverName, string $userName, string $remoteAddress)
		{
			$sql = "SELECT sessions.sessiontoken
					FROM sessions
					INNER JOIN users ON users.user_id = sessions.user_id
					INNER JOIN servicetowerinfos ON servicetowerinfos.servicetowerinfo_id = users.servicetowerinfo_id
					WHERE servicetowerinfos.servername = :serverName
						AND users.username = :userName
						AND sessions.remoteaddress = :remoteAddress";

			$bindings = new Bindings();
			$bindings->addBinding(':serverName', $serverName, \PDO::PARAM_STR);
			$bindings->addBinding(':userName', $userName, \PDO::PARAM_STR);
			$bindings->addBinding(':remoteAddress', $remoteAddress, \PDO::PARAM_STR);

			return $this->bindAndExec($sql, $bindings);
		}

		public function removeSession(string $url, int $userId, string $sessionToken, string $remoteAddress)
		{
			$stmt = $this->prepare("DELETE FROM wappssessions
									WHERE wappssessions.sessiontoken = :sessionToken
										AND wappssessions.remoteaddress = :remoteAddress
										AND wappssessions.wappuser_id IN
											(SELECT wappusers.wappuser_id
											 FROM wappusers
												INNER JOIN servicetowerinfos ON servicetowerinfos.servicetower_id = wappusers.servicetower_id
											 WHERE servicetowerinfos.url = :url
												AND wappusers.user_id = :userId)");
			$stmt->bindValue(':url', $url, \PDO::PARAM_STR);
			$stmt->bindValue(':userId', $userId, \PDO::PARAM_INT);
			$stmt->bindValue(':sessionToken', $sessionToken, \PDO::PARAM_STR);
			$stmt->bindValue(':remoteAddress', $remoteAddress, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					if($stmt->rowCount() === 1)
					{
						return new AuthError(AuthError::SUCCESS);
					}
					else
					{
						if($stmt->rowCount() < 1)
							return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database. No session found for user '$userId' from serviceTower '$url'.");
						else
							return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Deleted several sessions, even there should have been only one session deleted for user '$userId' from serviceTower '$url'.");
					}
				}
				else
				{
					return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database because: " . $stmt->errorInfo()[2]);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function removeAllSessions(string $url, int $userId)
		{
			$stmt = $this->prepare("DELETE FROM wappssessions
									WHERE wappssessions.wappuser_id IN
											(SELECT wappusers.wappuser_id
											 FROM wappusers
												INNER JOIN servicetowerinfos ON servicetowerinfos.servicetower_id = wappusers.servicetower_id
											 WHERE servicetowerinfos.url = :url
												AND wappusers.user_id = :userId)");
			$stmt->bindValue(':url', $url, \PDO::PARAM_STR);
			$stmt->bindValue(':userId', $userId, \PDO::PARAM_INT);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					if($stmt->rowCount() >= 1)
						return new AuthError(AuthError::SUCCESS);
					else
						return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database. No session found for user '$userId' from serviceTower '$url'.");
				}
				else
				{
					return new AuthError(AuthError::DESTROY_SESSION_ERROR, AuthError::ERR, "Could not delete session entry in database for user '$userId' from serviceTower '$url' because: " . $stmt->errorInfo()[2]);
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::DELETE_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function checkWappAccess(int $wappUserId, string $wappName)
		{
			$stmt = $this->prepare("SELECT wappaccess.access
									FROM wappaccess
									INNER JOIN wapps ON wapps.wapp_id = wappaccess.wapp_id
									WHERE wappaccess.wappuser_id = :wappUserId
									AND wapps.name = :wappName");
			$stmt->bindValue(':wappUserId', $wappUserId, \PDO::PARAM_INT);
			$stmt->bindValue(':wappName', $wappName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					// These three return statements are all ok!
					if(($row = $stmt->fetch(\PDO::FETCH_ASSOC)) && isset($row['access']))
					{
						if((bool)$row['access'] === true)
							return new AuthError(AuthError::SUCCESS);
						else if((bool)$row['access'] === false)
							return new AuthError(AuthError::ACCESS_DENIED, AuthError::NOTICE, "User with ID '$userId' from serviceTower '$url' has no access to wapp '$wappName'.");
					}
					else
					{
						// This means, that it now depends if the wapp is public or not
						return new AuthError(AuthError::UNKNOWN, AuthError::SILENT);
					}

				}
				else
				{
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, DbError::ERR, "Could not retreive wappaccess for user '$userId' from serviceTower '$url' to wapp '$wappName'.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getWapps()
		{
			$stmt = $this->prepare("SELECT wapps.name
									FROM wapps
									WHERE wapps.visible = 1");

			try
			{
				$result = $stmt->execute();
				if($result !== false)
				{
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				}
				else
				{
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not receive viewable wapps.");
				}
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getWappStatistics(string $wappName)
		{
			$stmt = $this->prepare("SELECT wapps.name, COUNT(DISTINCT sessions.user_id) AS activeusers
									FROM wapps
									INNER JOIN sessions ON sessions.wapp_id = wapps.wapp_id
									WHERE wapps.name = :wappName");

			$stmt->bindValue(':wappName', $wappName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				$entry = $stmt->fetch(\PDO::FETCH_ASSOC);
				if(empty($entry))
					return new DbError(DbError::NO_ENTRY_FOUND, DbError::ERR, "Wapp '$wappName' not found.");

				return $entry;
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function insertWapp(string $wappName, bool $visible=true)
		{
			$wappId = $this->getWappIdByName($wappName);

			if(TripleTowerError::isError($wappId) || $wappId !== false)
				return $wappId;

			$insertStmt = $this->prepare("INSERT INTO wapps (name, visible) VALUES (:wappName, :visible)");

			$insertStmt->bindValue(':wappName', $wappName, \PDO::PARAM_STR);
			$insertStmt->bindValue(':visible', $visible, \PDO::PARAM_BOOL);

			try
			{
				return $insertStmt->execute();
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getUserWappAccess(string $wappName)
		{
			$stmt = $this->prepare("SELECT wappusers.wappuser_id
									FROM wappusers
									INNER JOIN wappaccess ON wappaccess.wappuser_id = wappusers.wappuser_id
									INNER JOIN wapps ON wapps.wapp_id = wappaccess.wapp_id
									WHERE wapps.name = :wappName");

			$stmt->bindValue(':wappName', $wappName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not receive userlist");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getAllUser()
		{
			$stmt = $this->prepare("SELECT wappusers.user_id, wappusers.wappuser_id FROM wappusers");

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not receive userlist");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function getWappIdByName(string $wappName)
		{
			$stmt = $this->prepare("SELECT wapps.wapp_id FROM wapps WHERE wapps.name = :wappName");

			$stmt->bindValue(':wappName', $wappName, \PDO::PARAM_STR);

			try
			{
				$result = $stmt->execute();
				if($result === true)
					return $stmt->fetch(\PDO::FETCH_ASSOC);
				else
					return new DbError(DbError::SELECT_ERROR, DbError::ERR, "Could not receive wapp ID");
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::SELECT_ERROR, DbError::ERR, $ex->getMessage());
			}
		}

		public function insertUserIfNotExist(string $userName, string $serviceTowerServer)
		{
			$sql = "SELECT NULL
					FROM users
					INNER JOIN servicetowerinfos ON servicetowerinfos.servicetowerinfo_id = users.servicetowerinfo_id
					WHERE users.username = :userName
					AND servicetowerinfos.servername = :serviceTowerServer";

			$bindings = new Bindings();
			$bindings->addBinding(':userName', $userName, \PDO::PARAM_STR);
			$bindings->addBinding(':serviceTowerServer', $serviceTowerServer, \PDO::PARAM_STR);

			$userKnown = $this->bindAndExec($sql, $bindings, false);

			// User is known or an error occured
			if( !$userKnown->isSameError( new DbError(DbError::NO_ROWS_AFFECTED, DbError::SILENT)) )
				return $userKnown;

			// User is unknown and has to be inserted
			$sql = "INSERT INTO users (servicetowerinfo_id, username)
					SELECT servicetowerinfos.servicetowerinfo_id, :userName
					FROM servicetowerinfos
					WHERE servicetowerinfos.servername = :serviceTowerServer";

			$bindings = new Bindings();
			$bindings->addBinding(':userName', $userName, \PDO::PARAM_STR);
			$bindings->addBinding(':serviceTowerServer', $serviceTowerServer, \PDO::PARAM_STR);

			return $this->bindAndExec($sql, $bindings, false);
		}

		public function grantAccessToWapp(array $wappUserIds, int $wappId)
		{
			if(!empty($wappUserIds))
			{
				$stmt =	"INSERT INTO wappaccess
						(wappaccess.wappuser_id, wappaccess.wapp_id, wappaccess.access)
						VALUES ";

				for($i = 0; $i < count($wappUserIds); $i++)
				{
					if($i > 0)
						$stmt .= ", ";

					$stmt .= "(:wappUserId$i, :wappId, 1)";
				}

				$stmt .= "ON DUPLICATE KEY UPDATE wappaccess.access = 1";

				$query = $this->prepare($stmt);

				$query->bindValue(':wappId', $wappId, \PDO::PARAM_INT);

				$i = 0;
				foreach($wappUserIds as $wappUserId)
				{
					$query->bindValue(":wappUserId$i", trim($wappUserId), \PDO::PARAM_INT);
					$i++;
				}

				try
				{
					$result = $query->execute();
					if($result === true)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::INSERT_ERROR, DbError::ERR, "Could not insert or update wapp access to user IDs: '$wappUserIds'");
				}
				catch(\PDOException $ex)
				{
					return new DbError(DbError::INSERT_ERROR, DbError::ERR, $ex->getMessage());
				}
			}
		}

		public function revokeAccessToWapp(array $wappUserIds, int $wappId)
		{
			if(!empty($wappUserIds))
			{
				$stmt =	"UPDATE wappaccess SET wappaccess.access = 0 WHERE ";

				for($i = 0; $i < count($wappUserIds); $i++)
				{
					if($i > 0)
						$stmt .= " OR ";
					$stmt .= "(wappaccess.wapp_id = :wappId AND wappaccess.wappuser_id = :wappUserId$i)";
				}

				$query = $this->prepare($stmt);

				$query->bindValue(':wappId', $wappId, \PDO::PARAM_INT);

				$i = 0;
				foreach($wappUserIds as $wappUserId)
				{
					$query->bindValue(":wappUserId$i", trim($wappUserId), \PDO::PARAM_INT);
					$i++;
				}

				try
				{
					$result = $query->execute();
					if($result === true)
						return new DbError(DbError::SUCCESS);
					else
						return new DbError(DbError::UPDATE_ERROR, DbError::ERR, "Could not revoke wapp access to user IDs: '$wappUserIds'");
				}
				catch(\PDOException $ex)
				{
					return new DbError(DbError::UPDATE_ERROR, DbError::ERR, $ex->getMessage());
				}
			}
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/


	}
}