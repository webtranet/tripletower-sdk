<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Helper\JsonConfig;
	use TripleTowerSDK\Error\DevError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Error\DbError;

	class DataBaseManager
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $data = [];

		protected const PDO_OPTIONS =
		[
			"autocommit" => \PDO::ATTR_AUTOCOMMIT,
			"prefetch" => \PDO::ATTR_PREFETCH,
			"timeout" => \PDO::ATTR_TIMEOUT,
			"errmode" => \PDO::ATTR_ERRMODE,
			"case" => \PDO::ATTR_CASE,
			"cursorname" => \PDO::ATTR_CURSOR_NAME,
			"cursor" => \PDO::ATTR_CURSOR,
			"oraclenulls" => \PDO::ATTR_ORACLE_NULLS,
			"persistent" => \PDO::ATTR_PERSISTENT,
			"statementclass" => \PDO::ATTR_STATEMENT_CLASS,
			"fetchcatalognames" => \PDO::ATTR_FETCH_CATALOG_NAMES,
			"fetchtablenames" => \PDO::ATTR_FETCH_TABLE_NAMES,
			"stringifyfetches" => \PDO::ATTR_STRINGIFY_FETCHES,
			"maxcolumnlen" => \PDO::ATTR_MAX_COLUMN_LEN,
			"defaultfetchmode" => \PDO::ATTR_DEFAULT_FETCH_MODE,
			"emulateprepares" => \PDO::ATTR_EMULATE_PREPARES,
			"defaultstrparam" => \PDO::ATTR_DEFAULT_STR_PARAM
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct()
		{

		}

		public function getAvailableDataBases() : array
		{
			$availableDBCs = [];
			foreach($_SERVER["TT"]->conf as $configKey => $configValue)
			{
				if( !isset($configValue->dbcName) || !Helper::isClassRelative($configValue->dbcName, "TripleTowerSDK\\DataBase\\DataBaseConnector") )
					continue;

				$availableDBCs[] = $configKey;
			}
			return $availableDBCs;
		}

		public function addDataBase(string $linkName, JsonConfig $dbConf)
		{
			if( !$dbConf )
				throw new SysError(SysError::INVALID_PARAM);

			$linkName = strtolower($linkName);
			if( $linkName === "" )
				throw new SysError(SysError::EMPTY_STRING_ERROR);
			if( array_key_exists($linkName, $this->data) )
				throw new SysError(SysError::PROPERTY_ALREADY_EXISTS);

			try
			{
				if( !class_exists($dbConf->dbcName) )
				{
					require_once $dbConf->dbcName.".php";
					if( !class_exists($dbConf->dbcName) )
						throw new DevError(DevError::CLASS_NOT_FOUND, DevError::NOTICE, "Class '$dbConf->dbcName' not available.");
				}

				$options = $this->readPdoOptions($dbConf);
				$charset = !empty($dbConf->charset) ? $dbConf->charset : $dbConf->dbcName::DEFAULT_CHARSET;
				$dbc = new $dbConf->dbcName($dbConf->dbType, $dbConf->hostName, $dbConf->dbName, $dbConf->userName, $dbConf->password, $charset, $options);
				$this->data[$linkName] = $dbc;
				return $dbc;
			}
			catch(\Throwable $t)
			{
				throw new DbError(DbError::DATABASE_CONNECTION_ERROR, DbError::ERR, $t->getMessage());
			}
		}

		public function removeDataBase(string $linkName)
		{
			$linkName = strtolower($linkName);
			if(!array_key_exists($linkName, $this->data))
				throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The database '$linkName' could not be removed.");

			unset($this->data[$linkName]);
		}

		public function __get(string $key)
		{
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
			{
				return $this->data[$key];
			}
			else
			{
				// Database auto-connect
				$dbConf = $_SERVER["TT"]->conf->$key;
				if($dbConf !== null)
					return $this->addDataBase($key, $dbConf);
				else
					throw new SysError(SysError::NO_SUCH_PROPERTY_ERROR, SysError::ERR, "The property '$key' is not available.");

			}
		}

		public function __isset(string $key)
		{
			// Database should not auto-connect when isset() is used,
			// it should just test if there is an open connection
			$key = strtolower($key);
			if(array_key_exists($key, $this->data))
				return true;
			else
				return false;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function readPdoOptions($dbConf)
		{
			$ignoreList = ["dbcname", "dbtype", "hostname", "dbname", "username", "password", "charset"];
			$options = [];
			foreach($dbConf as $key => $value)
			{
				if( in_array(strtolower($key), $ignoreList) )
					continue;

				if( !isset(self::PDO_OPTIONS[$key]) )
					throw new SysError(SysError::INVALID_CONFIGURATION, SysError::ERR, "The property '$key' is not allowed for db config.");

				$options[self::PDO_OPTIONS[$key]] = defined("\PDO::$value") ? constant("\PDO::$value") : $value;
			}

			return $options;
		}
	}
}