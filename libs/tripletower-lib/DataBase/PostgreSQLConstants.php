<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{

	final class PostgreSQLConstants
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const PG_SUCCESSFUL_COMPLETION                                = "00000";
		const PG_WARNING                                              = "01000";
		const PG_DYNAMIC_RESULT_SETS_RETURNED                         = "0100C";
		const PG_IMPLICIT_ZERO_BIT_PADDING                            = "01008";
		const PG_NULL_VALUE_ELIMINATED_IN_SET_FUNCTION                = "01003";
		const PG_PRIVILEGE_NOT_GRANTED                                = "01007";
		const PG_PRIVILEGE_NOT_REVOKED                                = "01006";
		const PG_STRING_DATA_RIGHT_TRUNCATION                         = "01004";
		const PG_DEPRECATED_FEATURE                                   = "01P01";
		const PG_NO_DATA                                              = "02000";
		const PG_NO_ADDITIONAL_DYNAMIC_RESULT_SETS_RETURNED           = "02001";
		const PG_SQL_STATEMENT_NOT_YET_COMPLETE                       = "03000";
		const PG_CONNECTION_EXCEPTION                                 = "08000";
		const PG_CONNECTION_DOES_NOT_EXIST                            = "08003";
		const PG_CONNECTION_FAILURE                                   = "08006";
		const PG_SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION          = "08001";
		const PG_SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION    = "08004";
		const PG_TRANSACTION_RESOLUTION_UNKNOWN                       = "08007";
		const PG_PROTOCOL_VIOLATION                                   = "08P01";
		const PG_TRIGGERED_ACTION_EXCEPTION                           = "09000";
		const PG_FEATURE_NOT_SUPPORTED                                = "0A000";
		const PG_INVALID_TRANSACTION_INITIATION                       = "0B000";
		const PG_LOCATOR_EXCEPTION                                    = "0F000";
		const PG_INVALID_LOCATOR_SPECIFICATION                        = "0F001";
		const PG_INVALID_GRANTOR                                      = "0L000";
		const PG_INVALID_GRANT_OPERATION                              = "0LP01";
		const PG_INVALID_ROLE_SPECIFICATION                           = "0P000";
		const PG_CASE_NOT_FOUND                                       = "20000";
		const PG_CARDINALITY_VIOLATION                                = "21000";
		const PG_DATA_EXCEPTION                                       = "22000";
		const PG_ARRAY_SUBSCRIPT_ERROR                                = "2202E";
		const PG_CHARACTER_NOT_IN_REPERTOIRE                          = "22021";
		const PG_DATETIME_FIELD_OVERFLOW                              = "22008";
		const PG_DIVISION_BY_ZERO                                     = "22012";
		const PG_ERROR_IN_ASSIGNMENT                                  = "22005";
		const PG_ESCAPE_CHARACTER_CONFLICT                            = "2200B";
		const PG_INDICATOR_OVERFLOW                                   = "22022";
		const PG_INTERVAL_FIELD_OVERFLOW                              = "22015";
		const PG_INVALID_ARGUMENT_FOR_LOGARITHM                       = "2201E";
		const PG_INVALID_ARGUMENT_FOR_NTILE_FUNCTION                  = "22014";
		const PG_INVALID_ARGUMENT_FOR_NTH_VALUE_FUNCTION              = "22016";
		const PG_INVALID_ARGUMENT_FOR_POWER_FUNCTION                  = "2201F";
		const PG_INVALID_ARGUMENT_FOR_WIDTH_BUCKET_FUNCTION           = "2201G";
		const PG_INVALID_CHARACTER_VALUE_FOR_CAST                     = "22018";
		const PG_INVALID_DATETIME_FORMAT                              = "22007";
		const PG_INVALID_ESCAPE_CHARACTER                             = "22019";
		const PG_INVALID_ESCAPE_OCTET                                 = "2200D";
		const PG_INVALID_ESCAPE_SEQUENCE                              = "22025";
		const PG_NONSTANDARD_USE_OF_ESCAPE_CHARACTER                  = "22P06";
		const PG_INVALID_INDICATOR_PARAMETER_VALUE                    = "22010";
		const PG_INVALID_PARAMETER_VALUE                              = "22023";
		const PG_INVALID_REGULAR_EXPRESSION                           = "2201B";
		const PG_INVALID_ROW_COUNT_IN_LIMIT_CLAUSE                    = "2201W";
		const PG_INVALID_ROW_COUNT_IN_RESULT_OFFSET_CLAUSE            = "2201X";
		const PG_INVALID_TIME_ZONE_DISPLACEMENT_VALUE                 = "22009";
		const PG_INVALID_USE_OF_ESCAPE_CHARACTER                      = "2200C";
		const PG_MOST_SPECIFIC_TYPE_MISMATCH                          = "2200G";
		const PG_NULL_VALUE_NOT_ALLOWED                               = "22004";
		const PG_NULL_VALUE_NO_INDICATOR_PARAMETER                    = "22002";
		const PG_NUMERIC_VALUE_OUT_OF_RANGE                           = "22003";
		const PG_STRING_DATA_LENGTH_MISMATCH                          = "22026";
		const PG_STRING_DATA_RIGHT_TRUNCATION_ERROR                   = "22001";
		const PG_SUBSTRING_ERROR                                      = "22011";
		const PG_TRIM_ERROR                                           = "22027";
		const PG_UNTERMINATED_C_STRING                                = "22024";
		const PG_ZERO_LENGTH_CHARACTER_STRING                         = "2200F";
		const PG_FLOATING_POINT_EXCEPTION                             = "22P01";
		const PG_INVALID_TEXT_REPRESENTATION                          = "22P02";
		const PG_INVALID_BINARY_REPRESENTATION                        = "22P03";
		const PG_BAD_COPY_FILE_FORMAT                                 = "22P04";
		const PG_UNTRANSLATABLE_CHARACTER                             = "22P05";
		const PG_NOT_AN_XML_DOCUMENT                                  = "2200L";
		const PG_INVALID_XML_DOCUMENT                                 = "2200M";
		const PG_INVALID_XML_CONTENT                                  = "2200N";
		const PG_INVALID_XML_COMMENT                                  = "2200S";
		const PG_INVALID_XML_PROCESSING_INSTRUCTION                   = "2200T";
		const PG_INTEGRITY_CONSTRAINT_VIOLATION                       = "23000";
		const PG_RESTRICT_VIOLATION                                   = "23001";
		const PG_NOT_NULL_VIOLATION                                   = "23502";
		const PG_FOREIGN_KEY_VIOLATION                                = "23503";
		const PG_UNIQUE_VIOLATION                                     = "23505";
		const PG_CHECK_VIOLATION                                      = "23514";
		const PG_EXCLUSION_VIOLATION                                  = "23P01";
		const PG_INVALID_CURSOR_STATE                                 = "24000";
		const PG_INVALID_TRANSACTION_STATE                            = "25000";
		const PG_ACTIVE_SQL_TRANSACTION                               = "25001";
		const PG_BRANCH_TRANSACTION_ALREADY_ACTIVE                    = "25002";
		const PG_HELD_CURSOR_REQUIRES_SAME_ISOLATION_LEVEL            = "25008";
		const PG_INAPPROPRIATE_ACCESS_MODE_FOR_BRANCH_TRANSACTION     = "25003";
		const PG_INAPPROPRIATE_ISOLATION_LEVEL_FOR_BRANCH_TRANSACTION = "25004";
		const PG_NO_ACTIVE_SQL_TRANSACTION_FOR_BRANCH_TRANSACTION     = "25005";
		const PG_READ_ONLY_SQL_TRANSACTION                            = "25006";
		const PG_SCHEMA_AND_DATA_STATEMENT_MIXING_NOT_SUPPORTED       = "25007";
		const PG_NO_ACTIVE_SQL_TRANSACTION                            = "25P01";
		const PG_IN_FAILED_SQL_TRANSACTION                            = "25P02";
		const PG_INVALID_SQL_STATEMENT_NAME                           = "26000";
		const PG_TRIGGERED_DATA_CHANGE_VIOLATION                      = "27000";
		const PG_INVALID_AUTHORIZATION_SPECIFICATION                  = "28000";
		const PG_INVALID_PASSWORD                                     = "28P01";
		const PG_DEPENDENT_PRIVILEGE_DESCRIPTORS_STILL_EXIST          = "2B000";
		const PG_DEPENDENT_OBJECTS_STILL_EXIST                        = "2BP01";
		const PG_INVALID_TRANSACTION_TERMINATION                      = "2D000";
		const PG_SQL_ROUTINE_EXCEPTION                                = "2F000";
		const PG_FUNCTION_EXECUTED_NO_RETURN_STATEMENT                = "2F005";
		const PG_MODIFYING_SQL_DATA_NOT_PERMITTED                     = "2F002";
		const PG_PROHIBITED_SQL_STATEMENT_ATTEMPTED                   = "2F003";
		const PG_READING_SQL_DATA_NOT_PERMITTED                       = "2F004";
		const PG_INVALID_CURSOR_NAME                                  = "34000";
		const PG_EXTERNAL_ROUTINE_EXCEPTION                           = "38000";
		const PG_CONTAINING_SQL_NOT_PERMITTED                         = "38001";
		const PG_MODIFYING_SQL_DATA_NOT_PERMITTED_EXTERNAL            = "38002";
		const PG_PROHIBITED_SQL_STATEMENT_ATTEMPTED_EXTERNAL          = "38003";
		const PG_READING_SQL_DATA_NOT_PERMITTED_EXTERNAL              = "38004";
		const PG_EXTERNAL_ROUTINE_INVOCATION_EXCEPTION                = "39000";
		const PG_INVALID_SQLSTATE_RETURNED                            = "39001";
		const PG_NULL_VALUE_NOT_ALLOWED_EXTERNAL                      = "39004";
		const PG_TRIGGER_PROTOCOL_VIOLATED                            = "39P01";
		const PG_SRF_PROTOCOL_VIOLATED                                = "39P02";
		const PG_SAVEPOINT_EXCEPTION                                  = "3B000";
		const PG_INVALID_SAVEPOINT_SPECIFICATION                      = "3B001";
		const PG_INVALID_CATALOG_NAME                                 = "3D000";
		const PG_INVALID_SCHEMA_NAME                                  = "3F000";
		const PG_TRANSACTION_ROLLBACK                                 = "40000";
		const PG_TRANSACTION_INTEGRITY_CONSTRAINT_VIOLATION           = "40002";
		const PG_SERIALIZATION_FAILURE                                = "40001";
		const PG_STATEMENT_COMPLETION_UNKNOWN                         = "40003";
		const PG_DEADLOCK_DETECTED                                    = "40P01";
		const PG_SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION                = "42000";
		const PG_SYNTAX_ERROR                                         = "42601";
		const PG_INSUFFICIENT_PRIVILEGE                               = "42501";
		const PG_CANNOT_COERCE                                        = "42846";
		const PG_GROUPING_ERROR                                       = "42803";
		const PG_WINDOWING_ERROR                                      = "42P20";
		const PG_INVALID_RECURSION                                    = "42P19";
		const PG_INVALID_FOREIGN_KEY                                  = "42830";
		const PG_INVALID_NAME                                         = "42602";
		const PG_NAME_TOO_LONG                                        = "42622";
		const PG_RESERVED_NAME                                        = "42939";
		const PG_DATATYPE_MISMATCH                                    = "42804";
		const PG_INDETERMINATE_DATATYPE                               = "42P18";
		const PG_COLLATION_MISMATCH                                   = "42P21";
		const PG_INDETERMINATE_COLLATION                              = "42P22";
		const PG_WRONG_OBJECT_TYPE                                    = "42809";
		const PG_UNDEFINED_COLUMN                                     = "42703";
		const PG_UNDEFINED_FUNCTION                                   = "42883";
		const PG_UNDEFINED_TABLE                                      = "42P01";
		const PG_UNDEFINED_PARAMETER                                  = "42P02";
		const PG_UNDEFINED_OBJECT                                     = "42704";
		const PG_DUPLICATE_COLUMN                                     = "42701";
		const PG_DUPLICATE_CURSOR                                     = "42P03";
		const PG_DUPLICATE_DATABASE                                   = "42P04";
		const PG_DUPLICATE_FUNCTION                                   = "42723";
		const PG_DUPLICATE_PREPARED_STATEMENT                         = "42P05";
		const PG_DUPLICATE_SCHEMA                                     = "42P06";
		const PG_DUPLICATE_TABLE                                      = "42P07";
		const PG_DUPLICATE_ALIAS                                      = "42712";
		const PG_DUPLICATE_OBJECT                                     = "42710";
		const PG_AMBIGUOUS_COLUMN                                     = "42702";
		const PG_AMBIGUOUS_FUNCTION                                   = "42725";
		const PG_AMBIGUOUS_PARAMETER                                  = "42P08";
		const PG_AMBIGUOUS_ALIAS                                      = "42P09";
		const PG_INVALID_COLUMN_REFERENCE                             = "42P10";
		const PG_INVALID_COLUMN_DEFINITION                            = "42611";
		const PG_INVALID_CURSOR_DEFINITION                            = "42P11";
		const PG_INVALID_DATABASE_DEFINITION                          = "42P12";
		const PG_INVALID_FUNCTION_DEFINITION                          = "42P13";
		const PG_INVALID_PREPARED_STATEMENT_DEFINITION                = "42P14";
		const PG_INVALID_SCHEMA_DEFINITION                            = "42P15";
		const PG_INVALID_TABLE_DEFINITION                             = "42P16";
		const PG_INVALID_OBJECT_DEFINITION                            = "42P17";
		const PG_WITH_CHECK_OPTION_VIOLATION                          = "44000";
		const PG_INSUFFICIENT_RESOURCES                               = "53000";
		const PG_DISK_FULL                                            = "53100";
		const PG_OUT_OF_MEMORY                                        = "53200";
		const PG_TOO_MANY_CONNECTIONS                                 = "53300";
		const PG_PROGRAM_LIMIT_EXCEEDED                               = "54000";
		const PG_STATEMENT_TOO_COMPLEX                                = "54001";
		const PG_TOO_MANY_COLUMNS                                     = "54011";
		const PG_TOO_MANY_ARGUMENTS                                   = "54023";
		const PG_OBJECT_NOT_IN_PREREQUISITE_STATE                     = "55000";
		const PG_OBJECT_IN_USE                                        = "55006";
		const PG_CANT_CHANGE_RUNTIME_PARAM                            = "55P02";
		const PG_LOCK_NOT_AVAILABLE                                   = "55P03";
		const PG_OPERATOR_INTERVENTION                                = "57000";
		const PG_QUERY_CANCELED                                       = "57014";
		const PG_ADMIN_SHUTDOWN                                       = "57P01";
		const PG_CRASH_SHUTDOWN                                       = "57P02";
		const PG_CANNOT_CONNECT_NOW                                   = "57P03";
		const PG_DATABASE_DROPPED                                     = "57P04";
		const PG_IO_ERROR                                             = "58030";
		const PG_UNDEFINED_FILE                                       = "58P01";
		const PG_DUPLICATE_FILE                                       = "58P02";
		const PG_CONFIG_FILE_ERROR                                    = "F0000";
		const PG_LOCK_FILE_EXISTS                                     = "F0001";
		const PG_FDW_ERROR                                            = "HV000";
		const PG_FDW_COLUMN_NAME_NOT_FOUND                            = "HV005";
		const PG_FDW_DYNAMIC_PARAMETER_VALUE_NEEDED                   = "HV002";
		const PG_FDW_FUNCTION_SEQUENCE_ERROR                          = "HV010";
		const PG_FDW_INCONSISTENT_DESCRIPTOR_INFORMATION              = "HV021";
		const PG_FDW_INVALID_ATTRIBUTE_VALUE                          = "HV024";
		const PG_FDW_INVALID_COLUMN_NAME                              = "HV007";
		const PG_FDW_INVALID_COLUMN_NUMBER                            = "HV008";
		const PG_FDW_INVALID_DATA_TYPE                                = "HV004";
		const PG_FDW_INVALID_DATA_TYPE_DESCRIPTORS                    = "HV006";
		const PG_FDW_INVALID_DESCRIPTOR_FIELD_IDENTIFIER              = "HV091";
		const PG_FDW_INVALID_HANDLE                                   = "HV00B";
		const PG_FDW_INVALID_OPTION_INDEX                             = "HV00C";
		const PG_FDW_INVALID_OPTION_NAME                              = "HV00D";
		const PG_FDW_INVALID_STRING_LENGTH_OR_BUFFER_LENGTH           = "HV090";
		const PG_FDW_INVALID_STRING_FORMAT                            = "HV00A";
		const PG_FDW_INVALID_USE_OF_NULL_POINTER                      = "HV009";
		const PG_FDW_TOO_MANY_HANDLES                                 = "HV014";
		const PG_FDW_OUT_OF_MEMORY                                    = "HV001";
		const PG_FDW_NO_SCHEMAS                                       = "HV00P";
		const PG_FDW_OPTION_NAME_NOT_FOUND                            = "HV00J";
		const PG_FDW_REPLY_HANDLE                                     = "HV00K";
		const PG_FDW_SCHEMA_NOT_FOUND                                 = "HV00Q";
		const PG_FDW_TABLE_NOT_FOUND                                  = "HV00R";
		const PG_FDW_UNABLE_TO_CREATE_EXECUTION                       = "HV00L";
		const PG_FDW_UNABLE_TO_CREATE_REPLY                           = "HV00M";
		const PG_FDW_UNABLE_TO_ESTABLISH_CONNECTION                   = "HV00N";
		const PG_PLPGSQL_ERROR                                        = "P0000";
		const PG_RAISE_EXCEPTION                                      = "P0001";
		const PG_NO_DATA_FOUND                                        = "P0002";
		const PG_TOO_MANY_ROWS                                        = "P0003";
		const PG_INTERNAL_ERROR                                       = "XX000";
		const PG_DATA_CORRUPTED                                       = "XX001";
		const PG_INDEX_CORRUPTED                                      = "XX002";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}