<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{

	use TripleTowerSDK\DataBase\Bindings;
	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Error\DbError;

	abstract class DataBaseConnector extends \PDO
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const DEFAULT_CHARSET = "utf8";

		const DBTYPE_FIREBIRD     = "firebird";     // Borland firebird
		const DBTYPE_MYSQL        = "mysql";        // MySQL & MariaDB
		const DBTYPE_OCI          = "oci";          // OCI using "Easy Connect String"
		const DBTYPE_OCITNS       = "ociTns";       // OC// Borland firebirdI using "Transparent Network Substrate"
		const DBTYPE_POSTGRESQL   = "postgresql";   // PostgreSQL
		const DBTYPE_SQLITE       = "sqlite";       // SQLite
		const DBTYPE_SQLITEMEMORY = "sqliteMemory"; // SQLite in memory
		const DBTYPE_SQLSRV       = "sqlsrv";       // Microsoft SQL Server
		const DBTYPE_DB2          = "db2";          // IBM DB2

		const DATETIME_FORMAT_ISO8601_FIREBIRD      = "Y-m-d H:i:s.u";
		const DATETIME_FORMAT_ISO8601_MYSQL			= "%Y-%m-%dT%H:%i:%s.%f";
		const DATETIME_FORMAT_ISO8601_OCI			= "YYYY-MM-DD\"T\"HH24:MI:SS.FF3TZH:TZM";
		const DATETIME_FORMAT_ISO8601_POSTGRESQL	= "YYYY-MM-DD\"T\"HH24:MI:SS.FF3TZH:TZM";
		const DATETIME_FORMAT_ISO8601_SQLITE		= "%Y-%m-%dT%H:%i:%f000";
		const DATETIME_FORMAT_ISO8601_SQLSVR		= "127";


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $dbType   = "";
		protected $hostName = "";
		protected $dbName   = "";
		protected $userName = "";

		protected $defaultDbDateTimeFormat = "";

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		/**
		 * @name __construct
		 * Constructor for the DBCs extending the \PDO.
		 * Currently supported DB-Types: mysql, oci, postgresql, sqlite, sqliteMemory and sqlsvr.
		 * Future support: db2
		 *
		 * @param $dbType
		 * @param $hostName
		 * @param $dbName
		 * @param $userName
		 * @param $password
		 * @return DBC Object
		 *
		 */
		public function __construct(string $dbType, string $hostName, string $dbName, string $userName, string $password, string $charset=self::DEFAULT_CHARSET, array $pdoOptions = [])
		{
			if( !isset($pdoOptions[\PDO::ATTR_CASE]) )
				$pdoOptions[\PDO::ATTR_CASE] = \PDO::CASE_LOWER;
			if( !isset($pdoOptions[\PDO::ATTR_ERRMODE]) )
				$pdoOptions[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;
			// TODO: If commented in, weiredly mysql can't open new transactions !?
			// parent::setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

			try
			{
				switch($dbType)
				{
					case self::DBTYPE_FIREBIRD:

						// Host is optional
						if( !empty($hostName) )
							$hostName = str_replace(":", "/", $hostName) . ":";
						echo "firebird:dbname=$hostName$dbName;charset=$charset";
						parent::__construct("firebird:dbname=$hostName$dbName;charset=$charset", null, null, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_FIREBIRD;
						break;

					case self::DBTYPE_MYSQL:
						parent::__construct("mysql:host=$hostName;dbname=$dbName;charset=$charset", $userName, $password, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_MYSQL;
						$timeZoneOffset = (new DateTime())->getTimzoneOffsetString();
						$this->exec("SET time_zone = '$timeZoneOffset'");
						break;

					// OCI using "Easy Connect String"
					case self::DBTYPE_OCI:
						parent::__construct("oci:dbname=//$hostName/$dbName;charset=$charset", $userName, $password, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_OCI;
						break;

					case self::DBTYPE_OCITNS: // OCI using "Transparent Network Substrate" as a string in $dbName
						if( !empty($dbName) )
							parent::__construct("oci:dbname=$dbName;charset=$charset", $userName, $password, $pdoOptions);
						else
							throw new DbError(DbError::INVALID_TNS_ORA_SPECIFICATION);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_OCI;
						break;

					case self::DBTYPE_POSTGRESQL:
						parent::__construct("postgresql:dbname=$dbName;host=$hostName", $userName, $password, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_POSTGRESQL;
						break;

					case self::DBTYPE_SQLITE:
						parent::__construct("sqlite:$hostName", null, null, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_SQLITE;
						break;

					case self::DBTYPE_SQLITEMEMORY:
						parent::__construct("sqlite::memory:", null, null, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_SQLITE;
						break;

					// Watch out! You have to use a comma after hostName for another port!
					case self::DBTYPE_SQLSRV:
						// Because the connection pooling (persistence) is
						// done by the ODBC driver, it has to be always unset here
						unset($pdoOptions[self::ATTR_PERSISTENT]);
						parent::__construct ("sqlsrv:server=$hostName;database=$dbName", $userName, $password, $pdoOptions);
						$this->defaultDbDateTimeFormat = self::DATETIME_FORMAT_ISO8601_SQLSVR;
						break;

					/*
					case 'db2':
						parent::__construct("ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE=$dbName; HOSTNAME=$hostName;", $userName, $password, $pdoOptions);
						break;

					*/

					default:
						throw new DbError(DbError::INVALID_PDO_DBMS_TYPE, DbError::ERR, "The DBMS type '$dbType' is unknown");
				}

				$this->dbType   = $dbType;
				$this->hostName = $hostName;
				$this->dbName   = $dbName;
				$this->userName = $userName;

			}
			catch (\PDOException $ex)
			{
				throw new DbError(DbError::DATABASE_CONNECTION_ERROR, DbError::ERR, "Could not connect to $dbType-DB $hostName because of " . $ex->getMessage() . ".");
			}
		}

		public function __destruct()
		{
			if( $this->inTransaction() )
				$this->rollBack();
		}


		/***********************************
		 * PROTECTED METHODS               *
		***********************************/

		protected function executePlainSql(string $sqlString, bool $fetchResult = true)
		{
			$stmt = null;
			try
			{
				$stmt = $this->query($sqlString);
			}
			catch(\PDOException $ex)
			{
				return new DbError(DbError::BAD_SQL_STATEMENT, DbError::ERR, "SQL command could not be successfully executed because " . $ex->getMessage(), $ex);
			}

			if( $fetchResult )
			{
				try
				{
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				}
				catch(\PDOException $ex)
				{
					return $stmt->rowCount();
				}
			}
			else
			{
				if( $stmt->rowCount() === 0 )
					return new DbError(DbError::NO_ROWS_AFFECTED, DbError::SILENT);

				return new DbError(DbError::SUCCESS);
			}
		}

		/*
		 * This metod prepares a statement with the given $sqlString and executes one ore multiple times depending if all bindings are single values or
		 * if a binding is given which contains an array as value. If more than one binding contains an array then the row counts of the Arrays have to be the same.
		 * Also these arrays must have continious numeric keys to prevent mixing of data.
		 *
		 * If there are errors which should not be returned as errors and to prevent stopping the execution,
		 * these errors can be specified by the parameter $dbErrorCodesToIgnore.
		 *
		 * Returns differ:
		 *	If $fetchResult is true:
		 *		If $sqlString contains only one SELECT statement the result is returned else an error is returned to prevent mixing of records with different columns.
		 *		If $sqlString contains only INSERTS, UPDATES and DELETES the total number of affected rows for all executeted statements is returned.
		 *
		 *	If $fetchResult is false:
		 *		If a statement is executed that doesn't hit a row and $dbErrorCodesToIgnore does not include DbError::NO_ROWS_AFFECTED
		 *		then the error DbError::NO_ROWS_AFFECTED will be returned directly after the execution.
		 */
		protected function bindAndExec(string $sqlString, Bindings $bindings = null, bool $fetchResult = true, array $dbErrorCodesToIgnore = [])
		{
			$stmt = $this->prepare($sqlString);
			$arrayBindingNames = [];
			$arrayBindingCount = 0;

			// Bindings can be null if there is none given,
			// then simply skip and execute the sql
			if( $bindings !== null )
			{
				// Bindings itself is an array like object,
				// but it can also contain array values e.g.:
				// a list of values for inserts as prepared statements
				foreach($bindings as $bindingName => $binding)
				{
					$value = $binding[Bindings::DATA_BINDINGVALUE];
					if(is_array($value))
					{
						// If there are several values for a single Binding,
						// all Binding values with an array must have the exact same amount of values
						// E.g.: INSERT INTO (A, B) VALUES (:asdf1, :asdf2)
						// -> if :asdf1 and :asdf2 are both arrays, the number of values have to match!

						// First array sets the number of entries all arrays must have
						if( count($arrayBindingNames) === 0 )
							$arrayBindingCount = count($value);

						if( count($value) !== $arrayBindingCount )
								return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding of '$bindingName' failed because the number of bound values of given arrays differ");

						// Add binding to known and size-fitting arrays
						$arrayBindingNames[] = $bindingName;
					}
					else
					{
						switch( $binding[Bindings::DATA_BINDINGTYPE] )
						{
							case Bindings::BINDINGTYPE_VALUE:
								if( $stmt->bindValue($bindingName, $value, $binding[Bindings::DATA_PARAMTYPE]) !== true )
									return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding of '$bindingName' failed");
								break;

							case Bindings::BINDINGTYPE_PARAM:
								if( $stmt->bindParam($bindingName, $binding[Bindings::DATA_BINDINGVALUE], $binding[Bindings::DATA_PARAMTYPE], $binding[Bindings::DATA_OUTPARAMLENGTH]) !== true )
									return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The binding of '$bindingName' failed");
								break;

							default:
								return new DbError(DbError::BINDING_FAILED, DbError::ERR, "Unknown binding type '" . $binding[Bindings::DATA_BINDINGTYPE] . "', only '" . Bindings::BINDINGTYPE_VALUE . "' and '" . Bindings::BINDINGTYPE_PARAM . "' are allowed.");
						}
					}
				}
			}

			// Execute the SQL statement foreach prepared statement value, but at least once!
			$result = null; // Can become a resultset (array) or a result count (int)
			for($sqlExectionLoop = 0; $sqlExectionLoop < ($arrayBindingCount?$arrayBindingCount:1); $sqlExectionLoop++)
			{
				foreach($arrayBindingNames as $bindingName)
				{
					switch( $bindings[$bindingName][Bindings::DATA_BINDINGTYPE] )
					{
						case Bindings::BINDINGTYPE_VALUE:
							$bindResult = $stmt->bindValue(
									$bindingName,
									$bindings[$bindingName][Bindings::DATA_BINDINGVALUE][$sqlExectionLoop],
									$bindings[$bindingName][Bindings::DATA_PARAMTYPE]);

							if( $bindResult !== true )
								return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The value binding of '$bindingName' failed");

							break;

						case Bindings::BINDINGTYPE_PARAM:
							$bindResult = $stmt->bindParam(
									$bindingName,
									$bindings[$bindingName][Bindings::DATA_BINDINGVALUE][$sqlExectionLoop],
									$bindings[$bindingName][Bindings::DATA_PARAMTYPE],
									$bindings[$bindingName][Bindings::DATA_OUTPARAMLENGTH]);

							if( $bindResult !== true )
								return new DbError(DbError::BINDING_FAILED, DbError::ERR, "The param binding of '$bindingName' failed");

							break;

						default:
							return new DbError(DbError::BINDING_FAILED, DbError::ERR, "Unknown binding type '" . $binding[Bindings::DATA_BINDINGTYPE] . "', only '" . Bindings::BINDINGTYPE_VALUE . "' and '" . Bindings::BINDINGTYPE_PARAM . "' are allowed.");
					}
				}

				try
				{
					$stmt->execute();

					if( $fetchResult )
					{

						// SELECTs always have columnCount > 0, even if there are no rows
						// INSERTs, UPDATEs, or DELETEs don't
						if( $stmt->columnCount() > 0 )
						{
							// $result becomes an array (table)
							$result = !is_null($result) ? array_merge($result, $stmt->fetchAll(\PDO::FETCH_ASSOC)) : $stmt->fetchAll(\PDO::FETCH_ASSOC);
							if( $this->hasMultiRowsetSupport() && $stmt->nextRowset() )
								return new DbError(DbError::SELECT_ERROR, DbError::ERR, "SQL string must have only one SELECT statement that returns results.");
						}
						else
						{
							// $result becomes an int for counting the affected lines
							do
							{
								$result = !is_null($result) ? $result + $stmt->rowCount() : $stmt->rowCount();
							} while( $this->hasMultiRowsetSupport() && $stmt->nextRowset() );
						}
					}
					else
					{
						do
						{
							if( $stmt->rowCount() === 0 && !in_array(DbError::NO_ROWS_AFFECTED, $dbErrorCodesToIgnore) )
								return new DbError(DbError::NO_ROWS_AFFECTED, DbError::SILENT);
						} while( $this->hasMultiRowsetSupport() && $stmt->nextRowset() );
					}
				}
				catch(\PDOException $ex)
				{
					$dbError = $this->pdoExceptionToDbError($ex);
					if( !in_array($dbError->getCode(), $dbErrorCodesToIgnore) )
						return $dbError->raiseError(DbError::ERR);
				}
			}
			return $result ?? new DbError(DbError::SUCCESS);
		}

		protected function pdoExceptionToDbError(\PDOException $ex)
		{
			$sqlStateCode = $ex->getCode();
			$message = "SQL command could not be successfully executed because " . $ex->getMessage();
			$stateClass = strlen($sqlStateCode) > 1 ? substr($sqlStateCode, 0, 2) : "";

			// For SQL states see https://en.wikipedia.org/wiki/SQLSTATE
			switch($stateClass)
			{
				case "00":
					return new DbError(DbError::SUCCESS);
				case "08":
					return new DbError(DbError::DATABASE_CONNECTION_ERROR, DbError::SILENT, $message, $ex);
				case "02":
					return new DbError(DbError::NO_ENTRY_FOUND, DbError::SILENT, $message, $ex);
				case "23":
					return new DbError(DbError::DUPLICATE_ENTRY, DbError::SILENT, $message, $ex);
				case "25":
				case "2D":
					return new DbError(DbError::TRANSACTION_FAILED, DbError::SILENT, $message, $ex);
				case "40":
					return new DbError(DbError::ROLLBACK_ERROR, DbError::SILENT, $message, $ex);
				default:
					return new DbError(DbError::BAD_SQL_STATEMENT, DbError::SILENT, $message, $ex);
			}
		}

		protected function hasMultiRowsetSupport() : bool
		{
			$supportList =
			[
				self::DBTYPE_MYSQL,
				self::DBTYPE_SQLSRV
			];
			return in_array($this->dbType, $supportList);
		}
	}
}