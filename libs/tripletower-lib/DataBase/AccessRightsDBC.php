<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\DataBase
{
	use TripleTowerSDK\Error\TripleTowerError;

	abstract class AccessRightsDBC extends DataBaseConnector
	{
		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		abstract public function getGroupsWithAccess(string $contentProvider, string $contentType, string $contentName);

		public function checkUserInGroup( string $userName, string $accessGroupName ) : TripleTowerError
		{
			$sql = "SELECT users.username
					FROM users
					JOIN user_accessgroup_relations ON user_accessgroup_relations.user_id = users.user_id
					JOIN accessgroups ON accessgroups.accessgroup_id = user_accessgroup_relations.accessgroup_id
					WHERE LOWER(users.username) = LOWER(:userName)
					AND LOWER(accessgroups.accessgroupname) = LOWER(:accessGroupName)";

			$binding = new Bindings();
			$binding->addBinding(":userName", $userName, \PDO::PARAM_STR);
			$binding->addBinding(":accessGroupName", $accessGroupName, \PDO::PARAM_STR);

			return $this->bindAndExec($sql, $binding, false);
		}

		/***********************************
		* PROTECTED METHODS                *
		***********************************/

	}
}