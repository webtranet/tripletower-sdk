<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class SysError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */

		const INVALID_HANDLE                         =   1;
		const INVALID_PARAM                          =   2;
		const INVALID_FILE_NAME                      =   3;
		const INVALID_FILE_CONTENT                   =   4;
		const INVALID_START_TIME   	                 =   5;
		const INVALID_END_TIME                       =   6;
		const INVALID_LENGTH                         =   7;
		const INVALID_HEADER                         =   8;
		const NO_LOG_PATH                            =   9;
		const USER_ALREADY_EXISTS                    =  10;
		const MALFORMED_ZIPFILE                      =  11;
		const MALFORMED_XML                          =  12;
		const UNEXPECTED_ZIP_ERROR                   =  13;
		const FILESIZE_TOO_BIG                       =  14;
		const FILESIZE_TOO_SMALL                     =  15;
		const NAME_TOO_LONG                          =  16;
		const OUT_OF_RANGE_ERROR                     =  17;
		const INITIALIZATION_ERROR                   =  18;
		const NOT_INITIALIZED_ERROR                  =  19;
		const NO_ACCESS_TO_FRIENDS_PROPERTY          =  20;
		const NO_SUCH_PROPERTY_ERROR                 =  21;
		const PROPERTY_ALREADY_EXISTS                =  22;
		const EMPTY_STRING_ERROR                     =  23;
		const UNDEFINED_USER_SPAWN_POOL              =  24;
		const UPLOAD_NO_DATA_ERROR                   =  25;
		const UPLOAD_TRANSFER_ERROR                  =  26;
		const UPLOAD_PATH_ERROR                      =  27;
		const UPLOAD_MOVING_FILES_ERROR              =  28;
		const NEW_PASSWORD_MISMATCH_ERROR            =  29;
		const REBOOT_FAIL                            =  30;
		const EMPTY_INCLUDE_PATH                     =  31;
		const SERVICE_UNAVAILABLE                    =  32;
		const TYPE_MISMATCH                          =  33;
		const INVALID_CONFIGURATION                  =  34;
		const UNKNOWN_OS                             =  35;
		const INVALID_TOWER_CALL                     =  36;
		const SUBPROCESS_ERROR                       =  37;
		const MALFORMED_JSON                         =  38;
		const NOT_FINISHED                           =  39;
		const INVALID_EXPANSION                      =  40;
		const DAEMON_NOT_FOUND                       =  41;
		const DAEMON_START_FAILED                    =  42;
		const DAEMON_STOP_FAILED                     =  43;
		const DAEMON_QUERY_FAILED                    =  44;
		const DAEMON_DELETION_FAILED                 =  45;
		const DAEMON_NOT_RUNNING                     =  46;
		const DAEMON_NOT_STOPPED                     =  47;
		const DAEMON_ALREADY_EXISTS                  =  48;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::INVALID_HANDLE                =>  "Ungültige Verarbeitung",
			self::INVALID_PARAM                 =>  "Ungültiger Parameter",
			self::INVALID_FILE_NAME             =>  "Ungültiger Dateiname",
			self::INVALID_FILE_CONTENT          =>  "Inhalt der Datei ist Fehlerhaft",
			self::INVALID_START_TIME            =>  "Startzeit ist ungültig",
			self::INVALID_END_TIME              =>  "Endezeit ist ungültig",
			self::INVALID_LENGTH                =>  "Ungültige länge",
			self::INVALID_HEADER                =>  "Ungültiger header",
			self::NO_LOG_PATH	                =>  "Pfad zur Logdatei nicht vorhanden",
			self::USER_ALREADY_EXISTS           =>  "Der Benutzer ist bereits vorhanden",
			self::MALFORMED_ZIPFILE             =>  "Zip Datei beschädigt",
			self::MALFORMED_XML	                =>  "Es liegt kein valides XML vor",
			self::UNEXPECTED_ZIP_ERROR          =>  "Unerwarteter Zip Fehler",
			self::FILESIZE_TOO_BIG              =>  "Datei ist zu groß",
			self::FILESIZE_TOO_SMALL            =>  "Datei ist zu klein",
			self::NAME_TOO_LONG	                =>  "Name zu lang",
			self::OUT_OF_RANGE_ERROR            =>  "Außerhalb des Wertebereichs",
			self::INITIALIZATION_ERROR          =>  "Initialisierung ist fehlgeschlagen",
			self::NOT_INITIALIZED_ERROR         =>  "Nicht initialisiert",
			self::NO_ACCESS_TO_FRIENDS_PROPERTY =>  "Kein Zugriff auf friends Eigentümer",
			self::NO_SUCH_PROPERTY_ERROR        =>  "Nicht diese Eigenschaft",
			self::PROPERTY_ALREADY_EXISTS       =>  "Eigenschaft bereits vorhanden",
			self::EMPTY_STRING_ERROR            =>  "String ist leer",
			self::UNDEFINED_USER_SPAWN_POOL     =>  "Benutzer Pool ist undefiniert",
			self::UPLOAD_NO_DATA_ERROR          =>  "Keine Datei vorhanden",
			self::UPLOAD_TRANSFER_ERROR         =>  "Fehler beim hochladen",
			self::UPLOAD_PATH_ERROR             =>  "Fehlerhafter Pfad",
			self::UPLOAD_MOVING_FILES_ERROR     =>  "Fehler beim verschieben der Datei",
			self::NEW_PASSWORD_MISMATCH_ERROR   =>  "Neues Passwort stimmt nicht überein",
			self::REBOOT_FAIL                   =>  "Reboot ist fehlgeschlagen",
			self::EMPTY_INCLUDE_PATH            =>  "Der Includepfad darf nie leer sein",
			self::SERVICE_UNAVAILABLE           =>  "Der angeforderte Service ist nicht verfügbar",
			self::TYPE_MISMATCH                 =>  "Der Variablentyp passt nicht zum erwarteten Typ",
			self::INVALID_CONFIGURATION         =>  "Die Konfiguration ist fehlerhaft",
			self::UNKNOWN_OS                    =>  "Das Betriebssystem ist unbekannt",
			self::INVALID_TOWER_CALL            =>  "Diese Funktion ist auf diesem Turm nicht verfügbar",
			self::SUBPROCESS_ERROR              =>  "Ein aufgerufener Unterprozess hat einen Fehler verursacht",
			self::MALFORMED_JSON                =>  "Es liegt kein valides JSON vor",
			self::NOT_FINISHED                  =>  "Noch nicht fertig",
			self::INVALID_EXPANSION             =>  "Expansion fehlgeschlagen",
			self::DAEMON_NOT_FOUND              =>  "Daemon could not be found",
			self::DAEMON_START_FAILED           =>  "Daemon could not be started",
			self::DAEMON_STOP_FAILED            =>  "Daemon could not be stopped",
			self::DAEMON_QUERY_FAILED           =>  "Could not retrieve infos about daemon",
			self::DAEMON_DELETION_FAILED        =>  "Could not delete daemon",
			self::DAEMON_NOT_RUNNING            =>  "Daemon is not running",
			self::DAEMON_NOT_STOPPED            =>  "Daemon is not stopped",
			self::DAEMON_ALREADY_EXISTS         =>  "Daemon already exists"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}