<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class MailError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const NO_AT_FOUND                =   1;
		const LOCALPART_TOO_LONG         =   2;
		const DOMAIN_TOO_LONG            =   3;
		const LOCALPART_START_DOT_ERROR  =   4;
		const LOCALPART_END_DOT_ERROR    =   5;
		const LOCALPART_CONSECUTIVE_DOTS =   6;
		const DOMAIN_INVALID_CHARS       =   7;
		const DOMAIN_CONSECUTIVE_DOTS    =   8;
		const LOCALPART_INVALID_CHARS    =   9;
		const DOMAIN_NOT_EXISTING        =  10;
		const HEADER_PARSING_ERROR       =  11;
		const CONTENT_PARSING_ERROR      =  12;
		const INBOX_NOT_FOUND            =  13;
		const INBOX_ITEM_NOT_FOUND       =  14;
		const INVALID_MAIL_ADDRESS       =  15;
		const MAIL_SENDING_FAILED        =  16;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::NO_AT_FOUND 				=> 	"Es fehlt ein @ Zeichen",
			self::LOCALPART_TOO_LONG		=>	"Lokalteil ist zu lang",
			self::DOMAIN_TOO_LONG			=> 	"Domain ist zu lang",
			self::LOCALPART_START_DOT_ERROR	=>	"Lokalteil darf nicht mit einem Punkt beginngen",
			self::LOCALPART_END_DOT_ERROR	=>	"Lokalteil darf nicht mit einem Punkt enden",
			self::LOCALPART_CONSECUTIVE_DOTS=>	"Lokalteil darf nicht aufeinanderfolgende Punkte beinhalten",
			self::DOMAIN_INVALID_CHARS		=>	"Domain ist fehlerhaft",
			self::DOMAIN_CONSECUTIVE_DOTS	=>	"Domain darf nicht aufeinanderfolgende Punkte beinhalten",
			self::LOCALPART_INVALID_CHARS	=>	"Lokalteil ist fehlerhaft",
			self::DOMAIN_NOT_EXISTING		=>	"Domain exestiert nicht",
			self::HEADER_PARSING_ERROR      =>  "Header konnte nicht geparsed werden",
			self::CONTENT_PARSING_ERROR     =>  "Content konnte nicht geparsed werden",
			self::INBOX_NOT_FOUND           =>  "Inbox wurde nicht gefunden",
			self::INBOX_ITEM_NOT_FOUND      =>  "Das Item konnte in der Inbox nicht gefunden werden",
			self::INVALID_MAIL_ADDRESS      =>  "Mailadresse ist nicht gültig",
			self::MAIL_SENDING_FAILED       =>  "Mail konnte nicht versandt werden"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}