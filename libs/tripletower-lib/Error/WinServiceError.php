<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class WinServiceError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const SERVICE_NAME_FROM_DB					=	1;
		const SERVICE_INSTALL_FAIL					=	2;
		const SERVICE_DELETE_FAIL					=	3;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::SERVICE_NAME_FROM_DB		=>	"Servicename ist fehlerhaft",
			self::SERVICE_INSTALL_FAIL		=>	"Installation des Services ist fehlgeschlagen",
			self::SERVICE_DELETE_FAIL		=>	"Löschen des Services ist fehlgeschlagen"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}