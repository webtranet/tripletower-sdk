<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class FsError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */

		const FILE_NOT_FOUND                  =  1;
		const FOLDER_NOT_FOUND                =  2;
		const ACCESS_DENIED                   =  3;
		const FOLDER_IS_NOT_EMPTY             =  4;
		const UNLINK_FAILED                   =  5;
		const FILE_IS_READ_ONLY               =  6;
		const FOLDER_IS_READ_ONLY             =  7;
		const FILE_DELETION_FAILED            =  8;
		const FOLDER_DELETION_FAILED          =  9;
		const SCANDIR_FAILED                  = 10;
		const NAME_IS_INVALID                 = 11;
		const FILE_RENAMING_FAILED            = 12;
		const FILE_ALREADY_EXISTS             = 13;
		const FILE_CREATION_FAILED            = 14;
		const FOLDER_CREATION_FAILED          = 15;
		const FILENAME_NOT_EXISTS             = 16;
		const SECTION_NOT_FOUND               = 17;
		const LOAD_FILE_FAILED                = 18;
		const ATTRIBUTE_ALTERATION_FAILED     = 19;
		const FILE_COPYING_FAILED             = 20;
		const FOLDER_COPYING_FAILED           = 21;
		const FILE_FORMAT_CORRUPT             = 22;
		const MOUNTING_FAILED                 = 23;
		const UNMOUNTING_FAILED               = 24;
		const PATH_NOT_PARSABLE               = 25;
		const FILE_CLOSE_FAILED               = 26;
		const FILE_LOCK_FAILED                = 27;
		const FILE_UNLOCK_FAILED              = 28;
		const FOLDER_RENAMING_FAILED          = 29;
		const FILE_TIMECHANGE_FAILED          = 30;
		const IS_NOT_A_FILE                   = 31;
		const IS_NOT_A_FOLDER                 = 32;
		const IS_NOT_A_RESOURCE               = 33;
		const MIMETYPE_UNKNOWN                = 34;
		const FREE_SPACE_DETERMINATION_FAILED = 35;
		const ATTRIBUTE_READING_FAILED        = 36;
		const FILE_SHARING_FAILED             = 37;
		const FILE_WRITING_FAILED             = 38;
		const FILE_OPENING_FAILED             = 39;
		const FOLDER_OPENING_FAILED           = 40;
		const FILE_HASH_CREATION_FAILED       = 41;
		const FILE_HASH_COMPARISON_FAILED     = 42;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::FILE_NOT_FOUND                  => "Datei nicht gefunden",
			self::FOLDER_NOT_FOUND                => "Ordner nicht gefunden",
			self::ACCESS_DENIED                   => "Zugriff verweigert",
			self::FOLDER_IS_NOT_EMPTY             => "Ordner ist nicht leer",
			self::UNLINK_FAILED                   => "Löschen fehlgeschlagen",
			self::FILE_IS_READ_ONLY               => "Datei ist schreibgeschützt",
			self::FOLDER_IS_READ_ONLY             => "Ordner ist schreibgeschützt",
			self::FILE_DELETION_FAILED            => "Löschen der Datei ist fehlgeschlagen",
			self::FOLDER_DELETION_FAILED          => "Ordner löschen fehlgeschlagen",
			self::SCANDIR_FAILED                  => "Scannen des Ordners fehlgeschlagen",
			self::NAME_IS_INVALID                 => "Name ist ungültig",
			self::FILE_RENAMING_FAILED            => "Datei umbenennen fehlgeschlagen",
			self::FILE_ALREADY_EXISTS             => "Datei existiert schon",
			self::FILE_CREATION_FAILED            => "Datei konnte nicht erzeugt werden",
			self::FOLDER_CREATION_FAILED          => "Ordner konnte nicht erzeugt werden",
			self::ATTRIBUTE_ALTERATION_FAILED     => "Attribute konnten nicht geändert werden",
			self::FILE_COPYING_FAILED             => "Datei konnte nicht kopiert werden",
			self::FOLDER_COPYING_FAILED           => "Ordner konnte nicht kopiert werden",
			self::FILENAME_NOT_EXISTS             => "Dateiname nicht vorhanden",
			self::SECTION_NOT_FOUND               => "Abschnitt in der Datei nicht gefunden",
			self::LOAD_FILE_FAILED                => "Laden der Datei fehlgeschlagen",
			self::FILE_FORMAT_CORRUPT             => "Das Dateiformat ist möglicherweise korrupt.",
			self::MOUNTING_FAILED                 => "Der Pfad konnte nicht gemountet werden.",
			self::UNMOUNTING_FAILED               => "Der Pfad konnte nicht geunmountet werden.",
			self::PATH_NOT_PARSABLE               => "Der angegebene Pfad konnte nicht geparsed werden.",
			self::FILE_CLOSE_FAILED               => "Datei konnte nicht geschlossen werden.",
			self::FILE_LOCK_FAILED                => "Datei konnte nicht gelocked werden.",
			self::FILE_UNLOCK_FAILED              => "Datei konnte nicht geunlocked werden.",
			self::FOLDER_RENAMING_FAILED          => "Ordner umbenennen fehlgeschlagen",
			self::FILE_TIMECHANGE_FAILED          => "Could not update time for file",
			self::IS_NOT_A_FILE                   => "Is not a file",
			self::IS_NOT_A_FOLDER                 => "Is not a folder",
			self::MIMETYPE_UNKNOWN                => "Mimetype is unknown",
			self::FREE_SPACE_DETERMINATION_FAILED => "Could not determine free space",
			self::ATTRIBUTE_READING_FAILED        => "Could not read attributes",
			self::FILE_SHARING_FAILED             => "File can't be shared",
			self::FILE_WRITING_FAILED             => "File could not be written",
			self::FILE_OPENING_FAILED             => "Could not open file",
			self::FOLDER_OPENING_FAILED           => "Could not open folder",
			self::FILE_HASH_CREATION_FAILED       => "Could not create file hash",
			self::FILE_HASH_COMPARISON_FAILED     => "Could not compare file hashes"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}