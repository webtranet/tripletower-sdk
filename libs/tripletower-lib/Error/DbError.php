<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class DbError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const DATABASE_CONNECTION_ERROR              =   1;
		const INVALID_PDO_DBMS_TYPE                  =   2;
		const SELECT_ERROR                           =   3;
		const INSERT_ERROR                           =   4;
		const UPDATE_ERROR                           =   5;
		const DELETE_ERROR                           =   6;
		const START_TRANSACTION_ERROR                =   7;
		const COMMIT_ERROR                           =   8;
		const ROLLBACK_ERROR                         =   9;
		const BAD_SQL_STATEMENT                      =  10;
		const NO_ENTRY_FOUND                         =  11;
		const ENTRY_LOCKED                           =  12;
		const ENTRY_UNLOCKED                         =  13;
		const INVALID_TNS_ORA_SPECIFICATION          =  14;
		const TRANSACTION_FAILED                     =  15;
		const DUPLICATE_ENTRY						 =  16;
		const BINDING_FAILED                         =  17;
		const NO_ROWS_AFFECTED                       =  18;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::DATABASE_CONNECTION_ERROR     => "Verbindung zur Datenbank ist fehlgeschlagen",
			self::INVALID_PDO_DBMS_TYPE         => "Fehlerhafter PDO-DBMS Typ",
			self::SELECT_ERROR                  => "Fehlerhafter select Befehl",
			self::INSERT_ERROR                  => "Fehlerhafter insert Befehl",
			self::UPDATE_ERROR                  => "Fehlerhafter update Befehl",
			self::DELETE_ERROR                  => "Fehlerhafter delete Befehl",
			self::START_TRANSACTION_ERROR       => "Fehler beim Starten der Transaktion",
			self::COMMIT_ERROR                  => "Fehler beim commiten",
			self::ROLLBACK_ERROR                => "Fehler beim rollback",
			self::BAD_SQL_STATEMENT             => "SQL Befehl ist fehlerhaft",
			self::NO_ENTRY_FOUND                => "Kein Eintrag gefunden",
			self::ENTRY_LOCKED                  => "Inhalt ist gesperrt",
			self::ENTRY_UNLOCKED                => "Inhalt ist nicht gesperrt",
			self::INVALID_TNS_ORA_SPECIFICATION => "Angabe der TNS ORA ist nicht korrekt oder hat die falsche Reihenfolge",
			self::TRANSACTION_FAILED            => "Transaktion ist fehlgeschlagen",
			self::DUPLICATE_ENTRY				=> "Eintrag bereits vorhanden",
			self::BINDING_FAILED                => "Binding fehlgeschlagen",
			self::NO_ROWS_AFFECTED              => "Keine Einträge betroffen"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}