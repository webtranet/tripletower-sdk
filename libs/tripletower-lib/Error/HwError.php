<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class HwError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const HDD_NOT_RECOGNIZED        =   1;
		const HDD_EMPTY                 =   2;
		const HDD_FULL                  =   3;
		const HDD_READONLY              =   4;
		const CPU_TEMPERATURE_TOO_LOW   =   5;
		const CPU_TEMPERATURE_TOO_HIGH  =   6;
		const NETWORK_CONNECTION_BROKEN =   7;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::HDD_NOT_RECOGNIZED         => "Festplatte wurde nicht erkannt",
			self::HDD_EMPTY                  => "Festplatte ist leer",
			self::HDD_FULL                   => "Festplatte ist voll",
			self::HDD_READONLY               => "Festplatte ist schreibgeschützt",
			self::CPU_TEMPERATURE_TOO_LOW    => "Prozessortemperatur ist zu niedrig",
			self::CPU_TEMPERATURE_TOO_HIGH   => "Prozessortemperatur ist zu hoch",
			self::NETWORK_CONNECTION_BROKEN  => "Netzwerkverbindung unterbrochen"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}