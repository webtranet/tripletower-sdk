<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class LdapError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const CONNECT_FAILED    = 1;
		const SETOPTION_FAILED  = 2;
		const BIND_FAILED       = 3;
		const SEARCH_FAILED     = 4;
		const NO_ENTRIES_FOUND  = 5;
		const DISCONNECT_FAILED = 6;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::CONNECT_FAILED    => "Could not connect to LDAP host",
			self::SETOPTION_FAILED  => "Could not set options for LDAP connection",
			self::BIND_FAILED       => "Could not bind to LDAP connection",
			self::SEARCH_FAILED     => "Could not start LDAP search",
			self::NO_ENTRIES_FOUND  => "Could not find any entries in LDAP",
			self::DISCONNECT_FAILED => "Could not disconnect from LDAP host"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}