<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class WebError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const WEBCLIENT_FAILED    =   1;
		const AJAX_REQUEST_FAILED =   2;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::WEBCLIENT_FAILED    => "There was an error in your web client.",
			self::AJAX_REQUEST_FAILED => "An ajax request to the server failed.",
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}