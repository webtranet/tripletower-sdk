<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class ApiError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const VARIABLE_NOT_DEFINED          =   1;
		const VARIABLE_NOT_BOOL             =   2;
		const VARIABLE_NOT_INT              =   3;
		const VARIABLE_NOT_UINT             =   4;
		const VARIABLE_NOT_UINT_POSITIVE    =   5;
		const VARIABLE_NOT_FLOAT            =   6;
		const VARIABLE_NOT_STRING           =   7;
		const VARIABLE_NOT_DATETIME         =   8;
		const VARIABLE_NOT_TIME_INTERVAL    =   9;
		const VARIABLE_NOT_IP               =  10;
		const VARIABLE_NOT_IPV4             =  11;
		const VARIABLE_NOT_IPV6             =  12;
		const VARIABLE_NOT_EMAIL            =  13;
		const VARIABLE_NOT_URL              =  14;
		const VARIABLE_NOT_VERSION          =  15;
		const VARIABLE_NOT_OBJECT           =  16;
		const VARIABLE_NOT_DELIMITERABLE    =  17;
		const UNDEFINED_VARIABLE_TYPE       =  18;
		const COMPARE_CONDITION_NOT_MET     =  19;
		const CALLBACK_CONDITION_NOT_MET    =  20;
		const REGEX_CONDITION_NOT_MET       =  21;
		const ACTION_NOT_CALLABLE           =  22;
		const NO_CONDITION_SET              =  23;
		const KEYWORD_RESERVED              =  24;
		const UNDEFINED_CONDITION_TYPE      =  25;
		const INVALID_PARAMETER_COMBINATION =  26;
		const VARIABLE_ALREADY_DEFINED      =  27;
		const INVALID_REQUEST_TYPE          =  28;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::VARIABLE_NOT_DEFINED 	        => "Api Variable wurde nicht definiert",
			self::VARIABLE_NOT_BOOL			    => "Api Variable ist nicht vom Typ Boolean",
			self::VARIABLE_NOT_INT			    => "Api Variable ist nicht vom Typ Integer",
			self::VARIABLE_NOT_UINT			    => "Api Variable ist nicht vom Typ UInt",
			self::VARIABLE_NOT_UINT_POSITIVE    => "Api Variable ist nicht vom Typ UInt_Positive",
			self::VARIABLE_NOT_FLOAT		    => "Api Variable ist nicht vom Typ Float",
			self::VARIABLE_NOT_STRING		    => "Api Variable ist nicht vom Typ String",
			self::VARIABLE_NOT_DATETIME         => "Api Variable ist nicht vom Typ DateTime",
			self::VARIABLE_NOT_TIME_INTERVAL    => "Api Variable ist nicht vom Typ Time_Interval",
			self::VARIABLE_NOT_DELIMITERABLE    => "Api Variable ist kein Trennzeichen",
			self::VARIABLE_NOT_IP			    => "Api Variable ist nicht im IP Format",
			self::VARIABLE_NOT_IPV4			    => "Api Variable ist nicht im IPv4 Format",
			self::VARIABLE_NOT_IPV6			    => "Api Variable ist nicht im IPv6 Format",
			self::VARIABLE_NOT_EMAIL            => "Api Variable ist keine valide Emailadresse",
			self::UNDEFINED_VARIABLE_TYPE	    => "Api Variablentyp ist nicht bekannt",
			self::COMPARE_CONDITION_NOT_MET	    => "Api Vergleichsbedingung ist nicht erfüllt",
			self::CALLBACK_CONDITION_NOT_MET    => "Api Callbackbedingung ist nicht erfüllt",
			self::REGEX_CONDITION_NOT_MET	    => "Api Regex-Bedingung ist nicht erfüllt",
			self::ACTION_NOT_CALLABLE		    => "Aktion ist nicht aufrufbar",
			self::NO_CONDITION_SET			    => "Es wurden keine Bedingungen gesetzt",
			self::KEYWORD_RESERVED	            => "Das Keyword ist reserviert",
			self::UNDEFINED_CONDITION_TYPE      => "Der ConditionType ist nicht bekannt",
			self::INVALID_PARAMETER_COMBINATION => "Die Parameter können so nicht kombiniert werden",
			self::VARIABLE_ALREADY_DEFINED      => "Eine Variable wurde doppelt definiert",
			self::INVALID_REQUEST_TYPE          => "The chosen request type is invalid."
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}