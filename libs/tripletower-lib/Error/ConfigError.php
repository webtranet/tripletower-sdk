<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class ConfigError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const INVALID_CONFIGURATION         =  1;
		const COULD_NOT_PARSE_PARAMETER     =  2;
		const PARAMETER_NOT_FOUND           =  3;
		const WRONG_FILE_FORMAT             =  4;
		const CONFIGURATION_FILE_NOT_FOUND  =  5;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::INVALID_CONFIGURATION	       =>  "Die Konfiguration ist fehlerhaft",
			self::COULD_NOT_PARSE_PARAMETER    =>  "Der Parameter konnte nicht geparst werden",
			self::PARAMETER_NOT_FOUND	       =>  "Der Parameter wurde in der Konfig-Datei nicht gefunden",
			self::WRONG_FILE_FORMAT			   =>  "Fehlerhaftes Dateiformat",
			self::CONFIGURATION_FILE_NOT_FOUND =>  "Die Konfigurationsdatei wurde nicht gefunden"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}