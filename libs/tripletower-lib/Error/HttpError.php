<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	/**
	 * @name HttpError
	 *
	 * DON'T EDIT The Messages. These are also used for HTTP-Header messages.
	 */
	class HttpError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */

		// [No Connectino 0xx]
		const HTTP_NO_CONNECTION                   =   0;

		// [Informational 1xx]
		const HTTP_CONTINUE                        = 100;
		const HTTP_SWITCHING_PROTOCOLS             = 101;

		// [Successful 2xx]
		const HTTP_OK                              = 200;
		const HTTP_CREATED                         = 201;
		const HTTP_ACCEPTED                        = 202;
		const HTTP_NONAUTHORITATIVE_INFORMATION    = 203;
		const HTTP_NO_CONTENT                      = 204;
		const HTTP_RESET_CONTENT                   = 205;
		const HTTP_PARTIAL_CONTENT                 = 206;

		// [Redirection 3xx]
		const HTTP_MULTIPLE_CHOICES                = 300;
		const HTTP_MOVED_PERMANENTLY               = 301;
		const HTTP_FOUND                           = 302;
		const HTTP_SEE_OTHER                       = 303;
		const HTTP_NOT_MODIFIED                    = 304;
		const HTTP_USE_PROXY                       = 305;
		const HTTP_UNUSED                          = 306;
		const HTTP_TEMPORARY_REDIRECT              = 307;

		// [Client Error 4xx]
		const HTTP_BAD_REQUEST                     = 400;
		const HTTP_UNAUTHORIZED                    = 401;
		const HTTP_PAYMENT_REQUIRED                = 402;
		const HTTP_FORBIDDEN                       = 403;
		const HTTP_NOT_FOUND                       = 404;
		const HTTP_METHOD_NOT_ALLOWED              = 405;
		const HTTP_NOT_ACCEPTABLE                  = 406;
		const HTTP_PROXY_AUTHENTICATION_REQUIRED   = 407;
		const HTTP_REQUEST_TIMEOUT                 = 408;
		const HTTP_CONFLICT                        = 409;
		const HTTP_GONE                            = 410;
		const HTTP_LENGTH_REQUIRED                 = 411;
		const HTTP_PRECONDITION_FAILED             = 412;
		const HTTP_REQUEST_ENTITY_TOO_LARGE        = 413;
		const HTTP_REQUEST_URI_TOO_LONG            = 414;
		const HTTP_UNSUPPORTED_MEDIA_TYPE          = 415;
		const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
		const HTTP_EXPECTATION_FAILED              = 417;

		// [Server Error 5xx]
		const HTTP_INTERNAL_SERVER_ERROR           = 500;
		const HTTP_NOT_IMPLEMENTED                 = 501;
		const HTTP_BAD_GATEWAY                     = 502;
		const HTTP_SERVICE_UNAVAILABLE             = 503;
		const HTTP_GATEWAY_TIMEOUT                 = 504;
		const HTTP_VERSION_NOT_SUPPORTED           = 505;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			// [No Connectino 0xx]
			self::HTTP_NO_CONNECTION                   => "0 No Connection",

			// [Informational 1xx]
			self::HTTP_CONTINUE                        => "100 Continue",
			self::HTTP_SWITCHING_PROTOCOLS             => "101 Switching Protocols",

			// [Successful 2xx]
			self::HTTP_OK                              => "200 OK",
			self::HTTP_CREATED                         => "201 Created",
			self::HTTP_ACCEPTED                        => "202 Accepted",
			self::HTTP_NONAUTHORITATIVE_INFORMATION    => "203 Non-Authoritative Information",
			self::HTTP_NO_CONTENT                      => "204 No Content",
			self::HTTP_RESET_CONTENT                   => "205 Reset Content",
			self::HTTP_PARTIAL_CONTENT                 => "206 Partial Content",

			// [Redirection 3xx]
			self::HTTP_MULTIPLE_CHOICES                => "300 Multiple Choices",
			self::HTTP_MOVED_PERMANENTLY               => "301 Moved Permanently",
			self::HTTP_FOUND                           => "302 Found",
			self::HTTP_SEE_OTHER                       => "303 See Other",
			self::HTTP_NOT_MODIFIED                    => "304 Not Modified",
			self::HTTP_USE_PROXY                       => "305 Use Proxy",
			self::HTTP_UNUSED                          => "306 (Unused)",
			self::HTTP_TEMPORARY_REDIRECT              => "307 Temporary Redirect",

			// [Client Error 4xx]
			self::HTTP_BAD_REQUEST                     => "400 Bad Request",
			self::HTTP_UNAUTHORIZED                    => "401 Unauthorized",
			self::HTTP_PAYMENT_REQUIRED                => "402 Payment Required",
			self::HTTP_FORBIDDEN                       => "403 Forbidden",
			self::HTTP_NOT_FOUND                       => "404 Not Found",
			self::HTTP_METHOD_NOT_ALLOWED              => "405 Method Not Allowed",
			self::HTTP_NOT_ACCEPTABLE                  => "406 Not Acceptable",
			self::HTTP_PROXY_AUTHENTICATION_REQUIRED   => "407 Proxy Authentication Required",
			self::HTTP_REQUEST_TIMEOUT                 => "408 Request Timeout",
			self::HTTP_CONFLICT                        => "409 Conflict",
			self::HTTP_GONE                            => "410 Gone",
			self::HTTP_LENGTH_REQUIRED                 => "411 Length Required",
			self::HTTP_PRECONDITION_FAILED             => "412 Precondition Failed",
			self::HTTP_REQUEST_ENTITY_TOO_LARGE        => "413 Request Entity Too Large",
			self::HTTP_REQUEST_URI_TOO_LONG            => "414 Request-URI Too Long",
			self::HTTP_UNSUPPORTED_MEDIA_TYPE          => "415 Unsupported Media Type",
			self::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE => "416 Requested Range Not Satisfiable",
			self::HTTP_EXPECTATION_FAILED              => "417 Expectation Failed",

			// [Server Error 5xx]
			self::HTTP_INTERNAL_SERVER_ERROR           => "500 Internal Server Error",
			self::HTTP_NOT_IMPLEMENTED                 => "501 Not Implemented",
			self::HTTP_BAD_GATEWAY                     => "502 Bad Gateway",
			self::HTTP_SERVICE_UNAVAILABLE             => "503 Service Unavailable",
			self::HTTP_GATEWAY_TIMEOUT                 => "504 Gateway Timeout",
			self::HTTP_VERSION_NOT_SUPPORTED           => "505 HTTP Version Not Supported"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public static function getHttpHeaderString($code)
		{
			return 'HTTP/1.1 ' . self::$descArray[$code];
		}

		public static function canHaveBody($code)
		{
			return ($code < self::HTTP_CONTINUE || $code >= self::HTTP_OK) && // True if not in 100s
					$code != self::HTTP_NO_CONTENT &&                         // and not 204 NO CONTENT
					$code != self::HTTP_NOT_MODIFIED;                         // and not 304 NOT MODIFIED
		}

		public function isSuccess() : bool
		{
			// Everything below 400 is success, except for: 0 No Connection
			return self::HTTP_NO_CONNECTION < $this->code && $this->code < self::HTTP_BAD_REQUEST;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}