<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class DevError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */

		const ILLEGAL_CIRCULAR_INHERITANCE =  1;
		const CREATE_KEY_FAILED            =  2;
		const INVALID_KEY                  =  3;
		const NODE_ERROR                   =  4;
		const CONST_NOT_DEFINED            =  5;
		const SYNTAX_ERROR                 =  6;
		const CLASS_NOT_FOUND              =  7;
		const INVALID_INPUT_VALUE          =  8;
		const BAD_ENVIRONMENT              =  9;
		const NOT_YET_IMPLEMENTED          = 10;
		const TEST_FAILED                  = 11;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::ILLEGAL_CIRCULAR_INHERITANCE => "Kreisverweis in der Vererbungshierarchie",
			self::CREATE_KEY_FAILED            => "Erstellung der Schlüssel fehlgeschlagen",
			self::INVALID_KEY                  => "Ungültiger Schlüssel",
			self::NODE_ERROR                   => "Fehler bei der Knotenverarbeitung",
			self::CONST_NOT_DEFINED            => "Konstante wurde nicht definiert",
			self::SYNTAX_ERROR                 => "Syntax Fehler",
			self::CLASS_NOT_FOUND              => "Klasse nicht gefunden",
			self::INVALID_INPUT_VALUE          => "Ungültiger Eingabeparameter",
			self::BAD_ENVIRONMENT              => "Ungültige Laufzeitumgebung",
			self::NOT_YET_IMPLEMENTED          => "Noch nicht implementiert",
			self::TEST_FAILED                  => "Test failed"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}