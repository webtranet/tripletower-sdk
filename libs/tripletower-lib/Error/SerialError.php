<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class SerialError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const NO_SUCH_DEVICE          =   1;
		const NO_SSTY_AVAILABLE       =   2;
		const NOT_SUPPORTED_OS        =   3;
		const DEVICE_STILL_OPEN       =   4;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::NO_SUCH_DEVICE    => "No such serial device available.",
			self::NO_SSTY_AVAILABLE => "No ssty command available.",
			self::NOT_SUPPORTED_OS  => "Host OS is neither osx, linux nor windows. No php support for RS232 on this OS.",
			self::DEVICE_STILL_OPEN => "The device is still open. You must close your device before to set an other one."
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}