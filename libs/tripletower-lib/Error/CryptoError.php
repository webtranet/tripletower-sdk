<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class CryptoError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const BCRYPT_NO_BLOWFISH_AVAILABLE           =   1;
		const BCRYPT_INVALID_SALT_TYPE               =   2;
		const BCRYPT_INVALID_SALT_LENGTH             =   3;
		const OPENSSL_ENCRYPTION_FAILED              =   4;
		const OPENSSL_DECRYPTION_FAILED              =   5;
		const OPENSSL_SIGNING_FAILED                 =   6;
		const OPENSSL_KEY_CREATION_FAILED            =   7;
		const IV_CREATION_FAILED                     =   8;
		const AES_ENCRYPTION_FAILED                  =   9;
		const AES_DECRYPTION_FAILED                  =  10;
		const AES_UNSUPPORTED_KEYSIZE                =  11;
		const PASSWORD_PREPARATION_FAILED            =  12;
		const EXPIRATION_REACHED                     =  13;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::BCRYPT_NO_BLOWFISH_AVAILABLE 	=> "Blowfish ist nicht verfügbar",
			self::BCRYPT_INVALID_SALT_TYPE		=> "Salt Typ ist fehlerhaft",
			self::BCRYPT_INVALID_SALT_LENGTH	=> "Salt länge ist fehlerhaft",
			self::OPENSSL_ENCRYPTION_FAILED     => "OpenSSL-Verschlüsselung ist fehlgeschlagen",
			self::OPENSSL_DECRYPTION_FAILED     => "OpenSSL-Entschlüsselung ist fehlgeschlagen",
			self::OPENSSL_SIGNING_FAILED        => "OpenSSL-Signierung ist fehlgeschlagen",
			self::OPENSSL_KEY_CREATION_FAILED   => "OpenSSL-Keyerstellung ist fehlgeschlagen",
			self::IV_CREATION_FAILED            => "Zufälliger initialer Vector konnte nicht erzeugt werden",
			self::AES_ENCRYPTION_FAILED         => "AES-Verschlüsselung ist fehlgeschlagen",
			self::AES_DECRYPTION_FAILED         => "AES-Entschlüsselung ist fehlgeschlagen",
			self::AES_UNSUPPORTED_KEYSIZE       => "Schlüsselgröße wird nicht unterstützt.",
			self::PASSWORD_PREPARATION_FAILED   => "Die Passwortvorbereitung ist fehlgeschlagen.",
			self::EXPIRATION_REACHED            => "Ablaufdatum erreicht"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}