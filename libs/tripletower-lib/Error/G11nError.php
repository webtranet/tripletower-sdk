<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class G11nError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const NOT_INITIALIZED              =   1;
		const INVALID_GLOBALIZATION_FOLDER =   2;
		const INVALID_LANGUAGE_FILE        =   3;
		const INVALID_LOCATION_FILE        =   4;
		const INVALID_TIMEZONE_FILE        =   5;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::NOT_INITIALIZED               => "Internationalisierungsmodul wurde nicht initialisiert",
			self::INVALID_GLOBALIZATION_FOLDER  => "Der Ordner für Globalisierung wurde nicht gefunden",
			self::INVALID_LANGUAGE_FILE         => "Die Datei ist keine valide Sprachdatei",
			self::INVALID_LOCATION_FILE         => "Die Datei ist keine valide Ortsbeschreibungsdatei",
			self::INVALID_TIMEZONE_FILE         => "Die Datei ist keine valide Zeitzonendatei"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}