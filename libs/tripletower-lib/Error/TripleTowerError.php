<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	use TripleTowerSDK\Date\DateTime;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Helper\Helper;

	abstract class TripleTowerError extends \Exception
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error severities
		 * Severity constant must be defined as first
		 */
		const SILENT  = -1;  // Silent: absolute silence, no messages get generated
		const EMERG   =  0;  // Emergency: system is unusable
		const ALERT   =  1;  // Alert: action must be taken immediately
		const CRIT    =  2;  // Critical: critical conditions
		const ERR     =  3;  // Error: error conditions
		const WARN    =  4;  // Warning: warning conditions
		const NOTICE  =  5;  // Notice: normal but significant condition
		const INFO    =  6;  // Informational: informational messages
		const DEBUG   =  7;  // Debug: debug messages


		/**
		 * Error codes
		 * Error constant must be defined as second
		 */
		const SUCCESS = 0;
		const UNKNOWN = 0x7FFFFFFF; // highest integer value


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error message attributes
		 */
		// protected $code;       // e.g. 15
		protected $codeText;      // e.g. FILE_NOT_FOUND
		protected $severity;      // e.g. NOTICE
		protected $category;      // e.g. SysError
		// protected $file;       // e.g. C:/myDir/myFile.php
		// protected $line;       // e.g. 63
		protected $trace;         // e.g. foo() => Bla::bar() => ohMy->dear()
		// protected $message;    // e.g. An horrible error occured
		protected $time;          // e.g. DateTime(W3C) -> 2005-08-15T15:52:01+00:00

		/**
		 * Seperators
		 */
		protected $seperator = self::BIGARROW;
		const BIGARROW   = ' => ';
		const SMALLARROW = ' -> ';


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct($errCode, int $errSeverity = self::NOTICE, $errMessage = "", \Throwable $previous = null)
		{
			$errorChildName = get_class($this);
			if(!isset($errorChildName::$descArray[self::SUCCESS]))
				$errorChildName::$descArray[self::SUCCESS] = "Success";
			if(!isset($errorChildName::$descArray[self::UNKNOWN]))
				$errorChildName::$descArray[self::UNKNOWN] = "Unknown";

			if(Helper::isXmlObject($errCode) || Helper::isJsonObject($errCode))
			{
				if(	$errCode->code     !== "" &&
					$errCode->codeText !== "" &&
					$errCode->severity !== "" &&
					$errCode->category !== "" &&
					$errCode->file     !== "" &&
					$errCode->line     !== "" &&
					$errCode->time     !== "")
				{
					$this->code     = (int)$errCode->code;
					$this->codeText = (string)$errCode->codeText;
					$this->severity = (int)$errCode->severity;
					$this->category = (string)$errCode->category;
					$this->file     = Helper::normalizeFilePath((string)$errCode->file);
					$this->line     = (int)$errCode->line;
					$this->trace    = (string)$errCode->trace;
					$this->message  = (string)$errCode->message;
					$this->time     = DateTime::createDateTimeFromISOFormat($errCode->time);
				}
				else
				{
					if(Helper::isXmlObject($errCode))
						throw new SysError(SysError::MALFORMED_XML);
					else
						throw new SysError(SysError::MALFORMED_JSON);
				}
			}
			else
			{
				$trace = Helper::debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, ($errCode===self::SUCCESS ? 3 : 0));

				$this->code     = (int)$errCode;
				$this->category = (string)get_class($this);
				$class = new \ReflectionClass($this->category);
				$classParent = new \ReflectionClass("TripleTowerSDK\\Error\\TripleTowerError");
				$constants = array_flip(array_diff_key($class->getConstants(), $classParent->getConstants()));
				$constants[self::SUCCESS] = "SUCCESS";
				$constants[self::UNKNOWN] = "UNKNOWN";

				$this->codeText = (string)(isset($constants[$errCode]) ? $constants[$errCode] : $constants[self::UNKNOWN]);
				$this->severity = (int)$errSeverity;
				if(!is_numeric($errSeverity) )
					$_SERVER["TT"]->logger->log(self::EMERG, "There has been a false error declaration. Severity has to be a number, but is '$errSeverity'.");
				$this->file     = Helper::normalizeFilePath((string)isset($trace[1]['file']) ? $trace[1]['file'] : $trace[0]['file']);
				$this->line     = (int)isset($trace[1]['line']) ? $trace[1]['line'] : $trace[0]['line'];

				array_shift($trace);
				$trace = array_reverse($trace);

				$traceString = "";
				foreach($trace as $traceLine)
				{
					$traceString .= (isset($traceLine['class']) && isset($traceLine['type']) ? $traceLine['class'].$traceLine['type'] : '') . (isset($traceLine['function']) ? $traceLine['function'].'()' : '') . $this->seperator;
				}
				if(substr($traceString, strlen($this->seperator) * -1) === $this->seperator)
					$traceString = substr($traceString, 0, strlen($this->seperator) * -1);
				$this->trace = (string)$traceString;

				if(empty($errMessage) && array_key_exists($this->code, $errorChildName::$descArray))
					$this->message = (string)$errorChildName::$descArray[$this->code];
				else
					$this->message = (string)(Helper::isClassRelative($errMessage, "Throwable") ? $errMessage->getMessage() : $errMessage);
				$this->time = new DateTime();

				if($this->severity <= self::DEBUG && $this->severity >= self::EMERG && $this->code !== self::SUCCESS)
				{
					if(isset($_SERVER["TT"]) && isset($_SERVER["TT"]->logger))
						$_SERVER["TT"]->logger->log($this->severity, $this);
				}
			}

			if(isset($_SERVER["TT"]) && $this->code !== self::SUCCESS && $this->severity !== self::SILENT)
				$_SERVER["TT"]->setLastError($this);

			parent::__construct($this->message, $this->code, $previous);
		}

		/**
		 * toXml methode
		 */
		public function toXml(bool $header = true)
		{
			$xml = "";
			if($header === true)
			{
				Helper::setXmlHeader(false);
				$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
			}

			$codeXml		= $this->code;
			$codeTextXml	= $this->codeText;
			$severityXml 	= $this->severity;
			$categoryXml	= $this->category;
			$fileXml        = $this->file;
			$lineXml		= $this->line;
			$traceXml		= $this->trace;
			$messageXml	    = $this->message;
			$timeXml		= $this->time->format(DateTime::DEFAULT_FORMAT);

			if($this->isSuccess())
			{
				$xml .= "<tt-error>" .
							"<code>$codeXml</code>" .
							"<code-text>$codeTextXml</code-text>" .
							"<severity />" .
							"<category>$categoryXml</category>" .
							"<file />" .
							"<line />" .
							"<trace />" .
							"<message>$messageXml</message>" .
							"<time>$timeXml</time>" .
						"</tt-error>";
			}
			else
			{
				$xml .= "<tt-error>" .
							"<code>$codeXml</code>" .
							"<code-text>$codeTextXml</code-text>" .
							"<severity>$severityXml</severity>" .
							"<category>$categoryXml</category>" .
							"<file>$fileXml</file>" .
							"<line>$lineXml</line>" .
							"<trace>$traceXml</trace>" .
							"<message>$messageXml</message>" .
							"<time>$timeXml</time>" .
						"</tt-error>";
			}
			return $xml;
		}

		public static function fromXml($xmlString)
		{
			//TODO: Kommentar für Namespace
			$xmlObject = Helper::xmlToSimpleXMLObject($xmlString);
			$className = __NAMESPACE__.'\\'.$xmlObject->category;
			return new $className($xmlObject);
		}

		public static function fromJson(string $jsonString)
		{
			$jsonObject = json_decode($jsonString);
			if(empty($jsonObject))
				throw new SysError(SysError::MALFORMED_JSON, SysError::ERR, json_last_error_msg());
			$className = $jsonObject->category;
			return new $className($jsonObject);
		}

		public static function fromParameters(int $code, int $severity, string $category, string $file, int $line, string $trace, string $message, DateTime $time)
		{
			$error = new $category((int)$code, self::SILENT);
			$error->severity = (int)$severity;
			$error->file     = (string)$file;
			$error->line     = (int)$line;
			$error->trace    = (string)$trace;
			$error->message  = (string)$message;
			$error->time     = $time;
			return $error;
		}

		public static function isError(mixed $errObject) : bool
		{
			return Helper::isClassRelative($errObject, __CLASS__);
		}

		public static function isErrorAndSame(mixed $errObject, self $sameErrorObject) : bool
		{
			return self::isError($errObject) ? $errObject->isSameError($sameErrorObject) : false;
		}

		public static function throwIfIsError($errObject, string $additionalMessage = "") : void
		{
			if( Helper::isClassRelative($errObject, __CLASS__) )
				$errObject->throw($additionalMessage);
		}

		public function isSameError(TripleTowerError $sameErrorObject) : bool
		{
			return $this->category === $sameErrorObject->category && $this->code === $sameErrorObject->code;
		}

		public function isSuccess() : bool
		{
			return $this->code === self::SUCCESS;
		}

		public function addMessage(string $message) : self
		{
			if(empty($this->message))
				$this->message = $message;
			else
				$this->message .= $this->seperator . $message;

			return $this;
		}

		public function raiseError(int $severity, string $message = "") : self
		{
			if(is_int($severity))
			{
				if($this->severity > $severity || $this->severity === self::SILENT)
				{
					if(!empty($message))
						$this->addMessage($message);

					$this->severity = $severity;
					if($this->severity <= self::DEBUG && $this->severity >= self::EMERG && $this->code !== self::SUCCESS)
					{
						if(isset($_SERVER["TT"]) && isset($_SERVER["TT"]->logger))
							$_SERVER["TT"]->logger->log($this->severity, $this);
					}
				}
			}
			else
			{
				throw new SysError(SysError::INVALID_PARAM);
			}
			return $this;
		}

		public function getDefaultMessage()
		{
			$errorChildName = get_class($this);
			return $errorChildName::$descArray[$this->code];
		}

		public function getMinifiedMessage()
		{
			return Helper::removeNewLineFeedsAndTabs($this->message);
		}

		public function throw(string $additionalMessage = "") : void
		{
			if($additionalMessage !== "")
				$this->addMessage($additionalMessage);
			throw $this;
		}

		public function throwIfNotSuccess(string $additionalMessage = "")
		{
			if(!$this->isSuccess())
				$this->throw($additionalMessage);

			return $this;
		}

		public function throwIfNotSuccessOrSameAs($errObject, string $additionalMessage = "")
		{
			if( !self::isError($errObject) )
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Given error object is not an instance of " . __CLASS__);

			if(!$this->isSameError($errObject))
				$this->throwIfNotSuccess($additionalMessage);

			return $this;
		}

		/**
		 * __get method
		 */
		public function __get($key)
		{
			return isset($this->$key) ? $this->$key : self::$$key;
		}

		public function __isset($key)
		{
			return isset($this->$key) || isset(self::$$key);
		}

		/**
		 * setter
		 */
		public function setSeperator($seperator)
		{
			$this->seperator = $seperator;
		}

		/**
		 * __toString method
		 */
		public function __toString()
		{
			if($this->isSuccess())
				return $this->category."(". $this->codeText .")";
			else
				return $this->category."(". $this->codeText ."): '" . $this->message . "' in " . $this->file . "(line " . $this->line . ") at " . $this->time->format(DateTime::DEFAULT_FORMAT);
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}