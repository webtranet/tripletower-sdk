<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class AuthError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const INVALID_SID                       =   1;
		const INVALID_USER_NAME                 =   2;
		const INVALID_IPADDRESS                 =   3;
		const INVALID_SESSION                   =   4;
		const INVALID_AUTHSYSTEM                =   5;
		const DATABASE_ERROR                    =   6;
		const WRONG_USERNAME                    =   7;
		const WRONG_PASSWORD                    =   8;
		const COOKIE_COULD_NOT_BE_SAVED         =   9;
		const SESSION_CREATION_ERROR            =  10;
		const WRONG_USERNAME_OR_PASSWORD        =  11;
		const SERVICE_DOWN                      =  12;
		const DESTROY_SESSION_ERROR             =  13;
		const BOOTLOADER_ACCESS_ERROR           =  14;
		const VIEW_ACCESS_ERROR                 =  15;
		const ACCESS_DENIED                     =  16;
		const SESSION_DUPLICATE_FOUND           =  17;
		const USER_NOT_FOUND                    =  18;
		const WAPP_ACCESS_ERROR                 =  19;
		const NO_SESSION_SPECIFIED              =  20;
		const USER_ALREADY_EXISTS               =  21;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::INVALID_SID                   => "SID ist falsch",
			self::INVALID_USER_NAME				=> "User ID ist falsch",
			self::INVALID_IPADDRESS				=> "IP Adresse ist fehlerhaft",
			self::INVALID_SESSION				=> "Session ist fehlerhaft",
			self::INVALID_AUTHSYSTEM			=> "Authentifizierssystem ist unbekannt",
			self::DATABASE_ERROR				=> "Auth. an der Datenbank ist fehlgeschlagen",
			self::WRONG_USERNAME				=> "Benutzername ist falsch",
			self::WRONG_PASSWORD				=> "Passwort ist falsch",
			self::COOKIE_COULD_NOT_BE_SAVED		=> "Auth. cookie wurde nicht gespeichert",
			self::SESSION_CREATION_ERROR		=> "Fehler beim erstellen der Session",
			self::WRONG_USERNAME_OR_PASSWORD	=> "Benutzername oder Passwort ist falsch",
			self::SERVICE_DOWN					=> "Authentifizierungsservice nicht verfügbar",
			self::DESTROY_SESSION_ERROR			=> "Session konnte nicht zerstört werden",
			self::BOOTLOADER_ACCESS_ERROR       => "Benutzer hat keinen Zugriff auf den Bootloader",
			self::VIEW_ACCESS_ERROR             => "Benutzer hat keinen Zugriff auf die View",
			self::ACCESS_DENIED                 => "Zugriff verweigert",
			self::SESSION_DUPLICATE_FOUND       => "Sessionduplette gefunden",
			self::USER_NOT_FOUND                => "User nicht gefunden",
			self::WAPP_ACCESS_ERROR             => "Zugriff auf WebApplikation verweigert",
			self::NO_SESSION_SPECIFIED          => "Es wurde keine Session angegeben",
			self::USER_ALREADY_EXISTS           => "The user already exists"
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}