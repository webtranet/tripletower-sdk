<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class NetworkError extends TripleTowerError
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */
		const PING_ERROR            =  1;
		const CONNECTION_FAILED     =  2;
		const CONNECTION_RESET      =  3;
		const CONNECTION_LOST       =  4;
		const FIREWALL_BLOCKED      =  5;
		const SYN_FLOOD             =  6;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description arrray
		 */
		protected static $descArray =
		[
			self::PING_ERROR            => "A ping error occured.",
			self::CONNECTION_FAILED     => "Could not connect.",
			self::CONNECTION_RESET      => "Connection reset by peer.",
			self::CONNECTION_LOST       => "Connection got lost.",
			self::FIREWALL_BLOCKED      => "Firewall is blocking.",
			self::SYN_FLOOD             => "Syn Flooding detected."
		];


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/


		/***********************************
		* PROTECTED METHODS                *
		***********************************/
	}
}