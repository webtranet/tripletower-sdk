<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Error
{

	class OAuthError extends TripleTowerError
	{
		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		/**
		 * Error keys
		 */

		const ACCESS_TOKEN_EXPIRED            =  1;
		const ACCESS_TOKEN_REQUEST_FAILED     =  2;
		const ACCESS_TOKEN_JSON_EMPTY         =  3;
		const GRANT_TYPE_NOT_IMPLEMENTED      =  4;
		const AUTH_URL_EMPTY                  =  5;
		const CLIENT_ID_EMPTY                 =  6;
		const SCOPE_EMPTY                     =  7;
		const SET_TOKEN_COOKIE_FAILED         =  8;

		const ACCESS_DENIED                   =  9;
		const INVALID_REQUEST                 = 10;
		const UNAUTHORIZED_CLIENT             = 11;
		const INVALID_SCOPE                   = 12;
		const UNSUPPORTED_RESPONSE_TYPE       = 13;
		const SERVER_ERROR                    = 14;
		const TEMPORARILY_UNAVAILABLE         = 15;

		const INVALID_RESOURCE                = 16;

		const LOGIN_REQUIRED                  = 17;
		const CONSENT_REQUIRED                = 18;
		const INTERACTION_REQUIRED            = 19;

		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct($errCode, int $errSeverity = self::NOTICE, $errMessage = "", \Throwable $previous = null)
		{
			// Map Error codeText to errorCode
			if( is_string($errCode) )
			{
				$constName = strtoupper($errCode);
				$errCode = defined( get_class($this) . "::$constName") ? self::$constName : $errCode;
			}

			parent::__construct($errCode, $errSeverity, $errMessage, $previous);
		}

		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		/**
		 * Error description array
		 */
		protected static $descArray =
		[
			self::ACCESS_TOKEN_EXPIRED          => "Access token expired.",
			self::ACCESS_TOKEN_REQUEST_FAILED   => "Access token request failed.",
			self::ACCESS_TOKEN_JSON_EMPTY       => "Token JSON string is empty.",
			self::GRANT_TYPE_NOT_IMPLEMENTED    => "OAuth grant type not implemented.",
			self::AUTH_URL_EMPTY                => "OAuth authentication URL is empty.",
			self::CLIENT_ID_EMPTY               => "OAuth Client ID is empty.",
			self::SCOPE_EMPTY                   => "OAuth scope is empty.",
			self::SET_TOKEN_COOKIE_FAILED       => "OAuth set token cookie failed.",
			self::ACCESS_DENIED                 => "Access denied or authorization denied by user",
			self::INVALID_REQUEST               => "The request is missing a required parameter, includes an invalid parameter value or is otherwise malformed.",
			self::UNAUTHORIZED_CLIENT           => "The client is not authorized to request an authorization code using this method.",
			self::UNSUPPORTED_RESPONSE_TYPE     => "The authorization server does not support obtaining an authorization code using this method.",
			self::INVALID_SCOPE                 => "The requested scope is invalid, unknown or malformed.",
			self::SERVER_ERROR                  => "The authorization server encountered an unexpected condition which prevented it from fulfilling the request.",
			self::INVALID_RESOURCE              => "The target resource is invalid because it doesn't exist.",
			self::TEMPORARILY_UNAVAILABLE       => "The authorization server is currently unable to handle the request due to a temporary overloading or maintenance of the server.",
			self::LOGIN_REQUIRED                => "No silent authentication possible because user is not logged in.",
			self::CONSENT_REQUIRED              => "The user was logged in but needs to give consent to authorize the application.",
			self::INTERACTION_REQUIRED          => "User interaction is required for authentication."
		];
	}
}