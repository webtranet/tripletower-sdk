<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Logger;

class TripleTowerLogFormatter extends \Zend\Log\Formatter\Simple
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/


	/***********************************
	* PROTECTED ATTRIBUTES             *
	***********************************/

	protected $format;
	protected $extraInfoFormatPart = PHP_EOL . '%file% - line %line%';


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	public function __construct()
    {
        parent::__construct('%timestamp%-Z %priorityName%: %message%' . $this->extraInfoFormatPart, "Y-m-d H:i:s");
    }

    public function format($event)
    {
		$output = $this->format;
		if( isset($event['extra']) )
		{
			if( is_array($event['extra']) && isset($event["extra"]["file"]) && isset($event["extra"]["line"]) )
			{
				$event["file"] = $event["extra"]["file"];
				$event["line"] = $event["extra"]["line"];
			}
			else
			{
				$output = str_replace($this->extraInfoFormatPart, "", $output);
			}
		}

		foreach($event as $name => $value)
		{
			$output = str_replace("%$name%", strval($this->normalize($value)), $output);
		}

        return $output;


	/***********************************
	* PROTECTED METHODS                *
	***********************************/
    }
}