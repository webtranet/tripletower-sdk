<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Tower
{

	use TripleTowerSDK\Tower;
	use TripleTowerSDK\Error\TripleTowerError;
	use TripleTowerSDK\Authorizor\WappsTowerAuthorizor;
	use TripleTowerSDK\User\UserSpace\InstalledWapp\WappInfo;
	use Zend\Loader\ClassMapAutoloader;

	class WappsTower extends Tower

	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $rootFolder, string $sdkFolder, ClassMapAutoloader $loader)
		{
			// --- SETUP TOWER BASICS ---
			parent::__construct($rootFolder, $sdkFolder, $loader, self::WAPPS_TOWER);

			// --- REQUEST ROUTING ---
			$this->routeRequest()->throwIfNotSuccess();

			// --- REQUEST PREPARING ---
			$this->prepareRequest()->throwIfNotSuccess();
		}

		public function checkAuthentication()
		{
			// --- PRE AUTHENTICATION HOOK ---
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::PRE_AUTHENTICATION_HOOK)) )
				require $hookFile;

			// --- AUTHENTICATION ---
			$user = $_SERVER["TT"]->authSystems->tripleTowerAS->getAuthenticatedUser();
			if( TripleTowerError::isError($user) )
				return $user;

			// --- POST AUTHENTICATION HOOK ---
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::POST_AUTHENTICATION_HOOK)) )
				require $hookFile;

			// --- PERSONALIZATION ---
			return $_SERVER["TT"]->personalizeUserRequest($user);
		}

		public function checkAuthorization()
		{
			$authorizor = new WappsTowerAuthorizor();

			// --- PRE AUTHORIZATION HOOK ---
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::PRE_AUTHORIZATION_HOOK)) )
				require $hookFile;

			$accessResult = $authorizor->checkAccess($_SERVER["TT"]->user->userName, $_SERVER["TT"]->contentProvider, $_SERVER["TT"]->contentType, $_SERVER["TT"]->contentName);

			// --- POST AUTHORIZATION HOOK ---
			if( !empty($hookFile = $_SERVER["TT"]->meta->checkHookFilePresence(WappInfo::POST_AUTHORIZATION_HOOK)) )
				require $hookFile;

			return $accessResult;
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function removeTowerInfos()
		{
			unset($_SERVER["TT"]->conf->wappsTowerDb);
		}
	}
}