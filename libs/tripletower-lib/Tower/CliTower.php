<?php declare(strict_types=1);
/**
 * TripleTowerSDK - https://webtranet.online/tripletower-sdk
 *
 * @link      https://gitlab.com/webtranet/tripletower-sdk.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

namespace TripleTowerSDK\Tower
{

	use TripleTowerSDK\Helper\Helper;
	use TripleTowerSDK\Error\DevError;
	use TripleTowerSDK\Error\SysError;
	use TripleTowerSDK\Application\OsNames;
	use TripleTowerSDK\Error\FsError;
	use TripleTowerSDK\Tower;
	use TripleTowerSDK\User;
	use Zend\Loader\ClassMapAutoloader;

	class CliTower extends Tower
	{

		/***********************************
		* PUBLIC ATTRIBUTES                *
		***********************************/

		const SIGNAL_NONE    = "";
		const SIGNAL_START   = "start";
		const SIGNAL_STOP    = "stop";
		const SIGNAL_PAUSE   = "pause";
		const SIGNAL_RESUME  = "resume";
		const SIGNAL_LOAD    = "load";

		public $cmdLineOptions = null;


		/***********************************
		* PROTECTED ATTRIBUTES             *
		***********************************/

		protected $configKey = "";


		/***********************************
		* PUBLIC METHODS                   *
		***********************************/

		public function __construct(string $rootFolder, string $sdkFolder, ClassMapAutoloader $loader, array $cmdLineOptions = [], object $defaultCmdLineOptions = null)
		{
			// --- COMMAND LINE PARSING & INSTANCE DETERMINATION ---
			if( !$this->isCliTower() )
				throw new DevError(DevError::BAD_ENVIRONMENT, DevError::EMERG, "Class '" . get_class($this) . "' can only be used in cli based applications.");

			$this->cmdLineOptions = $defaultCmdLineOptions ?? new \stdClass();
			$this->parseCmdLineOptions(array_merge($cmdLineOptions, [["i:", "instance:"]]));
			if( empty($this->cmdLineOptions->instance) )
				throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Please specify an instance name via the '-i' or '--instance' parameter for starting the according instance/implementation.");

			// --- SETUP TOWER BASICS ---
			parent::__construct($rootFolder, $sdkFolder, $loader, $this->cmdLineOptions->instance);

			// --- PROCESS TITLE & CONSOLE ENCODING ---
			cli_set_process_title($this->instance);
			if( OsNames::isWindows() )
				exec("chcp 65001 >nul 2>&1"); // set UTF-8 encoding for console
		}

		public function addClassMapToLoader(string $classMapFile)
		{
			if( file_exists($classMapFile) === true )
				$this->classMapLoader->registerAutoloadMap($classMapFile);
		}

		public function checkAuthentication()
		{
			return new User(User::DEFAULT_USERNAME);
		}

		public function checkAuthorization()
		{
			return new SysError(SysError::SUCCESS);
		}

		public function getInstanceSignal()
		{
			$signalFile = "$this->tempFolder/$this->instance.signal";
			if( !is_file($signalFile) )
				return self::SIGNAL_NONE;

			$signal = file_get_contents($signalFile);
			if( $signal === false )
				return new FsError(FsError::ACCESS_DENIED, FsError::ERR, "File '$signalFile' exists but could not be read." );

			return $signal;
		}

		public function setInstanceSignal( $instanceSignal )
		{
			$signalFile = "$this->tempFolder/$this->instance.signal";
			$fileSetError = file_put_contents($signalFile, $instanceSignal);
			if( !$fileSetError )
				return new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "File '$signalFile' could not be created.");
			else
				return new FsError(FsError::SUCCESS);
		}

		public function removeInstanceSignal()
		{
			$signalFile = "$this->tempFolder/$this->instance.signal";
			if( !is_file($signalFile) )
				return new FsError(FsError::SUCCESS);

			if( !unlink($signalFile) )
				return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, "File '$signalFile' could not be deleted.");

			return new FsError(FsError::SUCCESS);
		}

		public function getSystemSignal()
		{
			$signalFile = "$this->tempFolder/SYSTEM.signal";
			if( !is_file($signalFile) )
				return self::SIGNAL_NONE;

			$signal = file_get_contents($signalFile);
			if( $signal === false )
				return new FsError(FsError::ACCESS_DENIED, FsError::ERR, "File '$signalFile' exists but could not be read." );

			return $signal;
		}

		public function setSystemSignal( $instanceSignal )
		{
			$signalFile = "$this->tempFolder/SYSTEM.signal";
			$fileSetError = file_put_contents($signalFile, $instanceSignal);
			if( !$fileSetError )
				return new FsError(FsError::FILE_CREATION_FAILED, FsError::ERR, "File '$signalFile' could not be created.");
			else
				return new FsError(FsError::SUCCESS);
		}

		public function removeSystemSignal()
		{
			$signalFile = "$this->tempFolder/SYSTEM.signal";
			if( !is_file($signalFile) )
				return new FsError(FsError::SUCCESS);

			if( !unlink($signalFile) )
				return new FsError(FsError::FILE_DELETION_FAILED, FsError::ERR, "File '$signalFile' could not be deleted.");

			return new FsError(FsError::SUCCESS);
		}

		public function getInstanceTempFilePath($tempName)
		{
			return "$this->tempFolder/$this->instance" . "_$tempName.tmp";
		}

		public function getSystemTempFilePath($tempName)
		{
			return "$this->tempFolder/SYSTEM_$tempName.tmp";
		}


		/***********************************
		* PROTECTED METHODS                *
		***********************************/

		protected function removeTowerInfos()
		{
			// Nothing to remove
		}

		protected function parseCmdLineOptions(array $cmdLineOptions)
		{
			$shortOptions = "";
			$longOptions = [];
			foreach($cmdLineOptions as $cmdLineOption)
			{
				if( count($cmdLineOption) !== 2 )
					throw new SysError(SysError::INVALID_PARAM, SysError::ERR, "Command line option parsing requires the programmer to specify two information: a short option and a long option. But " . count($cmdLineOption) . " found.");

				$shortOptions .= $cmdLineOption[0];
				$longOptions = array_merge($longOptions, (array)$cmdLineOption[1]);
			}

			$parsedOptions = getopt($shortOptions, $longOptions);
			foreach($cmdLineOptions as $cmdLineOption)
			{
				$shortOption = Helper::removeLastChar(Helper::removeLastChar($cmdLineOption[0], ":"), ":");
				$longOption  = Helper::removeLastChar(Helper::removeLastChar($cmdLineOption[1], ":"), ":");

				$this->cmdLineOptions->$longOption =
					$parsedOptions[$longOption] ??
					$parsedOptions[$shortOption] ??
					$this->cmdLineOptions->$longOption ??
					"";
			}
		}
	}
}